#! /usr/bin/perl -w

#  Ref: https://its.cern.ch/jira/browse/OS-15212

sub BEGIN {
    use Cwd 'abs_path';
    use File::Basename 'dirname';
    push(@INC,dirname(abs_path($0)));
    push(@INC,"/opt/cci-scripts/landb-tools");
}

use strict;
use diagnostics;
use Data::Dumper;
use Text::TabularDisplay;
use SOAP::Lite;# +trace => 'debug';
use Getopt::Long;
use OpenStack::Project;
use OpenStack::Nova;
use OpenStack::Flavor;
use OpenStack::Aggregate;
use Landb::Tools;
use JSON;

sub Usage($);

sub Usage($){

    print STDOUT <<EOUSAGE;

Check consistency between Nova and LanDB: do they agree on the hypervisor <-> VM mappings?

Usage: $0 [--debug] [--verbose] [--apply-fixes] <cell name>

EOUSAGE

    return;
}

my $username = "vmmaster";

my $debug = my $verbose = my $apply = 0;
my %opt = ("debug"       => \$debug,
	   "verbose"     => \$verbose,
	   "apply-fixes" => \$apply,
    );
#
# Parse the options
#
my $rc = Getopt::Long::GetOptions(
    \%opt,
    "debug","verbose",
    "apply-fixes"      => \$apply,
    );

exit 1 unless $rc;
print Dumper(\%opt) if $debug;

if (not @ARGV){
    Usage("");
    exit 1;
}

#
#  GetConnect to LanDB
#

my $password = Landb::Tools::get_credentials($username);

my $client = SOAP::Lite                                            
    -> uri('http://network.cern.ch/NetworkService')
    -> proxy('https://network.cern.ch/sc/soap/soap.fcgi?v=x');

my $call = $client->getAuthToken($username,$password,'CERN');

my $result = $call->result;
if ($call->fault) {
    die "FAILED: " . $call->faultstring;
}

my $auth  = SOAP::Header->name('Auth' => { "token" => $result });

#
# Get the info
#
my $table = Text::TabularDisplay->new("Cell name","Compute nodes","Wrong VMs","Total VMs","Fraction");
my %total = ();

for my $cell (map {lc} @ARGV){
    print "[V] Processing cell \"$cell\"...\n" if $verbose;
    my %aggr = OpenStack::Aggregate::Show($cell);
    my @HOSTNAMES = @{$aggr{hosts}};
    
    my %data = ();
    #
    # Get Nova info
    #
    for my $host (@HOSTNAMES){
	print "[V] Processing host \"$host\"...\n" if $verbose;
	my $cmd = "/usr/bin/openstack server list --all-projects --no-name-lookup --long -c Name -c ID -f json --host $host";
	print "[D] Executing \"$cmd\"\n" if $debug;
	my $result = `$cmd`;
	my @data = @{from_json($result)};
	for (@data){
	    my %tmp = %$_;
	    my $vm = lc($tmp{Name});
	    $data{$vm}{nova} = $host;
	    $data{$vm}{uuid} = $tmp{ID};
	}
    }
    
    #
    # Get LanDB info
    #
    for my $host (@HOSTNAMES){
	my $call = $client->vmGetInfo($auth,(split(/\./,$host))[0]);
	$result = $call->result;
	if ($call->fault) {
	    print "[ERR] Ignoring \"$host\": " . $call->faultstring . "\n";;
	    next;
	}
	next if not defined $result->{VMGuestList};
	for (@{$result->{VMGuestList}}){
	    my $vm = lc($_);
	    $data{$vm}{landb} = $host if exists $data{$vm};
	}
    }
    
    #
    # Act!
    #
    my $fix = my $ok = 0;

    for my $vm (keys %data){
	#
	# Everyting OK?
	#
	next if not exists $data{$vm}{nova};
        
	if ($data{$vm}{nova} eq $data{$vm}{landb}){
	    print "[V] Nova and Landb have consistent host information for VM \"$vm\"\n" if $verbose;
	    $ok++;
	    next;
	}
	
	#
	# Handle the inconsistent cases
	#
	print "[W] Nova and Landb have inconsistent host information for VM \"$vm\"\n";
	if ($apply){
	    my $cmd = "/usr/bin/openstack server show --fit-width $data{$vm}{uuid}";
	    print "[I] Running \"$cmd\" to update Landb information.\n";
	    system("$cmd 1>/dev/null 2>/dev/null");
	}
	$fix++;
    }
    my $tot = $ok + $fix;
    my $pct = sprintf("%4.1f",100 * $fix / $tot);

    $table->add($cell,scalar(@HOSTNAMES),$fix,$tot,$pct);
    $total{ok}  += $ok;
    $total{fix} += $fix;
    $total{hv}  += scalar(@HOSTNAMES);
}

if (scalar(@ARGV) > 1){
    my $tot = $total{ok} + $total{fix};
    my $pct = sprintf("%4.1f",100 * $total{fix} / $tot);
    $table->add("Total",$total{hv},$total{fix},$total{ok},$pct);
}

print $table->render . "\n";

exit 0;

__END__
