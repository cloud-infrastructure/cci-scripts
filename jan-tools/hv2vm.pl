#! /usr/bin/perl -w

# 

sub BEGIN {
    use Cwd 'abs_path';
    use File::Basename 'dirname';
    push(@INC,dirname(abs_path($0)));
    push(@INC,"/opt/cci-scripts/landb-tools");
}

use strict;
use diagnostics;
use Data::Dumper;
use Text::TabularDisplay;
use SOAP::Lite;# +trace => 'debug';
#use List::Util qw(min max);
#use Net::LDAP;
#use Net::LDAP::Control::Paged;
#use Net::LDAP::Constant qw(LDAP_CONTROL_PAGED);
use Getopt::Long;
use OpenStack::Project;
use OpenStack::Nova;
use OpenStack::Flavor;
use Landb::Tools;
use JSON;

use Config::IniFiles;
my $cfg = Config::IniFiles->new( -file => "/etc/landb.conf" );

sub Usage($);

sub Usage($){

    print STDOUT <<EOUSAGE;

     $0 --debug --verbose [--user <username>] [--summary] <hypervisor> [<hypervisor>...]

EOUSAGE

    return;
}

my $username = getlogin();

my $debug = my $verbose = my $host = my $summary = 0;
my %opt = ("debug"             => \$debug,
	   "verbose"           => \$verbose,
	   "summary",          => \$summary,
    );
#
# Parse the options
#
my $rc = Getopt::Long::GetOptions(
    \%opt,
    "debug",
    "verbose",
    "summary",
    "user=s" => \$username,
    );
exit 1 unless $rc;
print Dumper(\%opt) if $debug;

if (not @ARGV){
    Usage("");
    exit 1;
}

my @HOSTNAMES = map {lc} @ARGV;
@HOSTNAMES = map {/.cern\.ch$/ ? $_ : "$_.cern.ch" } @HOSTNAMES;

#
#  Get Landb info
#

$username = $cfg->val( 'default', 'landb_user' );
my $password = $cfg->val( 'default', 'landb_password' );

my $client = SOAP::Lite                                            
    -> uri('http://network.cern.ch/NetworkService')
    -> proxy('https://network.cern.ch/sc/soap/soap.fcgi?v=x');

my $call = $client->getAuthToken($username,$password,'CERN');

my $result = $call->result;
if ($call->fault) {
    die "FAILED: " . $call->faultstring;
}

my $auth = SOAP::Header->name('Auth' => { "token" => $result });

#
# Get flavor info
#
my %flavor = ();
print "[V] Get all flavours...\n" if $verbose;
%flavor = OpenStack::Flavor::List();
for my $name (keys %flavor){
    my $id = $flavor{$name}{id};
    %{$flavor{$id}} = %{$flavor{$name}};
    $flavor{$id}{name} = $name;
}
#print Dumper (\%flavor);#exit;

#
# Get project info
#
my %project = OpenStack::Project::List();
# reshuffle a bit
for my $id (keys %project){
    my $name = $project{$id}{name};
    delete $project{$id};
    $project{$id} = $name;
}
#print Dumper(\%project );exit;

my $table = Text::TabularDisplay->new("ID","Name","Status","Task\nState","power\nState","Landb Responsible\nNote: [*] = no CCID","Landb Main User\nNote: [*] = no CCID","OS","Project","Flavor","Host","BFV?");
#
# Now run "nova list ...."
#
print "[V] Now run \"/openstack server list ....\"\n" if $verbose;
my $cmd = "/usr/bin/openstack server list --all -n --long -c ID -c Name -c Status -c 'Task State' -c 'Power State' -c 'User ID' -c 'Project ID' -c 'Flavor ID' -c Host -c 'Image ID' -f json",

my @cmd = my %summary = ();
map {push(@cmd,"$cmd --host $_")} @HOSTNAMES;
print "[D] \@cmd = \"@cmd\"\n" if $debug;

for my $cmd (@cmd){
    print "[D] Executing \"$cmd\"\n" if $debug;
    my $result = `$cmd`;
    for (@{from_json($result)}){
        my %tmp = %$_;
	#print Dumper(\%tmp);# exit;
	my $uuid        = $tmp{"ID"};
	my $name        = $tmp{"Name"};
	my $status      = $tmp{"Status"};
	my $task_state  = $tmp{"Task State"} || "None";
	my $power_state = $tmp{"Power State"};
	my $image       = $tmp{"Image ID"};
	my $flavor_id   = $tmp{"Flavor ID"};
	my $host        = $tmp{"Host"};
	my $user_id     = $tmp{"User ID"};
	my $tenant_id   = $tmp{"Project ID"};

	# translate Power State using the lookup table from /usr/lib/python3.6/site-packages/openstackclient/compute/v2/server.py
	$power_state = qw(NOSTATE Running "" Paused Shutdown "" Crashed Suspended)[$power_state];
	#    power_states = [
	#            'NOSTATE',      # 0x00
	#            'Running',      # 0x01
	#            '',             # 0x02
	#            'Paused',       # 0x03
	#            'Shutdown',     # 0x04
	#            '',             # 0x05
	#            'Crashed',      # 0x06
	#            'Suspended'     # 0x07

	# try to fill in the gaps for flavors that have been deleted...
	if (not exists $flavor{$flavor_id}){
	    $result = `OS_COMPUTE_API_VERSION=2.56 /usr/bin/openstack server show -c flavor -f value $uuid 2>/dev/null`;
	    $result =~ s/\'/\"/g;
	    my %f = %{from_json($result)};
	    $f{name}   = $f{original_name};
	    $f{cores} = $f{vcpus};
	    $f{id}    = $flavor_id;
	    map {delete $f{$_}} qw(original_name extra_specs vpus);
	    %{$flavor{$flavor_id}} = %f;
	    %{$flavor{$f{name}}} = %f;
	}
	
	# get Landb owner
	print "[D] Getting Landb info for host $name ...\n" if $debug;
	my $landb_name = $name; # Landb corrects some names
	$landb_name =~ s/ /-/g;
	$landb_name =~ s/\(//g;
	$landb_name =~ s/\)//g;
	$call = $client->getDeviceInfo($auth,$landb_name);
	$result = $call->result;
	my $owner = my $mainuser = undef;
	if (not $call->fault) {
	    $owner = $result->{ResponsiblePerson}->{Email};
	    $owner .= " [*]" if not $result->{ResponsiblePerson}->{CCID} and $result->{ResponsiblePerson}->{FirstName} ne "E-GROUP"; # Mark owners that have no Compute ID...
	    $mainuser = $result->{UserPerson}->{Email};
	    $mainuser .= " [*]" if not $result->{UserPerson}->{CCID} and $result->{UserPerson}->{FirstName} ne "E-GROUP"; # Mark mainusers that have no Compute ID...
        }else{
	    $owner = $mainuser = "HUH???";
	}
        my $OperatingSystem  = $result->{OperatingSystem}->{Name} || "";

	my $boot_from_volume = $image =~ /booted from volume/ ? "BFV" : "";
	
	$table->add($uuid,$name,$status,$task_state,$power_state,$owner,$mainuser,$OperatingSystem,$project{$tenant_id},$flavor{$flavor_id}{name},$host,$boot_from_volume);

	if ($summary){
	    my $project = $project{$tenant_id} || "HUH $host - $uuid - $name";
	    $summary{$project}{vms}++;
	    $summary{$project}{cores} += $flavor{$flavor_id}{cores};
	}
    }
}

print $table->render . "\n";

if ($summary){
    my $table = Text::TabularDisplay->new("Project","Cores","VMs");
    my %total = ();
    for my $project (sort {$summary{$b}{cores} <=> $summary{$a}{cores}} keys %summary){
	$table->add($project,$summary{$project}{cores},$summary{$project}{vms});
	$total{cores} += $summary{$project}{cores};
	$total{vms}   += $summary{$project}{vms};
    }
    $table->add("TOTAL ".scalar(keys %summary)." projects",$total{cores},$total{vms});
    print "\n" . $table->render . "\n";
}

exit 0;
__END__
