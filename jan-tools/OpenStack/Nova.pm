package OpenStack::Nova;

#sub AbsoluteLimits($);

sub QuotaShow($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/nova quota-show $opt";
    #print " >> $cmd\n";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        next if /^\+/;
	s/\s+//g;
        #print;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Quota";
	$list{$key} = $value;
	#print ">>> $key, $value\n";
    }
    close(F);

    return %list;
}

sub Limits($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/nova limits $opt 2>/dev/null";
    #print " >> $cmd\n";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        #print;
        next if /^\+/;
	s/\s+//g;
	my ($key,$used,$max) = (split(/\|/,$_))[1,2,3];
        next if $key eq "Name";
	$list{$key}{Used} = $used;
	$list{$key}{Max}  = $max;
	#print ">>> $key, $value\n";
    }
    close(F);

    return %list;
}

sub HypervisorShow($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/nova hypervisor-show $opt";
    #print " >> $cmd\n";
    if (not open(F,"$cmd 2>/dev/null |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        #print;
        next if /^\+/;
	s/\s*\|\s*/\|/g;
	#s/\s+//g;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Name";
	$list{$key} = $value;
	#print ">>> $key, $value\n";
    }
    close(F);

    return %list;
}

sub FlavorList(){
    my $cmd = "/usr/bin/nova flavor-list --all";
    if (not open(F,"$cmd 2>/dev/null |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }
    my %list = ();
    my $instances = my $cores = my $ram = undef;
    while(<F>){
        next if /^\+/;
	s/\s+//g;
        #print;
        my ($id,$name,$ram,$disk,$ephemeral,$swap,$cores) = (split(/\|/,$_))[1,2,3,4,5,6,7,8];
        next if $name =~  /^Name/;
        %{$list{$name}} =
            (id        => $id,
	     ram       => $ram,
             disk      => $disk,
             ephemeral => ($ephemeral || 0),
             swap      => ($swap || 0),
             cores     => $cores,
            );
                        
        #print "$name,$ram,$disk,$ephemeral,$swap,$cores\n";exit;
    }
    close(F);
    return %list;
}

sub CellList(){
    my %cell = ();
    my $cmd = "/usr/bin/nova service-list --binary nova-cells";
    if (not open(NOVA,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    while(<NOVA>){
	next if /^\+/;
	s/\s+//g;
#	print;
	my ($service,$host,$status,$state) = (split(/\|/,$_))[2,3,5,6];
	next if not defined $service;
	next if $host eq "Host";	# skip header
	if ($service ne "nova-cells"){ # older version of phython-novaclient
	    ($host,$status,$state) = (split(/\|/,$_))[2,4,5];
	    next if $host eq "Host";	# skip header
	}
	next if $status ne "enabled";
	next if $state  ne "up";
	next if $host !~ /^father\!(cell\d+)@/;
	$cell{$1}++;
    }
    close(NOVA);

    return sort keys %cell;
}

sub HypervisorStats($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/nova hypervisor-stats";
    if ($opt){
	$cmd = "/usr/bin/nova --os-auth-url https://keystone.cern.ch/cell_main/v2.0 --os-region-name $opt hypervisor-stats";
    }
    #print " >> $cmd\n";
    if (not open(F,"$cmd |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return undef;
    }

    my %list = ();

    while(<F>){
        next if /^\+/;
	s/\s+//g;
        #print;
        my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Property";
	$list{$key} = $value;
	#print ">>> $key, $value\n";
    }
    close(F);

    return %list;
}

1;

