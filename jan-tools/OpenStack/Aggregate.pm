package OpenStack::Aggregate;

use JSON;

sub Show($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/openstack aggregate show --fit-width -f json $opt";
    my $out = `$cmd 2>/dev/null`;
    my %data = %{from_json($out)};
  
    return %data;
}

1;

