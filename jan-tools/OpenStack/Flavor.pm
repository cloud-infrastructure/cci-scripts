package OpenStack::Flavor;

use JSON;

sub List(){
  my $cmd = "/usr/bin/openstack flavor list --all --long  -f value -c ID -c Name -c RAM -c Disk -c Ephemeral -c VCPUs -c Swap";
  if (not open(F,"$cmd 2>/dev/null |")){
    print STDERR "[E] Cannot run \"$cmd\": $!\n";
    return undef;
  }
  my %list = ();
  #my $instances = my $cores = my $ram = undef;
  while(<F>){
    #print ;
    chomp;
    my ($id,$name,$ram,$disk,$ephemeral,$cores,$swap) = (split(/ /,$_));
    %{$list{$name}} =
      (id        => $id,
       ram       => $ram,
       disk      => $disk,
       ephemeral => $ephemeral,
       swap      => ($swap || 0),
       cores     => $cores,
      );
    
    #print "$id,$name,$ram,$cores,$disk,$ephemeral,$swap\n";exit;
  }
  close(F);
  return %list;
}

#sub Show($){
#  my $opt = shift @_;
#  my $cmd = "/usr/bin/openstack flavor show --fit-width -f json $opt";
#  my $out = `$cmd 2>/dev/null`;
#  my %data = %{from_json($out)};
#use Data::Dumper;
#  print Dumper (\%data);exit;
#  
#  return %data;
#}
sub Show($){
  my $opt = shift @_;
  my $cmd = "/usr/bin/openstack flavor show --fit-width $opt 2>/dev/null";
  if (not open(F,"$cmd 2>/dev/null |")){
    print STDERR "[E] Cannot run \"$cmd\": $!\n";
    return undef;
  }
  my %list = ();
  
  while(<F>){
    next if /^\+/;
    s/\s+//g;
    #print;
    my ($key,$value) = (split(/\|/,$_))[1,2];
    next if $key eq "Field";
    $list{$key} = $value;
    #print ">>> $key, $value\n";
  }
  close(F);
  
  return %list;
}

1;

