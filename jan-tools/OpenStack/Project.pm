package OpenStack::Project;

sub List(;$){
    my $filter = shift @_;
    #print "[F] $filter\n";
    my %project = ();
    if (open(KS, "/usr/bin/openstack project list --domain default --column Name --column ID --column Enabled --long 2>/dev/null |")){
	while(<KS>){
	    next if /^\+/;
	    s/\s*\|\s*/\|/g;
	    my (undef,$id,$name,$enabled) = split(/\|/,$_);
	    next if $id eq "ID"; 
	    #print "$id \"$name\" $domain_id - $description ($enabled)\n";
	    
	    if (not defined $filter){
		$project{$id}{name}    = $name;
		$project{$id}{enabled} = $enabled;
		next;
	    }	       

	    if ($name =~ /$filter/){
		$project{$id}{name}    = $name;
		$project{$id}{enabled} = $enabled;
		next;
	    }

	    if ($id =~ /$filter/){
		$project{$id}{name}    = $name;
		$project{$id}{enabled} = $enabled;
		next;
	    }

	    #next unless $name =~ /CMS/;
	    #print "$id $name\n";
	}
	close(KS);
    }
    return %project;
}

sub Show($){

    my $id = shift @_;
    my %project = ();
    if (open(KS, "/usr/bin/openstack project show \"$id\" 2>/dev/null |")){
	while(<KS>){
	    next if /^\+/;
	    s/\s*\|\s*/\|/g;
	    my (undef,$key,$value) = split(/\|/,$_);
	    next if $key eq "Field"; 
	    #print "$id \"$name\" $domain_id - $description ($enabled)\n";
	    $project{$key} = $value;
	}
	close(KS);
    }
    return %project;
}

1;
