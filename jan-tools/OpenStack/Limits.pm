package OpenStack::Limits;

sub Show($){
    my $uuid = shift @_;
    my $cmd = "/usr/bin/openstack limits show --absolute --project $uuid";
    #print " >> $cmd\n";
    if (not open(F,"$cmd 2>/dev/null |")){
        print STDERR "[E] Cannot run \"$cmd\": $!\n";
        return ();
    }

    my %list = ();

    while(<F>){
        #print;
        next if /^\+/;
	s/\s+//g;
	my ($key,$value) = (split(/\|/,$_))[1,2];
        next if $key eq "Name";
	$key = "maxTotalRAM" if $key eq "maxTotalRAMSize";
	#print ">> $key $value\n";
	if ($key =~ /^(max)/){
	    #print ">>>> $1 $'\n";
	    $list{$'}{"Max"} = $value;
	}elsif($key =~ /Used$/){
	    #print ">>>> $1 $'\n";
            $key = $`;
            $key =~ s/^total/Total/;
	    $list{$key}{"Used"} = $value;
        }
    }
    close(F);

    return %list;
}

1;

