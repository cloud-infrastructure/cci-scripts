#!/usr/bin/python3

from argparse import ArgumentParser
from urllib.parse import urlencode
from aitools.pdb import PdbClient
from aitools.config import PdbConfig, AiConfig

import openstack
import socket


def sync_main(pargs):

    config = AiConfig()
    config.read_config_and_override_with_pargs(pargs)

    conn = openstack.connect()
    pools = conn.load_balancer.pools(loadbalancer_id=pargs.lbaas)
    for p in pools:
        print(f"checking pool {p.id} '{p.name}'")
        if 'hostgroup=' in p.description:
            print(f"{p.id} has hostgroup")
            if 'port=' in p.description:
                port = int(p.description.split('port=')[1].split(';')[0])
                if port == 0:
                    print("ERROR: port could not be determined for pool " + p.id)
                    continue

            hgs = set(p.description.split('hostgroup=')[1].split(';')[0].split(','))
            hosts = set()
            for hg in hgs:
                print(f"querying HG: {hg}")
                hosts.update(retrieve_hosts_from_pdb_query(config, hg, pargs.subgroups))

            puppet_query_ips = set()
            for h in hosts:
                puppet_query_ips.update(dns_resolve_host(h, pargs.ip_version))

            members = [m for m in conn.load_balancer.members(pool=p.id)]
            members_ip = set([m.address for m in members])

            addresses_to_add = puppet_query_ips - members_ip
            addresses_to_remove = members_ip - puppet_query_ips

            print("Addresses to add:" + str(addresses_to_add))
            print("Addresses to remove:" + str(addresses_to_remove))

            members_to_remove = [m for m in members if m.address in addresses_to_remove]
            for a in addresses_to_add:
                print(f"add member {a}:{port} to pool {p.id}")
                if pargs.apply:
                    print("execute")
                    conn.load_balancer.create_member(
                        pool=p.id,
                        protocol_port=port,
                        address=a,
                        wait=True)
                    conn.load_balancer.wait_for_load_balancer(pargs.lbaas)

            for m in members_to_remove:
                print(f"remove member {m.id} with {m.address}:{m.protocol_port} from pool {p.id}")
                if pargs.apply:
                    print("execute")
                    conn.load_balancer.delete_member(pool=p.id, member=m.id)
                    conn.load_balancer.wait_for_load_balancer(pargs.lbaas)


def dns_resolve_host(hostname, ip_version='4'):
    # see `man getaddrinfo`
    dns_resolve = list(
        (i[0], i[4][0])
        for i in socket.getaddrinfo(hostname, 0)
        # ignore duplicate addresses with other socket types
        if i[1] is socket.SocketKind.SOCK_RAW
    )
    if ip_version == '4':
        return [r[1] for r in dns_resolve if r[0] == socket.AddressFamily.AF_INET]
    elif ip_version == '6':
        return [r[1] for r in dns_resolve if r[0] == socket.AddressFamily.AF_INET6]
    else:
        return [r[1] for r in dns_resolve]


def retrieve_hosts_from_pdb_query(config, hostgroup, subgroups=False):
    endpoint = "pdb/query/v4/facts/hostgroup"
    if subgroups:
        op = "~"
        hg = "^(?=%s)|(?=%s/.*)" % (hostgroup, hostgroup)
    else:
        op = "="
        hg = hostgroup
    query = urlencode({"query": '["%s", "value", "%s"]' % (op, hg)})

    pdb = PdbClient()
    (code, j) = pdb.raw_request("%s?%s" % (endpoint, query))

    res = list()
    for entry in j:
        entry["hostgroup"] = entry.pop("value")
        del (entry['name'])
        res.append(entry)
    res = sorted(res, key=lambda k: k["hostgroup"])
    return set([h['certname'] for h in res])


def main():
    parser = ArgumentParser(description="wrap puppetdb commands")

    pdb_config = PdbConfig()
    pdb_config.add_standard_args(parser)

    subparsers = parser.add_subparsers(title="subcommands", dest="subcommand")
    subparsers.required = True

    sync_parser = subparsers.add_parser("sync", help="sync LBaaS ID")
    sync_parser.add_argument("lbaas", metavar="lbaas", help="LBaaS ID")
    sync_parser.add_argument("--subgroups", action="store_true", default=False, dest="subgroups",
                             help="also sync with subgroups of the LB")
    sync_parser.add_argument("--ip-version", default='4', metavar="ip_version",
                             help="Which IP version to sync (4, 6, both; default=4)")
    sync_parser.add_argument("--apply", action="store_true", default=False, dest="apply",
                             help="Apply changed instead of only printing.")
    sync_parser.set_defaults(func=sync_main)

    pargs = parser.parse_args()
    pargs.func(pargs)


if __name__ == "__main__":
    main()
