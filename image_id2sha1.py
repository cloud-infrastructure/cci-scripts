#!/usr/bin/python

import hashlib
import sys

try:
    image_id = sys.argv[1]
    print(image_id, hashlib.sha1(image_id).hexdigest())
except Exception:
    print("Usage: image_id2sha1 <image_id>")
