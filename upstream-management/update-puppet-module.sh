#!/bin/bash

# fail directly if any of the command fails
set +e +x +v

MODULE=
OLD_VERSION=
NEW_VERSION=
OLD_VERSION_SANITIZED=
NEW_VERSION_SANITIZED=
LOGLEVEL=2 # default info
STEP=0
YES_NO_SET=undef
USE_ORIGIN_BRANCHES=true

UPSTREAM_PREFIX=upstream_
CERN_PREFIX=cern_

function usage {
  echo "Usage: $0 [-h] [-?] [-q] [-v] [-y] [-s STEP] MODULE OLD_VERSION NEW_VERSION" >&2
  echo "  -h/-?:  print this help" >&2
  echo "  -q:     be quiet" >&2
  echo "  -v:     print debug messages" >&2
  echo "  -y:     Answer all questions with yes" >&2
  echo "  -n:     Answer all questions with no" >&2
  echo "  -p:     Use local branches instead of origin to find CERN patches " >&2
  echo "    (e.g. when doing multiple updates xena->yoga->zed without intermediate push)" >&2
  echo "  -s STEP Start at step STEP" >&2
  echo "          0: Prepare the repository (default)" >&2
  echo "          1: Apply the upstream patches" >&2
  echo "          2: Apply CERN patches" >&2
  echo "          3: Push upstream" >&2
  echo "" >&2
  echo " MODULE:  Name of the puppet module (e.g. \"oslo\" for it-puppet-module-oslo)" >&2
  echo " OLD_VERSION: Name of the base Openstack version to be used." >&2
  echo " NEW_VERSION: Name of the new Openstack version that should be created." >&2
  echo "" >&2
}

function error {
  echo "$@" >&2
  #Do print errors regardless of loglevel [ "$LOGLEVEL" -ge 1 ] && echo "$@" >&2
}

function info {
  [ "$LOGLEVEL" -ge 2 ] && echo -e "$@" >&2
}

function debug {
  [ "$LOGLEVEL" -ge 3 ] && echo -e "$@" >&2
}

function parse_options {
  while getopts vqyns:l:ph OPTION
  do
    case $OPTION in
      v) LOGLEVEL=3 ;;
      q) LOGLEVEL=0 ;;
      l) LOGLEVEL="$OPTARG" ;;
      y) YES_NO_SET=yes ;;
      n) YES_NO_SET=no ;;
      p) USE_ORIGIN_BRANCHES=false ;;
      s)
        STEP="$OPTARG"
        if [ "$STEP" -lt 0 ] || [ "$STEP" -ge 4 ]; then
          error "There are only steps from 0 to 3"
          usage
          exit 2
        fi
        ;;
      h|?)
        usage
        exit 2
        ;;
    esac
  done
  shift $((OPTIND - 1))
  echo "$#: $*"

  if [ $# -lt 3 ]; then
    error "Not enough parameters"
    usage
    exit 2
  fi

  MODULE=$1
  OLD_VERSION=$2
  NEW_VERSION=$3
  OLD_VERSION_SANITIZED=${OLD_VERSION//[^a-zA-Z0-9_]/_}
  NEW_VERSION_SANITIZED=${NEW_VERSION//[^a-zA-Z0-9_]/_}
}

function yes_or_no {
  if [ "$YES_NO_SET" = "yes" ]; then
    return 0
  fi
  if [ "$YES_NO_SET" = "no" ]; then
    return 1
  fi

  while true; do
    read -r -p "$* [y/n]: " yn
    case $yn in
      [Yy]*) return 0  ;;
      [Nn]*) return  1 ;;
    esac
  done
}

# TODO do verification of input params
function check_git_clean_state {
  if [ -n "$(git status --porcelain)" ]; then
    error "#########################################"
    error ""
    error "The repository is not in a clean state!"
    error "Please read the previous output to verify and fix the problems in directory:"
    error "$(pwd)"
    error "$@"
    error ""
    error "#########################################"
    error ""
    git status >&2
    error ""
    exit 1
  fi
}

function prepare_repository {
  MODULE_DIR=it-puppet-module-$MODULE

  if [ ! -d "$MODULE_DIR" ]; then
    yes_or_no "Did not find the directory for the module, should I check it out in this directory?" &&
      git clone "https://:@gitlab.cern.ch:8443/ai/$MODULE_DIR" "$MODULE_DIR" || exit 1
    cd "$MODULE_DIR" || exit 1
  else
    # TODO git pull
    debug "Pulling repository"
    cd "$MODULE_DIR" || exit 1
    git pull --all
  fi

  check_git_clean_state
  debug "Changed the directory to $(pwd)"
}

function pull_upstream_subtree {
  debug "--------------------------------"
  debug "Applying upstream patches locally"

  # checkout old version as baseline
  git checkout "${UPSTREAM_PREFIX}${OLD_VERSION_SANITIZED}"
  debug "Checked out old: ${UPSTREAM_PREFIX}${OLD_VERSION_SANITIZED}\n$(git status)"

  # create new branch
  git checkout -b "${UPSTREAM_PREFIX}${NEW_VERSION_SANITIZED}"
  debug "Checked out new: ${UPSTREAM_PREFIX}${NEW_VERSION_SANITIZED}\n$(git status)"

  check_git_clean_state

  if ! git subtree --help &> /dev/null; then
    error "You need to install git subtree"
    exit 2
  fi

  # Pull upstream puppet module
  git subtree pull --prefix code "https://opendev.org/openstack/puppet-$MODULE.git" "stable/$NEW_VERSION" --squash -m "Updated from upstream $NEW_VERSION"

  check_git_clean_state "The subtree could not be updated correctly from upstream."
}

function apply_cern_patches {
  debug "--------------------------------"
  debug "Applying CERN specific patches"

  # checkout new CERN branch
  git checkout -b "${CERN_PREFIX}${NEW_VERSION_SANITIZED}"
  check_git_clean_state

  PREFIX="origin/"
  if [ "$USE_ORIGIN_BRANCHES" = "false" ]; then
    PREFIX=''
  fi
  # apply old patches
  git format-patch -k --stdout "${PREFIX}${UPSTREAM_PREFIX}${OLD_VERSION_SANITIZED}..${PREFIX}${CERN_PREFIX}${OLD_VERSION_SANITIZED}" | git am -k -3

  check_git_clean_state "The CERN specific patches could not be applied, fix the patch and use the relevant git am commands"

  debug "--------------------------------"
}

function create_qa_branch_from_master {
  git checkout "${CERN_PREFIX}${NEW_VERSION_SANITIZED}"
  git checkout -b "${CERN_PREFIX}${NEW_VERSION_SANITIZED}_qa"
}

function push_changes {
  yes_or_no "Should the changes be pushed to gitlab?" &&
    git push origin "${UPSTREAM_PREFIX}${NEW_VERSION_SANITIZED}" "${CERN_PREFIX}${NEW_VERSION_SANITIZED}" "${CERN_PREFIX}${NEW_VERSION_SANITIZED}_qa"
}

# run the commands
parse_options "$@"

case $STEP in
  0)
    prepare_repository
    info "Step 1 complete: Prepare repository and cd into"
    ;&
  1)
    pull_upstream_subtree
    info "Step 2 complete: Pull Upstream"
    ;&
  2)
    apply_cern_patches
    info "Step 3 complete: Apply CERN patches"
    info "Successfully created new branches for version $NEW_VERSION_SANITIZED from $OLD_VERSION_SANITIZED and applied the patches between ${UPSTREAM_PREFIX}${OLD_VERSION_SANITIZED}..${CERN_PREFIX}${OLD_VERSION_SANITIZED}"
    info "Please check the code for consistency!"
    ;&
  3)
    create_qa_branch_from_master
    push_changes
    ;;
  *)
    error "Invalid Step"
    exit 2
esac

exit 0
