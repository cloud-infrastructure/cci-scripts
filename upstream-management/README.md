# Repository Tools for Openstack upstream management

## update-puppet-module.sh
Puppet modules at CERN are the content of upstream in the code/ folder with patches for CERN.
We use git subtree to manage the information.
This script fetches a specified version and applies the changes on a specified base version.
Afterwards the local patches are applied again to the version.

### Parameters
```
Usage: $0 [-?] [-q] [-v] [-y] [-s STEP] MODULE OLD_VERSION NEW_VERSION"
  -h:     print this help
  -q:     be quiet
  -v:     print debug messages
  -y:     Answer all questions with yes
  -n:     Answer all questions with no
  -p:     Use local branches instead of origin to find CERN patches 
    (e.g. when doing multiple updates xena->yoga->zed without intermediate push)
  -s STEP Start at step STEP
          0: Prepare the repository (default)
          1: Apply the upstream patches
          2: Apply CERN patches
          3: Push upstream

 MODULE:  Name of the puppet module (e.g. \"oslo\" for it-puppet-module-oslo)
 OLD_VERSION: Name of the base Openstack version to be used.
 NEW_VERSION: Name of the new Openstack version that should be created.
```

### Steps
1. First step pulls the repository from CERN gitlab.
2. Step checks out the new branch based on the old upstream branch and pulls the upstream stable version branch.
3. Step applies all patches done locally on the old version for CERN.
4. Asks to push the changes.

### Multistep

In order to pull multiple versions, it is recommended to go from version to version.
Here is a script for nova, going form train to zed:
```
MODULE=nova
OLD_VERSION=train
while read version
do
  echo "update_puppet-module.sh -s 1 -n -p $MODULE $OLD_VERSION $version"

  OLD_VERSION=$version
done  <<<"ussuri
victor
wallaby
xena
yoga
zed"
```

### Troubleshooting
There are two steps, where this script may fail.

#### Fail during upstream pulling (Step 1)
Although this should not happen, in some cases the subtree pulling is not possible.
In this case there is a git merge that fails to apply.
You can use the `git merge ` commands to fix the merge and commit.
See also `git status` for that.

When the merge is commited, the script can be started again with parameter `-s 2` to start applying the patches for CERN.

#### Fail during apply of CERN patches (Step 2)
It might not be possible to apply all CERN patches without conflict.
In this case you need to fix the conflict and use the `git am` command to either continue, abort or skip the process.
After you fixed the commit and continue with `git am --continue`, the other patches are applied and there might be a conflict again.
Repeat until the `git status` command is clean again.

When the repository is in a clean state again, the script can be started again with parameter `-s 3` to push the branches.
