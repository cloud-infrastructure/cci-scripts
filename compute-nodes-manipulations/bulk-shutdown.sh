#! /bin/bash

# This script attempts to quickly shutdown a list of compute nodes.
#
# The intention is to use this script for bulk operations (in case of emergencies for example),
# where speed is critical.
#
# Also, we need to minimize the dependencies in this tool, as external services may not be
# available in case of emergency. For the same reason, calls to such services should be non-blocking

FM_HOSTS_FILE=`mktemp --tmpdir fm-hosts-XXXXXX.txt`
trap "/bin/rm ${FM_HOSTS_FILE} 2>/dev/null" EXIT

#
# Print help text
#
function help()
{
    cat <<EOF
Usage: $0 --host <host name> [<host name> ...]

This script tries to shutdown the specified hosts as fast as possible, by calling
"openstack server stop" for each of them.

In addition, it tries disable the nodes in Nova-compute, and to set the Roger state,

Optional arguments:

       -h : display this help

EOF

}
#
# Parse the options
#
PARAMS=""
apply=0
while (( "$#" ))
do
    case "$1" in
	--host)
	    host=1
	    shift
	    ;;
        -r)
            region=$2
            if [[ ! $region =~ cern|next|poc ]]
            then
                echo "Incorrect region name provided, should be one of \"cern|next|poc\""
                exit 1
            fi
            shift
            shift
            ;;
        -u)
            apply=1
            shift
            ;;
        -h)
            help
            exit 0
            ;;
        -*|--*=) # unsupported flags
            echo "Error: Unsupported flag $1" >&2
            echo
            help
            exit 1
            ;;
        *) # preserve positional arguments
            PARAMS="$PARAMS $1"
            shift
            ;;
    esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

if [ $# -eq 0 ]
then
    help
    exit 1
fi

HOST_NAMES=$@

#
# Ask Foreman about these nodes
#
ai-foreman showhost ${HOST_NAMES} 2>/dev/null | grep cern.ch >> ${FM_HOSTS_FILE}
if [ ! -s ${FM_HOSTS_FILE} ]
then
    echo '[E] Could not interrogate Foreman, giving up'
    exit 1
fi

#
# Generate commands to stop servers
#
grep cern.ch ${FM_HOSTS_FILE} | cut -d" "  -f2,4 | sed 's/\.cern.ch cloud_compute\/level2\// /; s/\// /' | awk '{printf "OS_PROJECT_NAME=\"Cloud Compute Nodes - %s\" OS_REGION_NAME=\"%s\" openstack server stop %s\n",$3,$2,$1}'

#
# Generate commands to disable servers in nova-compute
#
MSG="Disabled by "`basename $0`" ($USER, "`date "+%Y/%m/%d %H:%M"`")"
echo ${HOST_NAMES} | xargs -n1 | xargs -n1 -i{} --verbose echo openstack compute service set --disable --disable-reason "${MSG}" {} nova-compute

#
# Update the Roger state
#
echo ${HOST_NAMES} | xargs -n50 --verbose echo roger update --all_alarms=false --message "${MSG}"

exit 0
