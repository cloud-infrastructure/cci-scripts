#!/bin/bash

# Tries to fix the allocation of a machine that's reporting
#  Instance XX has been moved to another host YY. There are allocations remaining against the
#  source host that might need to be removed

INSTANCE_UUID=$1

echo "Checking instance $INSTANCE_UUID"
HYPERVISOR=$(openstack server show "$INSTANCE_UUID" -c OS-EXT-SRV-ATTR:host -f value)
if [ -z "$HYPERVISOR" ]; then
   echo "Unable to find hypervisor, skipping..."
   exit
fi

echo "Checking the state of the machine"
STATUS=$(openstack server show "$INSTANCE_UUID" -c status -f value)
if [[ "$STATUS" == "MIGRATING" ]]; then
   echo "The VM is currently being migrated, skipping..."
   exit
fi

echo "VM hosted on $HYPERVISOR"
RESOURCE_PROVIDER=$(openstack resource provider list --name "$HYPERVISOR" -f value -c uuid)
echo "VM should be using resource provider $RESOURCE_PROVIDER"

readarray -t ALLOCATIONS <<< "$(openstack resource provider allocation show "$INSTANCE_UUID" -f value -c resource_provider)"
echo "The VM has ${#ALLOCATIONS[*]} allocations on ${ALLOCATIONS[*]}"

if [[ ! " ${ALLOCATIONS[*]} " =~ ${RESOURCE_PROVIDER} ]] || [ "${#ALLOCATIONS[@]}" -gt 1 ]; then
    echo "Allocation does not match the host specified in nova"

    readarray -t RESOURCES  <<< "$(openstack resource provider allocation show "$INSTANCE_UUID" -f value -c resources)"
    readarray -t PROJECT_ID <<< "$(openstack resource provider allocation show "$INSTANCE_UUID" -f value -c project_id)"
    readarray -t USER_ID    <<< "$(openstack resource provider allocation show "$INSTANCE_UUID" -f value -c user_id)"

    echo "getting resource values"
    VCPU=$(echo "${RESOURCES[0]}" | sed 's/'\''/"/g' | jq .VCPU)
    MEMORY_MB=$(echo "${RESOURCES[0]}" | sed 's/'\''/"/g' | jq .MEMORY_MB)
    DISK_GB=$(echo "${RESOURCES[0]}" | sed 's/'\''/"/g' | jq .DISK_GB)
    echo "VM has VCPUS=$VCPU, MEMORY_MB=$MEMORY_MB and DISK_GB=$DISK_GB"

    echo "Setting allocation"
    openstack resource provider allocation set --allocation rp="$RESOURCE_PROVIDER",VCPU="$VCPU" --allocation rp="$RESOURCE_PROVIDER",MEMORY_MB="$MEMORY_MB" --allocation rp="$RESOURCE_PROVIDER",DISK_GB="$DISK_GB" --project-id "${PROJECT_ID[0]}" --user-id "${USER_ID[0]}" "$INSTANCE_UUID"
fi


