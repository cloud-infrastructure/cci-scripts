#!/usr/bin/python3
import logging
import logging.config
import json
import sys
import yaml

from argparse import ArgumentParser
from sqlalchemy import create_engine, Column, String, Integer, Text
from sqlalchemy.dialects.mysql import MEDIUMTEXT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from yaml import Loader, SafeLoader

BASE = declarative_base()
LOGGER = logging.getLogger(__name__)
actions = {}
usages = {}


def construct_yaml_str(self, node):
    # Override the default string handling function
    # to always return unicode objects
    return self.construct_scalar(node)


Loader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)
SafeLoader.add_constructor(u'tag:yaml.org,2002:str', construct_yaml_str)


def MediumText():
    return Text().with_variant(MEDIUMTEXT(), 'mysql')


class NovaBlockDeviceMapping(BASE):
    __tablename__ = "block_device_mapping"
    id = Column(Integer, primary_key=True, autoincrement=True)
    deleted = Column(Integer)
    connection_info = Column(MediumText())
    destination_type = Column(String(255))


class CinderVolume(BASE):
    __tablename__ = "volumes"
    id = Column(String(36), primary_key=True)
    deleted = Column(Integer)
    volume_type_id = Column(String(36))


class CinderVolumeAttachment(BASE):
    __tablename__ = "volume_attachment"
    id = Column(String(36), primary_key=True)
    volume_id = Column(String(36))
    connection_info = Column(Text)
    connector = Column(Text)
    attached_host = Column(String(255))
    attach_status = Column(String(255))
    deleted = Column(Integer)


def _get_configuration(args):
    # Open the configuration file with the required information
    with open(args.config, 'r') as ymlFile:
        config = yaml.load(ymlFile, Loader=yaml.BaseLoader)
    return config


def _retrieve_nova_connection_info(args, config):
    data = {}
    # apply filter by cell name if required
    nova_dbs = config['nova']['database']
    if args.cell and args.cell in nova_dbs:
        nova_dbs = {args.cell: nova_dbs[args.cell]}

    LOGGER.debug('Looping over nova databases')
    for nova_db in nova_dbs:
        # create an engine for Nova
        LOGGER.debug('[%s] Connecting to the database', nova_db)
        nova_engine = create_engine(config['nova']['database'][nova_db])
        NovaSession = sessionmaker(bind=nova_engine)
        nova_session = NovaSession()

        LOGGER.debug('[%s] Retrieving block_device_mapping entries', nova_db)
        bdms = nova_session.query(NovaBlockDeviceMapping).\
            filter(NovaBlockDeviceMapping.deleted == 0).\
            filter(NovaBlockDeviceMapping.destination_type == 'volume').all()

        iteration = 0
        total = len(bdms)
        LOGGER.debug('[%s] Found %s entries', nova_db, total)
        for bdm in bdms:
            iteration = iteration + 1
            LOGGER.debug('[%s] [%s/%s] Loading connection info...',
                         nova_db, iteration, total)
            try:
                connection = json.loads(bdm.connection_info)
            except Exception:
                LOGGER.info('[%s] [%s/%s] Error while loading the json data '
                            'skipping...', nova_db, iteration, total)
                continue

            if not connection or 'data' not in connection:
                LOGGER.info('[%s] [%s/%s] BDM has not valid connection data '
                            'skipping...', nova_db, iteration, total)
                continue

            if 'volume_id' not in connection['data']:
                volume_id = connection['serial']
            else:
                volume_id = connection['data']['volume_id']

            LOGGER.debug('[%s] [%s/%s] Processing bdm for %s',
                         nova_db, iteration, total, volume_id)

            data[volume_id] = connection

        nova_session.close()
    return data


def _fix_cinder_db(args, config, nova_info):
    # create an engine for Cinder
    LOGGER.debug('Connecting to Cinder Database')
    cinder_engine = create_engine(config['cinder']['database'])
    CinderSession = sessionmaker(bind=cinder_engine)
    cinder_session = CinderSession()

    LOGGER.debug('Retrieving volume attachments')
    attachs = cinder_session.query(CinderVolumeAttachment).\
        filter(CinderVolumeAttachment.deleted == 0).\
        filter(CinderVolumeAttachment.attach_status == 'attached').\
        filter(CinderVolumeAttachment.connection_info.is_(None)).\
        filter(CinderVolumeAttachment.connector.is_(None)).all()

    iteration = 0
    total = len(attachs)
    LOGGER.debug('[cinder] Found %s entries', total)
    for attach in attachs:
        iteration = iteration + 1
        LOGGER.debug('[cinder] [%s/%s] Calculating connection_info...',
                     iteration, total)

        if attach.volume_id not in nova_info:
            LOGGER.info('[cinder] [%s/%s] Volume not in the info db from nova '
                        'skipping...', iteration, total)
            continue

        volume_id = attach.volume_id
        att = nova_info[volume_id]
        connection_info = att['data']
        connector = att['connector']

        attached_host = connector['host']
        connection_info['driver_volume_type'] = att['driver_volume_type']
        connection_info['attachment_id'] = attach.id
        if 'cacheable' not in connection_info:
            connection_info['cacheable'] = False

        if 'keyring' in connection_info:
            del connection_info['keyring']

        LOGGER.warning('[cinder] [%s/%s] Doing change on volume %s',
                       iteration, total, volume_id)
        if not args.dryrun and attached_host and connection_info and connector:
            LOGGER.debug('[cinder] [%s/%s] [%s] Dumping JSON data in '
                         'connection_info', iteration, total, volume_id)

            attach.attached_host = attached_host
            attach.connection_info = json.dumps(connection_info,
                                                ensure_ascii=False)
            attach.connector = json.dumps(connector,
                                          ensure_ascii=False)

            LOGGER.debug('[cinder] [%s/%s] [%s] Committing change in db',
                         iteration, total, volume_id)
            cinder_session.commit()

    LOGGER.debug('Closing the connection with Cinder Database')
    cinder_session.close()


def add_logging_arguments(parser):
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-d", "--debug", help="increase output "
                        "to debug messages", action="store_true")


def configure_logging(args):
    level = logging.WARNING
    if args.verbose:
        level = logging.INFO
    if args.debug:
        level = logging.DEBUG
    logging.basicConfig(level=level,
                        format="%(levelname)s: %(message)s")


def setup_argparse():
    parser = ArgumentParser(
        description='Fix hardcoded information in the block device mapping '
                    'in Nova and volume attachments in Cinder')

    parser.add_argument('--dryrun',
                        action='store_true',
                        help="Do not execute any actions")
    parser.add_argument('--config',
                        required=True,
                        help="Location of configuration file")
    parser.add_argument('--cell',
                        default=None,
                        help='Apply changes ONLY from the cell specified')

    add_logging_arguments(parser)
    return parser


def main():
    parser = setup_argparse()
    args = parser.parse_args()
    configure_logging(args)

    config = _get_configuration(args)
    nova_info = _retrieve_nova_connection_info(args, config)
    _fix_cinder_db(args, config, nova_info)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        LOGGER.exception(e)
        sys.exit(-1)
