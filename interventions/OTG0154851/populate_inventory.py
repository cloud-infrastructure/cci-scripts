#!/bin/env python3

#
# Needs virtual environment with latest openstacksdk:
#   * python3 -m venv ./env/
#   * . ./env/bin/activate
#   * pip install openstacksdk
#

import openstack

sdk = openstack.connect()


class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


def update_inventory(inventory, reserved, total, generation):
    inventory.reserved = reserved
    inventory.total = total
    inventory._body._dirty.add('total')
    inventory._body._dirty.add('max_unit')
    inventory._body._dirty.add('reserved')
    inventory.max_unit = 1
    new_inventory = sdk.placement.update_resource_provider_inventory(
        inventory, resource_provider_generation=generation)
    return new_inventory


# By segment...
availabilities = next(sdk.network.network_ip_availabilities(
    network_name="CERN_NETWORK"))
for segment in sdk.network.segments():
    if not segment.name:
        print(f"{color.YELLOW}Skipping segment: {segment.name}{color.END}")
        continue

    print(f"{color.GREEN}Computing {segment.name} ({segment.id}){color.END}")
    subnets = sdk.network.subnets(segment_id=segment.id, ip_version=4)
    generation = next(sdk.placement.resource_providers(
        id=segment.id)).generation
    inventory = next(sdk.placement.resource_provider_inventories(
        resource_provider=segment.id))
    print(f"\tSegment {segment.name} has currently {inventory.reserved} "
          f"IPv4 addresses reserved out of {inventory.total} (Recomputing!)")

    new_reserved = 0
    new_total = 0
    for subnet in subnets:
        ips = [item for item in availabilities.subnet_ip_availability
               if item["subnet_name"] == subnet.name][0]
        print(f"\t\t{color.BOLD}Subnet {subnet.name} in {segment.name} has "
              f"{ips['used_ips']} IPs used out of {ips['total_ips']} total."
              f"{color.END}")
        new_reserved = new_reserved + ips['used_ips'] + 1  # 1 for gateway IPs
        new_total = new_total + ips['total_ips']

    print(f"\t\tComputed {new_total} IPs in total and {new_reserved}"
          f"already reserved (+1 gateway per subnet). Updating inventory!")
    update_inventory(inventory, new_reserved, new_total, generation)
