#!/bin/env python3

import configparser
import landbclient
import landbclient.conf
import openstack
import os

from landbclient.client import LanDB


CONF = landbclient.conf.CONF
DRY_RUN = False


def _get_landbclient():
    config_locations = [
        './landb.conf', os.path.expanduser('~/.landb.conf'),
        '/etc/landb.conf']
    config = configparser.ConfigParser()
    config.read(config_locations)
    if 'default' in config:
        options = config['default']
    landb_client = LanDB(
        username=options['landb_user'],
        password=options['landb_password'],
        host=options['landb_host'],
        port=options['landb_port'],
        protocol=options['protocol'],
        ca_file=options['ca_file'],
        version=int(options['landb_version']))
    return landb_client


class color:
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'


# Takes OS_CLOUD/OS_REGION_NAME from environment
sdk_conn = openstack.connect()
landb_cl = _get_landbclient()

#
# Target network
#
cern_network = sdk_conn.network.find_network(name_or_id='CERN_NETWORK')
print(f"{color.BOLD}[   ] CERN_NETWORK found with id "
      f"{cern_network.id}{color.END}")

for infra_subnet in sdk_conn.network.subnets(network_id=cern_network.id):

    if infra_subnet.name.startswith('public'):
        print(f"{color.BOLD}{color.YELLOW}[   ] Skipping subnet: "
              f"{infra_subnet.name}.{color.END}")
        continue

    print(f"{color.BOLD}[   ] Handling subnet: {infra_subnet.name}:"
          f"{color.END}")

    # Find associated primary
    service_name = infra_subnet.name.replace('-V6', '')

    try:
        primary_service = landb_cl.soap.getServiceInfo(service_name).Primary
    except Exception:
        print(f"{color.BOLD}{color.RED}\t[   ] Skipping subnet "
              f"{infra_subnet.name} without service in LanDB{color.END}")
        continue

    print(f"\t[   ] Primary Service: {primary_service}")

    # Check segment exists, create otherwise
    segment = sdk_conn.network.find_segment(name_or_id=primary_service)
    if not segment:
        print(f"\t{color.GREEN}[   ] Segment not found, creating new "
              f"{primary_service} segment in {cern_network.id} network"
              f"{color.END}")
        if not DRY_RUN:
            segment = sdk_conn.network.create_segment(
                name=primary_service,
                network_id=cern_network.id,
                network_type="flat",
                physical_network=primary_service)

    # Associate
    print(f"\t{color.GREEN}[   ] Associating {infra_subnet.name} to segment "
          f"{segment.name} with id {segment.id}{color.END}")

    if not DRY_RUN:
        infra_subnet.segment_id = segment.id
        sdk_conn.network.update_subnet(infra_subnet)
