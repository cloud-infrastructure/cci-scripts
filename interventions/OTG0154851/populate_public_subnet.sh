#!/bin/bash

openstack network segment create --physical-network legacy-public-net --network CERN_NETWORK --network-type flat legacy-public-segment
openstack subnet set --network-segment legacy-public-segment public-subnet-1
openstack subnet set --network-segment legacy-public-segment public-subnet-2
openstack subnet set --network-segment legacy-public-segment public-subnet-3
openstack subnet set --network-segment legacy-public-segment public-subnet-4
