UPDATE neutron.ml2_port_binding_levels AS m2
  JOIN neutron.ipallocations AS ip ON m2.port_id = ip.port_id
  JOIN neutron.subnets AS sb ON ip.subnet_id = sb.id
SET m2.segment_id = sb.segment_id
WHERE sb.ip_version = 4;