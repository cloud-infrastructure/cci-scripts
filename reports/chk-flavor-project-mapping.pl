#! /usr/bin/perl -w

use strict;
use diagnostics;
use Data::Dumper;
#use Text::TabularDisplay;
use Getopt::Long;

sub ProjectList(;$);
sub FlavorList();
sub FlavorShow($);


my $debug = my $verbose = 0;
my %opts = (
    debug   => \$debug,
    verbose => \$verbose,
    );


my $rc = Getopt::Long::GetOptions(
    \%opts,
    "debug","verbose",
    );

exit 1 unless $rc;
$verbose = 1 if $debug;
print Dumper(\%opts) if $debug;

my %project = ProjectList();
print "[V] Found ".scalar(keys %project)." projects\n" if $verbose;
print Dumper(\%project) if $debug;

for my $region (qw(cern next poc)){
    $ENV{OS_CLOUD} = $region;
    my %flavor = FlavorList();
    print "[V] Checking ".scalar(keys %flavor)." flavors in region ${region}\n" if $verbose;
    for my $name (sort keys %flavor){
        my %f = FlavorShow($name);
	print Dumper(\%f) if $debug;
	if (defined $f{access_project_ids}){
	    my @mapped = split(/,/,$f{access_project_ids});
	    for my $id (@mapped){
		next if $id eq "None"; # seems to be the value for default flavors (m2.large etc)
		print "[W] Flavour \"$name\" mapped to non-existent project \"$id\" in region \"$region\"\n" if not exists $project{$id};
		#exit;
	    }
	}
	# TODO: Check cell mapping:  properties | cells_mapping='gva_project_047',
    }
}

exit 0;

sub ProjectList(;$){
    my $filter = shift @_;
    my %project = ();
    if (open(KS, "OS_CLOUD=cern /usr/bin/openstack project list --domain default --column Name --column ID --column Enabled --long 2>/dev/null |")){
        while(<KS>){
            next if /^\+/;
            s/\s*\|\s*/\|/g;
            my (undef,$id,$name,$enabled) = split(/\|/,$_);
            next if $id eq "ID"; 
            #print "$id \"$name\" ($enabled)\n";
            
            if (not defined $filter){
                $project{$id}{name}    = $name;
                $project{$id}{enabled} = $enabled;
                next;
            }          

            if ($name =~ /$filter/){
                $project{$id}{name}    = $name;
                $project{$id}{enabled} = $enabled;
                next;
            }

            if ($id =~ /$filter/){
                $project{$id}{name}    = $name;
                $project{$id}{enabled} = $enabled;
                next;
            }

            #next unless $name =~ /CMS/;
            #print "$id $name\n";
        }
        close(KS);
    }
    return %project;
}

sub FlavorShow($){
    my $opt = shift @_;
    my $cmd = "/usr/bin/openstack flavor show --fit-width $opt 2>/dev/null";
    if (not open(F,"$cmd 2>/dev/null |")){
	print STDERR "[E] Cannot run \"$cmd\": $!\n";
	return ();
    }
    my %list = ();
    
    while(<F>){
	next if /^\+/;
	s/\s+//g;
	#print;
	my ($key,$value) = (split(/\|/,$_))[1,2];
	next if $key eq "Field";
	$list{$key} = $value;
	#print ">>> $key, $value\n";
    }
    close(F);
    
    return %list;
}

sub FlavorList(){
    my $cmd = "/usr/bin/openstack flavor list --all --long  -f value -c ID -c Name -c RAM -c Disk -c Ephemeral -c VCPUs -c Swap";
    if (not open(F,"$cmd 2>/dev/null |")){
	print STDERR "[E] Cannot run \"$cmd\": $!\n";
	return ();
    }
    my %list = ();
    #my $instances = my $cores = my $ram = undef;
    while(<F>){
	#print ;
	chomp;
	my ($id,$name,$ram,$disk,$ephemeral,$cores,$swap) = (split(/ /,$_));
	%{$list{$name}} =
	    (id        => $id,
	     ram       => $ram,
	     disk      => $disk,
	     ephemeral => $ephemeral,
	     swap      => ($swap || 0),
	     cores     => $cores,
	    );
	
	#print "$id,$name,$ram,$cores,$disk,$ephemeral,$swap\n";exit;
    }
    close(F);
    return %list;
}
