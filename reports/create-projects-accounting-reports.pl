#! /usr/bin/perl -w

# Running this script in "Bernd-mode" will produce two tables:
#
# * Information for each Cloud project (including 3500+ Personal projects)
# * Information for ~200 distinct chargegroups (ie. aggregates of the project info)
#
# Common questions like "how many cores are used by an experiment?" or "how much Ceph-stored data
# is used by the services of a team?" are easy to answer.
#
# More complicated questions may be more tricky to answer, interpreting this information needs to be done carefully:
#
# * For server information (incl. cores) there is no distinction between Virtual Machines and (Ironic managed) Bare Metal servers.
#   * Quite a few chargegroups have access to both VMs and BMs ("LXBATCH", "EOS Physics Support", "Elasticsearch", "ATLAS", etc)
#   * Note: there are separate entries for Cloud hypervisors and the VMs hosted on them. Beware of double counting.
# * Not all physical servers in the Data Center are Ironic-managed, and ~1000 older servers are missing from these tables.
#
# There are probably more pitfalls. Caveat emptor.
#
# Note: this script takes ~10 hours to run, and will produce a 1.1 MB document.

use strict;
use diagnostics;
use Data::Dumper;
use Text::TabularDisplay;
use Getopt::Long;
use JSON;
use Text::CSV_XS;

sub Usage($);


sub Usage($){

    print STDOUT <<EOUSAGE;

     $0 [--debug] [--verbose] [--show-storage] --charge-group <chargegroup name> [--matching]
     $0 [--debug] [--verbose] [--show-storage] --orgunit <org unit name> [--matching]
     $0 [--debug] [--verbose] [--show-storage] --all-functional-elements --exclude-personal-projects

       Parameters:

	 --matching     - filter on matching charge-group or orgunit, not on exact name
	 --show-storage - include Cinder, Manila and S3 numbers

       Example:

	 $0 --charge-group 'Application Provisioning' --charge-group 'E-Mail Infrastructure' --show-storage

	 $0 --charge-group '' --matching --show-storage # *ALL* chargegroup, all info. This the "Bernd-mode" :)

	 $0 --orgunit IT-CM --matching --show-storage # Chargegroups from IT-CM OrgUnits (ie. all sections in the group)

EOUSAGE

    return;
}

my %project = ();

my @chargegroup = my @orgunit = ();
my $allfes = 0;
my $exclude_personal_projects = my $matching = 0;

my $debug = my $verbose = 0;
my %opts = (
    debug   => \$debug,
    verbose => \$verbose,
    "charge-group=s@" => \@chargegroup,
    "orgunit=s@"      => \@orgunit,
    "all-functional-elements"   => \$allfes,
    "exclude-personal-projects" => \$exclude_personal_projects,
    "matching"                  => \$matching,
    );


my $rc = Getopt::Long::GetOptions(
    \%opts,
    "debug","verbose",
    "charge-group=s@" => \@chargegroup,
    "orgunit=s@"      => \@orgunit,
    "show-storage",
    "all-functional-elements"   => \$allfes,
    "exclude-personal-projects" => \$exclude_personal_projects,
    "matching"                  => \$matching,
    );

exit 1 unless $rc;
print STDERR Dumper(\%opts) if $debug;

if (not @chargegroup and not @orgunit and not $allfes){
    Usage("");
    exit 1;
}
#print STDERR "CHG = ".Dumper(\@chargegroup) if $debug;
#print STDERR "ORG = ".Dumper(\@orgunit) if $debug;

#$ENV{OS_CLOUD} = $ENV{OS_REGION_NAME} = "cern";

#
# Get chargegroup id <-> name mapping
#
# Source of truth: 3rd sheet of https://cernbox.cern.ch/index.php/apps/collabora/public/j8wYAiDkDsEC0ja/edit//chargegroup_mapping.ods
#
my %chargegroup = ();
my $csv = Text::CSV_XS->new ({ binary => 1, auto_diag => 1 });
$csv->getline (*DATA) ; # ship header
while (my $row = $csv->getline (*DATA)) {
    my ($id,$chargegroup,$category,$orgunit) = @$row[0,1,3,4];
    #print ">>> $id,$chargegroup,$category,$orgunit\n";
    next if not defined $id;
    if (@chargegroup){
	if ($matching){
	    next unless grep {$chargegroup =~ $_} @chargegroup;
	}else{
	    next unless grep {$_ eq $chargegroup} @chargegroup;
	}
    }elsif (@orgunit){
	if ($matching){
	    next unless grep {$orgunit =~ $_} @orgunit;
	}else{
	    next unless grep {$_ eq $orgunit} @orgunit;
	}
    }elsif ($allfes){
	next unless $category eq "FE";
    }
    $chargegroup{$id}{name} = $chargegroup;
    $chargegroup{$id}{orgunit} = $orgunit;
}
print STDERR Dumper(\%chargegroup) if $debug;
#
# Get list of projects for the given accounting group
#
if (not -x "/usr/bin/cci-iterator-project"){
    print "[E] cannot find \"cci-iterator-project\" tool, are you on ciadm?\n";
    exit 1;
}
my $cmd = "/usr/bin/cci-iterator-project -c name -c id -c chargegroup -f json" ; #--filter 'hasattr(project,\"accounting-group\") and getattr(project,\"type\") != \"personal\"";

print "[V] Getting accounting group information...\n" if $verbose;
print "[D] \$cmd = \"$cmd\"\n" if $debug;
my $result = `$cmd`;
for (@{from_json($result)}){
    my %tmp = %$_;
    #print Dumper(\%tmp);
    my $name           = $tmp{name};
    my $project_id     = $tmp{id};
    my $chargegroup_id = $tmp{chargegroup};

    next if $exclude_personal_projects and $name =~ /^Personal /;
    if (grep {$chargegroup_id eq $_} keys %chargegroup){
	$project{$name}{id} = $project_id;
	$project{$name}{chargegroup_id} = $chargegroup_id;
	$project{$name}{orgunit} = $chargegroup{$chargegroup_id}{orgunit};
    }

    #last if scalar(keys %project) == 15;
}


print "[I] Found ".scalar(keys %project)." projects...\n";
exit if scalar(keys %project) == 0;
#print Dumper(\%project);exit;

#
# Limits, to get VM + blockstorage info
#
print "[V] Getting project limits information for ".scalar(keys %project)." projects...\n" if $verbose;
for my $name (sort keys %project){
    print "[D] Getting Nova usage for project \"$name\"\n" if $debug;

    my $cmd = "/usr/bin/openstack quota list --project \"$name\" --compute --detail -f json 2>/dev/null";
    my $result = `$cmd`;
    #print Dumper(\$result);
    if (not $result){
	print "[W] Cannot get Nova quota for project \"$name\"\n";
	next;
    }
    map {$project{$name}{$_} = 0} qw(cores instances);
    for (@{from_json($result)}){
	my %tmp = %$_;
	for my $key (qw(cores instances)){
	    $project{$name}{$key} = $tmp{"In Use"} if $tmp{Resource} eq $key;
	}
    }
}

#
# Storage info
#
if ($opts{"show-storage"}){
    #
    # Correct Cinder data: "openstack limits ..." does not properly return Cinder limits, so let's ask Cinder directly.... (May 2019)
    #
    print "[V] Getting Cinder quota information for ".scalar(keys %project)." projects...\n" if $verbose;
    for my $name (sort keys %project){
	print "[D] Getting Cinder quota for project \"$name\"\n" if $debug;
	open(CINDER,"/usr/bin/cinder quota-usage $project{$name}{id} |") or die "aaargh... $name\n";
	while(<CINDER>){
	    s/ //g;
	    my ($id,$usage,$limit) = (split(/\|/,$_))[1,2,4];
	    next if not defined $id;
	    $project{$name}{gigabytes} = $usage if $id eq "gigabytes";
	    $project{$name}{volumes}   = $usage if $id eq "volumes";
	    $project{$name}{snapshots} = $usage if $id eq "snapshots";
	}
	close(CINDER);
    }

    #
    # S3 data
    #
    print "[V] Getting S3 quota information for ".scalar(keys %project)." projects...\n" if $verbose;
    for my $name (sort keys %project){
        next if $name =~ /^Personal /; # No S3 quota in personal projects :)
	print "[D] Getting S3 quota for project \"$name\"\n" if $debug;
	my $cmd = "/usr/bin/cci-s3-quota show --project \"$name\" -f json 2>/dev/null";
	my $result = `$cmd`;
	#print Dumper(\$result);
	if (not $result){
	    print "[W] Cannot get S3 quota for project \"$name\"\n";
	    next;
	}
	for (@{from_json($result)}){
	    my %tmp = %$_;
	    $project{$name}{s3usage} = $tmp{Used_GB};
	    $project{$name}{s3quota} = $tmp{Max_GB};
	}
    }

    #
    # Get Manila data
    #
    print "[V] Getting Manila quota information for ".scalar(keys %project)." projects...\n" if $verbose;
    for my $name (sort keys %project){
        next if $name =~ /^Personal /; # No Manila quota in personal projects :)
	print "[D] Getting Manila quota for project \"$name\"\n" if $debug;
	open(MANILA,"/usr/bin/manila quota-show --detail --tenant $project{$name}{id} |") or die "aaargh... $name\n";
	undef my $property;
	my %quota = ();
	while(<MANILA>){
	    s/ //g;
	    my ($key,$value) = (split(/\|/,$_))[1,2];
	    next if not defined $value;
	    #print "(k,v) = ($key,$value)\n";
	    $property = $key if $key ne "";
	    next if $property eq "Property"; # skip header
	    next if $property eq "id";
	    (my $attr,$value) = split(/=/,$value);
	    $quota{$property}{$attr} = $value if defined $value;
	}
	close(MANILA);
	$project{$name}{shares} = $quota{gigabytes}{in_use} || 0;
    }

    #print Dumper(\%project);
    #exit;
}

#
# Print the result
#
my @cols = ("Project Name","OrgUnit","Chargegroup","Cores","Servers");
push(@cols,"Volumes","Snapshots","Vols [GB]","S3 [GB]","Shares [GB]") if $opts{"show-storage"};
my $table = Text::TabularDisplay->new(@cols);

my %chggroup = my %total = ();
for my $name (sort keys %project){
    my @row = map {$project{$name}{$_}} qw(cores instances);
    push(@row,map {$project{$name}{$_}} qw(volumes snapshots gigabytes s3usage shares)) if $opts{"show-storage"};
    my $orgunit = $project{$name}{orgunit};
    my $chg_id = $project{$name}{chargegroup_id};
    my $chargegroup = $chargegroup{$chg_id}{name} || "(undefined)";
    $table->add($name,$orgunit,$chargegroup,@row);
    $total{count}++;
    map {$total{$_} += ($project{$name}{$_} || 0 )} qw(cores instances);
    map {$total{$_} += ($project{$name}{$_} || 0 )} qw(volumes snapshots gigabytes s3usage shares) if $opts{"show-storage"};

    $chggroup{$chargegroup}{count}++;
    $chggroup{$chargegroup}{orgunit} = $orgunit;
    map {$chggroup{$chargegroup}{$_} += $project{$name}{$_} || 0} qw(cores instances);
    map {$chggroup{$chargegroup}{$_} += $project{$name}{$_} || 0} qw(volumes snapshots gigabytes s3usage shares) if $opts{"show-storage"};
}

my @total = ("Total: $total{count} projects","","",$total{cores},$total{instances});
push(@total,map {$total{$_}} qw(volumes snapshots gigabytes s3usage shares)) if $opts{"show-storage"};
$table->add(@total);
print $table->render . "\n";

#
# Summary per Chargegroup
#
@cols = ("Chargegroup","OrgUnit","Projects","Cores","Servers");
push(@cols,"Volumes","Snapshots","Vols [GB]","S3 [GB]","Shares [GB]") if $opts{"show-storage"};

$table = Text::TabularDisplay->new(@cols);
for my $name (sort {$chggroup{$b}{cores} <=> $chggroup{$a}{cores}} keys %chggroup){
    my @data = ($name,map {$chggroup{$name}{$_}} qw(orgunit count cores instances));
    push(@data,map {$chggroup{$name}{$_}} qw(volumes snapshots gigabytes s3usage shares)) if $opts{"show-storage"};
    $table->add(@data);
}
@total = ("Total: ".scalar(%chggroup)." chargegroups","",map {$total{$_}} qw(count cores instances));
push(@total,map {$total{$_}} qw(volumes snapshots gigabytes s3usage shares)) if $opts{"show-storage"};
$table->add(@total);
print $table->render . "\n";

#
# Done
#
exit 0;

__END__
uuid,name,active,category,org_unit,admins
a0a79f7d-2423-f000-7904-c39b27667a46,3Com vendor,0,FE,IT-CF-SAO,
7a7f4d29-e24b-4b27-9ac4-97df2398a402,AARC2,1,Project,None,
d05575a9-609e-f040-c713-e946666f3f56,Academic training,1,FE,IT-CDA-IC,dimou
,,,,,tbaron
ea56fa70-0a0a-8c0a-017c-77c47438f2fc,Accelerator Database Instances,0,FE,IT-DB-DBR,
e5e66512-9491-6100-7904-7129f337bfd4,Accelerators consolidation,0,FE,SMB-SE-CEB,fmagnin
f4c7695b-661e-403d-9a9f-8d49d057f96f,ACCESS,1,Experiment,None,
ea571363-0a0a-8c0a-01c5-688d1b3a03ff,Access and Safety Systems Engineering,1,FE,EN-AA-CSE,fchapron
,,,,,ninin
ea57140d-0a0a-8c0a-0144-9fb5d36e55ac,Access Card Printing,1,FE,EN-AA-AC,fchapron
,,,,,ninin
,,,,,rnunes
ea571383-0a0a-8c0a-0159-d7520d291717,Access Control Systems Design,0,FE,BE-ICS-CSE,fchapron
,,,,,ninin
c7a3c60b-db99-7700-ae65-9ec4db9619ad,Accounts Receivable,0,FE,FAP-EF-TC,kgachet
b4f64356-f3fe-4e24-b7a1-1b3cf2406e64,ACE,1,Experiment,None,
fa8069b7-5dd3-0c00-9878-a85f147607bc,Acron,1,FE,IT-CM-LCS,manuel
,,,,,schwicke
,,,,,toteva
8fa793bd-2423-f000-7904-c39b27667a5b,Actemium vendor,0,FE,IT-CF-SAO,
d8879f3d-2423-f000-7904-c39b27667a6d,Action vendor,0,FE,IT-CF-SAO,
ea570036-0a0a-8c0a-0187-aef56b68050a,Active Directory,1,FE,IT-CDA-AD,bukowiec
,,,,,ormancey
,,,,,pmartinz
ea5710b0-0a0a-8c0a-01f7-87ab010876b2,Active Endpoints support contract,0,FE,GS-AIS-EB,dmathies
,,,,,rtitov
0905d98a-1083-49a2-b49c-3d63100ef3c1,AD-1,1,Experiment,None,
ef7a16ad-13ee-4703-b279-4e26f1625e42,AD-2,1,Experiment,None,
52e73f48-a79f-4e5a-95e2-706b0f7cce7a,AD-3,1,Experiment,None,
04e3a916-307b-451a-a052-ed66168251f0,AD-4,1,Experiment,None,
429a2182-ddc2-4d12-ae74-198c942ec358,AD-5,1,Experiment,None,
1ead2b26-244b-4ee1-8249-345ab3a85d97,AD-6,1,Experiment,None,
751d24d2-afee-4d8d-b436-1c0d49f834de,AD-7,1,Experiment,None,
aa982228-c4a7-4093-8d0c-06874f97c6e7,AD-8,1,Experiment,None,
ea57133e-0a0a-8c0a-010a-53154b95e232,ADaMS,1,FE,EN-AA-AC,fchapron
,,,,,ninin
,,,,,pmartel
,,,,,rnunes
ea5711e7-0a0a-8c0a-01db-d30355d7e256,Addressage Courier,0,FE,GS-AIS-GDI,jjanke
d0d326c4-1b35-6090-3120-a8afab4bcb18,Admin e-guide webmaster,1,FE,FAP-DHO-TP,claignel
,,,,,cspencer
ffd816d2-4fdb-5e00-b8cb-ca1f0310c7b9,Administrative Application Support,1,FE,FAP-BC-QS,dtsekour
,,,,,mcmarque
ea56f666-0a0a-8c0a-0032-d585dcd16c06,Administrative Processes,1,FE,FAP-DHO-TP,claignel
,,,,,cspencer
,,,,,favrot
570996d2-4fdb-5e00-b8cb-ca1f0310c765,Administrative Sector Computer Hardware Support,1,FE,FAP-BC-QS,dtsekour
,,,,,mcmarque
53a19d3e-4f2b-9a40-b8cb-ca1f0310c702,Administrative Software Application Access Rights,1,FE,FAP-BC-QS,dtsekour
,,,,,mcmarque
499f0ce9-4f97-a600-b8cb-ca1f0310c725,Administrative Websites,0,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
d114267e-dbc1-3740-a1cd-a4f84b96193b,Adressage and Courrier,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
699c5f12-088a-7800-6d21-41a546d44351,Advance retirement for staff members,1,FE,HR-CBS-B,ygrange
059c5d73-5875-4681-ac15-65c68f583ab0,Advanced Virgo,1,Experiment,None,
88062479-f9c0-4bf1-ab4a-bfd1401e8975,AEGIS,1,Experiment,None,
ea56fb96-0a0a-8c0a-00c4-1ac4eb58df12,AFS,1,FE,IT-ST-GSS,dalvesde
,,,,,iven
,,,,,moscicki
f54e4340-dbfd-1f00-1aab-9247db961924,AIADM,1,FE,IT-CM-LCS,ibarrien
,,,,,manuel
,,,,,reguero
,,,,,straylen
,,,,,toteva
ea5711d3-0a0a-8c0a-00db-4978cd833463,AIS Internal Productivity Tools,0,FE,FAP-AIS-GI,
ea570943-0a0a-8c0a-005f-0343842e065c,AIS Login,0,FE,GS-AIS-GDI,
718362c9-a4a2-d0c0-9878-a279f01cb70c,AIS Media,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
ea5711fc-0a0a-8c0a-013a-0c2d67af2a1b,AIS Reminder Platform,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
ea570a2d-0a0a-8c0a-0133-d69bf434e981,AIS Roles,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
779eaa8c-ea9c-44fa-80c1-67a824453a53,ALEPH,1,Experiment,None,
ea56ffb0-0a0a-8c0a-0083-bdd217521139,Alerter,1,FE,IT-CDA-AD,mkwiatek
6395494b-62ce-4a0e-ad06-da4b89f6625e,ALICE,1,Experiment,None,
72300922-4c8d-ce40-c713-26a72897113e,Alice Computing Support,1,FE,EP-AIP-SDS,hristov
,,,,,litmaath
8545936f-15a7-3540-c713-566b001b8473,ALICE DCS Computing,1,FE,EP-UAI,chochul
,,,,,pebond
ae2d70b2-db68-6740-ae65-9ec4db961937,ALICE Fence Framework Support,0,FE,EP-AIP-SDS,hristov
0d654cc7-dbcb-5340-ce6e-9c09db96197a,ALICE Secretariat,0,FE,EP-AGS-SE,
19a0a967-f0d0-ca80-c713-3d85b5657cec,ALICE SW Build Infrastructure,1,FE,EP-AIP-SDS,eulisse
,,,,,hristov
b659ed53-aaa3-4e34-ad6a-a32b6a80d891,ALPHA,1,Experiment,None,
8cb717bd-2423-f000-7904-c39b27667aff,Altusen vendor,0,FE,IT-CF-SAO,
1579b271-db4c-ff80-7c55-9eb3db9619de,Alumni platform support,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
827254ce-66b1-4004-ada4-6ec2a1a4aa3b,AMS,1,Experiment,None,
0f7fca90-4f9f-ca40-93ec-df601310c7c7,AMS Computing Operations,1,FE,EP-UGC,bshan
a16da71b-db9d-bb00-f995-6184da961909,AMS Website,1,FE,EP-UGC,bshan
f7ae6f08-82bb-4b8a-827e-925107698d79,AMS-RE1,1,Experiment,None,
ea56f47e-0a0a-8c0a-0050-00ca9a029ba6,Analysis and Modeling,0,FE,HR-CB-MI,lsmith
9321004f-db2e-9f80-ce6e-9c09db961913,Android Support,1,FE,IT-CDA-AD,mkwiatek
,,,,,sdellabe
8027ceeb-7bdd-4d04-8ea8-4e956a54b1a2,ANTARES,1,Experiment,None,
76e4d760-4fc0-2e00-b8cb-ca1f0310c718,Anti-virus,1,FE,IT-CDA-AD,dmamouzi
,,,,,mkwiatek
,,,,,sdellabe
e7975f7d-2423-f000-7904-c39b27667a07,APC vendor,0,FE,IT-CF-SAO,
75221ca1-1d31-7d80-c713-8e7019d56ac3,Apple Contract Support,1,FE,IT-CDA-AD,mkwiatek
,,,,,schroder
f0a7df7d-2423-f000-7904-c39b27667aa4,Apple vendor,0,FE,IT-CF-SAO,
03126ad1-4f5d-0380-06bc-10ee0310c712,Application Provisioning,1,FE,IT-CDA-AD,malandes
,,,,,mkwiatek
,,,,,schroder
,,,,,sdellabe
ea570ebd-0a0a-8c0a-0138-ac6397966992,APT Resource Planning,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
8080c327-ecfb-4f31-9f9d-216e2c05b0dc,ARCHIVER,1,Project,None,
0270400f-abde-400c-80c3-d1494a56f090,ArDM,1,Experiment,None,
ea56e906-0a0a-8c0a-015d-79f743b3289d,ARGUS,1,FE,IT-CM-IS,mccance
e2ae7558-0207-4b5b-8631-1fa274a9f5db,ASACUSA,1,Experiment,None,
c8a73cde-11d9-e500-adf9-ea91634bc65b,Asbestos removal,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
ea57196f-0a0a-8c0a-006e-49a92ac8a770,Assainissement - CC Vallier,0,FE,GS-SE-CWM,fmagnin
e33e473e-9445-bc80-6d21-20312bb3b665,Associates,1,FE,HR-TA-SGR,gballet
,,,,,kthomas
ea571610-0a0a-8c0a-0083-58bf7ba9d004,Assystem Industrial Control,0,FE,GS-ASE-AS,grau
d28883af-0627-4bfa-b23c-616ea19b2c07,ATLAS,1,Experiment,None,
4c443904-354c-a540-7904-21ec1a3bcf8a,ATLAS Central Computing Operations,1,FE,EP-UAT,chlee
,,,,,glushkov
bbafc824-4fc9-f6c0-7db7-d3ef0310c785,ATLAS Cloud Operation,0,FE,EP-UAT,berghaus
,,,,,plove
9b93f180-354c-a540-7904-21ec1a3bcf9e,ATLAS Computing Support Meyrin,0,FE,EP-UAT,
cbb2bdcc-350c-a540-7904-21ec1a3bcf51,ATLAS Fabric,0,FE,EP-UAT,
579a3876-db28-6740-ae65-9ec4db9619b1,ATLAS Fence framework Support,0,FE,EP-UAT,
cf77488f-dbcb-5340-ce6e-9c09db96193c,ATLAS Secretariat,0,FE,EP-AGS-SE,
87b43504-354c-a540-7904-21ec1a3bcf52,ATLAS Web-services,0,FE,EP-UAT,
168a12ac-58e9-4274-859f-b20749997110,atlas-wisconsin,1,Project,None,
ea570fed-0a0a-8c0a-0106-addddf23be28,Atlassian Support Contract,0,FE,GS-AIS-GDI,jjanke
f5975b7d-2423-f000-7904-c39b27667acd,ATOS vendor,0,FE,IT-CF-SAO,
aa853786-6c94-4736-aff9-5e9343f6836c,ATRAP,1,Experiment,None,
f07d383d-ded0-4ebe-85c2-bf33e18cd4a9,ATS,1,Department,ATS,
ea5711be-0a0a-8c0a-00ef-19a1a918adf5,ATS Applicant Tracking system,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
,,,,,mcmarque
4096b8c5-4fe9-d600-93ec-df601310c790,Audiovisual Production Team,1,FE,IR-ECO,catapano
,,,,,mbrice
26f29708-03d1-4a6b-a063-5fc77e88c528,AUGER PROJECT,1,Experiment,None,
ea56ff8f-0a0a-8c0a-0115-7c0e42603b88,Authentication,1,FE,IT-CDA-IC,ormancey
,,,,,ptedesco
,,,,,tbaron
a80b07a2-4f47-8b00-77fe-29001310c7e7,Authoring,1,FE,IT-CDA-WF,awagner
bce731e8-4f5a-5e00-87a2-f5601310c7a1,Automatic doors maintenance and works,1,FE,EN-ACE-COS,sgrillot
ea571624-0a0a-8c0a-0072-99f1648de20e,Automatic Fire Detection,1,FE,EN-AA-AS,fchapron
,,,,,grau
,,,,,ninin
ea5715b4-0a0a-8c0a-0158-c1a4aa7185f1,Automatic Gas Detection and ODH,1,FE,EN-AA-AS,fchapron
,,,,,grau
,,,,,ninin
ea5710db-0a0a-8c0a-01bf-7e4e66a9222a,AVCL/PECT,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
e08f8a14-6aed-484f-b89d-302596becbe9,AWAKE,1,Experiment,None,
ea570d7a-0a0a-8c0a-01d5-219e62d3eac9,Baan,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
9c22c1ba-0e1a-4832-9daf-69e19b23227c,BABAR,1,Experiment,None,
497fc1b9-840d-4268-b626-04d2e9e87891,Baby MIND,1,Experiment,None,
ea57070a-0a0a-8c0a-00da-b403dfb64cd2,Banker's Guarantees,0,FE,FAP-ACC-AP,cmarme
,,,,,jarobins
8db47c1a-11d9-e500-adf9-ea91634bc68b,Barracks rental and purchase service,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
62d6f565-7345-4cfa-9fad-94ab03c3ac68,BASE,1,Experiment,None,
12210b23-dbd2-4f0e-b00a-5352cbe95711,Batch Unknown,1,Project,None,
ab87d77d-2423-f000-7904-c39b27667a8c,Bberry vendor,0,FE,IT-CF-SAO,
ea5701cf-0a0a-8c0a-001d-f594534c5254,BDII,1,FE,IT-CM-IS,mccance
85bed1ec-90de-4b53-bf59-45daa785dca6,BE,1,Department,BE,
8d4366e9-4fb8-9740-b9cc-09de0310c7f1,Beam Instrumentation Electrical Request Validation,1,FE,SY-BI,ojones
05be631f-4fab-0300-7db7-d3ef0310c776,Beam Transfer Field Support,1,FE,SY-ABT-BTE,cboucly
ce1fe71f-4fab-0300-7db7-d3ef0310c792,Beam Transfer Production Support,1,FE,SY-ABT,cboucly
,,,,,goddard
fe682d4a-2af5-4022-9234-08087d63d4e7,BELLE,1,Experiment,None,
a3a38dd5-8a7b-4f57-8043-b7e946a07038,Belle II,1,Experiment,None,
2ed9b2e9-3ae7-4a97-8156-c8587fd3e861,BETS,1,Experiment,None,
cbf2a42d-9169-9504-adf9-a439183b9523,Bike Rental,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
26f3c450-4fbf-6200-7db7-d3ef0310c76d,Bikes and cars repair and maintenance,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
eb9060c7-4f30-dbc0-06bc-10ee0310c76b,Bikes and spare parts supplier,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
7e12f852-11d9-e500-adf9-ea91634bc663,Blinds maintenance,0,FE,SMB-SE-CEB,
60bfa01e-1199-e500-adf9-ea91634bc6ff,Blinds works,0,FE,SMB-SE-CEB,
f95c1be4-4f0b-8300-77fe-29001310c73b,"Blinds, windows",0,FE,SMB-SE-CBS,
467c4527-f087-dd00-7904-6a0c9f0e0118,BOINC Support,1,FE,IT-CM-IS,hoimyr
,,,,,mccance
d4376fe3-a2e7-40b4-8e1d-56dc7eb4a0aa,BOREX,1,Experiment,None,
55a713bd-2423-f000-7904-c39b27667a02,Brocade vendor,0,FE,IT-CF-SAO,
c77d5858-f501-d4c0-9878-e86b196c5cc1,BTE Desktop Support,1,FE,BE-ICS,fchapron
40c85ae1-b510-7100-7904-5e3417224b87,Burotel,1,FE,IT-CDA-IC,pferreir
,,,,,tbaron
ea570fc3-0a0a-8c0a-0078-47185049d0e9,Business Objects (BO),0,FE,FAP-AIS-GI,jjanke
ea570c54-0a0a-8c0a-0077-256b7587beb5,Business Objects Support Contract,0,FE,FAP-AIS-GI,jjanke
ea571210-0a0a-8c0a-0171-5df60a547b23,Business Process Consultancy,0,FE,GS-AIS-EB,rtitov
ea56ec4b-0a0a-8c0a-0103-eab3acad0694,Cabin Rental,0,FE,SMB-SE-DOP,yrobert
e2794760-4f84-ae00-b9cc-09de0310c7e3,Cabling UnitDEMO2015,0,FE,EN-EL,smachado
ea571249-0a0a-8c0a-01c3-b51c8dcd09d8,CAD,1,FE,EN-ACE-CAE,blepoitt
,,,,,friman
,,,,,mgarciac
ac8af4c4-4fbb-0700-fe13-2eff0310c71d,CAD Catalog Managers,1,FE,EN-MME,mgarciac
,,,,,rleuxe
ea5713e6-0a0a-8c0a-014b-90cadf904645,CAE Workstations Support,1,FE,EN-ACE-CAE,blepoitt
,,,,,friman
,,,,,fsoriano
,,,,,mgarciac
5434bf18-db30-73c0-269d-9e24db961934,Cafeterias,1,FE,SCE-SSC-CS,lalejeun
,,,,,pcorreia
,,,,,vmarchal
ca4f000c-96ec-4599-adfe-1926c1abd741,CALET,1,Experiment,None,
8f9ab477-228d-4c2e-9201-f73d89cc1bc0,CALICE,1,Experiment,None,
ea5701e6-0a0a-8c0a-0167-b031866247f8,CAProxy,1,FE,IT-ST-GSS,dvanders
,,,,,laman
c4342e7e-dbc1-3740-a1cd-a4f84b96190e,Car Pool and Driver ID Management,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea56ec7d-0a0a-8c0a-0109-3c50924f0a3f,Car Pool Management,0,FE,SMB-SIS,vmarchal
36a8e8f9-a1fc-ec00-9878-f17945ca4fc4,Card Payment system,0,FE,GS-ASE-AC,rnunes
ac541d6b-94e0-f400-6d21-20312bb3b683,Cards Office,1,FE,HR-CBS-RC,acook
,,,,,mquintas
ea57128a-0a0a-8c0a-015d-87bafdaf207f,CART Web,1,FE,EN-ACE-EDM,fanou
,,,,,kkrol
ac61b003-acc7-41bf-8974-d757d557e522,CAST,1,Experiment,None,
ea56fb81-0a0a-8c0a-0011-d887fe30e67d,CASTOR,1,FE,IT-ST-TAB,cano
,,,,,laman
,,,,,lopresti
8afc7195-a421-1000-9878-a279f01cb793,Castor Tape,1,FE,IT-ST-TAB,pace
,,,,,vlado
23fdb9d5-a421-1000-9878-a279f01cb75c,Castor Tape Vendor IBM (Contract),1,FE,IT-ST-TAB,pace
,,,,,vlado
c1fef9d5-a421-1000-9878-a279f01cb757,Castor Tape Vendor Oracle (Contract),0,FE,IT-ST-TAB,pace
75fdf27d-0a0a-8c08-0039-0920bc614931,CASTORALICE,0,FE,IT-DSS-FDO,
7600ff54-0a0a-8c08-0169-b2a45ab37542,CASTORATLAS,0,FE,IT-DSS-FDO,
7609c49c-0a0a-8c08-010d-40d42af2ca54,CASTORCERNT3,0,FE,IT-DSS-FDO,
7603801a-0a0a-8c08-01b4-f5de2fda39a2,CASTORCMS,0,FE,IT-DSS-FDO,
7605f727-0a0a-8c08-0090-77d3c97b110d,CASTORLHCB,0,FE,IT-DSS-FDO,
7607ee45-0a0a-8c08-0042-70fa7226cfd8,CASTORPUBLIC,0,FE,IT-DSS-FDO,
ea571002-0a0a-8c0a-01c6-4c777abd9fc2,Catalogue and Punchout,0,FE,GS-AIS-EB,rtitov
0df978d6-111d-e500-adf9-ea91634bc68e,Catering operation and maintenance,0,FE,SMB-SE-HE,martelc
b11a7cd6-111d-e500-adf9-ea91634bc600,Catering works,0,FE,SMB-SE-HE,martelc
23c0df97-09e1-492b-b42f-b2cf4b436d4d,CBM,1,Experiment,None,
d1942202-8438-b440-c713-6646aaa7ce50,CC HW Repair,0,FE,IT-CF,
ea56f747-0a0a-8c0a-01a9-3562c368938a,CC Operators,1,FE,IT-CF,barring
,,,,,grossir
,,,,,salter
,,,,,tfabio
0cdd3cae-0a0a-8c08-01f6-46f92b408824,CC Remote Console,0,FE,IT-CF,
1bc753fd-2423-f000-7904-c39b27667a05,Ccsysadmins vendor,0,FE,IT-CF-SAO,
c1cb6c5b-37ac-4a34-bc0f-5e37b9266568,CCX,1,Experiment,None,
ea57129e-0a0a-8c0a-00c7-4e9eb1b25aec,CDD,1,FE,EN-ACE-EDM,blepoitt
,,,,,cscoero
,,,,,mgarciac
ea57159f-0a0a-8c0a-0022-4be9462b7391,Cegelec SUSI Operation,0,FE,GS-ASE-AC,rnunes
ea571422-0a0a-8c0a-0080-51a9b0602def,Cegelec ZORA Operation,0,FE,GS-ASE-AC,rnunes
ea56edcb-0a0a-8c0a-01da-eb806dbd6c44,Centrales Thermiques - CC Dalkia,0,FE,SMB-SE-HE,martelc
af9298f2-041b-0944-7904-3b41fde4f97f,Ceph,1,FE,IT-ST-GSS,abchan
,,,,,dvanders
,,,,,espinal
,,,,,laman
ea56efa1-0a0a-8c0a-0022-731e1b8d94cf,CERN - Archive,1,FE,RCS-SIS-AR,ahollier
79765be8-98da-3000-6d21-708547f4c6ca,CERN Apartments,1,FE,SCE-SSC-CS,eweymaer
,,,,,gmathias
,,,,,lalejeun
,,,,,pcorreia
,,,,,vmarchal
75c99194-4f1d-7200-87a2-f5601310c742,CERN Car Insurance,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
ea5718f5-0a0a-8c0a-016b-371e60616659,CERN Car Rental - Long term,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
ea57199b-0a0a-8c0a-0111-b389d61ddbdc,CERN Car Rental - Short term,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
fe2bc216-f5b8-50c0-9878-e86b196c5c36,CERN Car Sharing,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
bbeddd46-19da-9900-7904-0003aab1c955,CERN Clubs Coordinating Committee,1,FE,DG,rbray
39168b81-84dc-f000-c713-6646aaa7ce59,Cern Competency Model (CCM),1,FE,HR-DHO,taillieu
ea56e986-0a0a-8c0a-0186-aeaa533e811a,CERN Document Server,1,FE,IT-CDA-DR,jbenito
,,,,,nitarocc
ea56fcae-0a0a-8c0a-01ce-4a2e66a1f359,CERN GRID 2nd Line Support,1,FE,IT-SC,aimar
,,,,,prodrigu
65e30471-a0e8-41c0-6d21-df786d7ffc88,CERN home website and social media,0,FE,IR-ECO-CO,katebrad
,,,,,sboutas
c3fa2526-4f8f-2a00-b9cc-09de0310c721,CERN Home Website content and news,1,FE,IR-ECO-CO,cmenard
,,,,,katebrad
0f9ac44d-4feb-f640-b316-c0501310c76b,CERN Mobility Coordinator,1,FE,SCE-SAM-TG,mpoehler
c7d779ca-1bff-10d4-aa9a-642abb4bcbc2,CERN Record Club Request,1,FE,HR,lefebure
,,,,,mjaekel
3c35a428-4f2b-1700-87a2-f5601310c7c8,CERN Schools of Physics Secretariat,0,FE,EP-AGS-SCS,mage
ad75219f-db29-33c0-eb72-9085db96194c,CERN Service Catalog Management,0,FE,SMB-SMS,ifernand
cd32455b-08e7-4dc4-adf9-6d3d8fa18aff,CERN Shops,1,FE,IR-ECO,moret
ea56eba6-0a0a-8c0a-01b6-19cd60d2604a,CERN Shuttle,0,FE,SMB-SIS,gbolling
c3b4a065-4faa-2600-b8cb-ca1f0310c7cd,CERN Social Media,1,FE,IR-ECO-CO,katebrad
1e8d0d78-db0b-5700-ce6e-9c09db96192f,CERN Users Office Web Master,0,FE,EP-AGS-UO,gdarvey
2785914f-a01c-c180-6d21-df786d7ffcd6,CERN Users' Office,1,FE,EP-AGS-UO,gdarvey
d837e812-4f38-5b40-b9cc-09de0310c7d5,CERN Visit execution,1,FE,IR-ECO-VI,briard
ea56ef45-0a0a-8c0a-0011-12c30a4f3bcf,CERN Web Archiving,0,FE,GS-SIS-ARC,ahollier
9caf961c-db9b-eb80-0261-75db8c96193c,CERN Web team support,1,FE,IR-ECO-CO,katebrad
,,,,,sboutas
a2546c25-4faa-2600-b8cb-ca1f0310c7cd,CERN Writing Support,1,FE,IR-ECO-CO,katebrad
,,,,,sboutas
ea56e880-0a0a-8c0a-01fc-0055dd663b96,CERN-PROD Nagios Instance,0,FE,IT-PES-PS,
e76154ab-118a-1580-7904-a06fd6378c4d,CERNBox,1,FE,IT-ST-GSS,gonzalhu
,,,,,moscicki
,,,,,rvalverd
8643acaa-db8b-4810-f4c8-949adb961952,CERNBox machines - EOS user/project/home/media,1,FE,IT-ST-GSS,gonzalhu
,,,,,moscicki
,,,,,rvalverd
d3abe512-1ba0-94d0-c917-65b0ab4bcb96,CERNMegabus,1,FE,IT-CM-LCS,timbell
,,,,,toteva
0036d5dc-dbfc-5c10-006f-d9f9f49619a4,CERNphone Application,1,FE,IT-CS,araczyns
,,,,,gcancio
,,,,,tnt
ea56fecc-0a0a-8c0a-0110-1112ae882a39,Certificate Procurement,1,FE,IT-CDA-WF,awagner
ea5700d6-0a0a-8c0a-0133-6a0757d405d7,Certification Authority,1,FE,IT-CDA-IC,ormancey
,,,,,ptedesco
,,,,,tbaron
bf7659d2-1ba8-54d0-c917-65b0ab4bcb5e,CERTMGR,1,FE,IT-CM-LCS,timbell
,,,,,toteva
cc230b09-1b69-a050-5a31-41588b4bcb07,CESAAM Application,1,FE,FAP-BC,dtsekour
,,,,,jjanke
,,,,,mcmarque
ea570d1d-0a0a-8c0a-00f7-2b447fdda160,CET,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
26cb1afd-4f2c-1380-d7b5-0ebf0310c7c4,CFU infrastructure,0,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
481d21e8-1bd7-e010-c1d8-fe62cb4bcba7,CGFES,1,FE,BE-CSS-FST,sdeghaye
b93cd40f-4ffc-9bc0-06bc-10ee0310c7da,CH Mobility,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
0b4150ba-94b4-3840-6d21-20312bb3b6b6,Children of Members of Personnel,1,FE,HR-TA-SGR,acook
,,,,,vgalvin
0d3f40c2-dbc7-17c0-1aab-9247db961956,CHIS Manager Office,0,FE,HR-CBS,
ea570a70-0a0a-8c0a-0035-296a0b7261cc,CHIS Membership Extraction,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
b1813a07-24eb-b400-7904-c39b27667ac8,CHIS SHIPID support,1,FE,HR-CBS,sbaudat
72be0918-4bbe-435f-b8d5-979958c992f9,CHORUS,1,Experiment,None,
df28d508-f791-44cb-9a07-5512070b681f,Citizen CyberScience,1,Project,None,
ea571819-0a0a-8c0a-0101-a0900e3ae0ea,Civil Engineering - CC GTM,0,FE,GS-SE-CWM,fmagnin
ea5717ff-0a0a-8c0a-00b1-3d4c7db5b2be,Civil Engineering - CC Scrasa,0,FE,GS-SE-CWM,fmagnin
ea56eed8-0a0a-8c0a-0097-6c7961ce1bcd,Civil Engineering Design,0,FE,SMB-SE-DOP,mpoehler
ea56ee6c-0a0a-8c0a-003b-1152b2fb4b66,Civil Engineering Works Supervision,0,FE,SMB-SE-CEB,fmagnin
ea56f831-0a0a-8c0a-00b6-f76b088e8771,CIXP,1,FE,IT-CS,dgutier
,,,,,jshade
ea570728-0a0a-8c0a-00c2-ef40210d703d,Claims and Third Party Paiments processing and Accounts,0,FE,FAP-EF-TC,kgachet
ea570787-0a0a-8c0a-010a-1160d29ad30a,Claims office,1,FE,FAP-ACC-PA,sdethure
64a0dc41-db25-6700-ae65-9ec4db961974,Cleaning Disinfection - Deratisa,0,FE,SMB-SIS,lalejeun
02d8bbd8-db61-2300-eb72-9085db96198a,Cleaning ELIOR,1,FE,SCE-SSC-CS,lalejeun
,,,,,pafonsec
,,,,,schaigne
ea571983-0a0a-8c0a-00e0-fab35be1dacf,Cleaning Management,1,FE,SCE-SSC-CS,lalejeun
,,,,,pafonsec
,,,,,schaigne
aa78375c-db61-2300-eb72-9085db9619fb,Cleaning STEM,1,FE,SCE-SSC-CS,lalejeun
,,,,,pafonsec
,,,,,schaigne
ea56ed8a-0a0a-8c0a-008e-bc18130d8027,Cleaning TOPnet,1,FE,SCE-SSC-CS,lalejeun
,,,,,pafonsec
,,,,,schaigne
914ac8c1-dbe1-6700-ae65-9ec4db9619c3,Cleaning Zone 3,0,FE,SMB-SIS,lalejeun
,,,,,vmarchal
d58b08c5-dbe1-6700-ae65-9ec4db961942,Cleaning Zone 4,0,FE,SMB-SIS,lalejeun
,,,,,vmarchal
741c8809-dbe1-6700-ae65-9ec4db96196e,Cleaning Zone 5,0,FE,SMB-SIS,lalejeun
,,,,,vmarchal
cd7f8ccd-dbe1-6700-ae65-9ec4db96191e,Cleaning Zone 99,0,FE,SMB-SIS,lalejeun
,,,,,vmarchal
93f45fea-6ffa-43aa-88aa-659eaa1b98c8,CLIC Detectors,1,Experiment,None,
1027b531-2fb4-42df-892f-acab492a8bc4,CLICdp,1,Experiment,None,
665e6ebd-f421-4657-9a28-3e30d616ce51,CLOUD,1,Experiment,None,
cc722ecf-6083-3880-c713-e946666f3fff,Cloud Infrastructure,1,FE,IT-CM-RPS,vaneldik
,,,,,wiebalck
98670121-4f21-1fc0-d7b5-0ebf0310c79a,Cloud Licence Office,1,FE,IT,reguero
7db7d7bd-2423-f000-7904-c39b27667aba,Clustervision vendor,0,FE,IT-CF-SAO,
ea5712b2-0a0a-8c0a-0127-51c21f8d84c5,CMMS Support and Consultancy,1,FE,EN-IM-AMM,widegren
23b588c6-064a-437e-bc0c-3b3df0aa6e97,CMS,1,Experiment,None,
21468810-b4f6-b100-7904-8a4404c0ebc0,CMS CRAB,1,FE,EP-UCM,camadeim
,,,,,ekizinev
6ec38c1c-b4b6-b100-7904-8a4404c0ebf6,CMS DB,1,FE,EP-UCM,camadeim
,,,,,ekizinev
dc170090-b4f6-b100-7904-8a4404c0eb36,CMS DocDB,1,FE,EP-UCM,camadeim
,,,,,ekizinev
a4870c90-b4f6-b100-7904-8a4404c0eb51,CMS Elog,1,FE,EP-UCM,camadeim
,,,,,ekizinev
82ae42af-191a-9540-7904-0003aab1c923,CMS EOS/CASTOR Support,1,FE,EP-UCM,camadeim
,,,,,ekizinev
,,,,,pfeiffer
b7984c14-b4f6-b100-7904-8a4404c0ebc2,CMS Frontier,1,FE,EP-UCM,camadeim
,,,,,ekizinev
d65a0818-b4f6-b100-7904-8a4404c0eb4a,CMS GlideinWMS,1,FE,EP-UCM,camadeim
,,,,,ekizinev
d379c894-b4f6-b100-7904-8a4404c0eb7f,CMS iCMS,1,FE,EP-UCM,camadeim
,,,,,ekizinev
ebda0c58-b4f6-b100-7904-8a4404c0eba9,CMS PdmV,1,FE,EP-UCM,camadeim
,,,,,ekizinev
685b0498-b4f6-b100-7904-8a4404c0ebb4,CMS PhEDEx,1,FE,EP-UCM,camadeim
,,,,,ekizinev
493c4cd8-b4f6-b100-7904-8a4404c0eb2e,CMS Production,1,FE,EP-UCM,camadeim
,,,,,ekizinev
6e1aee7c-4f41-0b00-b9cc-09de0310c752,CMS Safety Office,1,FE,EP-CMX-SCI,rperruzz
61bd4928-fdeb-7140-6d21-5d33119bb3b9,CMS SDT,1,FE,EP-UCM,camadeim
,,,,,ekizinev
006344cf-db8b-5340-ce6e-9c09db961962,CMS Secretariat,0,FE,EP-AGS-SE,zgarai
9f224437-5d19-7500-6d21-1f86c6a31e28,CMS Support,1,FE,EP-UCM,camadeim
,,,,,ekizinev
,,,,,pfeiffer
24ac805c-b4f6-b100-7904-8a4404c0eb43,CMS T0,1,FE,EP-UCM,camadeim
,,,,,ekizinev
23101c90-b43a-b100-7904-8a4404c0eb89,CMS Tracker,1,FE,EP-UCM,camadeim
,,,,,ekizinev
88f3444c-21d4-51c0-adf9-b5210b89fce6,CMS VOBox Support,1,FE,EP-UCM,camadeim
,,,,,ekizinev
49015414-b43a-b100-7904-8a4404c0eb29,CMS WBM,1,FE,EP-UCM,camadeim
,,,,,ekizinev
4d2f7723-5d19-7500-6d21-1f86c6a31ed1,CMS Webtools,1,FE,EP-UCM,camadeim
,,,,,ekizinev
,,,,,pfeiffer
f1715c14-b43a-b100-7904-8a4404c0ebad,CMS XrootD,1,FE,EP-UCM,camadeim
,,,,,ekizinev
0a7f8d08-2b1d-485e-acd9-56d4f061cec0,CNGS,1,Experiment,None,
83e95428-10e4-40ed-97f0-8cf26fc847a7,CNGS1,1,Experiment,None,
062e6b61-a9fd-4389-875b-9a253a886382,CNGS2,1,Experiment,None,
048a84f5-1bba-6450-4317-960cab4bcb9e,CodiMD,1,FE,IT-CDA-WF,awagner
,,,,,kolodzie
ea57085d-0a0a-8c0a-01a9-23d1cd2fb0d6,Collaboration Agreements,0,FE,IPT-PI-RC,
33e1bc45-db8a-eb00-7c55-9eb3db9619ea,Collective insurance against earnings loss,1,FE,HR-SA,cregelbr
,,,,,ghislain
ea5715d3-0a0a-8c0a-0026-988da39a03b9,Combined fire and gaz air sampling detection system (Sniffer),1,FE,EN-AA-AS,fchapron
,,,,,grau
,,,,,ninin
1e667288-05e4-4d81-9f39-a2fe9ba9a978,COMPASS,1,Experiment,None,
ea56f532-0a0a-8c0a-0003-fde01ba61a61,Compensation,1,FE,HR-CBS-C,sstapper
8f66145e-4f59-4740-b316-c0501310c7f2,Complaints Housing,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
,,,,,vmarchal
ea56fa9f-0a0a-8c0a-00cc-d78c7f519e36,Computer Security,1,FE,IT-DI-CSO,slopiens
,,,,,slueders
ffbc5c54-4f48-d600-93ec-df601310c721,Computer Security Infrastructure,1,FE,IT-DI-CSO,lvalsan
,,,,,slopiens
,,,,,slueders
,,,,,vbrillau
ea5703b1-0a0a-8c0a-0008-99eb4484303d,Computing Element,0,FE,IT-CM-IS,
47749a95-dbc2-2380-ae65-9ec4db96194d,Computing Facilities Technical Unit,0,FE,IT-CF-FPP,boissat
,,,,,ebonfill
ea56e9fa-0a0a-8c0a-001d-db81e75a840a,Computing Newsletter,0,FE,IT-CIS,
98a2bf19-0a0a-8c0a-0031-59f33aa43040,Computing Support ALICE,0,FE,IT-CF-SM,
98a17ffc-0a0a-8c0a-0118-0e872319f458,Computing Support ATLAS,0,FE,IT-CF-SM,
e0cc8bb6-0a0a-8c0a-01a8-f661cf05ef49,Computing Support BE,0,FE,IT-CF-SM,
989cbfdb-0a0a-8c0a-0066-89691c82bc16,Computing Support CMS,0,FE,IT-CF-SM,
e9f78e4e-0a0a-8c08-01da-3119fbaa4e03,Computing Support EN,0,FE,IT-CF-SM,
98a268a6-0a0a-8c0a-00ba-1c6b3d975178,Computing Support LHCb,0,FE,IT-CF-SM,
975013d7-0a0a-8c08-0184-6b0c292d1d40,Computing Support PH,0,FE,IT-CF-SM,
e0cef99c-0a0a-8c0a-004d-79904394a9f2,Computing Support TE,0,FE,IT-DI-SM,
ea56ea7e-0a0a-8c0a-0148-fdd9a917ecd3,Conference Rooms Infrastructure,1,FE,IT-CDA-IC,rsoroka
,,,,,tbaron
98fd7262-a4e2-58c0-9878-a279f01cb7b7,Conference Rooms Operations,1,FE,IT-CDA-IC,rsoroka
,,,,,tbaron
ea56ed05-0a0a-8c0a-0065-ea1f386e2869,Confidential Mail Management,1,FE,SCE-SSC-LS,elclerc
,,,,,pmuffat
,,,,,vmarchal
3b70b61b-4fbf-b200-b8cb-ca1f0310c73c,Configuration and Layout,1,FE,EN-ACE-CL,aperrot
,,,,,fanou
100c479f-2437-3440-7904-c39b27667a66,Configuration Management,1,FE,IT-CM-LCS,ibarrien
,,,,,manuel
,,,,,straylen
,,,,,toteva
ea570fd8-0a0a-8c0a-0031-47cece42a572,Confluence,0,FE,GS-AIS-GDI,jjanke
ea571a47-0a0a-8c0a-0130-195c5ba732a5,Contractors' personnel and Biometrics Registration,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
689f7db8-4f7b-7a80-b9cc-09de0310c722,Control Applications,1,FE,BE-ICS-CE,eblanco
,,,,,fchapron
,,,,,solp
0a351ab2-4f80-a240-b8cb-ca1f0310c78b,Control Audio/Video Installation,1,FE,BE-CEM-IN,ikozsar
21899dd2-dbb7-2780-eb72-9085db9619ae,Control Hardware Installation Internal Use,1,FE,BE-CEM-IN,ikozsar
,,,,,veym
ea56ec1f-0a0a-8c0a-0034-07b6f2f8f44a,Conventional Waste Collection and Classification,1,FE,SCE-SSC-CS,mlafager
,,,,,roussiez
cba39d1e-0827-45c4-adf9-6d3d8fa18ab6,Cooling and Ventilation Engineering,1,FE,EN-CV,mnonis
ea56fbe9-0a0a-8c0a-0090-9bb367ac104a,CORAL / COOL,1,FE,IT-DB-DBR,awiecek
,,,,,canali
,,,,,edafonte
,,,,,grancher
,,,,,valassi
424e140a-5cb8-451c-a807-f1c72c20148d,COSMOLEP,1,Experiment,None,
2154e2be-dbc1-3740-a1cd-a4f84b9619b1,Cost Centre Management,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea56f561-0a0a-8c0a-01b8-895ec36b296c,Counselling,0,FE,HR-SMC,gguinot
0b4b2c42-dbe3-9814-f807-89853296198e,COVID-19 HR,1,FE,HR-TA,acook
,,,,,taillieu
9635e80a-db23-9814-f807-89853296197f,COVID-19 Medical Support,1,FE,HSE-OHS,wfadel
,,,,,yloertsc
06535d7c-1b8e-dc54-aa9a-642abb4bcb93,"Covid-19 Occupational Safety - Expertise, Assistance and Verification",1,FE,HSE-OHS,delamare
,,,,,prouteau
,,,,,yloertsc
9b771b3d-2423-f000-7904-c39b27667abc,Cpi vendor,0,FE,IT-CF-SAO,
39423960-1b1b-e010-c1d8-fe62cb4bcb54,CPlusPlus Development Tools,1,FE,BE-CSS-FST,sdeghaye
ea56fbd3-0a0a-8c0a-01a0-c002973407c0,CRAB,0,FE,IT-SDC-OL,
943d73e4-dbf5-5450-b805-94dadb9619dc,Crane maintenance,1,FE,EN-HE-HEM,dlafarge
fb08345a-3abd-41df-a0c4-f63a6658f304,CREAM,1,Experiment,None,
4b8dab33-dbb2-c054-6c52-9e24db9619fd,CRIC,1,FE,IT-SC-OPS,andreeva
,,,,,anisyonk
,,,,,girolamo
,,,,,ppaparri
c801091d-f87d-4900-adf9-02fd20678c32,CRISP Participation,0,FE,IT-SDC-ID,
6c696d82-4011-43f5-8392-e8ce19024cac,CRYSTAL,1,Experiment,None,
3e67459c-2be6-410a-adf3-4b1a21cd096a,CRYSTAL CLEAR,1,Experiment,None,
86466da5-4321-49ba-b461-592dc3968df7,CRYSTALCLEAR,1,Experiment,None,
0e89e1e7-549b-4f2f-9abb-a9fce10f1b04,CS3,1,Project,None,
65852565-1d49-f500-c713-8e7019d56a28,CSA Actions,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
b5486c6f-db1e-5f44-1aab-9247db961918,CSA Control Room,1,FE,SCE-DOD,bejar
,,,,,dconstan
883cb5e5-1b3f-ac10-dc14-117a3b4bcbc6,CSA controleurs,1,FE,SCE-DOD,bejar
37ea1543-7cb6-6940-adf9-15f17b56190e,CSA main courante,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
f4bc069f-dbda-db44-1aab-9247db9619bb,CSA Manager,1,FE,SCE-DOD,bejar
,,,,,dconstan
9ebc9187-7cb6-6940-adf9-15f17b5619df,CSA Parking/Circulation,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
e27aac23-db5e-5f44-1aab-9247db9619c7,CSA Patrouilleurs,1,FE,SCE-DOD,bejar
,,,,,dconstan
960e2c67-db5e-5f44-1aab-9247db9619c8,CSA Reception 33,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
a589307d-4f82-5600-87a2-f5601310c786,CSA Service,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
a4dce027-db5e-5f44-1aab-9247db9619da,CSA Statiques,1,FE,SCE-DOD,bejar
,,,,,dconstan
d4adddc7-7cb6-6940-adf9-15f17b56197e,CSA video,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
d680258f-7cb6-6940-adf9-15f17b56199a,CSA Zora,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
ea571533-0a0a-8c0a-01a1-ad9dbda8deea,CSAM,1,FE,EN-AA-AS,fchapron
,,,,,grau
ea570e68-0a0a-8c0a-0185-209b39f204a8,CTA,0,FE,FAP-AIS-HR,
10533af8-da4c-4555-b89b-4dc415a02f34,CTA-PP,1,Experiment,None,
a1374950-c3fd-49b7-801d-1d7662055458,CTF3,1,Experiment,None,
2696e723-8498-3800-c713-6646aaa7cef9,Customs and Fiscal Advice,1,FE,SCE-SSC-LS,cbruggma
c82aca76-a461-1400-9878-a279f01cb700,CVMFS,1,FE,IT-ST-GSS,abchan
,,,,,dvanders
,,,,,laman
e9959951-0a0a-8c0a-00c2-73dd2dca44cb,D4ScienceII Server,0,FE,IT-CIS,
a51da581-6c1d-4c93-bb90-6389dbc14262,DAMPE,1,Experiment,None,
e6b81bff-0a0a-8c0a-01cc-64a1a7dfe534,Dangerous Waste Collection and Classification,1,FE,SCE-SSC-LS,cbruggma
,,,,,pmuffat
ea571663-0a0a-8c0a-01aa-8dbb2f27b757,Dassault CAD Support Contract,0,FE,EN-ACE-PLM,widegren
2ed1ffaf-99e7-adc0-7904-62f7b78ce145,Data Analysis Preservation Repository,1,FE,RCS-SIS-OS,akohls
,,,,,arlavasa
,,,,,pfokiano
63f4531d-fdef-f140-6d21-5d33119bb3b9,Data Analytics Infrastructure,0,FE,IT-DB-SAS,
0a5c4c80-7cd8-3184-adf9-15f17b5619c8,Data Centre Tools,0,FE,IT-CM-MM,
40a0ae41-dbdc-f300-a1cd-a4f84b961978,Data integration and reporting / Pentaho support,1,FE,IT-DB-DAR,awiecek
ea56fd3b-0a0a-8c0a-0098-7247c25ae4d2,Data Management Clients Development,1,FE,IT-ST-PDS,lmascett
8acb09a6-4fc7-3a00-87a2-f5601310c7b6,Data Streaming and Kafka,1,FE,IT-DB-SAS,jlunadur
,,,,,mmartinm
ea5710c5-0a0a-8c0a-0044-98edc6860938,Data warehouse,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
6e2357f9-fd7c-6900-7904-3c7edc1a53ae,Database and Application Server Monitoring,0,FE,IT-DB-DBR,
8ed213b9-fd7c-6900-7904-3c7edc1a53eb,Database Backup and Recovery,1,FE,IT-DB-DBR,awiecek
,,,,,canali
,,,,,edafonte
,,,,,grancher
ea5712c7-0a0a-8c0a-00af-e735463a5284,DataBase Consultancy,0,FE,GS-ASE-EDS,fanou
,,,,,kkrol
bb419855-941d-3cc0-6d21-20312bb3b6a8,Database on Demand,1,FE,IT-DB-DBR,awiecek
,,,,,canali
,,,,,dcollado
,,,,,edafonte
,,,,,grancher
320ec371-fd7c-6900-7904-3c7edc1a534a,Database Storage,1,FE,IT-DB-IA,awiecek
,,,,,canali
,,,,,edafonte
,,,,,grancher
,,,,,icoteril
ea56f92b-0a0a-8c0a-0161-0c4a1a2f7ac4,Datacenter Network,1,FE,IT-CS,dgutier
,,,,,vducret
ea56f779-0a0a-8c0a-0100-50ba8e14df42,Datacenter Projects,0,FE,IT-CF,
8b8a9dde-55d3-475d-be17-ef3854b501bb,DEEP-EST,1,Project,None,
ea57164f-0a0a-8c0a-015e-95a4e3da8dd0,DEF AFD/EVAC,0,FE,GS-ASE-AS,grau
3e87977d-2423-f000-7904-c39b27667a61,Dell vendor,1,FE,IT-CF,grossir
f7566de0-2a99-4254-acb9-4f89588a98c0,DELPHI,1,Experiment,None,
9978ed16-9491-6100-7904-7129f337bf26,Design and studies of civil engineering and architectural works,1,FE,SCE-SAM-TG,mpoehler
e2a6f3e7-db46-27c0-ae65-9ec4db961997,Detector Technologies Software,0,FE,EP-DT-DI,glehmann
,,,,,pmendez
,,,,,rsipos
ea570a4d-0a0a-8c0a-00cc-ffff9be99f54,Development and collaboration tools,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
68557164-1b1b-e010-c1d8-fe62cb4bcb43,Development and Diagnostics Tools,1,FE,BE-CSS-FST,sdeghaye
ea570099-0a0a-8c0a-0147-4a25d647aea8,DFS,1,FE,IT-CDA-AD,bukowiec
,,,,,mkwiatek
47792c50-5972-4935-88e0-0d7ec658e99a,DG,1,Department,DG,
18bc9f14-1bab-6050-f9c6-dd318b4bcbd9,DG Administrative Office,1,FE,DG-TMC,lleroux
15e3ce79-1b3f-a010-4317-960cab4bcbb4,DG Departmental Planning Office (DPO),1,FE,DG,arozycka
e5127341-1b3b-e810-4317-960cab4bcbff,DG Groups Administrative Secretariat,0,FE,DG-TMC,lleroux
2d17ff09-1b3b-e810-4317-960cab4bcba1,DG MERIT assistance,1,FE,DG-TMC,lleroux
97563309-1b3b-e810-4317-960cab4bcb1a,DG Training Assistance,1,FE,DG-TMC,lleroux
54484ecd-a2c0-4bc2-8506-057a6cc4a6df,DGS,1,Department,DGS,
15902053-9451-2500-7904-7129f337bf15,DICT response,1,FE,SCE-SAM-TG,mpoehler
b9973fef-dbf0-88d0-71b0-f9053296194b,Digital Object Identifier Registration support,1,FE,RCS-SIS-OS,arlavasa
,,,,,kanaim
6f330a34-4fbb-7a80-b9cc-09de0310c75f,DIP,1,FE,BE-CSS-CPA,fchapron
,,,,,mbraeger
,,,,,solp
e054d748-db7b-5494-f807-898532961931,Diplomatic request,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,roussiez
b7a45e13-944d-4892-9d48-3dbc6f1d75e2,DIRAC,1,Experiment,None,
b3a9261f-845c-f400-c713-6646aaa7cef7,Diversity and Inclusion Programme,1,FE,HR-DHO-DVY,lcarvalh
ea570174-0a0a-8c0a-00b1-7f03e65ae85c,DNS Load Balancing,1,FE,IT-CM-LCS,manuel
,,,,,psaiz
,,,,,reguero
5a4d6598-4f72-5200-b8cb-ca1f0310c738,Docker Registry,0,FE,IT-CM-LCS,
ea570ab2-0a0a-8c0a-0092-02dceb12fea0,DocLeg,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
ea56ef19-0a0a-8c0a-01f0-0deb9c171235,Document Scanning,1,FE,RCS-SIS-LB,vigen
63d4a0e1-7cbe-6540-adf9-15f17b5619cb,Domain Management,1,FE,IT-CS-CE,emartell
,,,,,katebrad
,,,,,sboutas
ea5714f7-0a0a-8c0a-003d-7727fd590fe4,Dosimeter Bluepart Production,1,FE,EN-AA-AC,fchapron
,,,,,ninin
,,,,,rnunes
f3bdea20-82ab-4fcf-8ef4-fc18c1185eae,DPHEP,1,Project,None,
ea56fe86-0a0a-8c0a-011f-99c360c8e4ba,DPM Development,1,FE,IT-ST-PDS,okeeble
b83d1b68-4f0b-8300-77fe-29001310c706,"Drainage, Sewage and Waste Water",1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
4f15f45a-11d9-e500-adf9-ea91634bc66e,"Drainage, sewage and waste water maintenance",0,FE,SMB-SE-CEB,
1904f896-11d9-e500-adf9-ea91634bc685,"Drainage, sewage and waste water works",0,FE,SMB-SE-CEB,
d45f5c2a-d883-4b96-bf13-ce1ae7548d9a,DREAM,1,Experiment,None,
b1b9c76f-4f49-4b40-06bc-10ee0310c7a8,Drone Mapping,1,FE,SCE-SAM-TG,yrobert
7683cd43-0a0a-8c08-0084-b5aa98bfcbf7,Drupal Infrastructure,1,FE,IT-CDA-WF,awagner
,,,,,eduardoa
13be72a2-4c78-ce00-c713-26a72897111c,DSS CI,1,FE,IT-ST-PDS,dirkd
,,,,,simonm
c72b5467-9253-4926-ac54-a5af93d20e11,DsTau,1,Experiment,None,
05fdd4b0-91c5-9500-adf9-a439183b956b,Dummy FE for DeptTesting,0,FE,GS,
9e985f82-134b-48a2-b70b-2586a0681db2,DUNE-PT,1,Experiment,None,
e982d587-db89-6010-7aca-890b06961996,Duty Travel Coordination,1,FE,FAP-ACC,bloublie
,,,,,jarobins
e3431123-4f96-2640-b9cc-09de0310c77c,DWH-EDH,0,FE,FAP-AIS-EB,ndecreve
9d23d9ef-4f56-2640-b9cc-09de0310c707,DWH-FP,0,FE,FAP-BC,varmeanu
c5f295ef-4f56-2640-b9cc-09de0310c734,DWH-HR,0,FE,FAP-AIS-GI,jjanke
4e63d123-4f96-2640-b9cc-09de0310c719,DWH-PM,0,FE,FAP-AIS-PM,bdaudin
ea570a9c-0a0a-8c0a-00ac-1354350d954c,E-Groups Application,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
ea570021-0a0a-8c0a-0069-514aee38f745,E-Mail Infrastructure,1,FE,IT-CDA-IC,gtenagli
,,,,,pgrzywac
,,,,,tbaron
0689c7a6-4f63-c3c0-87a2-f5601310c74f,e-procurement application,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
185f3f20-db9e-4818-006f-d9f9f496198b,e-Signature tool,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
5647d7f9-2423-f000-7904-c39b27667ab9,E4 vendor,0,FE,IT-CF-SAO,
33bf70ca-c192-485b-8ed6-85c1fa4010b6,E51,1,Experiment,None,
7fe24316-c6dc-454f-88f8-7c22434562d4,E52,1,Experiment,None,
8b0f5490-ba18-4b85-96cc-58eb654a3ccb,E53,1,Experiment,None,
e7916996-7406-4c84-9412-5b463bdf1b2d,E54,1,Experiment,None,
a0fa39f4-8c0d-4e87-967d-d7e580225fc8,E55,1,Experiment,None,
655d754e-c46d-410a-88b2-8f921c7543e0,E55a,1,Experiment,None,
631a2077-3beb-4130-8965-e225deb48718,E56,1,Experiment,None,
b163be09-d995-4aed-abf1-a5a64e1f7c95,E57,1,Experiment,None,
6b4242a4-8bcb-4780-9118-9c1effdee945,E58,1,Experiment,None,
358af754-96f9-43b2-b2fd-bdf797ecad96,EA-IRRAD Mixed-Field,1,Experiment,None,
f67b5ef7-8c31-4f60-a4fc-d6e8f7cd9003,EA-IRRAD Proton,1,Experiment,None,
4173d5a6-4b8a-4e47-86ba-d82866385d84,ECC,1,Experiment,None,
ea570ac9-0a0a-8c0a-005d-f66f7d4f9631,EDH,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
ea570ae4-0a0a-8c0a-0182-50510fe20f06,EDH Access and Safety,0,FE,GS-AIS-EB,rtitov
a7606ef6-db81-3740-a1cd-a4f84b9619e7,"EDH Accounting, Treasury and Payments",1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea570b0f-0a0a-8c0a-006f-ebcf8cf90ada,EDH Administration,0,FE,GS-AIS-EB,rtitov
a1d1a2be-db81-3740-a1cd-a4f84b96194a,EDH Benefits and Talent Management,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
ea570b29-0a0a-8c0a-01fa-1e6521d05978,EDH Catalogues,0,FE,GS-AIS-EB,rtitov
fbf1eefe-db81-3740-a1cd-a4f84b96196d,EDH Core HR and Talent Acquisition,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
ea570b40-0a0a-8c0a-01bb-81284e64a5fc,EDH Document Creation,0,FE,GS-AIS-EB,rtitov
ea570b5b-0a0a-8c0a-00e9-e1e87e8bfd52,EDH HR and Training,0,FE,GS-AIS-EB,rtitov
ea570eec-0a0a-8c0a-0090-e4a34a1dac26,EDH Leave and Overtime,0,FE,GS-AIS-EB,rtitov
ea570b75-0a0a-8c0a-008b-aa94f53d97a6,EDH Logistics,0,FE,GS-AIS-EB,rtitov
cce0267a-db81-3740-a1cd-a4f84b961923,EDH Planning and Control,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
5f1122ba-db81-3740-a1cd-a4f84b961959,EDH Procurement and Jobs,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea570b8b-0a0a-8c0a-0179-019e5c9c2b5e,EDH Purchasing,0,FE,GS-AIS-EB,rtitov
3ff0a67a-db81-3740-a1cd-a4f84b9619c0,EDH Site Management,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
7b316efa-db81-3740-a1cd-a4f84b9619ce,EDH Supply Chain and Logistics,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea570bb0-0a0a-8c0a-018a-867060e207ec,EDH Travel and Claims,0,FE,GS-AIS-EB,rtitov
ea5712de-0a0a-8c0a-007b-5bc637206165,EDMS,0,FE,GS-ASE-EDS,
ea571302-0a0a-8c0a-01da-4f7d5351947b,EDMS Support and Consultancy,1,FE,EN-IM-PLM,cscoero
1bbd2e75-3875-f000-6d21-90440d25b39a,Education fees,1,FE,HR-CBS-B,nbordon
,,,,,ygrange
c1209a73-dd1d-55c0-7904-7721dbdf3b5a,Eduroam,1,FE,IT-CS,asosnows
,,,,,dgutier
de2ccb0e-24f3-4fe4-8257-abb0715fd1da,EEE,1,Experiment,None,
c9efd5af-5c1e-438e-8bc2-0f29cd1a0b24,EELA,0,Project,None,
1c254548-fdff-7d40-6d21-5d33119bb31e,eFiles,1,FE,FAP-BC-QS,dtsekour
,,,,,materrap
,,,,,mcmarque
1312a632-dbc1-3740-a1cd-a4f84b961994,"eFiles Accounting, Treasury and Payments",1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
aad22676-dbc1-3740-a1cd-a4f84b961973,eFiles Benefits and Talent Management,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
5cf2ea76-dbc1-3740-a1cd-a4f84b96198c,eFiles Core HR and Talent Acquisition,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
6042ee32-dbc1-3740-a1cd-a4f84b9619c5,eFiles Planning and Control,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
088226f2-dbc1-3740-a1cd-a4f84b961998,eFiles Procurement and Jobs,0,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
da52e272-dbc1-3740-a1cd-a4f84b9619e4,eFiles Site Management,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
28a22a36-dbc1-3740-a1cd-a4f84b96195a,eFiles Supply Chain and Logistics,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
419fdc29-15ab-7d00-c713-566b001b843d,eFilesOLD,0,FE,GS-AIS-EB,
8343552d-1d5e-48ea-a8a7-5a69f896f0d0,EGEE,1,Experiment,None,
b9a713bd-2423-f000-7904-c39b27667a88,Einux vendor,0,FE,IT-CF-SAO,
ac309233-b022-4f66-a414-e1d98440999c,EISA,1,Experiment,None,
0b910728-4fa7-f240-b8cb-ca1f0310c7d6,EL Coordination,1,FE,EN-EL,evanuytv
,,,,,nbellega
ddffb4f6-4f30-a600-87a2-f5601310c710,EL DC Cabling for magnets,0,FE,EN-EL-FC,evanuytv
,,,,,guillauj
dc472076-4fbc-6600-87a2-f5601310c717,EL Fiber Optics Coord,1,FE,EN-EL-FC,evanuytv
,,,,,jeblanc
,,,,,meroli
,,,,,smachado
6bb5c56b-4f6e-3e04-7db7-d3ef0310c75d,EL Fiber Optics LHC-SM18,1,FE,EN-EL-FC,evanuytv
,,,,,jeblanc
,,,,,smachado
c4664dab-4f6e-3e04-7db7-d3ef0310c760,EL Fiber Optics LHCEXP,1,FE,EN-EL-FC,evanuytv
,,,,,jeblanc
,,,,,meroli
,,,,,smachado
b6f5c96b-4f6e-3e04-7db7-d3ef0310c7d4,EL Fiber Optics PS-MEY,1,FE,EN-EL-FC,evanuytv
,,,,,jeblanc
,,,,,meroli
,,,,,smachado
e026cd6b-4f6e-3e04-7db7-d3ef0310c763,EL Fiber Optics SPS-PREV,1,FE,EN-EL-FC,evanuytv
,,,,,jeblanc
,,,,,meroli
,,,,,smachado
cebbcee3-dbe1-6f40-f995-6184da96199f,EL HV Cabling Accessories,1,FE,EN-EL-ENP,evanuytv
824b8b58-5d2e-f504-6d21-1f86c6a31e49,EL Low Voltage Network (Internal Use Only),1,FE,EN-EL,bernard
,,,,,dbozzini
,,,,,evanuytv
,,,,,meroli
73e78e15-4f41-6e00-87a2-f5601310c70e,EL Material Procurement,0,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,jeblanc
,,,,,smachado
fd4d78c5-4fcd-ea00-87a2-f5601310c790,EL Operation,1,FE,EN-EL-EWS,bernard
,,,,,evanuytv
,,,,,tcharvet
624f3089-4fcd-ea00-87a2-f5601310c758,EL Quick Works (TPA),1,FE,EN-EL-EWS,bernard
,,,,,dbozzini
,,,,,evanuytv
,,,,,meroli
,,,,,tcharvet
c3d86371-4f56-e600-b8cb-ca1f0310c7e6,EL Safety Inspections (non tertiary),1,FE,EN-EL-EWS,bernard
,,,,,evanuytv
,,,,,tcharvet
1e2ce7ee-4f7a-2200-b8cb-ca1f0310c770,EL Signal Coord,1,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,meroli
,,,,,smachado
46bcb8f2-4f30-a600-87a2-f5601310c732,EL Signal LHC-SM18-EXP,1,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,meroli
,,,,,smachado
98effa9f-4fbe-3200-fe13-2eff0310c73a,EL Signal PRE,1,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,meroli
,,,,,smachado
f30b7872-4f30-a600-87a2-f5601310c78c,EL Signal PS-TL,1,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,meroli
,,,,,smachado
338f7a9f-4fbe-3200-fe13-2eff0310c703,EL Signal PSB-LI4,1,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,meroli
,,,,,smachado
691003df-4fbe-3200-fe13-2eff0310c785,EL Signal PSEXTRACT,1,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,meroli
,,,,,smachado
d0ebb8b2-4f30-a600-87a2-f5601310c73f,EL Signal SPS,1,FE,EN-EL-FC,evanuytv
,,,,,guillauj
,,,,,meroli
,,,,,smachado
0cfdbcc5-4fcd-ea00-87a2-f5601310c755,EL Works LHC,1,FE,EN-EL-EWS,bernard
,,,,,dbozzini
,,,,,evanuytv
,,,,,meroli
,,,,,tcharvet
b2992b2e-4f7a-2200-b8cb-ca1f0310c79a,EL Works LHC Experiments,1,FE,EN-EL-EWS,bernard
,,,,,dbozzini
,,,,,evanuytv
,,,,,meroli
,,,,,tcharvet
6bbdb009-4fcd-ea00-87a2-f5601310c7f4,EL Works PS,1,FE,EN-EL-EWS,bernard
,,,,,dbozzini
,,,,,evanuytv
,,,,,meroli
,,,,,tcharvet
1fee7849-4fcd-ea00-87a2-f5601310c7df,EL Works SPS,1,FE,EN-EL-EWS,bernard
,,,,,dbozzini
,,,,,evanuytv
,,,,,meroli
,,,,,tcharvet
fe4eebd9-4f74-da80-4b4a-bc511310c782,Elasticsearch,1,FE,IT-CM-LCS,manuel
,,,,,psaiz
,,,,,schwicke
e8918815-45e1-8100-6d21-14ec766365b0,Electrical Engineering,0,FE,EN-EL,frduval
,,,,,tcharvet
48892dd6-9491-6100-7904-7129f337bf98,Electrical engineering design and studies for tertiary installations,1,FE,SCE-SAM-IN,martelc
2ec0596f-0a0a-8c08-00e1-73148d8c2871,Electrical Network Supervision,0,FE,EN-EL-CO,evanuytv
,,,,,sinfante
ea56ee84-0a0a-8c0a-016a-9fd81c104d69,Electricity,0,FE,SMB-SE-HE,martelc
ea56eda3-0a0a-8c0a-0033-fe95b37cd88c,Electricity - CC Tech Indus,0,FE,SMB-SE-HE,martelc
fc997096-111d-e500-adf9-ea91634bc64e,Electricity for tertiary buildings,1,FE,SCE-SAM-IN,martelc
ffb93c96-111d-e500-adf9-ea91634bc644,Electricity works,1,FE,SCE-SAM-IN,fvincent
,,,,,martelc
ea56e9b9-0a0a-8c0a-0069-b86435ecf1c6,Electronic Bulletins,0,FE,IT-CDA-DR,
5083e211-4fb7-9600-b9cc-09de0310c743,Electronic Document Conversion,1,FE,IT-CDA-IC,rgaspar
,,,,,tbaron
ea5714e3-0a0a-8c0a-01b8-c802c12dd54a,Electronic Locks,0,FE,BE-ICS-AC,fchapron
,,,,,rnunes
ea57010d-0a0a-8c0a-01be-ff6371cb765d,Electronics Design Automation tools,1,FE,IT-CDA-AD,ifans
,,,,,mkwiatek
0331e5d0-1a92-452c-9e67-c9d6cd1d4eba,ELENA,1,Experiment,None,
ea56f695-0a0a-8c0a-0054-cd5c59731b61,ELF ms,0,FE,IT-PES-PS,
ea56fd10-0a0a-8c0a-00a3-30a0123e3c58,Elog Operations,0,FE,IT-CM-MM,
aa971f7d-2423-f000-7904-c39b27667a14,Elonex vendor,0,FE,IT-CF-SAO,
ea571315-0a0a-8c0a-012b-88356758ebf7,Emergency Evacuation Systems,1,FE,EN-AA-AS,fchapron
,,,,,grau
,,,,,ninin
ecb8b3be-4fc2-d780-b9cc-09de0310c723,Emergency Preparedness,1,FE,HSE-OHS,aarnalic
,,,,,delamare
,,,,,fchapron
b10fbcd6-d9b1-49a3-ac55-562f59ce7b90,EMI,1,Experiment,None,
95a01fcb-0a0a-8c0a-01de-5669141e8069,EMI QA,0,FE,IT-GT-SL,
ea56fd67-0a0a-8c0a-01d8-24c7e8a1b8b8,EMI Testbeds,0,FE,IT-GT-SL,
ad4a9ea4-d615-4d5c-9759-eafd75836260,EMU01,1,Experiment,None,
56133cff-6a36-43e7-ab3c-92cd4b806d01,EMU02,1,Experiment,None,
283093ca-2dfb-4067-b3be-3e8ff44b1ecf,EMU03,1,Experiment,None,
1f15742f-6583-4bb6-9d53-d137313c0ba9,EMU04,1,Experiment,None,
a7b9442a-1e89-4b4e-a979-12058ae5b54f,EMU05,1,Experiment,None,
52cf2aa1-4cf8-4976-a8fa-4604d44a8a28,EMU06,1,Experiment,None,
d6685253-4a85-405b-9197-746065852ebe,EMU07,1,Experiment,None,
1a3ec29c-5d8e-41ae-9751-26d0d394e007,EMU08,1,Experiment,None,
9902c0d7-2a0c-4db9-8606-ad3e21e3b3d5,EMU09,1,Experiment,None,
42b18b33-2f91-4e8f-b110-e130d8360ab8,EMU10,1,Experiment,None,
c95f005f-8f6c-4f75-be0d-18df3f37bb83,EMU11,1,Experiment,None,
9605d93a-aa66-4082-847d-be1393f63e2e,EMU12,1,Experiment,None,
d3611e5c-1eb3-4712-88c8-feade1f320a5,EMU13,1,Experiment,None,
e06aa683-add5-4a20-a3d5-d38a329ff506,EMU14,1,Experiment,None,
26d5b2e7-b76f-4d79-8726-3720ffadac19,EMU15,1,Experiment,None,
13b34c12-2a94-4aab-9b35-586ca31a5109,EMU16,1,Experiment,None,
4a83eaba-c050-4490-bf3f-794c1c779842,EMU17,1,Experiment,None,
2f11451a-f8eb-4995-b25a-24153602382f,EMU18,1,Experiment,None,
a388954a-5d0e-467c-86d7-4b7676d15bb8,EMU19,1,Experiment,None,
2cff46f2-f202-471d-be0a-7b5f5afdaf20,EMU20,1,Experiment,None,
5adb499e-8adc-4e15-9a81-acc0574788e8,EN,1,Department,EN,
2a3d6ebe-21cc-1980-adf9-b5210b89fc25,EN - Department Head Office,0,FE,EN-ARP,prodons
308c2ab2-2100-5980-adf9-b5210b89fc8c,EN - Departmental Administrative Office (DAO),0,FE,EN-ARP,prodons
e01110cf-2140-9980-adf9-b5210b89fcc1,EN - Departmental Key Manager,0,FE,EN-ARP-DPO,escaffre
552c6e72-2100-5980-adf9-b5210b89fc09,EN - Departmental Planning Office (DPO),0,FE,EN-ARP,prodons
4bec62f2-2100-5980-adf9-b5210b89fc8b,EN - Group Secretariats,0,FE,EN-ARP,prodons
3bc95612-9836-7840-6d21-708547f4c66a,EN Desktop Support,0,FE,EN-ARP,bonnal
,,,,,draper
f59ffdc5-fc3b-4900-7904-2318fd702e05,EN Workshops,1,FE,EN-PAS,bonnal
,,,,,olafsen
0a47d208-db91-6384-f995-6184da9619c6,EN-ACE Project Online,1,FE,EN-ACE,aansel
,,,,,fpedrosa
,,,,,jurgen
,,,,,mabernar
b2714fc6-64ac-b900-6d21-f8e2a49c38e4,EN-CV-DOW Projects management,1,FE,EN-CV-DOW,sdeleval
97b97376-4f5c-1740-06bc-10ee0310c714,EN-Engineering safety events,1,FE,EN-HDO,losito
a2e66278-dbb0-fb04-0261-75db8c9619a4,Engineering License Server,1,FE,IT-CDA-AD,mkwiatek
,,,,,pmartinz
6ac3605f-4868-0980-c713-cac2550e66b4,English Translation Team,0,FE,DG-TMC-TM,jpym
6db46d40-0483-5d44-adf9-793734b40e73,ENS Support,0,FE,BE-ICS,bcopy
,,,,,fchapron
,,,,,solp
ea56ecc7-0a0a-8c0a-00d5-5bfe380db074,Entrance Control and Guards,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
9feb962e-94a5-6140-7904-7129f337bf7d,Entrance Information Panels Content,1,FE,SCE-SMS,dconstan
,,,,,hmontane
ea571236-0a0a-8c0a-01ef-0cd6492146b0,Entrances Information Panels Maintenance,0,FE,BE-ICS-AC,fchapron
,,,,,ninin
,,,,,rnunes
4a602b77-c45f-4fd7-a47b-14c815d88176,ENUBET,1,Experiment,None,
e9010e94-a119-4ad4-bdbf-c37a7132d4b0,ENVIROGRID,0,Project,None,
4ebfbaf1-a4fa-3900-adf9-30a205b50ad2,Environment,1,FE,HSE-ENV-EM,delamare
,,,,,fmalacri
0e0a1cae-db87-4810-f4c8-949adb9619b3,EOS machines for experiments,1,FE,IT-ST-PDS,acontesc
,,,,,lmascett
,,,,,marsuaga
a4e93fd3-ffc8-64c0-9878-114e40b9adc5,EOS Physics Support,1,FE,IT-ST-PDS,acontesc
,,,,,lmascett
,,,,,marsuaga
4b55846a-db8f-0810-f4c8-949adb96192e,EOS user/project/home/media,1,FE,IT-ST-GSS,gonzalhu
,,,,,moscicki
,,,,,rvalverd
152e0840-0a0a-8c0a-01f2-e7806d53b9d1,EOSALICE,0,FE,IT-DSS-FDO,
7d270da0-49e7-46b0-9d20-03ce14d9e6aa,EOSC-hub,1,Project,None,
15350a24-0a0a-8c0a-0034-48c88ed2aad1,EOSCMS,0,FE,IT-DSS-FDO,
9f60e67b-3186-4081-a3b3-2ef0bee57918,EOSCsecretariat.eu,1,Project,None,
184b875d-dba2-7f80-8f2f-177e169619c9,EOSCTA,1,FE,IT-ST-TAB,jleduc
1e154876-9434-3840-6d21-20312bb3b632,EOSLHCB,0,FE,IT-DSS-FDO,
80f6041f-e93d-4aef-a0b4-b74027ffdb7e,EP,1,Department,EP,
3b2e0087-db4f-5340-ce6e-9c09db9619df,EP - Departmental Administrative Office (DAO),0,FE,EP-AGS,cdecosse
df954b14-db29-b740-f995-6184da961961,EP and RCS Data Privacy Protection Coordinator (DDPPC),1,FE,EP-AGS,bbordjah
b9d4dfae-4f2b-5340-87a2-f5601310c772,EP and TH - Departmental Data Privacy Protection Coordinator (DDPPC),0,FE,EP-AGS,cdecosse
9c21c507-4f47-df40-ce1b-401f0310c726,EP and TH Departments and RCS Sector - Departmental Planning Office (DPO),0,FE,EP-AGS,cdecosse
917ab407-4f07-df40-ce1b-401f0310c748,EP and TH Safety Office,0,FE,EP-DI-SO,obeltram
1a9ebcd8-0443-9d44-adf9-793734b40e5c,EP Car Fleet Coordination,0,FE,EP-AGS-SI,cmontagn
b6bcebe8-db34-2380-eb72-9085db96199d,EP Car Fleet Management,0,FE,EP-AGS-SI,bbordjah
,,,,,cmontagn
e6b520cb-db03-9340-ce6e-9c09db96197f,EP Central Secretariat,0,FE,EP-AGS-SE,rabier
4e5a59cb-4faf-9740-7db7-d3ef0310c7a7,EP Copier Machines Rental Coordinator,0,FE,EP-AGS-SI,bbordjah
,,,,,cmassip
c41f404b-db4f-5340-ce6e-9c09db96197d,EP Department Head Office,0,FE,EP-DI,krammer
6a218205-4fa7-5b00-7db7-d3ef0310c720,EP Departmental Records Officer (DRO),0,FE,EP-AGS,cdecosse
6ab18e45-4fa7-5b00-7db7-d3ef0310c702,EP Departmental Training Officer (DTO),0,FE,EP-AGS-SE,nknoors
ccdc824c-db5d-af04-7c55-9eb3db96199e,EP Fixed Line Phone Management,0,FE,EP-AGS-SI,bbordjah
,,,,,cmassip
42a7d507-4faf-9740-7db7-d3ef0310c7c0,EP Internal Transport/Removals,0,FE,EP-AGS-SI,
af33a0fc-4fa7-9700-7db7-d3ef0310c7e2,EP Inventory Office,0,FE,EP-AGS-SI,bbordjah
,,,,,cmontagn
fae8510b-4faf-9740-7db7-d3ef0310c799,EP Locks and Keys Departmental Correspondent,0,FE,EP-AGS-SI,bbordjah
,,,,,cmassip
15a2a8b8-4fa7-9700-7db7-d3ef0310c7bd,EP Logistics Office,0,FE,EP-AGS-SI,
8ca24ec5-4fa7-5b00-7db7-d3ef0310c776,EP MERIT Coordinator (MC),0,FE,EP-AGS-SE,nknoors
ce6b518f-4faf-9740-7db7-d3ef0310c7e9,EP Shipping Requests Coordinator,0,FE,EP-AGS-SI,bbordjah
,,,,,cmontagn
3832450b-4f47-df40-ce1b-401f0310c75a,EP Space Management Office,0,FE,EP-AGS-SI,bbordjah
b1335065-b06a-4fb5-b75a-7ac2e88e461f,ESCAPE,1,Project,None,
4b43782f-5c06-4c05-92aa-bcc7dc919236,ESI,1,Experiment,None,
ea56fd80-0a0a-8c0a-013d-34647b0a01de,ETICS,0,FE,IT-SDC-ID,
ea56fade-0a0a-8c0a-0121-106dca08166e,EU Projects,0,FE,IT-DI-EGE,
fd53b3d1-408a-4893-b9ed-c93cf03a8c50,Euclid,1,Experiment,None,
6232c19d-f87d-4900-adf9-02fd20678c63,EUDAT Participation,0,FE,IT-SDC-ID,
dd9e63f0-4146-42d9-a034-6cac2d9c8136,EUDET,1,Experiment,None,
5501893a-0179-a0c0-9878-151822750d3e,Evaluation and Consultancy on Third Parties Messaging Software,0,FE,IT-CM-MM,
ea570958-0a0a-8c0a-0185-b5aa2f5ab598,EVM (integrated in APT),1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea57158a-0a0a-8c0a-01c1-3f3e56369e5b,Evolynx SUSI Operation,0,FE,GS-ASE-AC,rnunes
ea56ecda-0a0a-8c0a-00e8-15f2371a5276,Exhibitions at CERN,0,FE,SMB-SIS,lalejeun
,,,,,roussiez
,,,,,vmarchal
ea56fbaa-0a0a-8c0a-0139-cdc1e532c585,Experiment Dashboards,0,FE,IT-SDC-MI,
ea56f958-0a0a-8c0a-01ca-5787fa651c5e,Experiment Database Instances,0,FE,IT-DB-DBR,
ea570f6d-0a0a-8c0a-0186-c92a7b0b880e,Experiments and Institutes,0,FE,GS-AIS-GDI,jjanke
ea56fbbe-0a0a-8c0a-00f8-5aa937828e1f,Experiments Development Support,0,FE,IT-DI-LCG,
cde97eba-c9c3-5900-adf9-4df2054ec562,Experiments Liaison,1,FE,IT-SC,andreeva
9ceaae60-eeb2-4ec6-84b3-6cb48b1331ba,EXPLORER,1,Experiment,None,
65e1f452-11d9-e500-adf9-ea91634bc6b0,Exterior building doors and windows maintenance,0,FE,SMB-SE-CEB,fmagnin
ce20b05e-1199-e500-adf9-ea91634bc607,Exterior building doors and windows works,0,FE,SMB-SE-CEB,fmagnin
63327caa-1b98-5410-c917-65b0ab4bcbd1,External Administrative Data Exposure,0,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
07d36080-0a0a-8c08-00b8-265c96cbc741,External Bus Rental - Europtours,0,FE,SMB-SIS,gbolling
7a24d452-4f8e-1380-b9cc-09de0310c7f5,External Car Rental - HERTZ,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
ddbb485c-4fbf-6200-7db7-d3ef0310c757,External Car Rental - SIXT,0,FE,SMB-SIS,gbolling
ea56f7ec-0a0a-8c0a-003a-613fa2670989,External Connectivity,1,FE,IT-CS,dgutier
,,,,,emartell
0ac7dfbd-2423-f000-7904-c39b27667abd,External vendor,0,FE,IT-CF-SAO,
c22d28ce-4fb8-1b40-b9cc-09de0310c759,External Visitors Kiosk application,1,FE,EN-AA-AC,ninin
,,,,,rnunes
699dd3e8-4f0b-8300-77fe-29001310c794,Fa�ade and Exterior Doors,0,FE,SMB-SE-CBS,
f7817812-11d9-e500-adf9-ea91634bc634,Fa�ade maintenance,0,FE,SMB-SE-CEB,fmagnin
ec473816-0a0a-8c0a-00c7-47493c848d90,Fa�ade Refurbishment,0,FE,SMB-SE-CEB,fmagnin
,,,,,srew
c2b0749e-1199-e500-adf9-ea91634bc674,Facade works,0,FE,SMB-SE-CEB,
5528339a-1b29-1410-c917-65b0ab4bcbe2,"Facade, windows and blinds",1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
ea56f6e1-0a0a-8c0a-01f1-8cded09e9109,Facilities Operation,1,FE,IT-CF,barring
,,,,,grossir
,,,,,mdupuis
,,,,,salter
a35d9bf5-383d-f000-6d21-90440d25b3fa,Family allowances,1,FE,HR-CBS-B,ygrange
fae86289-8ec0-4cf6-9e54-b94626c6621d,FAP,1,Department,FAP,
3fa83d2c-4f5a-5e00-87a2-f5601310c7f9,FAP Departmental Administrative Office (DAO),1,FE,FAP-DHO,claignel
34bb844c-1b05-6090-2a4f-a8afab4bcb04,FAP Departmental Key Manager (DKM),1,FE,FAP-DHO,claignel
,,,,,pperera
9f100afd-1bfb-a010-4317-960cab4bcb07,FAP Departmental Planning Office (DPO),1,FE,FAP-RPC,arozycka
,,,,,pokorska
f9eb484c-1b05-6090-2a4f-a8afab4bcbc8,FAP Departmental Space Manager (DSM),1,FE,FAP-DHO,claignel
,,,,,pperera
ff8b084c-1b05-6090-2a4f-a8afab4bcb36,FAP Group Administrative Office (GAO),1,FE,FAP-DHO,claignel
16cb884c-1b05-6090-2a4f-a8afab4bcbcf,FAP Website,1,FE,FAP-DHO,claignel
adb04d62-d24b-4d29-b71d-8cdc40803c83,FASER,1,Experiment,None,
a7421364-01f6-e884-9878-151822750d1f,FB-Drills,1,FE,HSE-FRS,delamare
,,,,,lmadridm
ea571729-0a0a-8c0a-009f-af84abac294b,"FB-Lost, found, theft and conflict",0,FE,GS-FB,deroma
ea571676-0a0a-8c0a-0049-4cacb2e0b778,FB-Maintenance,1,FE,HSE-FRS,delamare
,,,,,lmadridm
,,,,,pberling
ea571691-0a0a-8c0a-00dc-68ed72247716,FB-Operations,1,FE,HSE-FRS,delamare
,,,,,lmadridm
823e4728-01b6-e884-9878-151822750d5e,FB-Radiation Protection (RP),1,FE,HSE-FRS,delamare
,,,,,lmadridm
ea5716b5-0a0a-8c0a-01f5-6f34b27985a2,FB-Safety Consultancy,0,FE,GS-FB,deroma
ea571714-0a0a-8c0a-00c5-b8f523e0a152,FB-Safety Control Room,0,FE,GS-FB,deroma
aa41dfa0-01f6-e884-9878-151822750d4f,FB-VIP Support,1,FE,HSE-FRS,delamare
,,,,,lmadridm
09b7fcf2-4188-4060-b394-cb0dd17cef81,FCC,1,Experiment,None,
af58f1d1-f52d-0a00-6d21-714550182e40,FCC collaboration web site support,0,FE,DG-DI,gutleber
ebf1196f-8cd8-483b-b3a0-71e7b2d9ab4e,FELIX,1,Experiment,None,
ea56f4e9-0a0a-8c0a-01a6-e9913d70819b,Fellows,1,FE,HR-TA-SGR,gballet
,,,,,kthomas
ea299b28-11bd-d900-7904-a06fd6378cd9,Fence repairs,0,FE,SMB-SE-CEB,fmagnin
e603a4e1-a068-0200-6d21-ed5d48900415,Fence works,0,FE,SMB-SE-CEB,fmagnin
56ad8a95-f7ae-47cb-9b72-1cc0e99abaa3,FERMI,1,Experiment,None,
1a8bf8dc-1b93-a010-c1d8-fe62cb4bcbe7,FESA Framework,1,FE,BE-CSS-FST,sdeghaye
5e713199-dbaa-5f84-1aab-9247db9619a5,FESA support team,0,FE,BE-CO,hatzi
d4a94b60-4f84-ae00-b9cc-09de0310c74b,Fibre Optics ProcurementDEMO2016,0,FE,EN-EL-FC,jeblanc
,,,,,smachado
b7b7fb21-4fa2-ce00-93ec-df601310c7fa,Fibre OpticsDEMO2015,0,FE,EN-EL-CF,
1246bbad-4f62-ce00-93ec-df601310c7ba,Fibre Optique Installation demo2015,0,FE,EN-EL-CF,evanuytv
c0367a6b-4fb7-1e00-b9cc-09de0310c78e,FieldBus Installation,1,FE,BE-CEM-IN,ikozsar
,,,,,veym
1ad04a3c-4f7b-7a80-b9cc-09de0310c73c,Fieldbus technologies,1,FE,BE-ICS-CE,eblanco
,,,,,fchapron
,,,,,solp
49a91e4c-6097-b880-c713-e946666f3f11,Filer,1,FE,IT-ST-GSS,abchan
,,,,,laman
ea56eaf8-0a0a-8c0a-01af-9b2a1b6195e5,Film Recording,0,FE,DG-CO,cmenard
,,,,,jfichet
ea570740-0a0a-8c0a-0116-c0c305bd5980,Financial support to Institutions and CollaborationsCollaboration Accounting,0,FE,FAP-EF-TC,kgachet
a689aa1b-4f55-4b40-b316-c0501310c780,Fire detection system engineering,1,FE,EN-AA-CSE,fchapron
,,,,,ninin
d409bfbe-4fc2-d780-b9cc-09de0310c7e0,Fire Safety Engineering expertise,1,FE,HSE-OHS-IB,aarnalic
,,,,,delamare
,,,,,fchapron
62294fdf-f519-d804-9878-e86b196c5c50,Firms,0,FE,FAP-BC,wvanleer
ea571746-0a0a-8c0a-0141-fef802f6c02b,First-Aid Training,0,FE,GS-ME,
ea56f800-0a0a-8c0a-0137-4a720e38ec82,Fixed Telephony,1,FE,IT-CS,araczyns
,,,,,gcancio
,,,,,rsierra
e1f56f5d-db0e-77c0-ae65-9ec4db96197a,Fixed Telephony Clients,1,FE,IT-CDA-IC,fernanre
,,,,,gcancio
,,,,,tbaron
9c907ee4-db56-0818-006f-d9f9f49619a5,Flammable and Toxic Gas Safety,1,FE,HSE-OHS,delamare
,,,,,fchapron
,,,,,grau
,,,,,jgulley
0ff0c040-0a0a-8c0a-0108-6ef805fb7625,Floor maps,1,FE,SCE-SAM-TG,mpoehler
,,,,,yrobert
54f61070-db30-37c0-269d-9e24db961997,Food Truck,1,FE,SCE-SSC-CS,lalejeun
,,,,,vmarchal
72c713fd-2423-f000-7904-c39b27667a6a,Force10 vendor,0,FE,IT-CF-SAO,
cdd757fd-2423-f000-7904-c39b27667a7e,Force_10 vendor,0,FE,IT-CF-SAO,
665d3be4-dbf5-5450-b805-94dadb9619bd,Forklift maintenance,1,FE,EN-HE-HEM,dlafarge
ea570ea8-0a0a-8c0a-0164-07284f36c8ef,Foundation Data,0,FE,GS-AIS-GDI,
4e555cf1-ff1c-6000-9878-114e40b9addd,Foundation OHR Synchronization,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
ea570d33-0a0a-8c0a-012f-0cc3d9618666,Foundation Other Applications,0,FE,GS-AIS-GDI,
8836d4ae-4fc0-1240-4b4a-bc511310c726,Foyer Schuman,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
,,,,,vmarchal
ea570621-0a0a-8c0a-01a8-92375243b659,FP Computing Support,0,FE,FAP-TPR-TP,cspencer
,,,,,gthiede
ea57060d-0a0a-8c0a-0170-16694eddc242,FP Strategy,0,FE,IPT-DI,lagrange
6cb4ac9f-4868-0980-c713-cac2550e66bd,French Translation Team,0,FE,DG-TMC-TM,jpym
ea570371-0a0a-8c0a-0076-734a2bcd1da3,fts,1,FE,IT-ST-PDS,lmascett
ea56fe71-0a0a-8c0a-0152-4db868b3e23c,FTS Development,1,FE,IT-ST-PDS,lmascett
ed1f32a4-4f34-13c0-06bc-10ee0310c7b5,Fuel supplier CH,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
d2be3624-4f34-13c0-06bc-10ee0310c7e9,Fuel supplier FR,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
bf212fa0-4f4b-8300-77fe-29001310c7f4,Furniture / Mobilier,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
ce6d19d2-9451-6100-7904-7129f337bff4,Future major machine projects,1,FE,SCE-DOD-FS,josborne
a6a25f21-f027-9dc0-7904-6a0c9f0e01c9,GAC-EPA Commitee,0,FE,HR,cregelbr
ea570bc9-0a0a-8c0a-0115-d8cc7d661f33,GAD,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
014cc277-db71-ff04-7c55-9eb3db9619d0,Gas support and provider,1,FE,BE-EA-AS,gcanale
10c1ca91-f831-c900-adf9-02fd20678c19,Gate vendor,0,FE,IT-CF-SAO,
dc7dbfb2-bb3f-4693-8b98-0f53bbd20293,GBAR,1,Experiment,None,
bf670c37-f4af-49f2-a40a-d5d5bb7a52de,gear,0,Project,None,
ea570651-0a0a-8c0a-00d8-f748589a5b0d,General Accounting,1,FE,FAP-ACC-GA,jfalcon
ea57068c-0a0a-8c0a-0162-dd70c7438ed5,General Payment Control,0,FE,FAP-ACC-GA,cponcet
ea571855-0a0a-8c0a-01ca-f3bebab3f299,Geneve Espace Vert,0,FE,GS-IS-SIS,vmarchal
ea570d5e-0a0a-8c0a-01b7-25b8a286ae4a,Gescle,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea570cba-0a0a-8c0a-005b-05b3ef421268,Gesloc,0,FE,GS-AIS-GDI,jjanke
b7d0737f-48df-4210-895a-5f6d55e87113,GIF,1,Experiment,None,
41f569e3-71ae-440e-9962-a4efdead985c,GIN,0,Project,None,
2674d664-0a0a-8c08-0065-bf7f4798c772,GIS Portals,1,FE,SCE-SAM-TG,yrobert
ba81206f-941d-f404-6d21-20312bb3b6c4,GIT,0,FE,IT-PES-IS,
959c82a7-0a0a-8c0a-0163-7d3440df2cc1,gLite Release Pages and Repository,0,FE,IT-GT-SL,
959eb8c9-0a0a-8c0a-01c3-9c38847c475f,gLite UI,0,FE,IT-GT-SL,
959fa5e6-0a0a-8c0a-0066-c64375d193d1,gLite WN,0,FE,IT-GT-SL,
ea5718a3-0a0a-8c0a-01fb-5dfbaa6fbbd2,Goods Internal Distribution,1,FE,SCE-SSC-LS,pmuffat
ea571875-0a0a-8c0a-01b5-58f98e5922e2,Goods Reception,1,FE,SCE-SSC-LS,pmuffat
f502a8a6-db58-d410-1de7-3e482296197b,GPU Platform Consultancy,1,FE,IT-CM,mccance
,,,,,timbell
,,,,,vaneldik
38fc6a8c-e6de-438b-b705-7d878f439a5b,GR02,1,Experiment,None,
23e50c06-88e8-4006-a908-6c090fa0ef71,GR03,1,Experiment,None,
bf082204-7ba0-4339-b571-07931df73fc9,GR04,1,Experiment,None,
7b417cfc-01fe-4369-9048-1ca83679727c,GR1,1,Experiment,None,
ea5719af-0a0a-8c0a-0109-18856d8d6f3e,Green Space management,0,FE,SMB-SE-CEB,fmagnin
7a95b49a-11d9-e500-adf9-ea91634bc6b2,Green spaces maintenance,0,FE,SMB-SE-CEB,fmagnin
dae4bc1a-11d9-e500-adf9-ea91634bc694,Green spaces works,0,FE,SMB-SE-CEB,fmagnin
69ae536c-4f0b-8300-77fe-29001310c7d4,"Green Spaces, Fence and Surroundings",1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
bf9cc1f4-db0b-5700-ce6e-9c09db961901,Grey Book Secretariat,1,FE,EP-AGS-UO,gdarvey
ea570cf8-0a0a-8c0a-01c7-21f77fa4291f,Greybook Technical support,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
ea56fe15-0a0a-8c0a-01f6-aed601d24768,GRID monitoring,0,FE,IT-CM-MM,
ea56fe2b-0a0a-8c0a-0031-528068c4a5b4,GridMap,0,FE,IT-GT-TOM,
ea56fe40-0a0a-8c0a-018a-10a214387977,GridView,0,FE,IT-GT-TOM,
ea56fd27-0a0a-8c0a-0041-da816b16d967,GStat,0,FE,IT-GT-TOM,
33b7dfdb-4f1c-2200-87a2-f5601310c752,Guards Knowledge Management,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
b8196881-4fd9-5b40-d7b5-0ebf0310c734,Guided Tour Booking,1,FE,IR-ECO-VI,briard
657872b1-c49a-45e7-8e8c-3c1307ccbf6c,H1,1,Experiment,None,
07510b0e-607b-5904-adf9-de23891aee84,H323 Videoconference,0,FE,IT-CDA-IC,
69f03ac5-25ac-4712-ae61-1d7669e0c523,H8-RD22,1,Experiment,None,
1750f43d-34ce-4140-7904-fc95882b1573,Hadoop,0,FE,IT-ST,
f61891a0-fd2f-7140-6d21-5d33119bb3e1,Hadoop and Spark,1,FE,IT-DB-SAS,awiecek
,,,,,edafonte
,,,,,grancher
,,,,,jlunadur
ea56fbfd-0a0a-8c0a-00dd-08a36bc68984,HammerCloud,0,FE,IT-SDC-OL,
ea571638-0a0a-8c0a-006f-579dac8b7048,Hardware Alarm Integration,1,FE,EN-AA-AS,fchapron
,,,,,grau
,,,,,ninin
d4d9d810-4f48-d600-93ec-df601310c7fb,Hardware-Labs,0,FE,IT-CF-FPP,luatzori
a3b79bbd-2423-f000-7904-c39b27667ad2,Hardware/vendors/hpmib vendor,0,FE,IT-CF-SAO,
a7df103f-6a3e-4af9-bd69-f88c0f15f255,HERD,1,Experiment,None,
1e3c8f80-abc0-492a-9973-26dcf61915c8,HIOS,1,Project,None,
e2500c7d-d6e2-462c-b99f-27dc349f1c9c,HiRadMat,1,Experiment,None,
ea5709b2-0a0a-8c0a-00fd-0e96095c2c17,HireServe ICAMS Support Contract,0,FE,GS-AIS-HR,
20e33b06-4fd5-9a04-4b4a-bc511310c744,Hotel Booking for Group,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
,,,,,vmarchal
ea56ed5c-0a0a-8c0a-01f8-9a98ea23480c,Hotel Booking Simple,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
,,,,,vmarchal
009e124b-4fee-5240-aa3f-10ee0310c735,Hotel Cleaning,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
e1de520e-4f42-d240-4b4a-bc511310c745,Hotel Internal Issues,0,FE,SMB-SIS,pcorreia
,,,,,vmarchal
ea56ee16-0a0a-8c0a-00e9-d7c6fe920fe1,Hotel Lock system,0,FE,GS-IS-SIS,
ea56ed70-0a0a-8c0a-0124-2aa5cd74db29,Hotel Management,0,FE,SMB-SIS,pcorreia
fc6e5e0b-4fee-5240-aa3f-10ee0310c78b,Hotel Reception,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
,,,,,vmarchal
ea57195b-0a0a-8c0a-00dc-3f3796a83877,Hotel Software (Fidelio),1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
f4941e02-4fdc-5a40-93ec-df601310c726,Hotel Survey Management,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
,,,,,vmarchal
ea570e7d-0a0a-8c0a-0028-84275b0b09c1,Hotel Web Interface,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
c687177d-2423-f000-7904-c39b27667af3,Hp vendor,0,FE,IT-CF-SAO,
201c29f6-4c0d-0280-c713-26a7289711ec,HPC Engineering Applications,1,FE,IT-CDA-AD,lgunther
,,,,,mkwiatek
cb971f7d-2423-f000-7904-c39b27667a98,Hpmib vendor,0,FE,IT-CF-SAO,
6c313e6d-0ef1-4f15-98e4-cb1aa5519af3,HR,1,Department,HR,
e077b8d1-4ff0-b6c0-87a2-f5601310c787,HR - Departmental Planning Office (DPO),1,FE,HR-DHO,ppessy
ea570bfa-0a0a-8c0a-0126-4f186952c21f,HR Access,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea57102f-0a0a-8c0a-00bf-9dcba6ccf728,HR Access Support Contract,0,FE,GS-AIS-HR,
ea56f492-0a0a-8c0a-010a-65e33c9445c9,HR computing support and development,0,FE,FAP-TPR-MI,gthiede
ea56f588-0a0a-8c0a-00c4-2cb79ab60ec4,HR Contract Management,0,FE,HR-SMC,gguinot
22755b5d-1b31-2490-01d1-642abb4bcb8e,HR Departmental Administrative Office (DAO),1,FE,HR-CBS-B,nbordon
,,,,,ygrange
db26579d-1b31-2490-01d1-642abb4bcb29,HR Departmental Key Manager (DKM),1,FE,HR-CBS-B,nbordon
,,,,,ygrange
73e69f9d-1b31-2490-01d1-642abb4bcbfb,HR Departmental Space Manager (DSM),1,FE,HR-CBS-B,nbordon
,,,,,ygrange
597d9a8d-84d8-f000-c713-6646aaa7ce6a,HR Frontline Accelerators and Technology,1,FE,HR-FL,taillieu
417d928d-84d8-f000-c713-6646aaa7ce5e,HR Frontline Administration and Infrastructure,1,FE,HR-FL,falipou
,,,,,taillieu
95bf113f-db8c-bf04-ae65-9ec4db96193f,HR Frontline for ATS,1,FE,HR-FL,jsuutala
,,,,,taillieu
0110217f-db8c-bf04-ae65-9ec4db9619e0,HR Frontline for BE department,1,FE,HR-FL,jsuutala
,,,,,mrivierb
,,,,,taillieu
b320297f-db8c-bf04-ae65-9ec4db9619dd,HR Frontline for DG unit,1,FE,HR-FL,jsuutala
9c6061bf-db8c-bf04-ae65-9ec4db961984,HR Frontline for EN department,1,FE,HR-FL,jsuutala
,,,,,taillieu
437025bf-db8c-bf04-ae65-9ec4db961966,HR Frontline for EP department,1,FE,HR-FL,jsuutala
,,,,,taillieu
87e029ff-db8c-bf04-ae65-9ec4db9619e5,HR Frontline for FAP department,1,FE,HR-FL,jsuutala
,,,,,mrivierb
c501adff-db8c-bf04-ae65-9ec4db96195a,HR Frontline for HR department,1,FE,HR-FL,jsuutala
,,,,,taillieu
c0216133-dbcc-bf04-ae65-9ec4db961990,HR Frontline for HSE unit,1,FE,HR-FL,taillieu
6d512933-dbcc-bf04-ae65-9ec4db9619f4,HR Frontline for IPT department,1,FE,HR-FL,jsuutala
,,,,,taillieu
c8716933-dbcc-bf04-ae65-9ec4db9619dd,HR Frontline for IR sector,1,FE,HR-FL,jsuutala
,,,,,taillieu
a2816973-dbcc-bf04-ae65-9ec4db9619e1,HR Frontline for IT deparment,1,FE,HR-FL,mrivierb
,,,,,taillieu
e0a1e973-dbcc-bf04-ae65-9ec4db9619ce,HR Frontline for PF unit,1,FE,HR-FL,jsuutala
,,,,,taillieu
13b1e1b3-dbcc-bf04-ae65-9ec4db961915,HR Frontline for RCS,1,FE,HR-FL,taillieu
7dd1adb3-dbcc-bf04-ae65-9ec4db961986,HR Frontline for SCE department,1,FE,HR-FL,jsuutala
,,,,,taillieu
e7e125f3-dbcc-bf04-ae65-9ec4db9619d7,HR Frontline for TE department,1,FE,HR-FL,jsuutala
,,,,,taillieu
a102e5f3-dbcc-bf04-ae65-9ec4db9619af,HR Frontline for TH department,1,FE,HR-FL,jsuutala
,,,,,taillieu
c27e5acd-84d8-f000-c713-6646aaa7cece,HR Frontline Research and Scientific computing,1,FE,HR-FL,falipou
,,,,,taillieu
300c9e15-b48f-f940-7904-8a4404c0ebbb,HR Frontlines Internal Activities,1,FE,HR-FL,cdepoix
,,,,,taillieu
ea56f3d9-0a0a-8c0a-0084-7170ab669952,HR Internal Support,0,FE,HR-DHO-ADM,rogerson
ea56f36a-0a0a-8c0a-004f-fe1a26962871,HR Strategy and Development,1,FE,HR-DHO,cerne
,,,,,purvisj
ea570c11-0a0a-8c0a-01b8-637b74aa081e,HR Tools,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
ea570f83-0a0a-8c0a-01f2-60356e4e27d5,HRT,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
3c9c0333-e500-4c27-bf69-47be78ebc0e6,HSE,1,Department,HSE,
1e389740-dbb1-5f00-1aab-9247db961930,HSE - Groups Secretariat,1,FE,HSE-TS-AS,delamare
ea56eca8-0a0a-8c0a-00d2-fee5850830ee,HVAC,0,FE,SMB-SE-HE,martelc
cef86996-9491-6100-7904-7129f337bf1a,HVAC engineering design and studies for tertiary installations,1,FE,SCE-SAM-IN,martelc
eaa834d2-111d-e500-adf9-ea91634bc680,HVAC operation and maintenance,1,FE,SCE-SAM-IN,martelc
faf87c12-111d-e500-adf9-ea91634bc6cb,HVAC works,1,FE,SCE-SAM-IN,martelc
96fa08fd-4f18-aa40-b8cb-ca1f0310c750,HW Asset Manager,1,FE,BE-CEM-IN,ikozsar
,,,,,veym
e88f16c9-4fbb-5600-b9cc-09de0310c75b,HW Front End Installation,1,FE,BE-CEM-IN,ikozsar
,,,,,veym
ea56f72a-0a0a-8c0a-010f-2fddfd8e0a68,HW Procurement,1,FE,IT-CF,ebonfill
,,,,,salter
6eeb3647-a496-1c80-9878-a279f01cb757,HW Repair,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
04544628-4f10-b240-7db7-d3ef0310c746,HW Repair ART Computer,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
,,,,,schroder
3c2aa3bd-4fcf-7600-87a2-f5601310c787,HW Repair Danoffice,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
d788abf9-4fcf-7600-87a2-f5601310c7e4,HW Repair Darest,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
eca92231-4f8b-7600-87a2-f5601310c793,HW Repair Dell,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
ef26794c-4feb-3640-b9cc-09de0310c7ce,HW Repair MIB,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
456fea7d-4f8b-7600-87a2-f5601310c714,HW Repair TeknoService,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
e0c374e2-a164-e000-9878-f17945ca4f85,HW Resources,1,FE,IT-CF,meinhard
,,,,,panzer
,,,,,salter
77d1c32d-e34f-40ca-baaa-aa8d3254bd83,HYBRID PHOTODETECTOR,1,Experiment,None,
ed029db4-2de8-4b2e-ae67-349a852aa30e,I32,1,Experiment,None,
acb757bd-2423-f000-7904-c39b27667ad1,IBM vendor,0,FE,IT-CF-SAO,
21b797bd-2423-f000-7904-c39b27667ad4,Ibmgate vendor,0,FE,IT-CF-SAO,
1ebd59ae-de60-4c08-889b-c9ec51c3b69a,ICANOE,1,Experiment,None,
8824c893-e855-4199-87ec-a221d821a6e4,ICARUS,1,Experiment,None,
82c0d5b3-6020-4507-b744-e7191a59fd85,ICARUS/WA104,1,Experiment,None,
66d05523-63cf-4e3a-9805-198ac59339cf,ICECUBE,1,Experiment,None,
4825ca78-4fbb-7a80-b9cc-09de0310c700,ICS Internal Devops Tools,1,FE,BE-CSS,fchapron
,,,,,mbraeger
,,,,,solp
ea56feb7-0a0a-8c0a-0107-0ac7fe3ebeeb,Identity and Account Management,1,FE,IT-CDA-IC,aaguadoc
,,,,,ormancey
,,,,,ptedesco
,,,,,tbaron
ea570c9b-0a0a-8c0a-0083-3ef2d1101257,Identity Management,0,FE,GS-AIS-GDI,jjanke
ea56eb6b-0a0a-8c0a-00a6-1a9c24c62a8e,ILC Document Server,1,FE,IT-CDA-DR,jbenito
,,,,,lnielsen
,,,,,simko
95a0f1cf-0a0a-8c0a-015e-1f7bbd58fdb8,iMarine,0,FE,IT-SDC-ID,
ea570fae-0a0a-8c0a-0194-e8a4a24a080e,IMPACT,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea56ea0e-0a0a-8c0a-01c9-03820d794c5c,Indico,1,FE,IT-CDA-IC,pferreir
,,,,,tbaron
ea56f54a-0a0a-8c0a-002d-0b307990ac9f,Induction Programme,0,FE,HR-LD,pgoy
ece206f0-4fbb-7a80-b9cc-09de0310c75a,Industrial Control Frameworks,1,FE,BE-ICS-FT,fchapron
,,,,,fvarelar
,,,,,solp
451386f0-4fbb-7a80-b9cc-09de0310c7e4,Industrial Control Technology Evolution,1,FE,BE-ICS-FT,fchapron
,,,,,fvarelar
,,,,,solp
ea5708e5-0a0a-8c0a-0170-f5529e107813,Industrial Relations,1,FE,IPT-PI,gregorio
ea5708b6-0a0a-8c0a-01e6-5cdb26d098da,Industrial Support Contract Management,0,FE,IPT-PI-IS,larac
ea5708ce-0a0a-8c0a-00c8-689e0c129dfd,Industrial Support Industrial Relations,0,FE,IPT-PI-IS,larac
ea570816-0a0a-8c0a-0044-ef835db172ce,Industrial Support Invitations for Tender,0,FE,IPT-PI-IS,larac
ea57079e-0a0a-8c0a-019e-c02c5e5b1786,Industrial Support Market Surveys,0,FE,IPT-PI-IS,larac
ea570874-0a0a-8c0a-01b3-d94c3b5222ec,Industrial Support Service Procurement,0,FE,IPT-PI-IS,larac
ea57089f-0a0a-8c0a-014e-956cafc3d703,Industrial Support Supplier Management,0,FE,IPT-PI-IS,larac
ea5717d4-0a0a-8c0a-0156-fbf98ece28fc,Infirmary,1,FE,HSE-OHS-ME,delamare
,,,,,drichard
cd975b7d-2423-f000-7904-c39b27667a3e,Infodip vendor,0,FE,IT-CF-SAO,
ea570c6f-0a0a-8c0a-0035-8068a091eebb,INFOR ERP LN BAAN Support Contract,0,FE,GS-AIS-FP,varmeanu
ea57148a-0a0a-8c0a-0029-5bf7c7479723,INFOR InforEAM Support Contract,0,FE,GS-ASE-EDS,fanou
ea571441-0a0a-8c0a-01d8-f50cfbd58899,InforEAM,0,FE,GS-ASE-EDS,fanou
ea56fd51-0a0a-8c0a-01f5-2f80bed6480e,Information System Development,1,FE,IT-SC,andreeva
d248a116-9491-6100-7904-7129f337bf23,Infrastructure and building renovations,0,FE,SMB-SE-CEB,fmagnin
4428a5d2-9491-6100-7904-7129f337bfad,Infrastructure consolidation,1,FE,SCE-SAM-CE,fmagnin
4aec78dc-4fb9-2e00-b9cc-09de0310c7b6,Infrastructure Finance officer,1,FE,BE-CEM-IN,ikozsar
ea56ee2a-0a0a-8c0a-0176-1d2b918ebd84,Iniziative Industrian CE Project Collaboration,0,FE,GS-SE-CWM,fmagnin
ea56eb93-0a0a-8c0a-0004-af7b268a0987,INSPIRE Document Server,1,FE,RCS-SIS-IN,christod
ea56f5ca-0a0a-8c0a-00e5-c3dd38e91c71,Installation indemnity,1,FE,HR-CBS-B,ygrange
ea56f5ab-0a0a-8c0a-0152-f7aa5f30f874,Integration,0,FE,HR-TA-RC,
1dc79fbd-2423-f000-7904-c39b27667a89,Intel vendor,0,FE,IT-CF-SAO,
ea56f7b9-0a0a-8c0a-01ed-e544fb05900f,Intercom,0,FE,IT-CS,
7c133816-11d9-e500-adf9-ea91634bc6da,Interior maintenance,0,FE,SMB-SE-CBS,
ea57191d-0a0a-8c0a-002c-56929f1be9ea,Interior Refurbishment,0,FE,SMB-SE-CEB,fmagnin
,,,,,srew
03e030de-1199-e500-adf9-ea91634bc65a,Interior works,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
ea56f61d-0a0a-8c0a-0028-32c5e96cfab1,Internal Tax,1,FE,HR-DHO-SAS,ppessy
91fb89bf-db45-2f04-7c55-9eb3db96196c,Internal Training,1,FE,HR-LD,cparis
,,,,,mfiascar
,,,,,mosselma
,,,,,mthyes
ea56f78f-0a0a-8c0a-0151-98884b8c802d,Internet Access,0,FE,IT-CS,
ea570f98-0a0a-8c0a-00c5-42f72b3f66eb,Introscope Support Contract,0,FE,GS-AIS-GDI,jjanke
ea56e9cd-0a0a-8c0a-0161-db401d1cb283,Invenio External Support,1,FE,IT-CDA-DR,jbenito
,,,,,lnielsen
e994ec1a-0a0a-8c0a-016d-aebb6e6c4315,Invenio Software Development,1,FE,IT-CDA-DR,jbenito
,,,,,lnielsen
,,,,,simko
ea571086-0a0a-8c0a-0014-18baba85b368,Inventory,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea570830-0a0a-8c0a-0068-7047d9d5625b,Invitations for Tender,1,FE,IPT-PI,gregorio
786d6feb-e424-4711-b473-90631bd8894d,IONS/TPC-HADRONS,1,Experiment,None,
ad955e72-dba1-2340-7c55-9eb3db96190c,iOS Support,1,FE,IT-CDA-AD,mkwiatek
,,,,,schroder
3f527e9b-35a7-4c31-9401-9bba68dbadf9,IPT,1,Department,IPT,
85027019-1b73-6010-1559-65b0ab4bcb54,IPT Website management,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
77daa0f5-c753-47cd-96c0-468dcc1b1692,IR,1,Department,IR,
9722bccc-db8d-27c0-7c55-9eb3db9619da,IRIS Application,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
2e10f36b-9321-441b-97b1-4378b008d52c,IS10,1,Experiment,None,
cc7abe0e-a5bd-4671-9101-ff0ee54ab8bc,IS100,1,Experiment,None,
b8899f00-659f-4972-9082-b9214bd3ac3f,IS110,1,Experiment,None,
afc2b406-a2e1-42ec-8cf7-cfcfe111bf89,IS120,1,Experiment,None,
3472242d-97fb-4b3e-97e2-9acfc6dfe4e7,IS130,1,Experiment,None,
a93e45c6-37fa-422d-896a-2daf8a867d67,IS140,1,Experiment,None,
1b077250-dbcd-4d02-9812-cfdaafb49819,IS150,1,Experiment,None,
65f192b8-15f3-4853-9a9b-511d8a62bfe7,IS160,1,Experiment,None,
d3310c45-2599-46b3-a5fb-d61b502fb3c8,IS170,1,Experiment,None,
5fe16966-6631-4e86-95c2-538acc483e6f,IS180,1,Experiment,None,
6ba217e4-f2ef-4600-9bb1-f6029c37739a,IS181,1,Experiment,None,
8be08fd3-36bd-4dce-9538-0b7adf1df504,IS190,1,Experiment,None,
baa2626d-b09e-4296-9ef3-c13fce2b4ada,IS20,1,Experiment,None,
17abe22f-7001-4b94-be20-020fe71171c3,IS200,1,Experiment,None,
a97d9c45-db00-4b0c-bc9d-8bb1c2b167b3,IS21,1,Experiment,None,
e44504bc-e1a3-4a1f-85bc-0b2a0c12b3d4,IS210,1,Experiment,None,
85094bb6-0310-4203-9ca0-5c5f7357d2f8,IS220,1,Experiment,None,
7259d55e-aee9-47cd-a461-0501957b2063,IS230,1,Experiment,None,
cf79bc4d-436e-429f-bc13-4479d1227f7e,IS240,1,Experiment,None,
a62f4a8c-2f98-401a-83f2-93c7c1ceee22,IS260,1,Experiment,None,
b604b589-603a-4d83-81bd-23436b89fab2,IS270,1,Experiment,None,
b7ccd2f5-2463-4f4f-b01a-b1cc372bcbee,IS30,1,Experiment,None,
952e083a-822b-4014-8b22-142dbb8edb73,IS300,1,Experiment,None,
9b3f5198-c9e4-46eb-baea-7d92f4150f28,IS301,1,Experiment,None,
01cbda05-c3d0-43bd-ba2d-d94a34c98062,IS302,1,Experiment,None,
39cb72b6-724a-4d35-979d-ce0d140a62ca,IS303,1,Experiment,None,
562e5437-ca99-49a8-9257-a45372a2bc90,IS304,1,Experiment,None,
5019156e-39cf-46b8-805f-e304c040f008,IS305,1,Experiment,None,
833e0d91-ba4d-4dc1-baa1-5d6b35caa1de,IS306,1,Experiment,None,
dc8e2198-aee8-428b-8be0-7feec318908e,IS307,1,Experiment,None,
81a31e7c-b1c2-4d7d-b18d-0adc3332bc94,IS308,1,Experiment,None,
7d4158c7-145f-49aa-8587-28726d883048,IS309,1,Experiment,None,
a181c75d-965e-4c59-98c2-979d3b352310,IS31,1,Experiment,None,
823cc7ed-27f8-4d8f-81d4-e6200f9bd349,IS310,1,Experiment,None,
1e96a3fc-9533-417f-b358-f970aa16fb2d,IS311,1,Experiment,None,
6c931c9d-f739-4e8f-bb14-ce96d2633bc8,IS312,1,Experiment,None,
0a9e2aaf-c047-448e-8fc2-4a0460b74a4b,IS313,1,Experiment,None,
4780cab8-e27f-461a-a4c1-22e9b87e879c,IS314,1,Experiment,None,
e3a438f1-afef-440b-96ea-c1005f30f39a,IS315,1,Experiment,None,
cf6f2700-da7f-46a9-9316-d6461eebb755,IS316,1,Experiment,None,
245edd06-71bf-4a62-9032-b409a47d7afb,IS317,1,Experiment,None,
021d6132-c473-4aa2-8cd6-0f5d1eb564fd,IS318,1,Experiment,None,
536e4c03-8311-4769-85d8-1cbc755467ec,IS319,1,Experiment,None,
070d02a1-dde6-4249-9b12-666806066d61,IS320,1,Experiment,None,
91ecd4fc-4cab-4270-931b-b18111237eb8,IS321,1,Experiment,None,
bec85931-af26-4718-bd4b-bf70ca1f657b,IS322,1,Experiment,None,
1a97a1fd-5f83-4a5e-b91b-ba290428f4a6,IS323,1,Experiment,None,
a9278a0f-3bb9-4c36-a304-f9df3c1cbb39,IS324,1,Experiment,None,
b5df8b10-7c7f-4c0e-a100-23643a93887e,IS325,1,Experiment,None,
e2ad75bc-f129-4b5f-b592-4a49f4da9e80,IS326,1,Experiment,None,
2f944281-9579-4c7d-b2fb-250904a47f00,IS327,1,Experiment,None,
fbb4c3fb-8c5c-4ca9-b4f3-b90c4de55e8c,IS328,1,Experiment,None,
e9a0d088-93d4-4b55-a98d-acdb069c1ab2,IS329,1,Experiment,None,
4d67b6f8-0b0b-41ad-9548-e107e135d00b,IS330,1,Experiment,None,
36d5440b-eb7e-41de-b161-f19a9362abe3,IS331,1,Experiment,None,
ea371476-be30-4ea6-99e4-abd630b468a8,IS332,1,Experiment,None,
003ba0fe-a840-4e61-91ba-f1df8c4129db,IS333,1,Experiment,None,
dab05ed6-5e21-4c8e-bba9-10763758264d,IS334,1,Experiment,None,
9958e113-b9e7-4f9d-8da3-c808a27a4494,IS335,1,Experiment,None,
dc631412-1ee3-4e66-bc15-b01542cca1de,IS336,1,Experiment,None,
8eccac44-01ab-4a66-a94d-a66b938434ff,IS337,1,Experiment,None,
a4bf66ad-6043-4ac9-9a14-782f3fff137e,IS338,1,Experiment,None,
f6d5b2b9-9903-412c-944f-127af792980e,IS339,1,Experiment,None,
09fd6b8e-8611-4d2c-aabd-586def5890b4,IS340,1,Experiment,None,
1ec7fc51-db1a-471c-a237-ddfc567161c1,IS341,1,Experiment,None,
a4099aa1-f454-44f2-be29-27c9539f2701,IS342,1,Experiment,None,
987f85fe-e31c-4ac0-8c74-8c863870b3e4,IS343,1,Experiment,None,
a375bae6-8ab8-4741-b112-ad306f6f554b,IS344,1,Experiment,None,
5401af5c-1cdb-47ea-9844-f3dac1f96ecc,IS345,1,Experiment,None,
dd5c6b0c-9bec-4dfb-91ca-6e2bac73270a,IS346,1,Experiment,None,
4767f06e-bc0f-430a-ae5e-ffc1b9cd89fc,IS347,1,Experiment,None,
4b1f8c07-d43d-4828-ba46-ed15cdd2e767,IS348,1,Experiment,None,
b68a8687-2920-4cac-93ad-df24546fb092,IS349,1,Experiment,None,
acd52499-8ea9-4a6c-a873-37db94746884,IS350,1,Experiment,None,
d77ebb1b-f933-435a-b56e-ef206f5cc70a,IS351,1,Experiment,None,
2cc67dd4-bc47-461c-886c-5466594f58c4,IS352,1,Experiment,None,
b4f3bc7a-14b4-4400-858d-5b05cf63753e,IS353,1,Experiment,None,
63a74e9b-3af1-458d-b6f6-cfd02cd15215,IS354,1,Experiment,None,
ac927335-8f9c-4418-8847-b1fea66d0aa2,IS355,1,Experiment,None,
ad945340-e933-46d7-b093-706928fc3657,IS356,1,Experiment,None,
8d7d2e76-ccd0-4e08-8bb0-8c632b469237,IS357,1,Experiment,None,
1ba8d9ac-c6fb-4276-ba3c-93997e432d57,IS358,1,Experiment,None,
45cd8aeb-314d-49d0-bf47-6a5735b57bab,IS359,1,Experiment,None,
449428ec-9cc9-422f-83a5-363e46b2b27b,IS360,1,Experiment,None,
786414ad-e925-4a92-a7cb-e43ac7b66bcb,IS361,1,Experiment,None,
d917285d-fe27-46f0-8c42-84109c07fa6a,IS362,1,Experiment,None,
81b53029-dad4-4a82-9c39-eed67c2cbff6,IS363,1,Experiment,None,
ed4e9479-ea61-42f4-a49a-42d316d74879,IS364,1,Experiment,None,
1e181c59-745a-4b2d-a08b-069ac41ed375,IS365,1,Experiment,None,
361b9ddf-2df3-480c-8ee7-030d607d4a51,IS366,1,Experiment,None,
7f62bfcc-13b6-49ab-ac2b-f79e5eca956e,IS367,1,Experiment,None,
df658642-3d8b-46af-9130-1350b00665e5,IS368,1,Experiment,None,
8ef6249f-ba82-4286-975e-bc930c2b3872,IS369,1,Experiment,None,
78ede20f-d8b3-409b-b481-eb2e8920dc48,IS370,1,Experiment,None,
011c126f-8681-4149-9c74-86b1c6bde523,IS371,1,Experiment,None,
a6a378b1-5c21-4099-ab53-f996a1778eaa,IS372,1,Experiment,None,
38e1ab4a-5d0f-43d2-be45-4c07f5b02060,IS373,1,Experiment,None,
875e3ba6-6873-4a1b-9080-6b0247405924,IS374,1,Experiment,None,
ba756e51-26a6-4517-b068-148783c2923b,IS375,1,Experiment,None,
3b9c7909-0205-40b7-a4d6-6234a77d854a,IS376,1,Experiment,None,
0a74d273-95d4-4ae2-a468-f2ea496608ec,IS377,1,Experiment,None,
9eb0e0f0-35b7-41cc-8c24-12181f0a9f2b,IS378,1,Experiment,None,
b873cbc1-571b-4c75-8438-35cc34363592,IS379,1,Experiment,None,
55f0e1fc-7096-4e1b-8c14-3052498c0724,IS380,1,Experiment,None,
d15e1299-cdf2-4380-ab84-63c757d5e089,IS381,1,Experiment,None,
9065f684-5699-4596-8dcc-e22a1e47e24d,IS382,1,Experiment,None,
0ea8b17e-4340-4149-bd73-6549d0c445bf,IS383,1,Experiment,None,
2d785acf-0229-4fc3-a5d3-38bf6ad9f197,IS384,1,Experiment,None,
5316a614-a6ad-4642-8d45-b737aaa80640,IS385,1,Experiment,None,
d4b3aba1-78cc-4a4b-97e9-d7644c517eee,IS386,1,Experiment,None,
1d255bf9-502d-477b-ba35-e2e029450da0,IS387,1,Experiment,None,
72633b2a-571d-42f7-84f6-2c988c2c3452,IS388,1,Experiment,None,
e35aee03-9705-4a26-a68d-0431ec04c421,IS389,1,Experiment,None,
372bfd76-4fd4-4dbf-9cfc-e5093d3dae60,IS390,1,Experiment,None,
750bffa0-2768-46da-bc2f-625963a40fd5,IS391,1,Experiment,None,
9f12f9b0-95c2-442b-9365-dde3b9ee2ef6,IS392,1,Experiment,None,
dff6cd91-1f15-4314-839c-2467830cea81,IS393,1,Experiment,None,
ec3e226c-b7ed-4e97-aca9-d66c9f5425bd,IS394,1,Experiment,None,
68ded4ac-20fa-40e0-a283-00710ad6607a,IS395,1,Experiment,None,
b86fc7f9-e317-41ed-81b2-3cb4024128af,IS396,1,Experiment,None,
6bc8fcb6-deb1-4073-948c-f3da752b888b,IS397,1,Experiment,None,
f484db4a-4d81-46ef-9e90-2157cb384603,IS398,1,Experiment,None,
49f0f83e-2599-4e7f-ac24-f4293b396079,IS399,1,Experiment,None,
ae7a66d8-cca0-4c64-bb43-40af279eaffa,IS40,1,Experiment,None,
ff4fb0db-a8ce-46c4-80a5-92b7765af208,IS400,1,Experiment,None,
14efe563-490b-44f2-a7e3-da2456c1401a,IS401,1,Experiment,None,
edfc552c-668c-49de-9108-06f72ca12dad,IS402,1,Experiment,None,
61b164c1-ffc8-43c9-9c63-6ec6b4fa3a50,IS403,1,Experiment,None,
80db7291-b2da-4305-973b-e62478441839,IS404,1,Experiment,None,
6a37a4a1-4521-425e-a11a-dbad3bf149f3,IS405,1,Experiment,None,
bc547e07-55c1-48c9-9f36-e5d64a775f0d,IS406,1,Experiment,None,
20c4995f-322f-4953-8ef4-2b45047bc22f,IS407,1,Experiment,None,
48392486-739c-4024-ad71-e84b49a8d311,IS408,1,Experiment,None,
7a00a634-dbaf-4279-a0ba-97a4ad245927,IS409,1,Experiment,None,
852ad5f0-6e36-4457-bcc3-dcd75a5a7b31,IS410,1,Experiment,None,
6ab648c1-8e54-4a44-84d3-1186e1104c41,IS411,1,Experiment,None,
beef2934-4d78-4a14-8310-3aeecf6e17c1,IS412,1,Experiment,None,
1d49cd5c-06de-40a8-bf50-945570c885dc,IS413,1,Experiment,None,
b4af9955-4c02-464c-a68d-0233a3908e13,IS414,1,Experiment,None,
7e70d5e9-935d-4f6c-b6a2-14b72b0b5eb9,IS415,1,Experiment,None,
27ae90e5-e270-4274-bb08-e4f86a40f691,IS416,1,Experiment,None,
48700b45-95fc-4d0b-ab23-5079b3524fc3,IS417,1,Experiment,None,
cd53ba50-b721-4b71-a7f4-a2fb735ca254,IS418,1,Experiment,None,
40b0d1a0-6be2-4ff8-8b0c-5c07374afa7e,IS419,1,Experiment,None,
a32578b4-ce1d-49ec-bab7-bc4d8ef11040,IS420,1,Experiment,None,
4ea14e32-73de-4ac2-90c1-577e5debc0a8,IS421,1,Experiment,None,
7c890dbd-db31-4277-bb35-02bc170bc791,IS422,1,Experiment,None,
a49e9ccb-72c4-4575-8376-4a0a89372a25,IS423,1,Experiment,None,
f0de2b58-71ef-4a6b-9214-e46e202468cb,IS424,1,Experiment,None,
056ad0de-3fec-4604-b39d-1562dba41c7f,IS425,1,Experiment,None,
95520f89-05ab-48de-b981-a420c3127fd4,IS426,1,Experiment,None,
0e025b76-9896-424f-8c4a-dd9bb33827d9,IS427,1,Experiment,None,
c261b68f-e26f-498c-973c-633db7f51854,IS428,1,Experiment,None,
fb7ee8f1-7220-4120-b1ab-1db513c75f8a,IS429,1,Experiment,None,
77b1ceef-ce50-41e6-8236-b0ed3e5abefe,IS430,1,Experiment,None,
bca7324a-98b4-47ad-8d0d-ffc8120a4e8c,IS431,1,Experiment,None,
545d39fd-8022-4d9a-b84e-95dc8dd30688,IS432,1,Experiment,None,
7ceb80d7-1700-414c-923a-7e1ac5b958b8,IS433,1,Experiment,None,
5ae06785-9aa3-4864-9d6d-0bc7deaf11c2,IS434,1,Experiment,None,
40fc9689-93c9-43d0-b989-d36239e07407,IS435,1,Experiment,None,
c872aefd-113f-41fe-9399-0de87f155127,IS436,1,Experiment,None,
7433f713-8aaa-4bae-ae3d-f2cadabe8097,IS437,1,Experiment,None,
da88be97-f3c2-44a0-aeac-bed7e95f8031,IS438,1,Experiment,None,
0b75a9a1-684e-4295-a1bf-84541828657e,IS439,1,Experiment,None,
01963868-046a-4ad1-b0d4-ff2de473876d,IS440,1,Experiment,None,
b969711d-43ec-4667-ae87-592913bba91b,IS441,1,Experiment,None,
0696148c-5d25-4d24-92c7-983af73a1ef3,IS442,1,Experiment,None,
808c9d51-4748-4654-862a-4b762f2f98f6,IS443,1,Experiment,None,
6c640cd1-039b-4c37-831b-beed0eeb7880,IS444,1,Experiment,None,
7f6c1471-0639-47f5-aa49-ede25dbfc253,IS445,1,Experiment,None,
b1394d55-8ea1-4869-b543-a9eb35f55c05,IS446,1,Experiment,None,
f296c628-3599-4b06-9be7-cd0dc5f9326e,IS447,1,Experiment,None,
fd3d6081-49a6-4128-9113-15339bd29319,IS448,1,Experiment,None,
c31d72d3-4390-4026-b685-251f0353cfc3,IS449,1,Experiment,None,
0d77aaf2-e701-4c6a-a6e3-044a37c7a347,IS450,1,Experiment,None,
5b02a607-72b4-46be-9a42-8b492187933d,IS451,1,Experiment,None,
069dab70-d113-4f47-a57f-c815ea7a7045,IS452,1,Experiment,None,
75c0f730-db3f-4ff8-b60f-878df6582c7a,IS453,1,Experiment,None,
12e2ac15-81bb-4af5-a962-078165859047,IS454,1,Experiment,None,
15f089b3-0b10-4c75-8a72-f347f4ce0679,IS455,1,Experiment,None,
c1fefb61-25ca-4c43-8c0f-f7833f8c1bbc,IS456,1,Experiment,None,
979e32ed-4549-4363-9a30-78dbb230408c,IS457,1,Experiment,None,
fb53f237-dd02-4201-85b7-06d314c82e63,IS458,1,Experiment,None,
7e28471f-aa13-4877-8cb0-aec5d9cd147f,IS459,1,Experiment,None,
c519ea82-42e0-4f84-a51f-30fb02d0c0e0,IS460,1,Experiment,None,
59328620-0b8f-498a-82c4-f2c44c3ba93d,IS461,1,Experiment,None,
afbada3f-4fe5-4027-9f60-8e901cca291f,IS462,1,Experiment,None,
fd30b363-cc47-445a-988a-9bcc6f916838,IS463,1,Experiment,None,
46d31076-71cb-44ba-a391-15dcd2aee618,IS464,1,Experiment,None,
c7ce008f-44ed-4696-80e5-ef687ed1f127,IS465,1,Experiment,None,
1ee7cc66-0e3b-45b2-a3a8-ea3d593db0d9,IS466,1,Experiment,None,
272e319e-3799-40ef-a75f-e2971cb86004,IS467,1,Experiment,None,
063f8138-dc37-4c78-807e-b0f90b91b933,IS468,1,Experiment,None,
f2e7ee14-4491-4107-9a00-c2ed849f9504,IS469,1,Experiment,None,
0b1da4fa-b06a-4716-acba-cdce7a46d43a,IS470,1,Experiment,None,
734bb716-e9fd-4a16-a82d-e423378fee02,IS471,1,Experiment,None,
a21f8235-68d7-412d-95f4-022eaf298fd8,IS472,1,Experiment,None,
4901404e-9710-4873-9b5c-0f02dc97442a,IS473,1,Experiment,None,
aa85a879-3ede-4453-b117-adbeda5a4993,IS474,1,Experiment,None,
045b7fec-caa1-414e-811f-f73dfcbc19c6,IS475,1,Experiment,None,
36a6fb93-8d0c-46ee-9dd1-addf9029e4a6,IS476,1,Experiment,None,
627cc5c6-5b9d-4bdd-948e-441004a5bd78,IS477,1,Experiment,None,
99fcb5b7-ecdd-4e4e-85dc-c19a2f5d5ef3,IS478,1,Experiment,None,
43d99d0a-8e5b-48bf-b0d4-ead48d72c330,IS479,1,Experiment,None,
895c8e14-e156-454b-88c6-495e1f91a624,IS480,1,Experiment,None,
795fdccb-c26c-4a00-bccc-f6e44ef051a7,IS481,1,Experiment,None,
b9bbd13d-5ec0-4e81-8c3d-46eebc2fa0d9,IS482,1,Experiment,None,
0b229b8c-cb8c-428e-b8e7-ff30b1b4ee14,IS483,1,Experiment,None,
f5c72840-8c38-4ebd-8bf2-ac1de67a8b4b,IS484,1,Experiment,None,
67d803c5-be58-4548-9da5-6504c2fc60cd,IS485,1,Experiment,None,
21007b13-a2c6-4971-a9bb-b542fe3f9cd8,IS486,1,Experiment,None,
175b719c-3459-4a64-abad-b21fc33078b3,IS487,1,Experiment,None,
6c15fbca-6ae5-4a56-ae2d-89f8bd8b1364,IS488,1,Experiment,None,
1e3517f5-e92d-4954-8cd3-6aaa7ee954da,IS489,1,Experiment,None,
9d4cf85a-0c09-4f78-b627-8c651a5ca23b,IS490,1,Experiment,None,
9f1b6d34-1c4f-4e40-b6ff-617d16661ad3,IS491,1,Experiment,None,
a7bff398-f646-4151-a9bc-dcad0f9c1393,IS492,1,Experiment,None,
8397a56f-6170-48d6-b843-1cdfbb56ca90,IS493,1,Experiment,None,
4d502a0a-99ad-453c-a868-8369f989089d,IS494,1,Experiment,None,
bf3f5aec-61f5-4e2f-83f2-74ebaea32e7a,IS495,1,Experiment,None,
dcc58c75-07fd-4a6a-888f-26e058431ba0,IS496,1,Experiment,None,
078a304a-d033-4be8-ac9f-7696274baa76,IS497,1,Experiment,None,
985e0295-1429-4d7f-a13c-577fffeafc93,IS498,1,Experiment,None,
0edf988d-6c47-409f-80e3-7c8417de8d0a,IS499,1,Experiment,None,
402cee34-0915-4f1e-a649-151053ca6eb5,IS50,1,Experiment,None,
5a0074d1-d878-4711-889c-2479f855c036,IS500,1,Experiment,None,
6de7628a-5021-4d99-809a-2d2d24dd80d5,IS501,1,Experiment,None,
639f4a56-3f23-4197-9e24-dde232be8382,IS502,1,Experiment,None,
eea810b4-0c3c-4ce9-9bc3-c316fe7748a7,IS503,1,Experiment,None,
338fd848-0d92-4bce-8701-1991ae3627bd,IS504,1,Experiment,None,
632dac5e-3dc4-4fa5-921e-162ded1db21b,IS505,1,Experiment,None,
4fb5d17a-1d96-42af-ad3a-4562c6d001e0,IS506,1,Experiment,None,
459a9095-6236-4026-94e4-8a2c85d04c64,IS507,1,Experiment,None,
0f1aa2e6-e45a-4307-adf4-66e0ea702a9e,IS508,1,Experiment,None,
06a5e3dc-90ef-4632-aceb-7d8cd66972b9,IS509,1,Experiment,None,
3092a7b7-30f5-4d73-926c-283ead7429da,IS510,1,Experiment,None,
3c95d199-8cc4-4930-89f2-2f5b0ff791f6,IS511,1,Experiment,None,
6f137405-b905-4d2a-a219-5e5f7264c7ac,IS512,1,Experiment,None,
42d8e528-ab6c-4554-8923-3866da4df490,IS513,1,Experiment,None,
b52605ce-0e28-4004-bc01-f8b6349450af,IS514,1,Experiment,None,
2d9d2c03-55dd-48a2-b20e-ff493952e566,IS515,1,Experiment,None,
3609f6af-ca1a-45e5-bf9b-7668f58c55a5,IS516,1,Experiment,None,
3fb8f252-0502-4664-ac35-a8e411d3473d,IS517,1,Experiment,None,
15c5e6cf-aa69-4c8e-ba9a-0c3c1a5ca14b,IS518,1,Experiment,None,
4aafba40-7f0f-4e7a-b805-3045e47e0e82,IS519,1,Experiment,None,
b0d18aed-b9a8-4b9d-8755-a23f6ee3806e,IS520,1,Experiment,None,
114d84c3-11ee-4522-b083-2493e9da79ab,IS521,1,Experiment,None,
29d33e4a-4861-4acd-96d5-2b9bb8a1f654,IS522,1,Experiment,None,
f81df5d8-3cab-413f-b2f4-626dc1a4f184,IS523,1,Experiment,None,
a15c23b8-0b2e-4c37-9657-54824e6707a4,IS524,1,Experiment,None,
b5006cdb-3f75-4bc3-9184-b49b215c6830,IS525,1,Experiment,None,
b670e4c9-9503-4c05-bd5a-2ee03eaa5f9d,IS526,1,Experiment,None,
8b2448db-1073-4619-9328-67a7b8979584,IS527,1,Experiment,None,
a27ae725-269c-42a4-88db-e483af4bc822,IS528,1,Experiment,None,
6818b1f2-d058-48f3-a255-cb8f903aa1f7,IS529,1,Experiment,None,
acc1bc91-6575-4a34-bea0-f0a3451b8442,IS530,1,Experiment,None,
185fcc7c-a061-4350-bab6-6f6d18ff1138,IS531,1,Experiment,None,
a69cce96-be7e-4be7-9d98-fb641e9b458c,IS532,1,Experiment,None,
588cf6f6-2201-4cc7-a689-50f6eeed880d,IS533,1,Experiment,None,
4f67eeb3-8539-43a7-8ab8-93c17bd2ceb4,IS534,1,Experiment,None,
e6b35108-11c7-4c43-bb3b-ad16c1b3a9a1,IS535,1,Experiment,None,
0c09f6f2-597e-413a-a13c-1c12d25b2f85,IS536,1,Experiment,None,
53d76c92-ca08-4c6a-b35a-5e5b52b38e13,IS537,1,Experiment,None,
d1d1d5ba-5090-4ddc-8632-50e6da8f27e8,IS538,1,Experiment,None,
18d04b02-b8a6-4816-8fcb-be4f89bd1d54,IS539,1,Experiment,None,
33e777cb-5b0c-4d89-b342-1e1509e5384e,IS540,1,Experiment,None,
4da10dd7-c820-4de9-8e65-16df5f1ad18d,IS541,1,Experiment,None,
3643de3e-ea94-4150-a5cf-11991f9414b0,IS542,1,Experiment,None,
e72958fc-e0f0-4c44-88ce-0637a119be8b,IS543,1,Experiment,None,
9f7e8356-00e9-4558-bda3-24771a6fdfa7,IS544,1,Experiment,None,
32e061b2-c9ea-4fc5-a36c-5cd861e0184f,IS545,1,Experiment,None,
bf395221-7a14-4f8f-babf-96e498635de3,IS546,1,Experiment,None,
56541609-c064-4195-8101-e0c3d752082e,IS547,1,Experiment,None,
e40a2386-cb09-46d9-a309-b0ab4fb35cbc,IS548,1,Experiment,None,
8c7fb1e0-2a4b-4a3f-a258-ce5a77c1691d,IS549,1,Experiment,None,
be85d847-e57e-47b4-bf32-d8c7c9ee4993,IS550,1,Experiment,None,
350f8176-d406-4b5b-b83b-17a47cc9e166,IS551,1,Experiment,None,
261d715d-06ca-4424-b1cf-ea5f68008d43,IS552,1,Experiment,None,
7ddf61ea-026a-4dcd-adfa-d4fb763ffd3f,IS553,1,Experiment,None,
620e0628-bcae-40e7-9c79-0d57df8d7d93,IS554,1,Experiment,None,
815b432d-c61a-4a1d-b43b-b6bea23ae07f,IS555,1,Experiment,None,
2a7d8409-1a40-4ea7-a718-02a1c13f8bfc,IS556,1,Experiment,None,
5dfce6d5-6be7-49c7-9cd3-35227770d8ef,IS557,1,Experiment,None,
f95f4c31-c78e-48c4-bffc-de8436196b78,IS558,1,Experiment,None,
b8b3bb1d-a9eb-4552-bd4d-da6467c66902,IS559,1,Experiment,None,
4fd8858f-b560-468a-996d-3a72adbfde0e,IS560,1,Experiment,None,
e95a88ae-1b77-4473-8862-8f683f9208ac,IS561,1,Experiment,None,
3df63c42-3e06-444d-a332-d3ed0d8e18e4,IS562,1,Experiment,None,
5a0c175f-cdcb-4617-8ca8-bdd63db12586,IS563,1,Experiment,None,
03d28e65-78a6-4337-802c-55a119b2a958,IS564,1,Experiment,None,
158a0f84-c364-4675-af6f-1acbcc54149e,IS565,1,Experiment,None,
d71167c4-6bda-4b3f-990c-a52dacdb3abf,IS566,1,Experiment,None,
3d2eddfc-2b80-4f37-86da-79216a37540b,IS567,1,Experiment,None,
5d87732b-e743-45c4-8eaa-7177fe50be07,IS569,1,Experiment,None,
eab6063a-e735-4909-a88e-1264b7b2963e,IS570,1,Experiment,None,
5bcd6b1b-b3de-4f79-bd5b-d003882f96e3,IS571,1,Experiment,None,
5cc60d03-5db0-48b5-8bcd-5f36a99d284f,IS572,1,Experiment,None,
9ef1054f-98a3-4b2e-b48c-ce62298b5fc2,IS574,1,Experiment,None,
bfb9e803-2e08-42e5-97c3-d17fac1cb54c,IS575,1,Experiment,None,
6fb17469-a72f-4896-b8d4-400f31dc0463,IS576,1,Experiment,None,
e99b19ef-335c-4449-90e8-d6631d1c53b8,IS577,1,Experiment,None,
be9b938c-a12f-4677-b1d7-98c26f17554c,IS578,1,Experiment,None,
6284018d-8473-4537-95a0-7a09d2d2e594,IS579,1,Experiment,None,
82bc0d27-1df7-4432-a298-8de79d4377a4,IS580,1,Experiment,None,
3afa51ac-d627-42fe-86a1-4b576db94dd1,IS581,1,Experiment,None,
9ab33adb-9fa4-4062-8150-9b642107596d,IS582,1,Experiment,None,
b66c59e8-c435-4825-a4d7-71bdd186692e,IS583,1,Experiment,None,
980c6165-696e-407f-a7d6-dd46a1214096,IS584,1,Experiment,None,
f8b03119-7063-4290-972c-a269d3698b63,IS585,1,Experiment,None,
7c847729-b576-462d-b7da-4045460ccb6c,IS586,1,Experiment,None,
ca1b0aca-1ddd-4df7-91a8-2248ae503a07,IS587,1,Experiment,None,
f697cd1d-9e75-4e1f-9814-f26a8f4cd7a6,IS588,1,Experiment,None,
4bacfde5-2145-410a-a1f7-defa712452f3,IS589,1,Experiment,None,
f21b1f7b-2350-4b61-a23f-d43da3ae47d9,IS590,1,Experiment,None,
dcace439-6600-4bfb-a39f-b29a48a8e762,IS591,1,Experiment,None,
acc0eb65-df03-4f54-8ce9-08c688d5249a,IS592,1,Experiment,None,
41b2fa17-5001-4691-aed9-9a16a3c09e5a,IS593,1,Experiment,None,
03e94919-493e-45e9-9ba4-802455358205,IS594,1,Experiment,None,
0b19a40a-83b6-4083-906c-ba7ec5224cde,IS595,1,Experiment,None,
7f1c1230-b0f4-4d63-8ce3-9db5f06bfa1a,IS596,1,Experiment,None,
d309f625-4168-4f91-a140-84c5e8c1dee7,IS597,1,Experiment,None,
129e11e5-8c15-4337-a634-cf078475eabe,IS598,1,Experiment,None,
0729d5a0-c595-45c8-b1b4-b645e9bc3630,IS599,1,Experiment,None,
e4664713-3665-4b42-94cb-471a4f434c45,IS60,1,Experiment,None,
ffea3c54-3322-4839-824e-31a17c1d55e1,IS600,1,Experiment,None,
ef36b467-9d2d-439a-9a78-b2903fa2b25f,IS601,1,Experiment,None,
3b059851-0fef-4500-87fb-74393f314e04,IS602,1,Experiment,None,
c97086db-a8fd-43d4-b63c-94fb6a36455f,IS603,1,Experiment,None,
f47bb882-6241-4dca-9a6f-037c69d214db,IS604,1,Experiment,None,
a9bcc0cc-1d22-4ac1-a068-655e08714c77,IS605,1,Experiment,None,
22e3578b-7c3d-4209-bc04-796df943d758,IS606,1,Experiment,None,
6c06d568-65d3-4749-adaf-20014b5854f3,IS607,1,Experiment,None,
06304eed-250a-4b3c-b723-61b954276a71,IS608,1,Experiment,None,
ef9cc752-34cf-4e80-afe1-10991a22b3d0,IS609,1,Experiment,None,
fda329a8-3e1b-4b81-bd7a-fd165d47b4f2,IS610,1,Experiment,None,
e92b5565-7e28-4410-a5d3-7e95132c81a0,IS611,1,Experiment,None,
09d47f9b-3844-47d8-b966-77aeba81bedc,IS612,1,Experiment,None,
364c633c-8a43-4169-9d75-5ba806db297c,IS613,1,Experiment,None,
350883cd-8375-4b10-8305-e780098d3e79,IS614,1,Experiment,None,
61f17c2b-9a77-433b-86d6-13cad90c3559,IS615,1,Experiment,None,
79b77aa7-e6af-4fef-8b42-ef2670ae00d4,IS616,1,Experiment,None,
8efc15f8-b61d-4d82-97a8-becff62af198,IS617,1,Experiment,None,
2e79e27c-bf8d-4595-bf4d-cefd3e6add3f,IS618,1,Experiment,None,
f80b9de2-e401-43fa-a8bf-0760cc10fad2,IS619,1,Experiment,None,
e0140d66-c33d-43a3-a681-c0938a439858,IS620,1,Experiment,None,
88eed17b-094e-415c-8029-db697a36f837,IS621,1,Experiment,None,
d899667b-2653-4dd5-85f1-51543b38639e,IS622,1,Experiment,None,
13f768db-53db-40d9-91b4-5af395f13327,IS623,1,Experiment,None,
796542f1-e6f6-49be-a5c7-a2f950c04f38,IS624,1,Experiment,None,
f03da6c6-2f1a-4f41-a61b-5d2ff3a79fe4,IS625,1,Experiment,None,
eeef4f7f-8108-4683-9c54-eb95136cff9a,IS626,1,Experiment,None,
b6c1b81c-d580-40c6-b7e8-1633a024409b,IS627,1,Experiment,None,
11cc8061-d6e4-4bfc-ae24-e7f3de46dffc,IS628,1,Experiment,None,
c51a5c34-c61e-4b31-b95a-cd7a45bf4e6f,IS629,1,Experiment,None,
21164523-d38c-45cc-bb89-26416da56500,IS630,1,Experiment,None,
86ac9ecb-ead6-4944-ad65-88ddbb810371,IS631,1,Experiment,None,
7fbfe0b0-c191-4a69-8d1d-c6047d6d5a2f,IS632,1,Experiment,None,
c9cab844-6d40-4824-a192-caa50b0c0cd0,IS633,1,Experiment,None,
d97a5760-0dff-4229-854b-2569d83a7b64,IS634,1,Experiment,None,
64e8ef0b-911b-4a45-b92c-3637ed2f43c3,IS635,1,Experiment,None,
2dac84e3-1ede-4689-a01c-f4ea07869e4a,IS636,1,Experiment,None,
9f25fcd4-a1d7-4cb6-ab79-a3d564f450a2,IS637,1,Experiment,None,
b5b3672d-d0d2-4db6-ad81-d84650b0e54a,IS638,1,Experiment,None,
f0d814cf-a1fb-4447-b238-e786a6849b58,IS639,1,Experiment,None,
e56a9427-0985-413b-88c8-de0f0917ff7a,IS640,1,Experiment,None,
156b440f-f4fd-467a-99f4-9ae2e3c79569,IS646,1,Experiment,None,
0c3df764-9421-486f-8fd2-b0313cd940ba,IS647,1,Experiment,None,
9613cf30-5bca-46c4-833d-6afa124590ef,IS648,1,Experiment,None,
c1297137-7ad8-447a-a541-bc3f0f758a24,IS649,1,Experiment,None,
8cbdc2af-aaa4-4b46-86ef-5a4ab4a11a92,is650,1,Experiment,None,
bff30266-42f5-47b3-84a9-8edbbdced7eb,IS651,1,Experiment,None,
40c3528f-f2f3-4ea0-a78f-32a17ce63ee7,IS652,1,Experiment,None,
5d2bada7-84e8-42b6-8345-78dbdd5c9da1,IS653,1,Experiment,None,
6b27bc73-43a3-4b43-99d2-7d0c29831daa,IS654,1,Experiment,None,
f819c298-e529-4167-b5ca-1199e45b4d00,IS655,1,Experiment,None,
a18bd064-a922-4096-bc1a-0d9024e20394,IS656,1,Experiment,None,
ef4cf918-851f-4853-a1ec-993f7191aa44,IS657,1,Experiment,None,
44eeae2c-787a-4c01-9c79-39dd3f7fcae9,IS70,1,Experiment,None,
6e5dc7de-e442-420e-a433-76240bf5e1b6,IS80,1,Experiment,None,
059bf8bf-49b1-4b29-b4b8-9369e1f815f5,IS81,1,Experiment,None,
26a66f60-8472-42a4-bad2-bed46b3505ad,IS82,1,Experiment,None,
84ee2cb2-1562-4dda-8372-c4c071e4c82b,IS83,1,Experiment,None,
6bdd7264-301f-4704-ba1c-24e961c3a1fa,IS84,1,Experiment,None,
bc07985e-45de-424d-b5ac-58a9da79db02,IS90,1,Experiment,None,
2e9a1301-e043-4fa3-8540-d4c4dd55d155,ISOLDE,1,Experiment,None,
fcf4a8b2-9680-447a-b508-5d244376bfad,ISOLDE COLLABORATION,1,Experiment,None,
2ade6317-2ed5-4c14-9cad-1d2ce220f84b,ISOLDE2,1,Experiment,None,
7e5f1d1e-6013-4f1b-8a85-f44ed334c73c,IT,1,Department,IT,
e552e0a2-4f59-d640-93ec-df601310c733,IT Consultants,1,FE,IT,reguero
62bf3d80-e971-d540-adf9-3f25a98a68bb,IT Department Head Office,1,FE,IT,hemmer
ea56fb0b-0a0a-8c0a-00b9-41790d1b792c,IT DPO,1,FE,IT-DI,isnard
97cd3931-db9e-e3c0-f995-6184da9619b4,IT e-learning,1,FE,IT-CDA-IC,dimou
,,,,,tbaron
25d2d844-1cd8-3500-4baf-56233e3aa2be,IT General Support,1,FE,IT-CF-SM,pocock
ea56fac9-0a0a-8c0a-01f0-37207e926303,IT Secretariat,1,FE,IT-DI-SE,jtolomeo
,,,,,ndelvica
11940016-0a0a-8c08-00fd-39271933edd5,Indico,0,FE,IT-CF-SM,
ea56fb21-0a0a-8c0a-015a-591ddbed3676,IT Service Management Support,1,FE,IT-CF-SM,pocock
73ff031e-1b7b-94d4-aa9a-642abb4bcb2f,IT Services Business Continuity,1,FE,IT,iven
,,,,,meinhard
26a7935d-db5f-5010-b805-94dadb961912,IT Services Costing,1,FE,IT,mccance
,,,,,timbell
eba8b222-0a0a-8c0a-00c2-e5397c373fda,IT Services Support,0,FE,IT-CF-SM,
a8ab2844-01e9-2840-9878-151822750d61,IT Website,1,FE,IT-DI,eduardoa
,,,,,noble
701017bb-db98-fb00-269d-9e24db9619d2,IT-DB Automation Tools,1,FE,IT-DB-IA,akhodaba
,,,,,baparici
,,,,,icoteril
,,,,,pfs
b6e171e3-f04f-dd00-7904-6a0c9f0e01a0,IT-PES Statistics,0,FE,IT-PES,
ea29e966-98de-7400-6d21-708547f4c6c6,IT/CS Technicians,1,FE,IT-CS,apascal
,,,,,scaseno
33771f3d-2423-f000-7904-c39b27667a37,Itprocos vendor,1,FE,IT-CF-FPP,boissat
,,,,,ebonfill
49b65e21-b510-7100-7904-5e3417224b39,Jabber,0,FE,IT-CDA-IC,
6d0413bd-fd7c-6900-7904-3c7edc1a53f2,Java Web Infrastructure,0,FE,IT-DB-IMS,
a6421036-0191-6440-9878-151822750d0f,JIRA Issue Tracking,1,FE,IT-CDA-WF,awagner
,,,,,eduardoa
,,,,,kolodzie
ea571454-0a0a-8c0a-00fc-f4bbeb7aa33c,JMT,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
34c04ae9-8568-41c7-ab01-fe2b84bef085,JUNO,1,Experiment,None,
54459568-52d5-47a5-bf42-09f0dda47fe4,KASCADE,1,Experiment,None,
897bd15d-38dd-4822-8521-5e8088dbbd28,KATRIN,1,Experiment,None,
00be1dfc-fc37-c500-7904-2318fd702efb,KB import default,0,FE,IT-CF-SM,
ea56ee98-0a0a-8c0a-0192-fb460b4f9a80,Kitchen Equipment,0,FE,SMB-SE-HE,martelc
ea571840-0a0a-8c0a-0197-33694785d10f,Kitchen Equipment - CC Dalkia,0,FE,SMB-SE-HE,martelc
8b89d337-dbdc-7b00-f2d6-65925b961959,KITRY/NAMA,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
288d8fb8-c834-4577-a820-d9085944b7a9,KM3NeT,1,Experiment,None,
98971b7d-2423-f000-7904-c39b27667a47,KNURR vendor,0,FE,IT-CF-SAO,
ea570f59-0a0a-8c0a-0069-525ae0249d68,KTP,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
3ab75bbd-2423-f000-7904-c39b27667acc,Kvm vendor,0,FE,IT-CF-SAO,
07c27ed7-2e19-48c5-8e3f-c40b50ed09f1,L3,1,Experiment,None,
3be195cf-aae4-4581-991f-5002a661c475,LAA,1,Experiment,None,
c7a72798-4fae-5600-b8cb-ca1f0310c767,LabVIEW Support,1,FE,BE-CEM-MTA,cedric
,,,,,hreymond
,,,,,jtagg
,,,,,oddoa
ea56f46a-0a0a-8c0a-013c-f49973778cb2,Language Training,1,FE,HR-LD,mosselma
,,,,,ndumeaux
10c3d4b5-7b76-48db-9aaf-abaabeca777a,LANNDD,1,Experiment,None,
d27fb39c-1b1e-2010-29c2-744a9b4bcb4b,Laptop Rental Service,0,FE,IT-CDA,gmetral
,,,,,tjs
4ff3b761-4f59-8b40-b9cc-09de0310c72f,Laser Protection systems,1,FE,EN-AA-CSE,fchapron
,,,,,mmunoz
,,,,,ninin
,,,,,rnunes
338c0bb4-b1a7-44d9-8909-36231a69d0ef,LBNF/DUNE,1,Experiment,None,
ea56eac4-0a0a-8c0a-0186-d2c431887add,LC Agenda,1,FE,IT-CDA-IC,pferreir
,,,,,tbaron
9e9e8f36-0ac0-4dec-b0a3-2b1c8f7395aa,LCD,1,Experiment,None,
4127efd8-87c3-4730-bc09-49b35206d99e,LCG,1,Experiment,None,
ea56faf3-0a0a-8c0a-00a4-d0be0df29ae3,LCG Project,0,FE,IT-DI-LCG,
ea56f416-0a0a-8c0a-0098-3f0f47d10f1a,Leadership Training,1,FE,HR-LD,ndumeaux
1ea228fe-4f35-5700-06bc-10ee0310c714,Learning Hub,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
ea56f607-0a0a-8c0a-00d2-603c701fdb4f,Leave and Absence,1,FE,HR-CBS-B,ygrange
ea56eb0d-0a0a-8c0a-018e-9369ed16d74d,Lecture Recording,1,FE,IT-CDA-IC,tbaron
ea56f3a1-0a0a-8c0a-01c8-8aecf041db96,Legal Advice,0,FE,HR-DHO-LA,rbuquicc
7966faf6-606b-498a-b6e2-eb0ec527bb2e,LEP5,1,Experiment,None,
78ad8b70-a201-4dfa-9a52-976011010946,LEP6,1,Experiment,None,
ea5701ba-0a0a-8c0a-01ef-8b2ac94882d5,LFC,0,FE,IT-PES-PS,
ea56fe5c-0a0a-8c0a-01a6-95578d349294,LFC Development,0,FE,IT-SDC-ID,
686da915-220d-4bd6-b73b-7f791fd3cea8,LHCB,1,Experiment,None,
7d0aae00-04f2-5dc0-adf9-793734b40e78,LHCb Computing Support Meyrin,1,FE,EP-LBC,joel
,,,,,lbrarda
,,,,,neufeld
23b600df-6954-6540-adf9-2ce5ed422308,LHCb Core software,1,FE,EP-LBC,joel
58b438fe-dba4-6740-ae65-9ec4db961976,LHCb Fence framework Support,1,FE,EP-LBC,joel
6d08abf0-7cd3-e1c0-adf9-15f17b561998,LHCb Offline Operations,1,FE,EP-LBC,joel
c9292a8c-04b2-5dc0-adf9-793734b40e5b,LHCb Online Fabric,1,FE,EP-LBC,joel
,,,,,lbrarda
,,,,,neufeld
adfca648-04f2-5dc0-adf9-793734b40e8d,LHCb Online Operations,1,FE,EP-LBC,joel
,,,,,lbrarda
,,,,,neufeld
e3b944c5-5d65-7900-6d21-1f86c6a31e54,LHCb online ST,1,FE,EP-LBC,joel
,,,,,neufeld
ccfdea0c-04f2-5dc0-adf9-793734b40eef,LHCb Online Web-services,1,FE,EP-LBC,joel
,,,,,lbrarda
,,,,,neufeld
1d86c0cb-dbcb-5340-ce6e-9c09db961922,LHCb Secretariat,0,FE,EP-AGS-SE,
3815658f-450f-3100-adf9-4a461508ba1f,LHCb VOBOX support,1,FE,EP-LBC,joel
,,,,,jost
1f048654-fb3d-4f20-a1ef-79789d5ee3db,LHCF,1,Experiment,None,
c7f5cb5f-ff1d-4f37-baea-92749b2ed719,LHeC,1,Experiment,None,
ea56f058-0a0a-8c0a-0144-e626c2f2bf40,Library Acquisition And Collection Management,1,FE,RCS-SIS-LB,basaglia
9f290d92-94c1-7880-6d21-20312bb3b62d,Library apprentices,1,FE,RCS-SIS-LB,agentilb
,,,,,basaglia
ea56efb6-0a0a-8c0a-00f7-f25f37350685,Library Distribution,1,FE,RCS-SIS-LB,basaglia
ea56ef5b-0a0a-8c0a-003e-696cc59b8fcc,Library Events and Sales,1,FE,RCS-SIS-LB,basaglia
ea56f02d-0a0a-8c0a-0072-1bae3dde7e5d,Library Loan,1,FE,RCS-SIS-LB,basaglia
ea56f019-0a0a-8c0a-005d-e8d04db2699d,"Library processing, bibliographic data",1,FE,RCS-SIS-LB,basaglia
ea56ef77-0a0a-8c0a-01b3-a4e291502519,Library Terminal,1,FE,RCS-SIS-LB,basaglia
ea56f042-0a0a-8c0a-01b2-d3a869cca445,Library-assisted literature and citations search,1,FE,RCS-SIS-LB,basaglia
56fcbf64-dbf5-5450-b805-94dadb9619ef,Lift maintenance,1,FE,EN-HE-HEM,dlafarge
363d99d1-62a7-4076-8101-f2a60bd49d8a,LIL,1,Experiment,None,
4b1ca2ee-1b6b-2090-6fe5-a7d23b4bcbde,LimeSurvey,1,FE,IT-CDA-WF,awagner
,,,,,eduardoa
f4b797bd-2423-f000-7904-c39b27667a62,Lindy vendor,0,FE,IT-CF-SAO,
c19d4c2a-4602-4b64-b78c-672abba8ea9e,Linear Collider Detector,1,Experiment,None,
925a2e34-4f8c-ae00-b9cc-09de0310c794,Linux HPC Infrastructure,1,FE,IT-CM-IS,hoimyr
,,,,,mccance
ea56e8ec-0a0a-8c0a-0101-1811e96a9185,Linux License Server,0,FE,IT-CM-IS,
0fd95138-6960-2940-adf9-2ce5ed4223f6,Linux Software Building,1,FE,IT-CM-LCS,airibarr
,,,,,manuel
,,,,,schwicke
ea5700af-0a0a-8c0a-0158-9ba13dc310bc,Linux Support,1,FE,IT-CM-LCS,airibarr
,,,,,manuel
,,,,,schwicke
9c13c839-38df-455e-8443-da9513092288,LISA,1,Experiment,None,
7c93e9b8-4f91-9240-93ec-df601310c776,Locations Application,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea571a08-0a0a-8c0a-0176-bab7a9f9b54a,Locks and Keys,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
b8dd87ee-65df-7000-c713-3077c4af33ef,Locksmith,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
ef060cfa-1b58-d410-c917-65b0ab4bcb04,Logistics of PPE to protect against COVID-19,1,FE,SCE-SSC-SC,cgarino
f0c24226-dbab-1414-f4c8-949adb9619d5,LoRaWAN,1,FE,IT-CS,gcancio
,,,,,rsierra
,,,,,tnt
e48638c6-1956-9900-7904-0003aab1c96a,"Lost, Theft And Found",1,FE,SCE-SMS,dconstan
,,,,,hmontane
b36449d3-95d9-4c1c-8efb-98a98d2d2df2,LPM,1,Experiment,None,
af1ef625-db56-ab80-7c55-9eb3db96190f,LS2 Controls Interventions Coordination,1,FE,BE-CSS,hatzi
,,,,,marine
87e914fa-dbb5-eb80-7c55-9eb3db9619f7,LS2 Coordinator for Electrical Power Converters,1,FE,SY-EPC,dnisbet
7605f70d-4f16-97c0-b9cc-09de0310c73a,LS2 Storage Management,1,FE,TE-VSC,chigg
,,,,,riddone
34357f0d-4f16-97c0-b9cc-09de0310c7e6,LS2 Storage Operation,1,FE,TE-VSC,elclerc
,,,,,riddone
5031b899-dd95-1180-7904-7721dbdf3bc9,LSF Authentication,1,FE,IT-CDA-IC,amonnich
,,,,,ptedesco
,,,,,tbaron
ea5702c0-0a0a-8c0a-01da-80659d93cfb5,LXADM,0,FE,IT-PES-PS,
ea56e91b-0a0a-8c0a-0010-54a58b63b8f4,LXBATCH,1,FE,IT-CM-IS,bejones
,,,,,mccance
23cfdd9f-0a0a-8c08-01f6-e062f0d5a1b7,LXBATCH internal,0,FE,IT-CM-IS,
ea57023a-0a0a-8c0a-00c6-5d50c249662c,LXCLOUD,0,FE,IT-PES-PS,
ea56e8d8-0a0a-8c0a-018b-977bc6160697,LXParc,0,FE,IT-PES-IS,
ea57014c-0a0a-8c0a-0085-6e6eb36f5e6a,LXPLUS,1,FE,IT-CM-LCS,manuel
,,,,,psaiz
,,,,,reguero
,,,,,straylen
23d0ef9d-0a0a-8c08-01bb-3bc8145d016e,LXPLUS internal,0,FE,IT-PES-PS,
ea56ffde-0a0a-8c0a-0167-050804332fef,Mac Support,1,FE,IT-CDA-AD,mkwiatek
,,,,,schroder
1fcf20ec-5ed6-4040-9e5e-d9ca7ceac674,MACFLY,1,Experiment,None,
74da2969-9465-ed00-7904-7129f337bffa,Machine building renovations,0,FE,SMB-SE-CEB,fmagnin
4a3e71fe-4fc8-e240-b9cc-09de0310c742,Machine Control Coordination,0,FE,BE-CO,ikozsar
171623c4-1bae-00d0-c917-65b0ab4bcb1e,Machine Tools,1,FE,EN-MME-MA,fbailom
,,,,,pnaisson
,,,,,satieh
365c7751-db6f-4182-b733-c89b8378122b,MadMax,1,Experiment,None,
a02788c1-0f4a-4e87-8ddb-2d9235a3006f,MAGIC,1,Experiment,None,
ea5718bf-0a0a-8c0a-01d9-23b49c5eb3fe,Mail Office,1,FE,SCE-SSC-LS,elclerc
,,,,,pmuffat
e641de6a-b437-a040-6d21-53f17c76edd4,Maintenance Management Project (MMP),0,FE,DG-DI-DAT,bordry
c925e8b4-4f12-76c0-fe13-2eff0310c77c,Manutention operation,1,FE,SCE-SSC-LS,elclerc
,,,,,pmuffat
7b72a20f-dbc3-7344-71b0-f90532961996,MapCERN mobile application,1,FE,SCE-SAM-TG,yrobert
ea56f51c-0a0a-8c0a-01f6-ecf3375aabf0,Marie Sklodowska-Curie Programmes,1,FE,HR-TA-SGR,ischmid
ea570846-0a0a-8c0a-003d-8e7d3fff8eee,Market Surveys,1,FE,IPT-PI,gregorio
ea5707cf-0a0a-8c0a-0163-8597e8cf8e43,Material Contract Management,1,FE,IPT-PI-RC,gregorio
ea56e8b7-0a0a-8c0a-0105-127f7e1dc770,Mathematics tools,1,FE,IT-CDA-AD,lgunther
,,,,,mkwiatek
dcb290b9-4fb7-6600-7db7-d3ef0310c77a,Mattermost,1,FE,IT-CDA-IC,amonnich
,,,,,ormancey
,,,,,pferreir
,,,,,tbaron
ea570138-0a0a-8c0a-019d-c601e42f4b18,Mechanical Engineering tools,1,FE,IT-CDA-AD,ifans
,,,,,lgunther
,,,,,mkwiatek
ea57000a-0a0a-8c0a-01bd-43c5313bacb2,Media Archive Processing,0,FE,IT-OIS-WLS,
ea57177f-0a0a-8c0a-0019-b41a5276ca78,Medical Consultancy,1,FE,HSE-OHS-ME,delamare
,,,,,modossan
ea571794-0a0a-8c0a-01f7-362df7290d63,Medical Laboratory,0,FE,GS-ME,iauvigne
ea5717ab-0a0a-8c0a-00e4-c82eb4116e3f,Medical Secretariat,1,FE,HSE-OHS-ME,delamare
,,,,,modossan
9f484517-0b2f-4021-90b1-7303ac793174,MEDIPIX,1,Experiment,None,
c97183d1-59ed-47ad-96a9-03da486157d5,MEDIPIX 3,1,Experiment,None,
e3faaa6a-c3e3-4ce6-9220-a2f945d9ca75,MEG,1,Experiment,None,
a2d75bfd-2423-f000-7904-c39b27667a3c,Megware vendor,0,FE,IT-CF-SAO,
18d7d3fd-2423-f000-7904-c39b27667a94,Melrow vendor,0,FE,IT-CF-SAO,
ea56edde-0a0a-8c0a-0004-c2e596f8f336,Menuiserie - CC Costa,0,FE,GS-SE-CWM,fmagnin
ea570c27-0a0a-8c0a-0010-7aa625895786,MERIT tool,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
cac8ae25-f8f5-8d00-adf9-02fd20678c09,Message Broker for EGI,0,FE,IT-SDC-MI,
5cfbb76e-0a0a-8c08-015d-8d742f9de1a5,Message Broker Infrastructure,0,FE,IT-CM-MM,
ad161be2-4fe9-1e00-93ec-df601310c7f9,Messaging,1,FE,IT-CM-MM,aimar
,,,,,cons
ea56fce7-0a0a-8c0a-00bd-7ff17a7eb5d3,Messaging Software Development and Customization,0,FE,IT-CM-MM,
9a1f93ac-4f0b-8300-77fe-29001310c7d8,Metallic Structure,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
a87270d2-11d9-e500-adf9-ea91634bc677,Metallic structure maintenance,0,FE,SMB-SE-CEB,fmagnin
93217cde-1199-e500-adf9-ea91634bc6d5,Metallic structure works,0,FE,SMB-SE-CEB,fmagnin
39969f85-5db5-7d40-6d21-1f86c6a31e1d,Method's office and maintenance management,1,FE,EN-CV-GEM,akjaeran
37c793fd-2423-f000-7904-c39b27667a30,MGE vendor,0,FE,IT-CF-SAO,
ea5713fa-0a0a-8c0a-0048-a668091ccd1d,Micado,0,FE,GS-ASE-EDS,fanou
,,,,,kkrol
aa80c785-23e1-48ad-aa11-f388a29dc976,MICE,1,Experiment,None,
f6a18c37-1dd5-b140-c713-8e7019d56a3d,MICE Procurement,0,FE,IPT-PI-RC,fnajeh
c582655e-082b-45c4-adf9-6d3d8fa18a99,Microcosm,0,FE,IR-ECO-PE,moret
1abfa918-2088-40bf-b3a0-48829adafffe,microScint,1,Experiment,None,
ad3f3060-4fcf-0280-4b4a-bc511310c77e,Microscopy and Mechanical Tests,0,FE,EN-MME,bertine
3187177d-2423-f000-7904-c39b27667a0e,Microsoft vendor,0,FE,IT-CF-SAO,
98f0ce7c-4f7b-7a80-b9cc-09de0310c79f,Middleware technologies,1,FE,BE-ICS-CE,eblanco
,,,,,fchapron
,,,,,solp
b3bdc7e1-db89-8454-71b0-f90532961978,Milestone Tracking Application,1,FE,FAP-BC-FIN,bdaudin
,,,,,dtsekour
,,,,,mcmarque
9e0a9e93-0a0a-8c0a-0107-2f4a76c1247e,Minor Repair Works - CC FSU,0,FE,GS-SE-CWM,fmagnin
46d92714-ff73-4b07-b648-ad33a0611254,MINOS,1,Experiment,None,
1265691c-4f41-9bc0-d7b5-0ebf0310c734,Minsky Project,0,FE,EP-LBC,
850b4880-4f87-6200-b9cc-09de0310c778,Mobile dewar management,0,FE,TE-CRG,dimitri
ea56f7a4-0a0a-8c0a-00e7-63ecbb3d2e0c,Mobile Telephony,1,FE,IT-CS,araczyns
,,,,,gcancio
,,,,,rsierra
e5b16741-4f8e-aa40-b9cc-09de0310c7d4,Mobility Operation Coordination,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
ea571329-0a0a-8c0a-00ab-6824c282d072,MoDESTI Support and Consultancy,1,FE,BE-CSS-CSA,fchapron
,,,,,mbraeger
,,,,,widegren
c4ce278d-8858-4096-8278-3d33020b093d,MoEDAL,1,Experiment,None,
b18bef2e-4f2d-1e00-93ec-df601310c739,Monitoring,1,FE,IT-CM-MM,aimar
,,,,,prodrigu
ea56f6fb-0a0a-8c0a-0179-b585e0b4b7f7,Monitoring tools,0,FE,IT-CM-MM,
b76506b8-4fbb-7a80-b9cc-09de0310c733,MOON,1,FE,BE-ICS-CE,eblanco
,,,,,fchapron
,,,,,solp
a743195f-8429-3880-c713-6646aaa7ce4c,Multifunction Copier Support,0,FE,IT-OIS-DS,
e993ff17-0a0a-8c0a-01e0-36015c55c628,MultiMedia Archive,1,FE,IT-CDA-DR,jbenito
,,,,,nitarocc
ea570398-0a0a-8c0a-005c-5a7ecbe5cdcc,MyProxy,1,FE,IT-CM-IS,mccance
06db7881-b1d2-47c3-a820-3fe2f47779a7,NA1,1,Experiment,None,
21e3f80d-6d5f-420f-8a7e-fdec75bc8baa,NA10,1,Experiment,None,
4973350d-37f9-44f7-b9c7-461f89aeb856,NA11,1,Experiment,None,
d18cbca8-2759-463d-b585-b15e57cdbdd1,NA12,1,Experiment,None,
d1b40a7a-a348-406d-afe2-c0138f3e07c5,NA12/2,1,Experiment,None,
0965342c-99fc-4858-b45b-63b5e4f1d271,NA120,1,Experiment,None,
2c76e799-57ab-4018-b59b-219d32d05fa5,NA13,1,Experiment,None,
472b6328-1e6d-4b1e-9176-0d4e46c01e4b,NA14,1,Experiment,None,
21a1cc0a-87f7-4dc7-b97f-41c745e84600,NA14/2,1,Experiment,None,
28dc8102-a5e3-48a6-a857-5b9ffec0c42c,NA15,1,Experiment,None,
e1930596-0d6e-4af1-b1ea-b5d3057ac546,NA16,1,Experiment,None,
c5b52a0f-929a-4e50-890f-d70b1d38a9fe,NA17,1,Experiment,None,
e6dc2cfe-0c60-473a-a060-82353a3c9494,NA18,1,Experiment,None,
3eddc99a-c56b-4ff0-b7f6-322a730c70b1,NA19,1,Experiment,None,
e7e228ce-8bfb-48ce-8881-425785f806d8,NA2,1,Experiment,None,
91684b89-911d-4dd4-802b-5bc7bc7cdb39,NA20,1,Experiment,None,
af170d72-bfbe-4fe0-b05a-08faf40ad81c,NA21,1,Experiment,None,
f2801e99-ee51-4f3c-a19f-339b0a9406ed,NA22,1,Experiment,None,
1841af8a-32e2-4547-955e-bf459e4b3ac9,NA23,1,Experiment,None,
f9dcf213-305a-4f8e-9ef3-59b5e6f0fa7e,NA24,1,Experiment,None,
8b68356b-3f48-446e-b576-5d3222bece55,NA25,1,Experiment,None,
5ed66bc9-2891-4e61-92d9-dea40c3161cd,NA26,1,Experiment,None,
69d2a0af-ce79-4807-bd3c-fdee18d0c368,NA27,1,Experiment,None,
d439e3fd-bc07-434d-862f-4abfcb4e164d,NA28,1,Experiment,None,
9b8c8915-c210-4f86-96da-59c60b3b478f,NA29,1,Experiment,None,
573164fc-5a8f-4df2-bd56-6b5a0e10b39e,NA3,1,Experiment,None,
ec28f7c4-511f-4d51-9deb-2fa748f8c550,NA30,1,Experiment,None,
53da9cad-a72f-4a49-950f-d7b24472f587,NA31,1,Experiment,None,
137ff8c3-8bb1-45d2-833e-fbfcedc5d74b,NA31/2,1,Experiment,None,
4954d9d1-ce1f-4418-bccf-7ef59f6b2c3b,NA32,1,Experiment,None,
99e68f53-9e20-455f-85f5-96c6d5409c70,NA33,1,Experiment,None,
e3fd41de-9095-4e5b-834b-ee72fb22cc28,NA34,1,Experiment,None,
ba1f3224-4c29-4818-97ac-bc1dd39190be,NA34/2,1,Experiment,None,
93ea01cf-f29e-4a75-8018-75dcfa2bf87f,NA34/3,1,Experiment,None,
0e5b2cfb-6b3c-4048-81b7-d995b1545e39,NA35,1,Experiment,None,
107db26e-ba82-499a-9396-591fafde2074,NA36,1,Experiment,None,
5aa5ead9-b90a-4745-ac88-e9c3e89338e9,NA37,1,Experiment,None,
d9d08f65-8076-4f44-8215-d4e1a7b0106d,NA38,1,Experiment,None,
f670ead7-b73a-438e-8c37-d49e95b5436f,NA39,1,Experiment,None,
01e5b0be-d303-4254-99bf-7d409fcb2c08,NA4,1,Experiment,None,
3a30a5d9-e44a-4ec8-8851-d7676ee5c5bd,NA40,1,Experiment,None,
26fa75de-56cf-4c4e-a724-528f3970e352,NA41,1,Experiment,None,
7758ef5b-bdf7-40fd-b861-f6b85c430642,NA42,1,Experiment,None,
0123ce43-4d01-4d08-a5d5-b8ae0af10fec,NA43,1,Experiment,None,
f883e7f7-b008-4836-b504-9ab4d0a3234b,NA43/2,1,Experiment,None,
59c1b5d7-0b80-4d10-9cca-45a37dcbea79,NA44,1,Experiment,None,
963a90a1-ea3c-4804-9177-41d0d404abb3,NA45,1,Experiment,None,
bd4ec8d8-aba2-4a6d-a315-c8b326ef76ce,NA45/2,1,Experiment,None,
0bd765df-968f-411d-9467-e29370a6b131,NA46,1,Experiment,None,
4663e2cb-cfa9-491c-9a34-24493c3c4f7d,NA47,1,Experiment,None,
1d5c8c04-d8ee-4b31-b53b-b7c04301b0da,NA48,1,Experiment,None,
ecca0845-d876-42f1-b3b0-594a7d2e8115,NA48/1,1,Experiment,None,
8cd59a6f-d696-4e08-a045-9c49cb1b291d,NA48/2,1,Experiment,None,
c9e04377-e1e8-445a-b4f3-161cc363016b,NA48/3,1,Experiment,None,
cecb1313-3778-44aa-b26f-6d4568fc6793,NA49,1,Experiment,None,
483568f1-7f2d-4332-b808-8fa2e9571d4a,NA5,1,Experiment,None,
b1e0d649-6f0f-4f43-b4d0-ef400449126e,NA50,1,Experiment,None,
c2948ce9-8b58-4fdd-8072-52da5b75d842,NA51,1,Experiment,None,
e622dee3-0928-4f53-8774-7826ba21dfb2,NA52,1,Experiment,None,
584192aa-a134-4aeb-9af2-d7a245566a94,NA53,1,Experiment,None,
ba6bf596-0ec3-42bf-94f8-cd7e96b42c12,NA54,1,Experiment,None,
645a29c9-302e-42d1-96b9-60f824d811da,NA55,1,Experiment,None,
79ff44cc-2da7-4404-b249-fe8214e71a36,NA56,1,Experiment,None,
9ea1a9c1-5358-49d8-8546-b5d8fb7dfa23,NA57,1,Experiment,None,
1bb61d94-b87a-4f7f-b2ed-bf486feb4d62,NA58,1,Experiment,None,
0ef827ad-73dc-41bc-ad65-9f0e35dd2960,NA59,1,Experiment,None,
6d0a974d-ce59-485d-aaf4-60c12bdccb5f,NA6,1,Experiment,None,
635ca4f3-6d52-42e9-a32a-e57f257b1334,NA60,1,Experiment,None,
fbbd05ee-a8f9-4c74-a108-8ea767debe63,NA61,1,Experiment,None,
94b424ac-4f0f-0280-4b4a-bc511310c7e1,NA61/SHINE Computing Operations,1,FE,EP-UFT,marek
,,,,,skowalsk
6b3056d6-c42b-4f44-8143-826f6b6af53e,NA62,1,Experiment,None,
2537ded6-f3a6-4610-a366-b8ce3a5e72ff,NA63,1,Experiment,None,
f869bac6-d870-4ffb-8d5c-07dcc3f9e34c,NA64,1,Experiment,None,
7bb16c6d-8748-4e5c-9f94-8149492804e9,NA65,1,Experiment,None,
b683648c-3fc4-4580-9822-4f372e1f9c4a,NA7,1,Experiment,None,
f0a05871-c967-480c-8dcc-03d30cba4324,NA8,1,Experiment,None,
ae2f883c-ddb1-45b9-97fa-214b962622d8,NA9,1,Experiment,None,
ea56f1e6-0a0a-8c0a-01b6-f293c97a9213,NCC Service Management consultancy,0,FE,GS-SMS,
6087df3d-2423-f000-7904-c39b27667ac3,Netapp vendor,1,FE,IT-CF,grossir
34c71fbd-2423-f000-7904-c39b27667a7f,Netapps vendor,0,FE,IT-CF-SAO,
ea56f847-0a0a-8c0a-01ac-ab1ec22fd223,Network and Telecom Deployment,1,FE,IT-CS,apascal
ea56f8a5-0a0a-8c0a-0106-c6f05dae55d0,Network Operations,1,FE,IT-CS,apascal
,,,,,dgutier
ea56f8e4-0a0a-8c0a-00dc-acdfdb6caeb8,Network Service Monitoring,1,FE,IT-CS,dgutier
,,,,,lefebure
ea56f88e-0a0a-8c0a-01f3-08a92493d694,Networking for Experiments,1,FE,IT-CS,apascal
ea2d93ee-a762-46dc-b231-7d09337040b7,"Networking for LHC (LHCOPN, LHCONE, IndiaLink, ESnet)",1,Project,None,
718669de-9451-6100-7904-7129f337bfe6,New machine projects,1,FE,SCE-SAM-CE,llopezhe
f2e76992-9491-6100-7904-7129f337bf9c,New technical systems for tertiary buildings,0,FE,SMB-SE-HE,martelc
b2576552-9491-6100-7904-7129f337bf03,New tertiary projects including technical systems and infrastructure,1,FE,SCE-SAM-CE,mpoehler
8a8c676c-dbb2-c090-f4c8-949adb9619a1,Newdle,1,FE,IT-CDA-IC,amonnich
,,,,,pferreir
,,,,,tbaron
26507e09-fcff-4900-7904-2318fd702e86,Newsagent (Kiosque � journaux),0,FE,SMB-SIS,lalejeun
,,,,,unnervik
bee375a3-09e6-4ecf-a23c-586bf4870e17,NEXT,1,Experiment,None,
971d55b8-4fe3-d700-7db7-d3ef0310c70d,Non-LHC Experiment Secretariats,0,FE,EP-AGS-SE,nknoors
84a75f7d-2423-f000-7904-c39b27667a89,None vendor,0,FE,IT-CF-SAO,
fac7effc-256d-4ead-941c-6355b30b4e96,NP01,1,Experiment,None,
258b5ab8-3b8b-454a-a823-488d9c984b01,NP02,1,Experiment,None,
709373b7-75b8-48bf-a614-283d32d9e4ba,NP03,1,Experiment,None,
979491f7-979c-4dd7-83de-9a5110c98360,NP04,1,Experiment,None,
acf741ce-4837-4deb-8772-f927e0e8396b,NP05,1,Experiment,None,
fe2f34b3-2236-451c-9921-a269ac7d809d,NP06,1,Experiment,None,
eec079d8-65aa-45a7-a39e-4bc8f2b2e86e,NP07,1,Experiment,None,
60b117f2-51c6-417e-8392-e46b958cdfef,NP4,1,Experiment,None,
4198e534-ad9e-41fc-b2b6-8c8ca035e987,NP5,1,Experiment,None,
f90fc9b8-8432-475b-b81c-2ec0c3ba14f8,nTOF COLLABORATION,1,Experiment,None,
eca96f45-277d-414f-bfcb-9506969cef5a,nTOF1,1,Experiment,None,
e8634602-8395-49bd-b875-7547ed522346,nTOF10,1,Experiment,None,
8307551a-c3e4-43a1-87ce-f469f200e6e3,nTOF11,1,Experiment,None,
dc5b4420-e7d5-4f54-977b-2f54b01c9e6c,nTOF12,1,Experiment,None,
aaa286de-4bcd-47d2-8630-362c75042c62,nTOF13,1,Experiment,None,
bd6972e4-3977-4e41-9dcf-c6f3c1387694,nTOF14,1,Experiment,None,
d9523d8c-c06c-455e-83d3-6dfd64060a4c,nTOF15,1,Experiment,None,
594b0147-4099-48fe-9dce-eb1fb7046727,nTOF16,1,Experiment,None,
3bda9531-30be-48bc-a729-cd452a27600c,nTOF17,1,Experiment,None,
d7010bde-64d7-4f29-aa4e-77dc45609b59,nTOF18,1,Experiment,None,
1d2ea5c4-5287-4e84-8f2a-e0dd37ba2cf6,nTOF19,1,Experiment,None,
ebf8f2ca-4ca3-47c4-bc01-c287fce3bf2d,nTOF2,1,Experiment,None,
72dbf976-d57c-4d06-9ee7-1ca78749a3fc,nTOF20,1,Experiment,None,
102b9372-c7c8-4878-a391-7e02bc14881a,nTOF21,1,Experiment,None,
87026426-0a9c-4c82-ac5d-bd575224da84,nTOF22,1,Experiment,None,
7b4390e3-2326-4f40-9538-a11ca1090f5a,nTOF23,1,Experiment,None,
3eb2efb8-85c9-486f-9531-79fcaa19d5b9,nTOF24,1,Experiment,None,
5396e422-cd84-4c1d-b5d2-b87c81285688,nTOF25,1,Experiment,None,
4383c970-5fec-48de-babd-5dd0b4a2ce85,nTOF26,1,Experiment,None,
9c175100-0ebc-4362-8476-0eb76ffd0641,nTOF27,1,Experiment,None,
45ab3ee7-abc5-46bd-ab6c-17614da3ed44,nTOF28,1,Experiment,None,
e31e70c2-96f6-40da-aa3d-ff3dfe1fe1d1,nTOF29,1,Experiment,None,
fbbe700c-c368-4575-b506-46f918b5469e,nTOF3,1,Experiment,None,
2a768cb5-d1db-4f8c-af1f-57cd552cb78e,nTOF30,1,Experiment,None,
26fa6ede-9a1a-4d2d-bbff-565060161673,nTOF31,1,Experiment,None,
dd5e5432-1d85-44fb-bc15-4605f5b02b79,nTOF32,1,Experiment,None,
96be4445-51f7-4390-b230-7e35930c2317,nTOF33,1,Experiment,None,
5e810404-f093-4970-aec3-b90d1b3ee1d7,nTOF34,1,Experiment,None,
160fe054-aadc-420b-af08-d2d7bdf9201e,nTOF35,1,Experiment,None,
1b0d1b1f-6c2f-4996-852e-eefb7d3d2ec8,nTOF36,1,Experiment,None,
d589ad57-8ff2-4a0c-b2cf-84b87b47fcb1,nTOF37,1,Experiment,None,
a99c1281-45b0-46ff-a395-dd8a71926d63,nTOF38,1,Experiment,None,
f58e3ab2-bfee-4fa2-995e-0b45fd2fc0b7,nTOF39,1,Experiment,None,
d6b7cc41-6ee7-495d-a822-dbe2d349aa1e,nTOF4,1,Experiment,None,
661dd678-0a77-4c1f-9ba6-a4a6621eb5ea,nTOF40,1,Experiment,None,
b95cfef0-caa0-4e8a-b4c7-6803d168f081,nTOF41,1,Experiment,None,
6068c7c0-460d-4126-bdf0-eb1ae31555ff,nTOF42,1,Experiment,None,
60bb5f38-1cd2-459a-93b9-3df76dd60ff7,nTOF43,1,Experiment,None,
5fd0fe2d-6c51-48ec-a3c4-fc6673ff600a,nTOF44,1,Experiment,None,
ca6ae5c6-dba7-437a-aea1-e306c3e11ed1,nTOF45,1,Experiment,None,
9b3ae512-5b03-4f48-9b73-57de3ab3eecb,nTOF46,1,Experiment,None,
322c87a6-3cb8-4728-a30e-8cf385e8cb2c,nTOF47,1,Experiment,None,
e8930f50-a3ce-420f-b0cf-1817603d2654,nTOF48,1,Experiment,None,
abcc9971-ac82-4c16-aba5-fe2d32ba2ab4,nTOF49,1,Experiment,None,
9aa4b74f-8bda-4d39-8cf1-f07a41c8a421,nTOF5,1,Experiment,None,
c2067972-62aa-4afc-9bd3-898c60ca426a,nTOF50,1,Experiment,None,
345838e0-6a89-419f-a05c-b03e276b57de,nTOF51,1,Experiment,None,
bd10a151-eaa7-4952-b480-178c136f4fef,nTOF52,1,Experiment,None,
3811674d-e21b-4d04-8f7e-bc91027360fe,nTOF53,1,Experiment,None,
b7308c25-78cd-4114-ad8a-a198d256a22c,nTOF54,1,Experiment,None,
a09cb325-926e-40d1-b4e7-ba13979bf0bf,nTOF55,1,Experiment,None,
a4b1b2df-6ee9-474d-a99b-464adb5ac771,nTOF56,1,Experiment,None,
0903b847-891f-40c3-a52d-613e3121a057,nTOF57,1,Experiment,None,
2a135a93-fba1-428b-b75b-554dd20d56a4,nTOF58,1,Experiment,None,
97cc0065-0d0a-40f1-8a61-e151fe5e3f84,nTOF59,1,Experiment,None,
3ef5abc5-f5db-4162-b10e-2814956b5d0c,nTOF6,1,Experiment,None,
c7313875-d22d-49ab-bf23-6787172fbbc1,nTOF60,1,Experiment,None,
e4c3b241-c689-40d8-8995-87ecd09a34df,nTOF61,1,Experiment,None,
1534aff6-5d95-4469-b77f-0479cc0a134f,nTOF62,1,Experiment,None,
a0b5f6ad-663f-467d-ac3b-6ffb891bb5a3,nTOF7,1,Experiment,None,
847948ae-b323-4788-b8f0-5f52f6a60186,nTOF8,1,Experiment,None,
8cf0a302-c775-4341-99d4-d28137898507,nTOF9,1,Experiment,None,
e7bd4b98-7f78-41a7-af20-5ad8a985fd42,NUCLEON,1,Experiment,None,
3d2b32e5-f023-9dc0-7904-6a0c9f0e0132,Nursery School Secretariat,1,FE,HR,cregelbr
7edef0d4-1bd3-a010-c1d8-fe62cb4bcba4,OASIS FEC,1,FE,BE-CSS-FST,sdeghaye
e2265060-1bdf-a010-c1d8-fe62cb4bcb97,OASIS GUI,1,FE,BE-OP,rende
44759cec-1b9f-a010-c1d8-fe62cb4bcbd3,OASIS Server,1,FE,BE-CSS-CSA,lburdzan
ea56f9f3-0a0a-8c0a-01a4-b4658e11ad83,OBSOLETE Administrative DB Instances,0,FE,IT-DB-DBF,
664190d1-945d-3cc0-6d21-20312bb3b633,OBSOLETE Database on demand infrastructure,0,FE,IT-DB-DBB,
ea56f9b8-0a0a-8c0a-01c2-83a86a0bac21,OBSOLETE EDMS DB Instances,0,FE,IT-DB-DBF,
ea56f98c-0a0a-8c0a-015c-46d38a3e3541,OBSOLETE General Purpose DB Instances,0,FE,IT-DB-DBF,
ea56f9df-0a0a-8c0a-013d-229e52565313,OBSOLETE Tier0 mass storage DB Server Infrastructure,0,FE,IT-DB-DBB,
ea56fa31-0a0a-8c0a-00f8-d6569a8b6131,OBSOLETE - Accelerator DB Server Infrastructure,0,FE,IT-DB-DBB,
ea56fa1e-0a0a-8c0a-00d2-fb408a12d7dd,OBSOLETE Administrative DB Server Infrastructure,0,FE,IT-DB-DBB,
ea56f941-0a0a-8c0a-004c-24ae90889566,OBSOLETE EDMS DB Server Infrastructure,0,FE,IT-DB-DBB,
ea56f9cc-0a0a-8c0a-01eb-943103ce977a,OBSOLETE Physics DB Server Infrastructure,0,FE,IT-DB-DBB,
ea56fa5a-0a0a-8c0a-0089-248cdb1de946,OBSOLETE Tier0 mass storage DB Instances,0,FE,IT-DB-DBF,
ce9b164d-db5c-f300-a1cd-a4f84b961951,Occupation infrastructures routi�res,1,FE,SCE-DOD,bejar
c67db418-7b59-401b-950d-ae837e450237,OCRE,1,Project,None,
f9840423-4f5c-3280-87a2-f5601310c721,Office for Alumni Relations,1,FE,IR-DO,laure
,,,,,rbray
456375fa-4f54-be40-87a2-f5601310c75a,Office of Data Privacy Protection,1,FE,FAP-DHO,gthiede
,,,,,mmarques
a61d7eac-4f22-1340-ce1b-401f0310c70c,Office panoramic view,1,FE,SCE-SAM-TG,mpoehler
ea5710f2-0a0a-8c0a-016b-0f829d181953,OHR LiveView,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
ea57125d-0a0a-8c0a-0137-3685c0101c5a,Oldham AGD/ODH,0,FE,GS-ASE-AS,grau
ea571a1d-0a0a-8c0a-008f-12baaa2c3452,ONET Cleaning,0,FE,SMB-SIS,lalejeun
,,,,,pafonsec
,,,,,vmarchal
e651fbe6-db89-8090-3831-890b069619f8,Online Travel Booking Tool,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea56eaaf-0a0a-8c0a-016d-44f139800a8c,Onsite Information Screens,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
b7756439-0d3a-4a41-a901-46b7a3be45d3,OPAL,1,Experiment,None,
8d4b84d6-db26-c410-f807-8985329619a0,Open Access Publishing Support,1,FE,RCS-SIS,agentilb
,,,,,basaglia
00877b7c-e189-0200-6d21-d3dd13d347ae,Open Archival Information System Store,1,FE,IT-CDA-DR,berghaus
,,,,,jbenito
,,,,,jeanyves
,,,,,lnielsen
,,,,,simko
cfb0f3af-99e7-adc0-7904-62f7b78ce1c0,Open Data Repository,1,FE,IT-CDA-DR,jbenito
,,,,,lnielsen
,,,,,simko
da82019b-08e7-4dc4-adf9-6d3d8fa18ac3,Open Days Event Ticket System,0,FE,IR-ECO-VI,briard
0674cddf-08e7-4dc4-adf9-6d3d8fa18a8d,Open Days ODFlow - Event Flow Management tool,1,FE,IR-ECO-VI,briard
4cc78499-45e1-8100-6d21-14ec7663656c,Open Days Official Event Coordination Committee,1,FE,IR-ECO-VI,angodinh
bdb1430e-dbd9-3bc0-f2d6-65925b961985,Open Days SMB Services Coordination,0,FE,SCE,bejar
,,,,,dconstan
8b5d8c58-dbcb-7bc0-8f2f-177e16961965,Open Days Volunteers and Visitors,1,FE,IR-ECO-VI,briard
58f1a117-7326-45e7-92e9-b032e83dde85,OpenAIRE-Advance,1,Project,None,
02de157f-3a21-43a1-ab4e-033682cb53ab,OPENLAB,1,Experiment,None,
ea56fb36-0a0a-8c0a-01f2-467a820a5a1e,Openlab,0,FE,IT-DI-OPL,
8cfe1c77-db43-8810-1de7-3e48229619da,openlab-IBM,1,FE,IT-CF-FPP,ebonfill
29d26645-db06-d054-f807-8985329619fb,openlab-systems,1,FE,IT-CF-FPP,luatzori
64e8451f-4d47-48c3-aa6f-3271fd17336d,OPERA,1,Experiment,None,
ea56fc2a-0a0a-8c0a-0130-59872ca7125c,Operations Support,1,FE,IT-SC,andreeva
,,,,,girolamo
,,,,,kdziedzi
,,,,,litmaath
7da37329-db14-3b00-269d-9e24db961929,Oracle Database,1,FE,IT-DB-DBR,edafonte
,,,,,grancher
ea56f96e-0a0a-8c0a-01c7-a4462fbe348e,Oracle Database Services for IT/ADMIN/GENERAL/ENGINEERING,0,FE,IT-DB-DBR,
ea571121-0a0a-8c0a-00e2-c103dfb6104d,Oracle HR,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
ea571018-0a0a-8c0a-003f-79e80440656e,Oracle HR Support Contract,0,FE,GS-AIS-HR,
738f7794-4fe0-7e40-7db7-d3ef0310c7b0,Oranges Orders,0,FE,SMB-SMS,vossen
1747a249-4f0f-4740-87a2-f5601310c76a,"Organization of visit for Protocol office , Host State Member visit",0,FE,IR,
2e5ca56b-56b5-4956-a933-765be98593fe,OSQAR,1,Experiment,None,
a64fb82a-feb1-402a-9561-3b056bf58046,Other,1,Department,Other,
ea57110c-0a0a-8c0a-0020-aa49e305715b,OTP,0,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
5e51ba6c-db56-0818-006f-d9f9f4961939,Oxygen Deficiency Hazard Safety,1,FE,HSE-OHS,delamare
,,,,,fchapron
,,,,,grau
,,,,,lindell
4c1f898f-d40a-410b-a32f-f980e3627d1b,P349,1,Experiment,None,
ea5718d5-0a0a-8c0a-0074-63a32de9ee35,Painting - CC Prezioso,0,FE,GS-SE-CWM,fmagnin
ea56e944-0a0a-8c0a-00c9-33ca78b2b668,Pakiti Infrastructure,0,FE,IT-PES-PS,
ef5dd461-d1bf-4215-8b3a-3bc78a8d24f4,PAMELA,1,Experiment,None,
6e3f61a8-90b6-43a4-8f45-e25fed776d07,PANDA,1,Experiment,None,
c6adfac3-dbf5-2700-f995-6184da961921,Panoramas,1,FE,EN-ACE-CL,aperrot
,,,,,tbirtwis
5fd24f77-4f19-5b80-b9cc-09de0310c7d3,Partnerships and Fundraising Office,0,FE,IR-REL-PFU,castoldi
ea56eec5-0a0a-8c0a-004c-a65e6dab0375,Patrimony data,1,FE,SCE-SAM-TG,yrobert
ea5709e4-0a0a-8c0a-0123-b413ef1f968f,Pay Tools,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea5703e2-0a0a-8c0a-0034-d0d2184ec012,Payments,0,FE,FP-DI-TPS,cspencer
ea570d49-0a0a-8c0a-00c7-083dd640db7b,Payslips,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea56ff34-0a0a-8c0a-00eb-50b879cd49b8,PC Procurement,1,FE,IT-CDA-AD,gmetral
,,,,,mkwiatek
6de458ff-a436-fd00-adf9-30a205b50aab,Pension Fund - Attestations,0,FE,PF-OP-BS,manola
860fb846-4fe2-0200-93ec-df601310c7e9,Pension Fund - Benefits,1,FE,PF-OP-BS,eclerc
ee1eaf2a-4f4d-3a04-87a2-f5601310c7e6,Pension Fund Legacy Application,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
d377b846-db6a-17c4-1aab-9247db961994,Pension Fund Web Application,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
ea56fc99-0a0a-8c0a-0147-941ce81c2983,Persistency Framework,0,FE,IT-SDC-ID,
ea5706e6-0a0a-8c0a-00f5-50060611dbee,Person Bank Account Details,1,FE,FAP-ACC-PA,sdethure
ea571065-0a0a-8c0a-01bd-6d9126ca6d28,Person Matching,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
ea571160-0a0a-8c0a-0094-8180b53974d9,Person Search,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
4a654a8b-4fbe-4740-77fe-29001310c792,Person transportation dispatch,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
3ea44e0b-4fbe-4740-77fe-29001310c7d3,Person transportation execution,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
6bfc6490-6428-b100-6d21-f8e2a49c383b,Personal and Operational Dosimetry,1,FE,HSE-RP-DC,carbonez
,,,,,delamare
,,,,,mrettig
3f67a419-981a-b000-6d21-708547f4c695,Personal Development and Communication,1,FE,HR-LD,flicciou
e6dd78c2-7c04-f944-adf9-15f17b561941,Personal Protective Equipment,1,FE,SCE-SSC-SC,cgarino
,,,,,challoin
ea570664-0a0a-8c0a-0120-eabdf61bfb9a,Personnel Payment Control,1,FE,FAP-ACC-PA,sdethure
33935340-dbfa-5b00-1aab-9247db9619f5,Petrol station maintenance-PTC,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
b1c04535-4f6a-6600-b8cb-ca1f0310c78a,Petrol Stations,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
0fe77975-89d9-46d3-90ff-a967c1fc5f7e,PF,1,Department,PF,
19597947-5467-4f18-a767-30ad1030856a,PH,1,Department,PH,
ea570ed3-0a0a-8c0a-0180-510e39c9203b,PH Technical DB,0,FE,FAP-AIS-FP,
14459b55-ff42-4776-b730-806be1c03cf7,PHENIX,1,Experiment,None,
c0e535e9-0a0a-8c08-0029-ff3ce7d7215c,Phonebook Application,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
c0e6342c-0a0a-8c08-00f1-75339c2a372f,Phonebook Data for Services,1,FE,IT-CF-SM,pocock
902c33f1-3800-4985-b107-b9529d30e032,Physics Beyond Colliders Study,1,Experiment,None,
ea57092e-0a0a-8c0a-01a0-cbb0c093b321,PIE/PAD,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
619664a5-ff80-ac80-9878-114e40b9ade2,PINK Service Management Consultancy,0,FE,GS-SMS,
7812d81f-e095-491b-af57-8ea18020f84f,PLAFOND,1,Experiment,None,
cb4793da-4ff9-c240-93ec-df601310c76a,PLAN,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
61c1766e-041a-c100-7904-3b41fde4f9a2,Planbook,0,FE,FAP-AIS-PM,bdaudin
ea56ee02-0a0a-8c0a-0159-04d609188f0f,Plans and archived drawings,1,FE,SCE-SAM-TG,yrobert
e6a0c63c-4f7b-7a80-b9cc-09de0310c7db,PLC technologies,1,FE,BE-ICS-CE,eblanco
,,,,,fchapron
,,,,,solp
52947330-1bd0-2050-5372-41588b4bcb93,PLM21 project,1,FE,EN-IM-PLM,aaversa
,,,,,friman
,,,,,jurgen
,,,,,kkrol
,,,,,mgarciac
4340a970-dbdf-d058-f4c8-949adb961918,Point 2,1,FE,EP-AID,atauro
,,,,,august
,,,,,lbrarda
7d5e1d38-db5f-d058-f4c8-949adb961975,Point 8,1,FE,EP-LBC,clara
,,,,,lbrarda
,,,,,neufeld
fe555113-8469-3880-c713-6646aaa7cebc,Poster Printing Support,0,FE,IT-CDA-AD,
ea571135-0a0a-8c0a-0034-cb6674b30ee6,PPT/EU,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea56eb22-0a0a-8c0a-00f9-45c3518b9653,Print Device Management,0,FE,IT-CIS,
ea56e9a3-0a0a-8c0a-019c-9169c9ccd1d5,Print Device Support,1,FE,IT-CDA-AD,bclement
,,,,,mkwiatek
,,,,,pocock
ea5700eb-0a0a-8c0a-0146-8a9fa2d46a40,Printing Infrastructure,0,FE,IT-OIS-IN,
ea56eb43-0a0a-8c0a-01a8-c7e4c8687518,Printshop Management,0,FE,IT-CIS,
ea56eb57-0a0a-8c0a-01f4-06162c5199cd,Printshop Operation,1,FE,IT-CDA-AD,bclement
,,,,,mkwiatek
,,,,,pocock
4f3267e4-db0c-a498-f807-898532961930,Private car,1,FE,SCE-SSC-SC,gbolling
,,,,,lalejeun
06bcefd4-a403-dc04-9878-a279f01cb7ee,Private Market Accomodation,1,FE,SCE-SSC-CS,gmathias
,,,,,lalejeun
,,,,,pcorreia
ea571a9e-0a0a-8c0a-00a3-675ad0346c49,Private removal,1,FE,SCE-SSC-CS,lalejeun
,,,,,roussiez
ea571a89-0a0a-8c0a-01ef-e083f12c8164,Product and Store Management,1,FE,SCE-SSC-SC,cgarino
,,,,,lakhouay
ea5708fc-0a0a-8c0a-002e-eaa5e34736bd,Product Procurement,1,FE,IPT-PI-RC,gregorio
8158737e-4fc2-d780-b9cc-09de0310c75b,Project and Experiment Safety Support,1,FE,HSE-OHS,aarnalic
,,,,,delamare
,,,,,fchapron
ea571174-0a0a-8c0a-0093-6cbd99fc7539,Project Costing Tool,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea5709ce-0a0a-8c0a-0046-ab314f0debec,Project Support/Consultancy,0,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,favrot
dc1b73b8-4ffd-6e00-b8cb-ca1f0310c787,Project Web Apps,1,FE,IT-CDA-WF,awagner
632865e7-0a0a-8c0a-0005-10f22bcfdff2,Projector Support,0,FE,IT-CIS-AVC,
1bd31adf-4f7a-cf00-7db7-d3ef0310c7ee,Protocol Office,1,FE,IR-DS-PRT,cwarakau
fe06d6db-830a-4164-8c58-580b198a7795,ProtoDUNE-DP,1,Experiment,None,
79611848-4315-4664-ac25-76459aa406ac,ProtoDUNE-SP,1,Experiment,None,
e507ff48-1b8d-6490-86ec-40498b4bcbc3,Proximeter services,1,FE,BE-ICS-CE,eblanco
ea57114c-0a0a-8c0a-012e-ff5cbf6ca368,PRT (Pre Registration Tool),1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
0a112c4e-136c-4dbe-8b8a-6b717b1fc2ab,PS140,1,Experiment,None,
8c0ed75a-3973-467d-9fd4-32992f237d17,PS141,1,Experiment,None,
61c3e500-e612-435e-9582-11bca216cae3,PS142,1,Experiment,None,
72eb217a-7d67-4597-a07d-cfcaef854991,PS143,1,Experiment,None,
33ee741b-e7a7-4759-8705-1422f09c8fe6,PS149,1,Experiment,None,
01f9c1cb-3d37-4588-8d7e-fd8bbfcc86be,PS150,1,Experiment,None,
653f0c75-6fec-41a1-88e4-876805497a0a,PS151,1,Experiment,None,
3f89983d-6174-4ec3-9c0b-09384178a007,PS152,1,Experiment,None,
c8ce5923-727c-4d9d-9c6c-15668c0d3fce,PS153,1,Experiment,None,
ace66caf-2130-48cb-835f-e0257e6000f3,PS154,1,Experiment,None,
001c37f5-83de-4756-a781-616df057bf4c,PS155,1,Experiment,None,
12f5436b-5d72-4b27-bdde-f4055ef9f956,PS156,1,Experiment,None,
2b1a4f02-3ce3-4b11-86de-8e297311dd5f,PS157,1,Experiment,None,
a9785460-100b-4e0f-809f-1bf6a2316677,PS158,1,Experiment,None,
1841a732-646b-4958-a6b4-5e7e2f50e43c,PS159,1,Experiment,None,
0524a008-97a6-4a16-8673-5749c37f8386,PS160,1,Experiment,None,
43b7f6e7-e8b9-486b-bc12-50c1585664e2,PS161,1,Experiment,None,
0c57d087-669c-4a6d-9008-235a25d45ab5,PS162,1,Experiment,None,
0d2985bb-589a-4994-83af-badbce0f6a8c,PS163,1,Experiment,None,
a5041c26-b0e2-40cf-aab6-7885c7df7904,PS164,1,Experiment,None,
8bbe439e-5f97-4e92-a400-ef771dc31c0d,PS165,1,Experiment,None,
ae69e4de-d472-4fbd-bb69-01adc7021a13,PS166,1,Experiment,None,
e38a236f-9326-45ff-8c95-df5e40211773,PS167,1,Experiment,None,
70ddacce-026e-4fba-b63e-de593ae74ba1,PS168,1,Experiment,None,
2600dbbf-f0d9-48eb-9f85-972b3ec5d87d,PS169,1,Experiment,None,
b0494666-6b9a-48fb-a26d-014f4876098b,PS170,1,Experiment,None,
2d7deba9-d94c-4f2b-bea0-72b66dc21e60,PS171,1,Experiment,None,
b8f3cf4d-e113-4dac-b52b-122631a2086d,PS172,1,Experiment,None,
981b7be8-db85-4ea9-9f89-486ef2b591e8,PS173,1,Experiment,None,
6c8479e5-cd6a-4855-a6f3-4b18ec49c06d,PS174,1,Experiment,None,
f2da7c4c-b953-463e-ae57-e6d51a01000a,PS175,1,Experiment,None,
d8faf558-3638-46ed-99b7-921584f0e1e1,PS176,1,Experiment,None,
a7a48d8a-00c8-42d8-ae63-9b5ec278f7a1,PS177,1,Experiment,None,
cb6b1cd7-32fa-4300-9f01-4349519bb695,PS178,1,Experiment,None,
7839850a-8324-4b26-b957-d6eb6d6d3de8,PS179,1,Experiment,None,
a2d4ef2b-6963-4beb-b1d5-afdf73df186c,PS180,1,Experiment,None,
ae163467-1d2d-4a6d-b58f-a1e43c84566d,PS181,1,Experiment,None,
5e309704-4812-4c82-b5e1-adc74ddd7e63,PS182,1,Experiment,None,
ef264f24-2062-4dd1-97aa-5b276cf4a43f,PS183,1,Experiment,None,
f659eaaf-ea7f-4e3b-9a4d-1f054166635e,PS184,1,Experiment,None,
3dd6462a-abe6-4b84-b485-eb513352324b,PS185,1,Experiment,None,
1ee9dfef-c927-46d1-a044-dda898548a4f,PS185/2,1,Experiment,None,
d7b60854-f788-4c04-91c5-1b4467c3e8b6,PS185/3,1,Experiment,None,
a723bd0c-e603-4cd9-a937-dd748abffc5e,PS186,1,Experiment,None,
59f4d320-2f81-4b2c-b095-dcf63d5c0c06,PS187,1,Experiment,None,
b91813d9-2eef-4ab0-a949-e91547f18713,PS188,1,Experiment,None,
a3f668ae-874c-4fd8-9d86-03385a647934,PS189,1,Experiment,None,
05fc7aa2-acca-41ee-97b5-17cb71296c30,PS190,1,Experiment,None,
28f45bc1-edb0-455c-8b62-2719015b0722,PS191,1,Experiment,None,
45e924b2-7046-4c63-9239-da4edaf66fb0,PS192,1,Experiment,None,
5cf65af4-289c-482b-bf61-9aeb789f7dab,PS193,1,Experiment,None,
6a4bcb37-e1dd-4728-8b49-f1f217455e17,PS194,1,Experiment,None,
747da611-cc43-4427-9f37-33a05d2a525a,PS194/2,1,Experiment,None,
0c673094-02c3-4970-90c4-eba453a250b6,PS194/3,1,Experiment,None,
922dd4c8-c236-4846-b489-a151ca106990,PS195,1,Experiment,None,
40e9e33b-e90f-43d7-b0d8-f92cfaa00d63,PS196,1,Experiment,None,
898ddc7b-2ac0-4c19-977a-8dd2df90aa49,PS197,1,Experiment,None,
fe886a9e-30dc-4847-81b7-9d35dbf130e1,PS198,1,Experiment,None,
2ad1488b-3e34-493b-b294-5be17ca3ec60,PS199,1,Experiment,None,
3778f754-cc9d-4c0c-957c-3335d8b72656,PS200,1,Experiment,None,
88c28438-cc31-481e-8640-1d46d98b2611,PS201,1,Experiment,None,
6f8d275d-d314-4443-a868-f67078c77839,PS202,1,Experiment,None,
967c8758-9209-431f-b8d0-7f69b8ca78bb,PS203,1,Experiment,None,
aa524ebc-2bc8-494a-b220-48df1efa9fea,PS204,1,Experiment,None,
d457dc96-a070-494e-a4c9-83579cc6ebe3,PS205,1,Experiment,None,
5d2d47a6-1490-4a76-b78f-fef54e18bfd1,PS206,1,Experiment,None,
293e185e-b68c-4e63-af48-3e77f5c39a12,PS207,1,Experiment,None,
0a4eef9d-d0a3-48ea-8522-4118fbc804c0,PS208,1,Experiment,None,
bf502f60-cdff-4760-85ec-3535a89c356a,PS209,1,Experiment,None,
e79634bc-81af-4154-b05b-7c482d635c41,PS210,1,Experiment,None,
2b600a6f-5626-4551-bea9-465a6138e38e,PS211,1,Experiment,None,
f6cd4b6a-07ed-4224-a21d-4c1a93cf29ed,PS212,1,Experiment,None,
9b5eacdb-077b-495e-896e-305bddd3c324,PS213,1,Experiment,None,
71f8ab62-5359-44a7-a575-a742a1f64940,PS214,1,Experiment,None,
626c584e-b870-4863-8915-060949a6dcc1,PS215,1,Experiment,None,
ea57175d-0a0a-8c0a-0001-47697a6cbfb9,Psychologist,1,FE,HSE-OHS-ME,delamare
,,,,,modossan
ea570d94-0a0a-8c0a-0000-4a3efadaa688,Public Outreach and My Visits Applications,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea56ea9a-0a0a-8c0a-01e0-8aa5f9c90346,Public PC,1,FE,IT-CDA-AD,mkwiatek
ea56e9e5-0a0a-8c0a-00be-54699e4f162a,Publishing,1,FE,IT-CDA-WF,awagner
5ea8fb59-1b18-d49c-c917-65b0ab4bcbce,Punch-out supplier,1,FE,SCE-SSC-SC,cgarino
,,,,,skrattin
d7c3a338-b4b6-7500-7904-8a4404c0eb71,Purchasing External Catalogue,0,FE,SMB-SC-SO,cgarino
,,,,,challoin
,,,,,skrattin
,,,,,tjitske
a2bccbf8-3265-4fc5-9d9d-7df2bbc1e574,PVLAS,1,Experiment,None,
4697db7d-2423-f000-7904-c39b27667a16,Pyramid vendor,0,FE,IT-CF-SAO,
ea570f39-0a0a-8c0a-003a-e10079909e2a,Qualiac CFU,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea570ce3-0a0a-8c0a-00fc-b12522ddcc31,Qualiac Finance,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea570ccf-0a0a-8c0a-0007-fe5bf4d0cd95,Qualiac Import/Export,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
79ad11bb-4f30-8b00-06bc-10ee0310c796,Qualiac Purchasing,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
b14e99fb-4f30-8b00-06bc-10ee0310c757,Qualiac Sales,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea570c3e-0a0a-8c0a-01d9-46097fb2385e,Qualiac Support Contract,0,FE,GS-AIS-FP,varmeanu
0d2050af-dea0-427f-a535-8d1436dff5cd,QUOG-DP,1,Project,None,
abfb0a72-5cca-439d-93d9-a1c4a1531c0a,R107,1,Experiment,None,
40e4c439-b524-4a52-8122-69548d3a8bef,R108,1,Experiment,None,
9b967264-6ab2-4708-9c79-3c120fcead19,R109,1,Experiment,None,
be762212-b17b-416b-80e7-8f022acf9897,R110,1,Experiment,None,
205d81c3-b3aa-4e74-859c-09d96e5a6a20,r18,1,Project,None,
fd737973-a4d8-4710-914b-9e0978a5bc0d,R207,1,Experiment,None,
82269959-e305-46aa-83f4-6473c0d0b12b,R208,1,Experiment,None,
4d117774-6ff4-4405-afc5-4227126803b8,R209,1,Experiment,None,
fc8f2331-5462-4bba-9359-914a5fe47571,R210,1,Experiment,None,
71ec357e-c5ed-484b-aa9a-739f2c5e80f8,R211,1,Experiment,None,
38e3110e-32de-49fa-bed3-c519fa0b00fb,R301,1,Experiment,None,
c76f90a1-38ea-40b3-b264-b9a230fae0c5,R401,1,Experiment,None,
220fb712-5ac9-4700-80eb-3b6abdcba960,R406,1,Experiment,None,
78532599-e5c2-4611-b924-dce95a375418,R407,1,Experiment,None,
946c3960-c7e0-4c06-b895-1e64b16f7c96,R409,1,Experiment,None,
c226efe7-188f-4ba6-9385-3465d0644459,R410,1,Experiment,None,
481395dc-cefd-45db-87a7-0fa64b845ff4,R411,1,Experiment,None,
b1f357f1-9713-456c-8399-50d6f3b2aa81,R414,1,Experiment,None,
2ce5d67c-5440-4666-b270-3f99fd5ae1cc,R415,1,Experiment,None,
b96b5e38-566b-4cc3-84f4-c16e25e58fd8,R416,1,Experiment,None,
c7b2dea2-d8f8-4db9-9432-6dbe9170492c,R417,1,Experiment,None,
ea13cd6b-c0b6-4e24-b416-285f8cb7b00e,R418,1,Experiment,None,
16118ad9-ab0f-475b-9f84-44890ca34147,R419,1,Experiment,None,
ccaacf02-5b5f-4ca9-9d6f-6e05d1787642,R420,1,Experiment,None,
058092aa-faee-48b9-9831-629923f4fc53,R421,1,Experiment,None,
bee84ba4-752b-498c-9089-c6f5274fd2f1,R422,1,Experiment,None,
393f863f-dfc3-4154-ab3e-408fa3556ae9,R501,1,Experiment,None,
91e7f6e2-e05c-446e-aeab-0236eb494b29,R605,1,Experiment,None,
a0139317-4c97-491a-984d-041adcf82c7f,R606,1,Experiment,None,
ac01a94f-038f-4197-8326-3df64183a7f2,R607,1,Experiment,None,
a1cc844b-0c0b-4ed0-8bf8-8ddd527a6680,R608,1,Experiment,None,
ea98dc33-8b0d-4460-8e61-44e5b6033b1f,R702,1,Experiment,None,
74a4ca3f-6fc4-4204-83a7-e141fed7efd4,R703,1,Experiment,None,
eaf0be9c-2666-4f06-95bd-e9c9f5ddb740,R704,1,Experiment,None,
c225641a-531d-47b9-80b6-f6e429a2ec4d,R805,1,Experiment,None,
ba42be51-644a-4072-aeb1-d8fcf1d8c7db,R806,1,Experiment,None,
57f71ae2-46ea-4183-a118-4a1da1bc5173,R807,1,Experiment,None,
79073e38-5d00-47ed-a847-8fdc50e89a6c,R808,1,Experiment,None,
ea56f8c1-0a0a-8c0a-014b-780b592457b9,Radio Communication Infrastructure,1,FE,IT-CS,araczyns
,,,,,gcancio
,,,,,rsierra
eda32058-2272-4855-b36b-265a2d0029b4,RCS,1,Department,RCS,
3734c668-1d9d-4c41-b1b6-c1c92f47cc9a,RD-1,1,Experiment,None,
10fbf72d-4d4e-40ba-bfdd-bda7b4aab6d7,RD-10,1,Experiment,None,
a183faaf-c990-4387-9fd6-b3c1cb04ed24,RD-11,1,Experiment,None,
6ef2d3f9-028e-46be-93b8-ecf45b8ba5ae,RD-12,1,Experiment,None,
6dda368a-1066-4fb5-bf9f-ebedbcdbfa1e,RD-13,1,Experiment,None,
27df8acc-3d06-401a-9f7d-8d1d3a4bf2f8,RD-14,1,Experiment,None,
557980bd-7f8c-43c1-af84-5aef8ad88d90,RD-15,1,Experiment,None,
5352c720-2d36-434c-9e2e-e7db547b5803,RD-16,1,Experiment,None,
7d5b1e06-5665-4bd3-baf3-c7812826ab27,RD-17,1,Experiment,None,
3fef3a21-6588-407a-be8f-51b75f231917,RD-18,1,Experiment,None,
010d88f8-b6d8-4660-9afb-fdc6da0cea36,RD-19,1,Experiment,None,
82c56bb7-4f95-4c08-a7c1-d707e6371ceb,RD-2,1,Experiment,None,
e9578a21-c5d1-4f23-a83a-5aa6e07b90bf,RD-20,1,Experiment,None,
936b1d79-41b9-4fc6-a035-07117470981b,RD-3,1,Experiment,None,
2f05d704-58b5-4e70-88c4-454b27ae7c24,RD-4,1,Experiment,None,
7fc0da28-b979-47b4-8e4a-71b8c07f022e,RD-5,1,Experiment,None,
a1a545db-297f-4ba4-9654-53ef1a16a0db,RD-6,1,Experiment,None,
0ef02cf2-5dae-4621-994e-bbcac6564f43,RD-7,1,Experiment,None,
29f588aa-3c66-4b30-a0a2-3c6673a209d2,RD-8,1,Experiment,None,
de80bbfe-f668-4852-b8e6-6748f5dd742e,RD-9,1,Experiment,None,
91896b68-e5e7-4ce4-8d8d-6375432baa59,RD21,1,Experiment,None,
d153f1ee-71c4-4d10-ab9c-44bb50a99456,RD22,1,Experiment,None,
49d59696-f151-4384-910a-ebd1dfee2373,RD23,1,Experiment,None,
a2901e69-5252-4bbd-af34-3b5c573d0ea0,RD24,1,Experiment,None,
f72747f4-b417-4953-b985-0b2155a823e4,RD25,1,Experiment,None,
b79920e3-9ff1-4a98-bc49-9fed69032245,RD26,1,Experiment,None,
de495eee-b8d8-4ca7-a6ce-aa355151d33c,RD27,1,Experiment,None,
7624e3fa-671b-4418-a6a7-50af99e44897,RD28,1,Experiment,None,
e2a58b22-737c-4d3c-a1bd-63d35435def6,RD29,1,Experiment,None,
0f0515ce-9c7b-4c3c-a1f9-a7b972b00aed,RD30,1,Experiment,None,
52ddb75b-5d4c-4f2f-a024-ceec12605820,RD31,1,Experiment,None,
59a4bf78-ab0f-45d9-81cd-57d7b67ec652,RD32,1,Experiment,None,
887a1dda-7911-4b69-9317-5d315a77fb7f,RD33,1,Experiment,None,
ace83739-8c8c-4a51-87fe-088331b40564,RD34,1,Experiment,None,
858082b5-5965-41fc-ba64-f03e536c265a,RD35,1,Experiment,None,
b98bf7fd-d9cf-42db-a75b-4a0f1c75544a,RD36,1,Experiment,None,
73ce1695-05b0-4600-935f-bc3b1ee7c8b6,RD37,1,Experiment,None,
cf708b1c-56b0-49d4-9972-d7038c25ddef,RD38,1,Experiment,None,
98bfeca8-3ba0-44b6-9c14-00908cec2183,RD39,1,Experiment,None,
145b6dca-439b-4763-a85e-147f1ab4a4bf,RD40,1,Experiment,None,
1f701e38-ef84-4358-8262-ae83ff63458b,RD41,1,Experiment,None,
2e98ebf3-a1ea-4c27-abb2-071a29cef8b2,RD42,1,Experiment,None,
247471bf-13cf-434a-9336-479717a804b9,RD43,1,Experiment,None,
b52b5b1a-023d-4c4c-8e14-dc5cff1f35d7,RD44,1,Experiment,None,
7158e779-ee5f-4c2a-9579-944f16f6b9fb,RD45,1,Experiment,None,
15acd58d-3304-4703-b114-b104af8f9ae9,RD46,1,Experiment,None,
557b3f26-eff5-48fc-a5a6-30fde8f5613d,RD47,1,Experiment,None,
7f3ce652-2a5f-4bbd-b16d-1f14e24f50a4,RD48,1,Experiment,None,
8c7fa1f9-26f8-4b74-b11e-dc3edad850fc,RD49,1,Experiment,None,
14568b09-eb65-4c6a-9696-5f66b6e5a03b,RD50,1,Experiment,None,
ba240a3c-d27e-4ec2-bc0c-7815fcb2a3ce,RD51,1,Experiment,None,
fbdbb608-7680-4f98-a2ae-1a8578f369fa,RD52,1,Experiment,None,
db5973ff-bd1b-4d5e-96b0-a3881eb35d44,RD53,1,Experiment,None,
23428e10-6dbb-4c8c-9739-7845fb4533ec,RE1,1,Experiment,None,
f28bac97-e6aa-4e77-9a09-07afcf8d67f2,RE10,1,Experiment,None,
3297b199-ac3d-4b13-96d5-410ad5b16607,RE11,1,Experiment,None,
4ce9a34d-f9b0-4d9d-9706-e4b8269e0be5,RE12,1,Experiment,None,
f7f66d89-9962-4687-973e-fc63ae9b9c16,RE13,1,Experiment,None,
4d9fe8ea-7332-4fda-93c0-4a72724b29d5,RE14,1,Experiment,None,
438eddda-36b2-4991-b720-cc7d6812c4ab,RE15,1,Experiment,None,
1ec116fc-effb-49d5-9f52-4005227d1292,RE16,1,Experiment,None,
fac9d4bb-6b3d-4b75-8f88-4e2c5d53d1b0,RE17,1,Experiment,None,
2c7e5cd1-5b7a-4c25-a280-933fffd5b8cc,RE18,1,Experiment,None,
ab962eb8-a48a-4463-bbf9-e5c1c10bbe6c,RE19,1,Experiment,None,
61e211e8-8742-4d0a-9c2e-0f78c11af94b,RE20,1,Experiment,None,
b55349db-824a-4fbd-b01d-20920d6d4a5b,RE21,1,Experiment,None,
7176a954-3110-4586-91ef-0f683f71758d,RE22,1,Experiment,None,
a7e317c3-de8b-4a8c-9597-460f8cf9f2e2,RE23,1,Experiment,None,
3ea6b2b2-6ba1-4495-8c48-1bd4f447272e,RE24,1,Experiment,None,
d2cc8371-6c10-4c32-b9c4-8a12f204dc04,RE25,1,Experiment,None,
b1300bff-4425-4fb8-bc9f-294676adc668,RE26,1,Experiment,None,
c8bb32d1-d8a7-49ff-b848-675e52e2c5f3,RE27,1,Experiment,None,
a73026b7-e7ff-4d8d-a566-880aec15d484,RE28,1,Experiment,None,
51fd9d6d-7624-4e54-8b59-8879047d4b5f,RE29,1,Experiment,None,
1377f0f6-d393-40bd-9bf4-5399a03a5cd1,RE2A,1,Experiment,None,
5ecb3154-592b-45bb-9ccd-fd366943a912,RE2B,1,Experiment,None,
15302b0a-73b4-4f82-9be0-102cfe429b59,RE3,1,Experiment,None,
babd714f-061a-4c80-9dd0-188421c65447,RE30,1,Experiment,None,
5a9d0067-c96a-4584-a7fb-d1dfb2400959,RE31,1,Experiment,None,
5307ed1e-5a33-4aae-a6a8-0231c19b3cb1,RE33,1,Experiment,None,
226e79f9-7031-4062-98b8-8b3ceb4f0faf,RE34,1,Experiment,None,
de728124-0672-4485-9366-794474c0bd05,RE35,1,Experiment,None,
6e053b8e-4ebd-49ce-971d-8543914121aa,RE36,1,Experiment,None,
badcebb0-c74b-4000-a660-4c50022b1713,RE37,1,Experiment,None,
6970b555-2604-4c2a-9904-c2d30f8d3b82,RE38,1,Experiment,None,
6b33ea95-e46a-4554-9d89-2dccc3b94e94,RE39,1,Experiment,None,
a8b404db-a499-4972-806d-b8fca53efef4,RE4,1,Experiment,None,
29cab0a5-5505-4ada-bf26-5157275ee4e4,RE40,1,Experiment,None,
3d6c617b-4042-46e9-b99e-722357590a8f,RE5,1,Experiment,None,
dc8536eb-54f1-498a-b93b-4c7f77f4c20c,RE6,1,Experiment,None,
3e5cb582-0bcf-4bc8-8b55-2e2191066db5,RE7,1,Experiment,None,
a308d0a9-e367-4a13-b16b-101b6eba9521,RE8,1,Experiment,None,
4c63a8b5-1d61-4d6e-a9fe-1ca1b478a99b,RE9,1,Experiment,None,
b19f6964-dbc2-9c14-f4c8-949adb9619c7,REANA reproducible analysis platform,1,FE,IT-CDA-DR,jbenito
,,,,,simko
2afe7ec7-013d-24c0-9878-151822750d56,REBUS,0,FE,IT-SC,
ea56f652-0a0a-8c0a-011c-dbcc3e557402,Records Office,1,FE,HR-CBS-RC,laluberd
,,,,,mtardif
20bbed81-db7c-e380-eb72-9085db9619bd,Records Office Internal Actions,1,FE,HR-CBS-RC,acook
,,,,,mtardif
ea56f508-0a0a-8c0a-0172-113c89ce3534,Recruitment Office,1,FE,HR-TA-SGR,acook
ea5715fd-0a0a-8c0a-0100-322ab9b5b964,Red Telephones,1,FE,EN-AA-AS,fchapron
,,,,,grau
,,,,,ninin
ea570e3c-0a0a-8c0a-0049-d05461fd1571,Registration Office Tools,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
c6b1e75a-088a-7800-6d21-41a546d4435c,Reinstallation indemnity,1,FE,HR-CBS-B,ygrange
cde2689e-04a2-5d80-adf9-793734b40e18,Relations Secretariat - Serious Events,1,FE,IR-DS-RH,eder
,,,,,nlavyups
8015c5e8-19f6-ddc0-7904-0003aab1c915,Relations Secretariat - Theft,0,FE,DG-RH,eder
,,,,,nlavyups
a38fdcd2-04a2-5d80-adf9-793734b40e20,Relations Secretariat Office,1,FE,IR-DS-RH,eder
,,,,,nlavyups
8fdbc02c-db9b-e010-b839-d9f9f49619d5,Remote Operations Gateway,1,FE,BE-CSS,toulevey
d987937d-2423-f000-7904-c39b27667ab8,Rentron vendor,0,FE,IT-CF-SAO,
ea57099d-0a0a-8c0a-01b2-a1a4f8d621b4,Reorganisation Tool (integrated in APT),1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
ea56f6aa-0a0a-8c0a-01a1-e36d6077c97f,Reporting,0,FE,IT-CF,
00336ab6-dbc1-3740-a1cd-a4f84b9619cc,"Reporting Accounting, Treasury and Payments",1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
00a09456-4f38-32c0-7db7-d3ef0310c77e,Reporting and Statistics Support,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
feb7444b-dbcd-7740-a1cd-a4f84b961937,Reporting Benefits and Talent Management,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
bad3e63e-dbc1-3740-a1cd-a4f84b96193e,Reporting Core HR and Talent Acquisition,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
a521bf65-15ef-7d00-c713-566b001b845e,Reporting Infrastructure,1,FE,FAP-BC,dtsekour
,,,,,materrap
,,,,,mcmarque
7c53e2f6-dbc1-3740-a1cd-a4f84b9619b8,Reporting Planning and Control,1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
6483e27a-dbc1-3740-a1cd-a4f84b961917,Reporting Procurement and Jobs,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
df63aaf6-dbc1-3740-a1cd-a4f84b9619aa,Reporting Site Management,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
e693ea7a-dbc1-3740-a1cd-a4f84b9619c6,Reporting Supply Chain and Logistics,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea5713d2-0a0a-8c0a-002c-f1086faf1b87,Reprography,1,FE,EN-ACE-CAE,friman
fa206dbc-4feb-4a80-93ec-df601310c7be,Restaurant 1,1,FE,SCE-SSC-CS,lalejeun
a8ae29c4-4f90-5640-4b4a-bc511310c7a6,Restaurant 2,1,FE,SCE-SSC-CS,lalejeun
38474093-4f23-7280-b316-c0501310c7b7,Restaurant 3,1,FE,SCE-SSC-CS,lalejeun
206754b0-db30-37c0-269d-9e24db96191d,Restaurant 774,0,FE,SMB-SIS,pcorreia
,,,,,vmarchal
f03dc429-4f4e-aa40-b8cb-ca1f0310c76f,Restaurant Contract Management,0,FE,SMB-SIS,lalejeun
,,,,,vmarchal
86cec88d-dbe1-6700-ae65-9ec4db96196f,Restaurants Cleaning,0,FE,SMB-SIS,lalejeun
,,,,,vmarchal
ea571a32-0a0a-8c0a-0190-4c443f1553c9,Road and Drainage Works,0,FE,SMB-SE-CEB,fmagnin
fff67466-db6b-2700-ae65-9ec4db9619e0,Road safety coordination,0,FE,SMB-DI,vadon
2505871a-dbc2-bfc0-ae65-9ec4db96192c,Road Safety Support,1,FE,SCE-DOD,bejar
,,,,,vadon
778f9fac-4f0b-8300-77fe-29001310c745,Roads,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
8f45f85a-11d9-e500-adf9-ea91634bc638,Roads maintenance,0,FE,SMB-SE-CEB,fmagnin
3d44b8d6-11d9-e500-adf9-ea91634bc627,Roads works,0,FE,SMB-SE-CEB,fmagnin
8acc07c4-2d75-47b0-acb7-db49392f65fd,ROG,1,Experiment,None,
8befd320-4f4b-8300-77fe-29001310c7b3,Roofs,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
ea5719c3-0a0a-8c0a-014a-0e25787859f5,Roofs and Water Infiltration,0,FE,SMB-SE-CEB,fmagnin
,,,,,srew
c751f012-11d9-e500-adf9-ea91634bc677,Roofs maintenance,0,FE,SMB-SE-CEB,fmagnin
4fc203d6-4d89-6580-adf9-1eaa1e7b4d96,Roofs works,0,FE,SMB-SE-CEB,
ea56ea3a-0a0a-8c0a-00e0-0fb54700fad2,Room Videoconferencing,0,FE,IT-CIS-AVC,
a71f606c-2520-f100-c713-88d28b86df2e,RP Analytic Laboratory,1,FE,HSE-RP-CS,delamare
,,,,,fmalacri
,,,,,lulrici
992a93b0-4ff1-9200-4b4a-bc511310c7bc,RP Operation (Radiation Protection Operation),1,FE,HSE-RP-AS,ctromel
,,,,,delamare
cd828330-e185-0200-6d21-d3dd13d34770,RP Source,1,FE,HSE-RP-CS,delamare
,,,,,pbertrei
,,,,,vtromel
a75a5bb0-4ff1-9200-4b4a-bc511310c730,RW TREC Operation,1,FE,HSE-RP-RWM,delamare
,,,,,gdumont
,,,,,lbruno
,,,,,rmichaud
56dd763a-f8cb-4acf-bfa8-b878bbc656a7,S0,1,Experiment,None,
9468b936-7d4e-48d1-8063-2371a8ca14c5,S10,1,Experiment,None,
2fa1e037-234a-4044-927b-d4f97c56368b,S100,1,Experiment,None,
00631719-4c61-4c25-ba85-f8b1349fecfc,S101,1,Experiment,None,
c5a33bf2-c9d2-4216-85ee-fe2703585543,S102,1,Experiment,None,
2e82d387-230a-4a2a-ba3c-932ad815a3ab,S103,1,Experiment,None,
86c54ed1-bfd1-4d43-aa36-b8afb35d3f09,S104,1,Experiment,None,
59fe50aa-9906-47e1-8d76-cd5ac73691c8,S105,1,Experiment,None,
7becb520-eb1b-47dd-89c1-819a15c1e2ca,S106,1,Experiment,None,
47556e5b-5a09-42e0-87f4-184afb9fe6ef,S107,1,Experiment,None,
188d4ca7-29fd-4d64-a1b4-2a5d77f4f3f0,S108,1,Experiment,None,
d9f48109-2a04-4717-ab68-7b3f5361ab57,S109,1,Experiment,None,
652ffc29-faba-4e59-8c28-993e398825af,S110,1,Experiment,None,
b195b3fd-4247-4d22-b05d-8d7b955a5838,S111,1,Experiment,None,
7ca3975d-a50c-4133-b7c1-e3c4c90dc7f5,S112,1,Experiment,None,
adb39707-964d-4277-b0ae-627abe2f896d,S113,1,Experiment,None,
9d616460-a704-4b03-8a22-2756e77977e9,S114,1,Experiment,None,
5b6ed21f-5cc3-4ffb-8201-164916bf76c1,S115,1,Experiment,None,
7c8fb8d9-40cf-4a71-91f5-5773da41a41e,S116,1,Experiment,None,
b60ccde2-5d5f-4f00-a66a-b037a0bce91a,S117,1,Experiment,None,
8246e315-2dae-48df-a687-5f58a6126c6e,S118,1,Experiment,None,
3905a229-241b-4827-aa23-09495487e9de,S119,1,Experiment,None,
48bb47da-e831-4fb0-b409-bb4de523221d,S120,1,Experiment,None,
06ccf97e-76e0-4fde-9c80-a61536382641,S121,1,Experiment,None,
8527bdd0-da36-40ae-a6f2-e0fe8fed12c9,S122,1,Experiment,None,
3363a163-ecfc-4fb7-ada8-37a1d67e49a5,S123,1,Experiment,None,
f234c768-f5ff-41b0-b2ee-fa192b79c246,S124,1,Experiment,None,
1de9a379-0f6c-4c70-8b38-6e6ac860dc75,S125,1,Experiment,None,
67ed3f25-d58f-4137-b252-e5d0fbeddbbb,S126,1,Experiment,None,
cfea3029-0f36-4363-9d29-d17bc54ec16f,S127,1,Experiment,None,
c464c4e6-6807-448e-94e1-22d92c83b263,S128,1,Experiment,None,
d74918af-96b3-49c8-955e-13e79bc16e78,S129,1,Experiment,None,
c4699e86-503d-4bc1-abf6-b6250f05c6e0,S130,1,Experiment,None,
d280e1d9-5b9a-4278-afe0-bd11e165f6b9,S131,1,Experiment,None,
8dc247e7-fcaa-4c48-9935-97e376395f30,S132,1,Experiment,None,
4a5783bb-bb75-494a-995d-cf006335f2a5,S133,1,Experiment,None,
997c1993-b21f-40c7-afbc-58b01f41c717,S134,1,Experiment,None,
94f4acf2-1fd5-48fe-8790-8181b0e3d19d,S135,1,Experiment,None,
027f8497-027e-419c-8be7-028af33f7197,S136,1,Experiment,None,
b5213871-a6bf-4f9b-a59c-2bfa97cc5780,S137,1,Experiment,None,
8c7b0239-c9be-4063-88b4-be960d494d8f,S138,1,Experiment,None,
ba9a6879-1214-4d5c-bccc-f59f3a6b5a0a,S139,1,Experiment,None,
e1d16af1-4f7f-9780-fe13-2eff0310c723,S3 Object Storage,1,FE,IT-ST-GSS,laman
a077e6de-f804-49ea-ac15-2635520c6cb5,S35,1,Experiment,None,
38c63920-f0bf-4452-8d99-a66b9017a59f,S36,1,Experiment,None,
c73fca86-3e2f-4daa-bd01-a89c5eef2f9d,S37,1,Experiment,None,
df10f65b-4eae-4e4a-a2a5-55eae57f9030,S38,1,Experiment,None,
d838ec6f-9605-44bf-a006-728de7a1ffde,S38a,1,Experiment,None,
1643a374-e072-4a21-bde9-4677acc8ac0a,S39-and-S39a,1,Experiment,None,
8dd11597-85e6-4fe5-8afa-941f5a1b0432,S40,1,Experiment,None,
20474e44-2836-4117-9731-8690eee44799,S41,1,Experiment,None,
08613ff2-a203-4885-a341-5f0f936ecbd3,S42,1,Experiment,None,
a0d14931-0734-4b01-bcb8-c384a31e279f,S43,1,Experiment,None,
f9d1a3ad-76f2-422e-8e7f-67f17aa3c56f,S44,1,Experiment,None,
43337712-674b-43da-8d5b-f40d21a672df,S44a,1,Experiment,None,
c4e6b2c8-00ab-4d89-b8b4-9aabc4cc880e,S45,1,Experiment,None,
908ebc0d-8902-44c3-8524-13c62aba294e,S46,1,Experiment,None,
06817fe7-1a58-4348-9955-084bb93e0fcd,S47,1,Experiment,None,
0aea50f8-e149-40de-903b-e231c2174d46,S48,1,Experiment,None,
5bfe0f6c-c71f-4162-a80f-bd5744b5f801,S49,1,Experiment,None,
548c8998-160d-422f-828f-4ae19120ada2,S5,1,Experiment,None,
7c9cbaad-4cc1-4562-b87f-88a312f3f84a,S50,1,Experiment,None,
1228fd64-2471-40dd-bb59-973ad1fc8378,S51,1,Experiment,None,
fd2a16f8-99e4-4e92-ad2b-7252ad852167,S52,1,Experiment,None,
690d0931-1f41-44d7-a804-03aec77ff034,S53,1,Experiment,None,
926c878b-edd0-4098-a578-0518346293fb,S54,1,Experiment,None,
4b639402-f0e5-4843-92e0-63ed2b8ec148,S55,1,Experiment,None,
a9482fdc-e667-448d-ab48-1c42d93d6b63,S56,1,Experiment,None,
00d0a7c3-f0c1-4d05-a98b-f312b3953eb1,S57,1,Experiment,None,
87aed2e2-6b42-4e94-9ae9-90426e71d44f,S58,1,Experiment,None,
7469a902-05d6-4044-ac1b-3051df4b9592,S59,1,Experiment,None,
16dc0374-df02-4c88-99c5-fc4060f39e68,S60,1,Experiment,None,
5a98449b-4fb9-4289-8ab0-88eed3455176,S61,1,Experiment,None,
0e1ea018-9111-412e-bfd3-004ca3ff928b,S62,1,Experiment,None,
13de5c9f-9fe9-4d98-8d47-37d829beb42b,S63,1,Experiment,None,
fa8bdf23-9372-4862-9c01-8384c91254f2,S64,1,Experiment,None,
4448a819-a71a-4338-bd6d-bc416e842cd4,S65,1,Experiment,None,
38c8032d-1860-4e36-8615-253fb9a95129,S66,1,Experiment,None,
2e578a04-8141-47fc-a2c9-c2562a4f60ba,S67,1,Experiment,None,
2f5795d4-70cd-43b0-9b53-577b8c76f27a,S68,1,Experiment,None,
841815b1-66b0-4796-b58d-d510b851693f,S69,1,Experiment,None,
c6804474-45d9-4910-ab80-51bd16733b1b,S70,1,Experiment,None,
4c9db90b-af43-49cf-969b-6b016e454aec,S71,1,Experiment,None,
bfecadf9-1850-4108-b363-0347538b6894,S72,1,Experiment,None,
ebbc5144-644e-413f-af31-8d23a243e823,S73,1,Experiment,None,
550d2494-7665-4e46-b117-ab3055a0d068,S74,1,Experiment,None,
334d3d93-17c3-45ac-b725-9d70354e1f18,S75,1,Experiment,None,
adf9d543-f001-43a1-824c-bbeb62bc3af9,S76,1,Experiment,None,
5fd0f4d4-fcef-4953-b16a-fdfbba12a8a7,S77,1,Experiment,None,
82ad6aee-6bc9-42c4-8f73-b12326335347,S78,1,Experiment,None,
f8a6e6b2-47f7-4ac5-820f-efa7fd6328ce,S79,1,Experiment,None,
4149e05a-5f70-45fb-b8e0-123c0660b823,S80,1,Experiment,None,
16344d02-d3ea-4174-9041-f59752e5c001,S81,1,Experiment,None,
48946feb-aace-40ea-825e-f2cc81c95448,S82,1,Experiment,None,
080a7974-4e6d-42cf-9de0-96b85dcca675,S83,1,Experiment,None,
99b4309c-744f-4f04-be52-1e16183ef3bd,S84,1,Experiment,None,
4c0518f3-24db-4fec-b83c-7a526431e053,S85,1,Experiment,None,
e59b6961-77c0-421e-a755-f27da69e5f21,S86,1,Experiment,None,
7552b245-0cc2-4514-a6a6-95010e68e50d,S87,1,Experiment,None,
a1714337-6db6-4682-a6ed-9042051d17b7,S88,1,Experiment,None,
141c7455-0852-4913-baae-3cc0aed0256c,S89,1,Experiment,None,
c6dc6082-476d-458e-922d-fb88578ae8b2,S90,1,Experiment,None,
4405af31-275c-460a-9428-cbf44d431ff7,S91,1,Experiment,None,
51c8ee72-4ef8-4159-8297-da74af2f6dc2,S92,1,Experiment,None,
8cd8ff17-3f76-4b84-b830-f26097d8cf1f,S93,1,Experiment,None,
bfc63d3e-238a-40b0-929c-c72022bb29b0,S94,1,Experiment,None,
09f5f833-758f-414c-9241-fc7ccf9578a9,S95,1,Experiment,None,
f57631ba-0269-4e87-bd85-71fcefcf4fc0,S96,1,Experiment,None,
0f7999ff-ea14-4c14-9433-301fc453a6dd,S97,1,Experiment,None,
63267119-6433-4cc6-8a9a-4c4297c0a6f4,S98,1,Experiment,None,
728134ba-2a27-4208-824c-4e7f4c5b8f06,S99,1,Experiment,None,
ea570e93-0a0a-8c0a-0031-ab5724469ab9,Safety (SOS),1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
c3282ed7-4f55-4b40-b316-c0501310c772,Safety Alarm Projects,0,FE,EN-AA,grau
,,,,,ninin
ea571551-0a0a-8c0a-016b-c618d0e87253,Safety Alarm Projects and Engineering,1,FE,EN-AA-AS,fchapron
,,,,,grau
,,,,,ninin
ea5713be-0a0a-8c0a-011d-c48f0630f81c,Safety Conception and Consultancy,0,FE,BE-ICS-CSE,fchapron
,,,,,ninin
b08bb750-a0c5-8184-6d21-df786d7ffcfe,Safety Deviation Reporting,1,FE,BE-ASR-SU,cgaignan
,,,,,tavlet
24116a5f-4f5b-2e00-b8cb-ca1f0310c748,Safety Inspection Visits,1,FE,HSE-OHS,delamare
82a92dbd-4f65-8700-b6f7-ab6ba110c7c2,Safety Risk Analysis,0,FE,EN-AA,rnunes
7fd0b9fa-4ff1-d600-4b4a-bc511310c72d,Safety Tetra Validation,1,FE,HSE-OHS,apascal
,,,,,delamare
ed7f1926-94a8-3400-6d21-20312bb3b675,Safety Training,1,FE,HSE-TS-ST,balle
,,,,,cparis
,,,,,mthyes
7ffd729e-4fd0-5e40-4b4a-bc511310c710,Safety Training Business Analysis,0,FE,HSE-TC-ST,balle
,,,,,delamare
7080f4ca-4f98-da40-4b4a-bc511310c77d,Safety Training Centre Operation,1,FE,HSE-TS-ST,balle
,,,,,delamare
593df61e-4fd0-5e40-4b4a-bc511310c7e2,Safety Training Programme Design,1,FE,HSE-TS-ST,delamare
,,,,,jbiringe
2cf15d69-0a0a-8c08-000b-a599c480a558,Safety works,0,FE,SMB-SE-CEB,fmagnin
,,,,,srew
ea570678-0a0a-8c0a-00ea-8b2373761249,Salary Office,1,FE,FAP-ACC-PA,sdethure
ea56fcd2-0a0a-8c0a-013e-db607f137f8e,SAM Development,0,FE,IT-SDC-MI,
61c037c2-f5a8-5480-9878-e86b196c5c18,Sanitaire - CC Dalkia,0,FE,GS-SE-HE,martelc
3059a5d6-9491-6100-7904-7129f337bfe3,Sanitary engineering design and studies,1,FE,SCE-SAM-IN,martelc
84b3dd4b-0a0a-8c0a-0148-2da674a1639b,Sanitary Equipment,0,FE,SMB-SE-HE,martelc
4c49f056-111d-e500-adf9-ea91634bc6cf,Sanitary operation and maintenance,1,FE,SCE-SAM-IN,martelc
17693c56-111d-e500-adf9-ea91634bc62e,Sanitary works,1,FE,SCE-SAM-IN,martelc
06a753bd-2423-f000-7904-c39b27667a18,Sarel vendor,0,FE,IT-CF-SAO,
cd61320d-8e1d-438a-809e-5bce6782ff60,SATAN,1,Experiment,None,
56ec6fa6-09f7-446e-a6d4-899bb6513bf7,SBN/ICARUS,1,Experiment,None,
8bce4fe8-5ec7-4827-aa16-ddd34029c8cb,SC21,1,Experiment,None,
54718494-2ef0-4433-aed9-4b1fc6f06d52,SC50,1,Experiment,None,
aad94d92-09e5-41cf-bf03-d4189d647ea8,SC52,1,Experiment,None,
5e4a715a-2d55-439b-a294-a14d04f3bf13,SC53,1,Experiment,None,
15b0d961-4297-47ea-9093-882b65af8a56,SC55,1,Experiment,None,
725db65c-912b-4ae2-a3c3-ac588834e0df,SC57,1,Experiment,None,
162daee3-2db0-41ee-a9f6-9498d677ffb4,SC58,1,Experiment,None,
885182c9-1fb9-476f-801d-7124688979d8,SC59,1,Experiment,None,
a0eb69b7-2b10-4be2-af45-4364de38912f,SC60,1,Experiment,None,
71758911-0ba4-49b6-8b7e-ec85b6cc8aef,SC63,1,Experiment,None,
0b81976a-d61a-438a-8985-78dbf9b06b7f,SC64,1,Experiment,None,
d7ec6acb-4975-40c8-bb2b-8926b8bbe9b6,SC65,1,Experiment,None,
c22e1f86-70d9-4387-8e1a-4817aa926489,SC66,1,Experiment,None,
905bae45-d4f1-4cd9-9e75-6bf651e64ce8,SC67,1,Experiment,None,
45db3de8-0792-4695-bd56-27f19fbd5fce,SC68,1,Experiment,None,
de4ae01e-9167-4231-be0a-a22979d8d6dd,SC69,1,Experiment,None,
dd35b900-2cde-4a90-9d33-a5de053a69c6,SC70,1,Experiment,None,
8e4d092e-64ed-4d0c-9970-b21ed949f9dd,SC71,1,Experiment,None,
a861d839-8748-4b74-be9c-b7f9c7527612,SC72,1,Experiment,None,
d4008f1f-91d2-4c01-ba75-a4e6d2c7349a,SC73,1,Experiment,None,
766f5984-1b77-4f1c-bcd4-d4f67fc3957a,SC74,1,Experiment,None,
5757d0e1-a8be-4e55-bef2-cce23288e906,SC75,1,Experiment,None,
8dcfab24-b769-4ce5-8014-efd36656bf35,SC76,1,Experiment,None,
829b0900-0203-4ff1-ad37-ac161e26a1c3,SC77,1,Experiment,None,
d374bdc2-8fac-46be-9c22-7edaf7cb5105,SC78,1,Experiment,None,
761dead8-6549-402a-b918-8c9b0a7b5856,SC79,1,Experiment,None,
016ead4b-fb02-4433-8130-ed7cf9ffabf6,SC80,1,Experiment,None,
67e224ea-0799-4d2e-a3b4-85d9de13bfb8,SC81,1,Experiment,None,
c9aa46fe-859a-44c2-8090-0a5e60f3de1d,SC82,1,Experiment,None,
ab255f32-8e70-4429-9cad-886c9f85f3e3,SC83,1,Experiment,None,
d7dbd0b7-9081-4f6b-bd31-fc0e67f52d7c,SC84,1,Experiment,None,
68575e4b-7207-4ae4-8c6e-cc032246686c,SC85,1,Experiment,None,
85eb4578-bac4-476f-b80a-f68fb4e3f0f6,SC86,1,Experiment,None,
18aae7cf-6ec2-422f-96f4-8546316f85ec,SC87,1,Experiment,None,
f5657413-4ec9-459f-99c9-2fb6a863ba06,SC88,1,Experiment,None,
1205ac72-e5af-4679-920c-0b40b06cafa6,SC89,1,Experiment,None,
ba8f7411-cde8-4640-a423-10d8c1921c87,SC90,1,Experiment,None,
56977998-e7d5-46ab-b447-95a5d25f9442,SC91,1,Experiment,None,
a63b5d36-5ba9-4c12-9e5b-1b1608b619b9,SC92,1,Experiment,None,
6256e507-0c7a-42a5-bc94-6eadedf92c8c,SC93,1,Experiment,None,
ef56bb4a-859b-440e-b515-9f4a106d645d,SC94,1,Experiment,None,
5f9b7215-e006-4d76-bd34-18867c85e7f7,SC95,1,Experiment,None,
69b872b0-0e2f-4b94-9c78-2ebc02de89de,SC96,1,Experiment,None,
ce0855f6-dcbd-4455-bad4-096ab30093e7,SC97,1,Experiment,None,
98ed48b4-801e-4317-bec5-aac2e934965f,SC98,1,Experiment,None,
0970023c-4f7b-7a80-b9cc-09de0310c700,SCADA technologies,1,FE,BE-ICS-CE,eblanco
,,,,,fchapron
,,,,,solp
4c11e993-4fab-1b40-7db7-d3ef0310c7b9,Scaffolding Service,1,FE,EN-EA-CT,gcanale
,,,,,wilhelmm
77a9052a-b42c-4a36-b908-667395b2effb,SCE,1,Department,SCE,
f1f640f7-dbcb-2780-a1cd-a4f84b9619f6,SCE - Departemental Safety Officer (DSO),1,FE,SCE-DOD,bejar
000e0843-2140-9980-adf9-b5210b89fc09,SCE - Department Head Office,1,FE,SCE-DOD,bejar
23ecc00f-2100-9980-adf9-b5210b89fc34,SCE - Departmental Administrative Office (DAO),1,FE,SCE-DOD,bejar
,,,,,sbudunko
c4615443-2180-9980-adf9-b5210b89fc6b,SCE - Departmental Key Manager,0,FE,SCE-DOD,bejar
,,,,,callmas
e13bc8c3-2100-9980-adf9-b5210b89fcab,SCE - Departmental Planning Office (DPO),1,FE,SCE-DOD,bejar
,,,,,eweymaer
f49ea9e4-0a0a-8c0a-002f-535abdef750c,SCE - Groups Secretariat,1,FE,SCE-DOD,bejar
,,,,,callmas
59127d18-dbe7-c414-1de7-3e4822961914,SCE - Safety operation,1,FE,SCE-DOD,bejar
515098c1-dbd6-4c18-006f-d9f9f49619f4,SCE Merit Panel,1,FE,SCE-DOD,atantot
,,,,,ifernand
,,,,,mcapeans
ea56f31e-0a0a-8c0a-01bf-dedf8169f7a6,SCE Service Management Support,1,FE,SCE-SMS,gbalazs
c1303cbc-91c9-9500-adf9-a439183b95f5,SCE Website,1,FE,SCE-SMS,gbalazs
119a6919-4fc4-8b00-b8cb-ca1f0310c750,SCE-SSC KPI,1,FE,SCE-SSC,cgarino
43b79bbd-2423-f000-7904-c39b27667a32,Schroff vendor,0,FE,IT-CF-SAO,
855e308f-4f07-df40-ce1b-401f0310c75b,Scientific Committees Secretariat,0,FE,EP-AGS-SCS,mage
ea56efc9-0a0a-8c0a-00e7-167f6f4926f4,Scientific Information Projects,1,FE,RCS-SIS-OS,kanaim
ea56f6bf-0a0a-8c0a-011c-4082bfacdcd2,SDB,0,FE,IT-CF-SM,
ea571574-0a0a-8c0a-0086-2abe6d867d32,Securiton AFD/EVAC,0,FE,GS-ASE-AS,grau
06d7d7fd-2423-f000-7904-c39b27667abd,Seil vendor,0,FE,IT-CF-SAO,
ea571475-0a0a-8c0a-0088-73e677943865,SEMER ZORA Operation,0,FE,GS-ASE-AC,rnunes
05629e69-b5dc-3100-7904-5e3417224b94,Sentry Error Tracking,1,FE,IT-CDA-WF,awagner
8c97d77d-2423-f000-7904-c39b27667ae9,Servertech vendor,0,FE,IT-CF-SAO,
ea5702d5-0a0a-8c0a-018c-4f65703f7f45,Service Consolidation Infrastructure,0,FE,IT-PES-PS,
ea56f1f9-0a0a-8c0a-0192-7a6dd23a2f3d,Service Desk,1,FE,SCE-SMS,ekirschn
,,,,,elienard
,,,,,gbalazs
b915cf0c-ff27-9800-9878-114e40b9adc3,Service Management Support Contract,0,FE,IT-CF-SM,
f9f5988d-3dd0-3100-adf9-86cfea8941e8,Service-now Test Team,0,FE,SMB-SMS,
579fb3d9-0a0a-8c08-017a-c8a1137c8ee6,ServiceNow,1,FE,IT-CF-SM,garciacj
,,,,,gbalazs
,,,,,pocock
ea5700c3-0a0a-8c0a-016a-3ae45272cf1e,Sharepoint,1,FE,IT-CDA-WF,awagner
88ab1490-3716-42a8-b09f-024927d55d66,SHINE,1,Experiment,None,
a304dfb2-69a0-4d3d-b57e-ffd3c395a3d0,SHiP,1,Experiment,None,
ea571ab2-0a0a-8c0a-0154-7539e0972514,Shipping Management,1,FE,SCE-SSC-LS,pmuffat
51e6dca0-1bdf-a010-c1d8-fe62cb4bcb3d,SILECS,1,FE,BE-CSS-FST,sdeghaye
ea5714a4-0a0a-8c0a-0090-314dd9e26bd0,Simtronics Sniffer,0,FE,GS-ASE-AS,grau
ea571189-0a0a-8c0a-007f-4bb7319cfa2e,SIR,0,FE,FAP-AIS-HR,
21466bc9-f06c-ce80-c713-3d85b5657c30,Site Access and Security Management,0,FE,SMB-DI,dconstan
,,,,,hmontane
0ff2ee7f-0a0a-8c0a-0115-22f510aaab46,Site and General maps,1,FE,SCE-SAM-TG,mpoehler
,,,,,yrobert
49073c3e-dbf7-d818-f807-8985329619ff,"Site Services Projects, Process and Contracts Office",0,FE,SCE-SSC,cgarino
,,,,,pcorreia
,,,,,vmarchal
a51c1785-f813-4e96-a483-760fc9cd2a67,sixt,0,Project,None,
0d7d053d-a0fc-0d04-6d21-df786d7ffce7,Skype for Business,1,FE,IT-CDA-IC,gtenagli
,,,,,pgrzywac
,,,,,tbaron
26d77c54-5df4-430a-872d-8c9c57b0eefc,SLHC-PP,1,Experiment,None,
25ca0c49-430b-46cc-87cd-12fdf0409fe3,SLIM5,1,Experiment,None,
06d4e632-db05-3740-a1cd-a4f84b96191c,SLMA,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
030ea966-1bc3-2450-2f4a-dc6a9b4bcb98,SmartRecruiters,1,FE,FAP-BC,dmathies
,,,,,gballet
c53c5d70-13ec-4301-9a2e-4023b7d1d891,SMB,1,Department,SMB,
2e572661-db02-2340-7c55-9eb3db96194e,SMB KPI Reporting Service,0,FE,SMB-SMS,
ea570987-0a0a-8c0a-00fd-494a659e85be,SMT (integrated in APT),1,FE,FAP-BC,bdaudin
,,,,,dtsekour
,,,,,mcmarque
512ceee1-de6c-460e-803a-96a10276f359,SND@LHC,1,Experiment,None,
913e5982-281c-4cb1-8bbd-66833b6a7091,SNO+,1,Experiment,None,
fb13bb25-db57-8490-006f-d9f9f4961917,SNUG,0,FE,SMB-SMS,
ea56f63e-0a0a-8c0a-00b8-3468d6d90bf4,Social Affairs Office,1,FE,HR-DHO-SAS,gguinot
,,,,,ppessy
2aa753bd-2423-f000-7904-c39b27667abf,SOCOMEC vendor,0,FE,IT-CF-SAO,
75cc075f-ddc9-d140-7904-7721dbdf3b02,Software Development for Experiments,1,FE,EP-SFT,mato
ea56f7cf-0a0a-8c0a-0149-bbb155d28701,Software for Network and Telecom,1,FE,IT-CS,mkhelif
5961742d-4fa9-dbc0-d7b5-0ebf0310c7bf,Software Licence Office,1,FE,IT,reguero
ea5714b9-0a0a-8c0a-0033-6230246cee13,SonicMQ,0,FE,GS-ASE-EDS,fanou
04545b7c-5d93-4000-9878-a85f147607e4,Space Management Forum,0,FE,SMB-SE,
f0a695de-0467-8984-7904-3b41fde4f985,Special Event Support Teams,1,FE,IR-ECO,moret
6b3d8a14-94bd-3484-6d21-20312bb3b611,Special working hours,1,FE,HR-CBS-B,ygrange
9b6dee40-4f56-b2c0-fe13-2eff0310c7d8,Specialized Computing Support,1,FE,IT-CF-SM,cremel
,,,,,pocock
e0ddb363-0a0a-8c0a-0078-281a718115dd,Specific IT Contract Support,1,FE,IT-CF-SM,cremel
,,,,,pocock
b9d924fc-4f38-1740-b9cc-09de0310c706,Specific SMB-SMS Contract Support,0,FE,SMB-SMS,gbalazs
28cd16a5-f02f-5dc0-7904-6a0c9f0e01ff,Staff Association Council,0,FE,HR,
994d1a65-f02f-5dc0-7904-6a0c9f0e01d9,Staff Association Secretariat,1,FE,HR-SA,cregelbr
ea56f4bd-0a0a-8c0a-00f1-afa182b8b79a,Staff Recruitment,1,FE,HR-TA-SR,acook
dd7fdaa3-84c9-7c40-c713-6646aaa7cede,Staff Unit - Outreach,1,FE,HR-TA-SR,acook
,,,,,vgalvin
ea571947-0a0a-8c0a-015c-0b2c369b2b3e,Steel Structures,0,FE,SMB-SE-CEB,
ea56ebb9-0a0a-8c0a-00de-a374f3e26672,Storage Area Operation,1,FE,SCE-SSC-LS,elclerc
,,,,,pmuffat
ea5705d2-0a0a-8c0a-01e6-462bd7198fea,"Storage, Recuperation and Sales",1,FE,SCE-SSC,gbolling
,,,,,jecarnot
79c79fbd-2423-f000-7904-c39b27667ae6,Storagetek vendor,0,FE,IT-CF-SAO,
40e123b2-985a-3800-6d21-708547f4c67f,Stores - Chemical Products,1,FE,SCE-SSC-SC,cgarino
05953633-0a0a-8c0a-01ab-456452d8312a,Stores - Gas Distribution,1,FE,SCE-SSC-SC,cgarino
,,,,,jbenelha
5fbe9fba-981a-3800-6d21-708547f4c6e7,Stores - Orders reception and quality control,0,FE,SMB-SC-SO,cgarino
1493efb6-985a-3800-6d21-708547f4c6cb,Stores - Raw material,1,FE,SCE-SSC-SC,cgarino
486667fe-985a-3800-6d21-708547f4c62c,Stores - Safety masks,0,FE,SMB-SC-SO,cgarino
ea56ebcd-0a0a-8c0a-0136-f8effcaf05c7,Stores Catalogue,0,FE,SMB-SC-SO,
ea570a85-0a0a-8c0a-0102-49e2ff83b49d,Stores Catalogue Maintenance tool,1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
ea56ebe3-0a0a-8c0a-009a-c622d75d0670,Stores Operation,1,FE,SCE-SSC-SC,cgarino
ea56f4d5-0a0a-8c0a-0042-875600d85625,Students and Trainees,1,FE,HR-TA-SGR,acook
,,,,,ischmid
ac2ddf39-2444-0d40-7904-c39b27667a09,Suggestion and Complaint Management,0,FE,SMB-SMS,gbalazs
54c7dbbd-2423-f000-7904-c39b27667abd,Sun vendor,0,FE,IT-CF-SAO,
ea5706c5-0a0a-8c0a-0146-25f8a3f1e32a,Supplier Invoice Handling,1,FE,FAP-ACC-AP,cmarme
,,,,,jarobins
ea5707b6-0a0a-8c0a-0157-530b02b4ae5e,Supplier Management,1,FE,IPT-PI-RC,gregorio
0568e8b7-0a0a-8c0a-0147-0410e05e7bc9,Supply Chain Desk,1,FE,SCE-SSC-SC,cbruggma
,,,,,cgarino
,,,,,challoin
,,,,,gtruelov
,,,,,lakhouay
,,,,,skrattin
,,,,,tjitske
fd36919d-1b53-a410-dc14-117a3b4bcbdb,Support for the Request of External Funds document,1,FE,FAP-EF-TC,kgachet
1294b41a-11d9-e500-adf9-ea91634bc63b,Surface civil works,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
ea57150b-0a0a-8c0a-00d2-f13b045bb30d,SUSI Operation,1,FE,EN-AA-AC,fchapron
,,,,,ninin
,,,,,rnunes
ba9a8dd7-4f96-e240-b9cc-09de0310c730,SUSI Projects,1,FE,EN-AA-AC,fchapron
,,,,,ninin
,,,,,rnunes
c82abf4f-4f3e-9a00-b8cb-ca1f0310c7b3,SWAN,1,FE,IT-ST-GSS,abchan
,,,,,dalvesde
,,,,,ebocchi
,,,,,etejedor
,,,,,moscicki
,,,,,pkothuri
ffcd36c7-e985-4101-a52e-b751215f93a7,SY,1,Department,SY,
7620d9a4-1b8d-2010-86ec-40498b4bcbec,SY Central Secretariat,1,FE,SY-AR,ksigerud
ea56f67a-0a0a-8c0a-0087-616a5ce1c49b,Sys Admin,0,FE,IT-CF,
01101cbd-513f-4b38-bd2d-b56d6b12b4b4,T209,1,Experiment,None,
91aa033f-fcb7-4a0b-8735-3f3f27857ac2,T211,1,Experiment,None,
52d27445-9de2-44c1-a48f-07a9e2214e1e,T227,1,Experiment,None,
8cba86c3-2e04-4f11-81f7-a3f62b192d3b,T236,1,Experiment,None,
e6a0e1d5-5e87-4165-a88e-2ae8e07d9555,T237,1,Experiment,None,
2816bbe9-9b76-4130-8faf-1ab4359f5691,T239,1,Experiment,None,
1ffeb3f0-54c7-4493-a0d6-912455ff842e,T248,1,Experiment,None,
0027b0a8-c015-49e6-85d5-31004092944e,T250,1,Experiment,None,
f8eaa90d-5349-44c0-b193-5b41fadec4a1,T2K,1,Experiment,None,
7f0a7542-db95-f7c0-f2d6-65925b96190d,Talent Acquisition Strategy,1,FE,HR-TA,taillieu
a2612b3b-4f6f-8300-77fe-29001310c706,TAXI services,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
ea570635-0a0a-8c0a-0124-e27e209a5506,TC Payment Control,0,FE,FAP-EF-TC,kgachet
dfe9bf47-d504-446a-ba60-9606d53e939e,TE,1,Department,TE,
ea5706b0-0a0a-8c0a-01c2-79588b51962f,Team Account Invoice Creation,0,FE,FAP-EF-TC,kgachet
a1d797fd-2423-f000-7904-c39b27667aa8,Tech as vendor,0,FE,IT-CF-SAO,
2428c87e-9434-3840-6d21-20312bb3b60b,Technical apprentices,0,FE,TE-PPR,prieto
216632ab-4fb7-1e00-b9cc-09de0310c763,Technical Console Support,1,FE,BE-CO-IN,veym
b60b822e-0a0a-8c08-01c3-379da879aa95,Technical Infrastructure for the Training Center,1,FE,HR-LD,mfiascar
66c5ea01-fcbf-4900-7904-2318fd702ef4,Technical Infrastructure Transport Dispatching,0,FE,EN-HE,ruehli
e3cb32f8-a074-4504-6d21-df786d7ffc14,Technical Management Training,1,FE,HR-LD,mosselma
ea56f913-0a0a-8c0a-000a-0df90eb034c8,Technical Network,0,FE,IT-CS,
ea56f401-0a0a-8c0a-0150-876617722cf9,Technical Training,1,FE,HR-LD,mfiascar
6d5de956-1ba0-94d0-c917-65b0ab4bcb71,TEIGI tool suite and Secrets Management,1,FE,IT-CM-LCS,timbell
,,,,,toteva
ea5709fa-0a0a-8c0a-0151-2d5f27e0d61d,Telecom Lab Support (Phone Admin. and Billing),1,FE,FAP-BC,dtsekour
,,,,,mcmarque
,,,,,varmeanu
497b500f-4ffc-9bc0-06bc-10ee0310c70d,Telematic KUANTIC,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
e89b2c19-4fff-1380-77fe-29001310c7b4,Telework Officer,1,FE,HR-FL,afavre
,,,,,taillieu
ea570919-0a0a-8c0a-01b3-8de2aef54368,Temporary Labour Office,0,FE,IPT-PI-IS,larac
,,,,,sblanch
1c48ef51-1136-4933-bf0c-727185bc5b2d,TERA,1,Experiment,None,
679a6569-9465-ed00-7904-7129f337bf6e,Tertiary building renovations,0,FE,SMB-SE-CEB,fmagnin
f852fe51-e5ba-4936-bd93-cc4bda453434,TESLA,1,Experiment,None,
25e96df7-c6cb-4513-910d-f79c67123c84,TH,1,Department,TH,
ca74b087-4fc3-df40-ce1b-401f0310c746,TH Secretariat,0,FE,TH-GS,giudice
b0f40678-4fbb-7a80-b9cc-09de0310c768,TI Alarm Support,1,FE,BE-CSS-GTA,fchapron
,,,,,mbraeger
,,,,,solp
41c17faa-ff67-1440-9878-114e40b9ad85,TI Operations,1,FE,BE-OP-TI,jespern
ea570265-0a0a-8c0a-00e4-21e7828706ee,Tier-0 Accounting,1,FE,IT-CM-IS,mccance
ea5713aa-0a0a-8c0a-0045-fcda59a3f291,TIM,0,FE,BE-ICS-CSE,fchapron
,,,,,mbraeger
,,,,,suwalska
ea571398-0a0a-8c0a-0048-ea10e80c7978,TIM Support and Consultancy,1,FE,BE-CSS,fchapron
,,,,,mbraeger
,,,,,ninin
,,,,,solp
07487edc-1b4f-e810-2f4a-dc6a9b4bcbc7,TIMBER,1,FE,BE-CSS-GTA,mbraeger
c0bee96c-1bd7-e010-c1d8-fe62cb4bcbcd,Timing,1,FE,BE-CSS-FST,sdeghaye
0ff77da2-0a0a-8c0a-0098-8f483e5aea58,Topographical Survey,1,FE,SCE-SAM-TG,mpoehler
,,,,,yrobert
0ca3c790-cf00-4344-b995-6ab02319333e,TOTEM,1,Experiment,None,
e9ae5eca-4f42-d240-4b4a-bc511310c7d4,TPG Annual Subscription,0,FE,SMB-SIS,lalejeun
,,,,,vmarchal
6762ecae-dba0-5380-1aab-9247db9619bf,Track it,1,FE,EN-ACE,fanou
bd7a37c9-2427-3000-7904-c39b27667aa3,Training Internal Reporting support,0,FE,HR-LD,
ea56f42d-0a0a-8c0a-001d-fcb602118bfa,Training Room and Infrastructure Management,0,FE,HR-LD,hanan
d387977d-2423-f000-7904-c39b27667afe,Transtec vendor,0,FE,IT-CF-SAO,
d11788ba-01e6-e084-9878-151822750d10,Transvoirie,1,FE,SCE-SSC-CS,mlafager
,,,,,pmuffat
582f6279-3875-f000-6d21-90440d25b3b2,Travel fees,1,FE,HR-CBS-B,ygrange
ea5703f7-0a0a-8c0a-0017-2dd486dd64eb,Treasury and Cash Management,1,FE,FAP-DHO-TP,cspencer
,,,,,lpedreno
a6020a6f-f55c-5c40-9878-e86b196c5c4d,TREC Application,1,FE,EN-IM-AMM,smallon
ea56fb56-0a0a-8c0a-0066-294c5b0cd352,TSM Backup,1,FE,IT-ST-TAB,okeeble
,,,,,vlado
fb2a2e93-da93-446a-986d-db9ba42b59bc,TT-PET,1,Experiment,None,
3fd0a3a0-4f4b-8300-77fe-29001310c7dc,Tunnels and technical galleries,1,FE,SCE-SAM-CE,pcardon
,,,,,sroulet
d265b09a-11d9-e500-adf9-ea91634bc60d,Tunnels and technical galleries maintenance,0,FE,SMB-SE-CEB,fmagnin
b4647cd6-11d9-e500-adf9-ea91634bc615,Tunnels and technical galleries works,0,FE,SMB-SE-CEB,fmagnin
ea56e95a-0a0a-8c0a-01d3-1866fff58f95,Twiki,1,FE,IT-CDA-WF,awagner
,,,,,pete
8a9123b0-b816-4067-a8ce-93ab7f070343,UA1,1,Experiment,None,
4221ed21-8189-4922-98a4-b656b83f96f8,UA2,1,Experiment,None,
fd2003f0-b04c-4c85-96d5-406077dad317,UA3,1,Experiment,None,
483175a7-32f1-487f-92e0-41b96ea7a1c9,UA4,1,Experiment,None,
84f2d319-e6c2-4ca4-9ef6-e8cac637ac7c,UA4/2,1,Experiment,None,
5b0c6b37-f2fc-4132-b70e-e039f49945c9,UA5,1,Experiment,None,
1a5460e3-dad3-4e52-939b-8f80129bec6c,UA5/2,1,Experiment,None,
38dbb81f-24ab-483d-b3cb-82fb5b0141bc,UA6,1,Experiment,None,
2a56dcdb-6c73-4087-a8c2-1de66f9f21f7,UA7,1,Experiment,None,
da14d170-04d1-4674-8d0f-9ff487bd8021,UA8,1,Experiment,None,
782d0540-0ff3-4671-943e-5a3f79dc7005,UA9,1,Experiment,None,
2fa717bd-2423-f000-7904-c39b27667a0b,Undefined vendor,0,FE,IT-CF-SAO,
ea571909-0a0a-8c0a-0077-1f20a3ba8f6d,Underground Works,0,FE,SMB-SE-CEB,fmagnin
c22b4e50-94bd-3484-6d21-20312bb3b6ef,Unemployment benefits,1,FE,HR-CBS-B,ygrange
8fab634a-8468-bc00-c713-6646aaa7ce57,UNIQA,0,FE,HR-CB-HIS,matheys
ea56f5df-0a0a-8c0a-0003-aec8d75645e3,UNIQA Contrat Manager,0,FE,HR-CB-HIS,matheys
aecf1b64-6851-4ca2-b315-f9ad8f3f23e8,UNOSAT,1,Experiment,None,
92771c28-9d4f-4e82-98bc-0cb85cd445ea,Up2U,1,Project,None,
9aa9a11a-9491-6100-7904-7129f337bfaf,Urban planning,1,FE,SCE-SAM-TG,mpoehler
ea57119e-0a0a-8c0a-01ca-e9f7ea6b72aa,User Office Tools,1,FE,FAP-BC,dtsekour
,,,,,lsmith
,,,,,mcmarque
3b4ebee0-4f34-13c0-06bc-10ee0310c793,Vaudoise Insurance,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
,,,,,vmarchal
6146eb38-4fab-6200-87a2-f5601310c768,Vehicle Application,1,FE,FAP-BC,ctsiplak
,,,,,dtsekour
,,,,,lsmith
ea56ec94-0a0a-8c0a-0196-57139f9a89bc,Vehicle Registration,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
9c854b4d-1b69-a050-5a31-41588b4bcbbc,Vehicles Bodywork- Europ carrosserie,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
e51363c5-db68-9b40-1aab-9247db9619b8,Vehicles Supplier - ByMyCar,1,FE,SCE-SSC-CS,gbolling
,,,,,lalejeun
f19fce24-db70-f3c0-269d-9e24db961988,Vending Machines Meyrin site,1,FE,SCE-SSC-CS,vmarchal
a9d59878-dbfc-f3c0-269d-9e24db9619e1,Vending Machines Other site than Meyrin site,1,FE,SCE-SSC-CS,lalejeun
,,,,,vmarchal
ea5702aa-0a0a-8c0a-0159-9ffdf06b206d,Version Control Systems,1,FE,IT-CDA-WF,alossent
,,,,,awagner
,,,,,eduardoa
ea570f10-0a0a-8c0a-007d-439730c30443,Vertical Line-of-business Applications,0,FE,GS-AIS-GDI,jjanke
4b0b1ae5-b510-7100-7904-5e3417224bf7,Video Publishing Workflow,0,FE,IT-CDA-IC,
6f2d0cc2-6980-2100-adf9-2ce5ed422308,Video surveillance and access logs,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
44d536a1-a465-5000-9878-a279f01cb799,Vidyo,0,FE,IT-CDA-IC,rgaspar
,,,,,tbaron
9eb75bbd-2423-f000-7904-c39b27667a6f,Vidyo vendor,0,FE,IT-CF-SAO,
f487537d-2423-f000-7904-c39b27667a31,Viglen vendor,0,FE,IT-CF-SAO,
ada0b1e7-4f7a-4e40-4b4a-bc511310c74b,Virtual Personal Computer,1,FE,BE-CSS-ISA,toulevey
0bfbc42c-db9b-e010-b839-d9f9f4961904,Virtual Personal Computers --doublon,0,FE,BE-CSS,solp
ea56fee7-0a0a-8c0a-008f-6b070db2e992,Virtualisation,0,FE,IT-CM-RPS,
7309a592-ddd5-1d80-7904-7721dbdf3b9f,Visit Exhibitions Content,1,FE,IR-ECO,esanders
e7a8a592-ddd5-1d80-7904-7721dbdf3b6b,Visit Exhibitions Media Maintenance,1,FE,IR-ECO,denarie
,,,,,esanders
f7c7953e-e965-d900-adf9-3f25a98a680e,Visit Exhibitions Non-Media Maintenance,1,FE,IR-ECO,denarie
,,,,,dominiqu
,,,,,esanders
567ea873-4fc2-e600-b9cc-09de0310c786,Visit Safety Officer,1,FE,BE-ASR-SU,tavlet
b4cf4e27-0a0a-8c08-0022-b840573a9311,Visitor access card,1,FE,SCE-DOD,bejar
,,,,,dconstan
,,,,,hmontane
ea56e92f-0a0a-8c0a-0102-501cc458970c,VOBox infrastructure support,0,FE,IT-PES-PS,
ea570294-0a0a-8c0a-0076-5f1d55a693dc,VOMRS,0,FE,IT-PES-PS,
ea570161-0a0a-8c0a-00fa-89d86c2954da,VOMS,1,FE,IT-CM-IS,mccance
ea56f003-0a0a-8c0a-0138-5b71b8a7eb76,W. Pauli Archive,1,FE,RCS-SIS-AR,ahollier
f35d0215-35a7-4c07-a327-b4f64b382ad2,WA1,1,Experiment,None,
04f508af-244c-433f-a6bb-acb99987352f,WA1/2,1,Experiment,None,
176bd9d5-2ed3-414e-adb9-932f88af107f,WA10,1,Experiment,None,
c4fa590c-a9ab-47f9-bac0-12cd7a1ea9b6,WA100,1,Experiment,None,
a29f6dca-05e8-4887-b40a-f8e7fdd02d93,WA101,1,Experiment,None,
bbe836fd-411b-4890-8ab5-ce58e9e23b8e,WA102,1,Experiment,None,
bdfc3b07-f324-4ee5-bb26-ae4b106daf85,WA103,1,Experiment,None,
6d2ca5be-24cb-4be9-a211-f1a59e12f6d6,WA104,1,Experiment,None,
eec7b163-8cb0-4ed8-90f8-a072bf04a32c,WA105,1,Experiment,None,
4b876cd9-bed3-424e-983d-4a998b111324,WA11,1,Experiment,None,
0ba5a0e7-005e-4d7f-aa1b-524922a395a6,WA12,1,Experiment,None,
3f8a012e-436b-426b-a4b1-9a0df9301b5a,WA13,1,Experiment,None,
052a308d-b9d8-492e-b770-88bfc36da1bb,WA14,1,Experiment,None,
95e59f7f-4ab1-4c8b-bcb3-bf673d95147a,WA15,1,Experiment,None,
6c024919-c5b0-4571-9ddb-fdb797521dcf,WA16,1,Experiment,None,
18bbae75-b60e-4952-a2d7-83ea3c612df2,WA17,1,Experiment,None,
a6a9ca7b-37f4-4bd8-8eaa-6cc1e3fe8033,WA18,1,Experiment,None,
71a2f706-49c7-4908-a738-aba69796d71d,WA18/2,1,Experiment,None,
1d0a4717-90c4-4b2c-8b07-8c1ddb58ec4a,WA19,1,Experiment,None,
7b4bcdb2-4fdd-4fe8-b124-8ba3036cba6c,WA2,1,Experiment,None,
b1fef0b8-113e-4834-a3f1-80fe4a0a6cfc,WA20,1,Experiment,None,
66ecd748-faba-4dba-ad28-6934bb3d7f5c,WA21,1,Experiment,None,
8f0dd806-8fff-41ce-bc91-a23d69713f9e,WA22,1,Experiment,None,
88b0a28c-c57a-41c2-8945-b44d634c9264,WA23,1,Experiment,None,
cf962d2e-c45b-4c12-9942-5563069d4131,WA24,1,Experiment,None,
c1037f71-e1d8-44a4-8b0e-5a32ff0db8ab,WA25,1,Experiment,None,
b6870191-7bbd-400b-9f43-98dbfe747eee,WA26,1,Experiment,None,
a9418f3e-8397-426e-a25a-1d664856dfc5,WA27,1,Experiment,None,
41a7a02e-06c8-4c96-84c7-9f5c162bca14,WA28,1,Experiment,None,
3e3e13cc-a2f5-413b-981b-9abce31186a5,WA29,1,Experiment,None,
9731e18b-7f4d-41ad-8f45-ef0a95c03a49,WA3,1,Experiment,None,
477e89e6-6b2d-47a0-b01e-ce43521d0b7b,WA30,1,Experiment,None,
6ed64f0f-d836-48ac-b332-e6441102a8c5,WA31,1,Experiment,None,
ef18a58d-276d-407c-a4c5-14884aea2a80,WA32,1,Experiment,None,
0f09fdf8-12db-44bd-9438-10a97dd2b3db,WA33,1,Experiment,None,
0db61d0c-7f2f-4443-a194-a1f13d6527b6,WA34,1,Experiment,None,
846605ba-7570-45f2-8702-b4d40c0833b3,WA35,1,Experiment,None,
fcce05b4-6027-4532-9cf1-e6d8b8429ea7,WA36,1,Experiment,None,
6e1b1598-6695-4c27-aa99-bc958472c102,WA37,1,Experiment,None,
5b934fbb-9132-4d3f-87ee-664913835870,WA38,1,Experiment,None,
f0030a24-30d9-4374-978b-ef9d6a4fa025,WA39,1,Experiment,None,
831de888-e0af-4964-a0bf-e217267ccbd4,WA4,1,Experiment,None,
5fb01a6d-453b-4fae-a754-2a4c44228c61,WA40,1,Experiment,None,
d2d277a0-bdc5-4ee5-97c8-5035e89b8308,WA41,1,Experiment,None,
6fe0303f-81c6-4330-a71c-a706b3589944,WA42,1,Experiment,None,
026775ec-d8ba-4550-ab1e-2b20554c5957,WA43,1,Experiment,None,
d58d62b0-c113-4a10-b707-7e61150ecbe1,WA44,1,Experiment,None,
6710a69c-fa5a-4c52-8bfd-068ddd42b4ad,WA45,1,Experiment,None,
9f8fa40f-dd4c-49be-a68a-2a26bb332542,WA46,1,Experiment,None,
e0e89786-b57c-4189-b294-898f4d2903ba,WA47,1,Experiment,None,
32d02ad4-e693-40e4-ae17-cbcda6eaf5c6,WA48,1,Experiment,None,
d931f270-e0a1-45e7-8df4-3715c4952ce6,WA49,1,Experiment,None,
eb654f3b-cb53-4084-a3eb-2bd9670f41c8,WA5,1,Experiment,None,
c65a436e-0412-4ffd-b850-99e2e6c54d65,WA50,1,Experiment,None,
267d3e9f-9d0e-4474-8f6f-ff774a2dcd9f,WA51,1,Experiment,None,
ccb02e68-a84e-4129-a12a-5c711a3a9f92,WA52,1,Experiment,None,
3b6c6168-4ac7-4b20-ab6a-025d97f4ca04,WA53,1,Experiment,None,
a16149d6-fc01-427f-ba61-08bb48a4bd92,WA54,1,Experiment,None,
bf3006e6-4f28-4658-a9b9-2710d57c9e8e,WA55,1,Experiment,None,
f7343d13-3573-4ddc-8523-40acfed9715d,WA56,1,Experiment,None,
edcf1f97-b83a-4239-b6bc-475a7c5fda3b,WA57,1,Experiment,None,
3a5598ff-eaf2-4e56-b457-4820673986ed,WA58,1,Experiment,None,
a27eec64-1754-4a87-ab08-ca5b58c376f5,WA59,1,Experiment,None,
4950991a-58a1-483c-bbe3-9c2a05a64eee,WA6,1,Experiment,None,
99825662-a64a-49af-96c2-ccbaf8a45e36,WA60,1,Experiment,None,
cd53b16a-0e89-4cb0-a9c6-2912f181bef1,WA61,1,Experiment,None,
4d816100-8617-422b-af58-c7c6fe3a51bc,WA62,1,Experiment,None,
b710ae17-1332-4d77-991f-57ea30550cad,WA63,1,Experiment,None,
7bbed525-ae5c-4c8d-b97d-845f8663a2b9,WA64,1,Experiment,None,
327623a0-479c-4c02-b57f-13da65a7137c,WA65,1,Experiment,None,
6232cbf5-8052-468d-b7c7-aea6c8b815be,WA66,1,Experiment,None,
af9d16a9-86c0-43a5-9f1e-9bf7f58125b9,WA67,1,Experiment,None,
c2d3027d-b383-47aa-be24-305d8a19bb18,WA68,1,Experiment,None,
2ae4dd3c-6485-40bf-a91e-f2588add39dc,WA69,1,Experiment,None,
fa7cc657-4af1-434b-b4b1-312872cd8330,WA7,1,Experiment,None,
4289ccc4-b8ae-4d82-a4dd-642b8c8df4ae,WA70,1,Experiment,None,
d007dabf-49ed-4c7e-88df-59fa651dff05,WA71,1,Experiment,None,
c33f1fff-1b3b-4b02-bb92-243df3c28061,WA72,1,Experiment,None,
886f7829-f650-4359-ade3-cd4bfba28055,WA73,1,Experiment,None,
84bb8156-d50d-48de-a5a3-c1f696acf4ef,WA74,1,Experiment,None,
cf6be714-40d7-4263-bf2c-22e5a029d43d,WA75,1,Experiment,None,
1c5df7f1-cbbb-47eb-884b-6a1b1647b2c6,WA76,1,Experiment,None,
918bcf47-4555-4226-bb40-3f5e2a5b4c99,WA77,1,Experiment,None,
c9fa8a75-25c2-4e19-b24f-f407526edefa,WA78,1,Experiment,None,
9148cb69-b02b-472c-aa4e-6d9cf4588592,WA79,1,Experiment,None,
c4b40a27-d252-491e-bd76-0aba411a1fa9,WA8,1,Experiment,None,
97c12a73-960a-4b2b-a537-480167ee63b6,WA80,1,Experiment,None,
e9372df1-d060-49ef-b9fd-0d4913dd0b61,WA81,1,Experiment,None,
1244708f-51e4-444c-be1a-1138abcc094e,WA82,1,Experiment,None,
07432ce6-dec2-45a6-97fd-9c1c96e7b4de,WA83,1,Experiment,None,
295f8c58-3142-47ac-872b-a4a494cca678,WA84,1,Experiment,None,
b67566c6-8ebb-4128-8fee-d79c0d735640,WA85,1,Experiment,None,
ea1ed43f-0ace-4b31-93f5-49b399c0e50a,WA86,1,Experiment,None,
7894e8b4-f0d6-47b3-be18-dd09369ae00d,WA87,1,Experiment,None,
83d955a4-7041-43c7-a60d-a5a8e1af8431,WA88,1,Experiment,None,
0f0457c0-29e4-4122-870b-e50a7077b817,WA89,1,Experiment,None,
63b27f66-e274-4e69-81e9-639eeb3d22ff,WA9,1,Experiment,None,
096d83f8-c9b0-4c37-9089-49c3f27ffce6,WA90,1,Experiment,None,
1ffa90aa-1c63-4890-82f4-55aab46cde68,WA91,1,Experiment,None,
57adf2e9-2c54-4989-8940-a0cae2023dcc,WA92,1,Experiment,None,
9d38eda8-f012-40ac-9e1c-8507a36e8e30,WA93,1,Experiment,None,
4cad225c-c5db-47b9-86eb-df9d3c1d2c2c,WA94,1,Experiment,None,
d579ee02-2899-4bbd-a649-65007a88ee3f,WA95,1,Experiment,None,
3906356e-22ca-4edb-8ad1-d7dc51447d3d,WA96,1,Experiment,None,
8c1fdf27-ccd0-44ad-b6db-2d9366ec62af,WA97,1,Experiment,None,
775b5b56-0fc5-426e-baeb-7c42ff27109e,WA98,1,Experiment,None,
62f2865b-0632-453f-8ae9-6da3eefaeec8,WA99,1,Experiment,None,
0979315b-39e9-462a-9e8a-be444af7771e,WA99/2,1,Experiment,None,
a9c8f234-99b7-6104-7904-62f7b78ce1e3,Water fountain design and studies,1,FE,SCE-SAM-IN,martelc
d685f2fc-9977-6104-7904-62f7b78ce163,Water fountain operation and maintenance,1,FE,SCE-SAM-IN,martelc
9dd7fab0-99b7-6104-7904-62f7b78ce149,Water fountain works,1,FE,SCE-SAM-IN,martelc
ea56ff48-0a0a-8c0a-0108-96c60c99ffbc,Web Infrastructure,1,FE,IT-CDA-WF,awagner
ea56ea69-0a0a-8c0a-0021-8973c11fa6cb,Webcast,1,FE,IT-CDA-IC,rgaspar
,,,,,tbaron
ea56f9a5-0a0a-8c0a-00f3-45eb014ca19d,"Weblogic, Tomcat Java application servers and 3rd party packages support",1,FE,IT-DB-DAR,awiecek
,,,,,edafonte
,,,,,grancher
8d6bd57c-9434-bc00-6d21-20312bb3b689,WIFI Network,1,FE,IT-CS,asosnows
,,,,,dgutier
0ddde364-0836-4980-adf9-6d3d8fa18a56,Wigner Hardware Repair,0,FE,IT-CF-FPP,
6a729e67-6557-b400-c713-3077c4af33b2,Wigner Operations,0,FE,IT-CF,
e7110295-f831-c900-adf9-02fd20678c82,Wigner vendor,0,FE,IT-CF-SAO,
0782c6b0-4fbb-7a80-b9cc-09de0310c7c7,WinCC OA Software Solutions,1,FE,BE-ICS-FT,fchapron
,,,,,fvarelar
,,,,,solp
ea57004b-0a0a-8c0a-00eb-1eb820dd187d,Windows Client Support,1,FE,IT-CDA-AD,mkwiatek
,,,,,sdellabe
ea56fefc-0a0a-8c0a-009d-213140b15051,Windows Installation and CMF,1,FE,IT-CDA-AD,deloose
,,,,,gmetral
,,,,,mkwiatek
ea570191-0a0a-8c0a-01b4-ddedfbf92845,Windows License Server,0,FE,IT-CDA-AD,
2e5e4ade-dbda-1c10-f807-898532961985,Windows Samba gateways for CERNBox,1,FE,IT-ST-GSS,abchan
,,,,,abrosaia
,,,,,lopresti
,,,,,moscicki
ea56ffc9-0a0a-8c0a-0087-0fd36a48874c,Windows Server Hosting,0,FE,IT-CDA-AD,
ea570079-0a0a-8c0a-0086-dcb00e2bc4f8,Windows Server Infrastructure,1,FE,IT-CDA-AD,bukowiec
,,,,,mkwiatek
ea56ff5c-0a0a-8c0a-00a5-a8c28da73d50,Windows Terminal Infrastructure,1,FE,IT-CDA-AD,bukowiec
,,,,,mkwiatek
cecc91bc-8f6d-4117-946c-b6b6ea708617,WIZARD,1,Experiment,None,
94d320aa-bf3a-44bc-8ba6-d79872fdee18,WIZCAL,1,Experiment,None,
448e4472-dbdf-8c90-1de7-3e4822961921,WLCG Accounting Utility,1,FE,IT-SC-OPS,andreeva
,,,,,dichrist
,,,,,ppaparri
c8d8363a-c9c3-5900-adf9-4df2054ec534,WLCG Cloud Integration and Support,0,FE,IT-CM-IS,
84baff94-99af-65c0-7904-62f7b78ce1a7,WLCG Cloud Monitoring,0,FE,IT-CM-MM,
c0d7cda1-19ea-9580-7904-0003aab1c917,WLCG Data Management Monitoring,0,FE,IT-CM-MM,
cbd4a1a9-1962-d580-7904-0003aab1c965,WLCG Experiment Probe Submission Framework,1,FE,IT-CM-MM,aimar
,,,,,mbabik
60da0921-192e-9580-7904-0003aab1c96f,WLCG Google Earth Dashboard,0,FE,IT-CM-MM,
cd36a1e1-19a2-d580-7904-0003aab1c9c2,WLCG HammerCloud,1,FE,IT-CM-IS,jschovan
,,,,,mccance
,,,,,psaiz
8939c929-19ea-9580-7904-0003aab1c9d8,WLCG Infrastructure Monitoring,0,FE,IT-CM-MM,
80650969-19aa-9580-7904-0003aab1c9d0,WLCG Job Monitoring,0,FE,IT-CM-MM,
948329e5-1962-d580-7904-0003aab1c9cd,WLCG Network Monitoring,1,FE,IT-CM-MM,aimar
,,,,,mbabik
722bc9b0-fc33-c500-7904-2318fd702ece,WLCG Resource Coordination,1,FE,IT-SC,panzer
,,,,,timbell
0bcbcd25-192e-9580-7904-0003aab1c911,WLCG SAM,0,FE,IT-SDC-MI,
ea56fc11-0a0a-8c0a-01df-3b210375f50f,WLCG Service Coordination,1,FE,IT-SC,andreeva
,,,,,litmaath
2bdc8c76-db9f-8c90-1de7-3e4822961923,WLCG Storage Space Accounting,1,FE,IT-SC-OPS,andreeva
,,,,,dichrist
,,,,,ppaparri
f78fae5a-4f1b-7e00-b9cc-09de0310c715,WLCG Understanding Performance,1,FE,IT-SC,dhsmith
,,,,,markusw
,,,,,sciaba
,,,,,valassi
ea56fde4-0a0a-8c0a-011d-d7d0f81fc71b,WLCG VOBOX,0,FE,IT-SDC-OL,
ea570251-0a0a-8c0a-017c-1f90a538dd4a,WMS,0,FE,IT-PES-PS,
49a7a280-4f12-fe80-7db7-d3ef0310c710,Wooden Pallets Collection,1,FE,SCE-SSC-LS,cgarino
,,,,,gtruelov
ea5717c0-0a0a-8c0a-0078-4d0719eb9549,Work Place Visit,1,FE,HSE-OHS,delamare
,,,,,wfadel
ea56f763-0a0a-8c0a-0184-f2c44bed8ef9,Workflow support,0,FE,IT-CF-SM,
e9c3f496-11d9-e500-adf9-ea91634bc636,Works following safety inspections,1,FE,SCE-SAM-CE,pcardon
,,,,,sftysnow
,,,,,sroulet
70e568a8-4f2b-1700-87a2-f5601310c79a,Workshops - Conferences and Scientific Events Secretariat,0,FE,EP-AGS-SCS,mage
4ca344c4-16ca-4018-836f-15405377e329,XDC,1,Project,None,
2c971b7d-2423-f000-7904-c39b27667a97,Xen vendor,0,FE,IT-CF-SAO,
9387a2a0-4402-6500-adf9-0d7d26052799,Your career at CERN program (CTM Administration),1,FE,HR-LD,eeastwoo
,,,,,mosselma
,,,,,ndumeaux
e9347869-0a0a-8c0a-009a-bf9422a74c71,Zenodo Repository,1,FE,IT-CDA-DR,jbenito
,,,,,lnielsen
,,,,,simko
051c1c47-e17d-4a47-879e-24d4ede0cfa6,ZEUS,1,Experiment,None,
2400cd20-dbf7-08d4-006f-d9f9f4961930,Zoom,1,FE,IT-CDA-IC,rgaspar
,,,,,tbaron
ea571520-0a0a-8c0a-0082-81a739763117,Zora Operation,1,FE,EN-AA-AC,fchapron
,,,,,ninin
,,,,,rnunes
