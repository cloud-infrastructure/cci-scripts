#! /usr/bin/perl -w

#   script -f roger.out -c "./chk-roger-state.pl  -h cloud_infrastructure  -h cloud_orchestration  -h cloud_telemetry  -h cloud_identity  -h cloud_adm  -h cloud_image  -h cloud_blockstorage -h cloud_dashboard  -h cloud_monitoring  -h cloud_compute  -h cloud_network -v"
#
# Todo:
#   - filter output by data, ie. only show older entries
#
use strict;
use diagnostics;
use Data::Dumper;
use Text::TabularDisplay;
use Getopt::Long;
use Date::Manip;
use JSON;


my $debug = my $verbose = 0;
my @hostgroup = ();
my $cutoffdate = undef;

my %opts = (debug   => \$debug,
            verbose => \$verbose,
    );
#
# Parse the options
#
my $rc = Getopt::Long::GetOptions(
    \%opts,
    "debug",
    "verbose",
    "hostgroup=s@" => \@hostgroup,
    "date=s"       => \$cutoffdate,
    );

#if (defined $cutoffdate){
#    my $date = new Date::Manip::Date;
#    my $err = $date->parse($cutoffdate);
#    if ($err){
#        print "[ERROR] Cannot parse \"--date $cutoffdate\"\n";
#        exit 1;
#    }
#    $opt{"date-of-intervention"} = $date->printf("%A, %B %e");
#    $opt{"time-of-intervention"} = $date->printf("%H:%M");
#}


my %hostgroup = ();

for my $hostgroup (@hostgroup){
    my $pdb = "/usr/bin/ai-pdb hostgroup --subgroups $hostgroup";#raw /v3/nodes --query '[\"=\",[\"fact\",\"hostgroup_0\"],\"" . $hostgroup . "\"]'";
    my $out = `$pdb`;
    my @data = @{from_json($out)};
    #print Dumper (\@data);exit;


    for my $href (@data){
	#print Dumper($href);
	my %data = %$href;
	#  print Dumper(\%data);
	my $host = (split(/\./,$data{certname}))[0];
	if (not gethostbyname($host)){
	    print STDERR "[W] PuppetDB returns non-existent host \"$host\", ignoring it...\n";
	    next;
	}
	#print " >> $host\n";
	$hostgroup{$host} = $data{hostgroup};
    }
}

#print Dumper(\%hostgroup);

my @host = keys %hostgroup;    
print "[I] Found ".scalar(@host)." servers in PuppetDB\n";
#print "@host\n";

my %state = ();
while (my @tmp = splice(@host,0,50)){
    last if not @tmp;
    my $roger = `roger show @tmp`;
    #print $roger;
    my @data = @{from_json($roger)};
    for my $href (@data){
	#print Dumper($href);
	my %data = %$href;
	#print Dumper(\%data);
	#map {print ">> $data{$_}\n";} qw(hw_alarmed nc_alarmed os_alarmed app_alarmed update_time updated_by message);
	my $hostname = (split(/\./,$data{hostname}))[0];
	map {$state{$hostname}{$_} = $data{$_} ? "true" : "false" } qw(hw_alarmed nc_alarmed os_alarmed app_alarmed);
	map {$state{$hostname}{$_} = $data{$_}} qw(update_time updated_by message appstate);
	#print Dumper(\%state);
	#exit;
    }
    print "[I] Still ".scalar(@host)." servers to process...\n" unless scalar(@host) == 0;
    #print Dumper(\%state);
    #last if scalar(@host) < 3600;
    #exit;
}

my $table = Text::TabularDisplay->new("Host","Hostgroup","HW","NC","OS","APP","AppState","User","Date","Message");
for my $host (sort {$state{$a}{update_time} <=> $state{$b}{update_time}} keys %state){
    my $alarmed = "true";
    map {$alarmed = "false" if $state{$host}{$_} eq "false"} qw(hw_alarmed nc_alarmed os_alarmed app_alarmed);
    next if $alarmed eq "true";
#    my $hostgroup = ForemanHostgroup($host);

    $table->add($host,
		$hostgroup{$host},
		$state{$host}{hw_alarmed},
		$state{$host}{nc_alarmed},
		$state{$host}{os_alarmed},
		$state{$host}{app_alarmed},
		$state{$host}{appstate},
		$state{$host}{updated_by},
		scalar(localtime($state{$host}{update_time})),
		$state{$host}{message},
	)
}

my $items = $table->items;
if ($items > 0){
    print "[I] $items/".scalar(keys %state)." hosts have one or more masked alarms\n";
    print $table->render . "\n";
}else{
    print "[I] No hosts with masked alarms found\n";
}

exit;
