from cryptography import fernet
from Crypto.Hash import HMAC
from Crypto import Random
from oslo_utils import importutils
import base64
import sys
from oslo_utils import encodeutils
import MySQLdb
from keystoneclient.v3 import client
from sets import Set

# variables

LOG_ONLY = True

HEAT_DB_PASSWORD = 'test'

SVCHEAT_PASSWORD = 'test'

CURRENT_ENCRYPT_PASSWORD = 'current'

# get_valid_encryption_key should double the key when the size does not match
# but it doesn't seem to work reliably, please add the double version here too
OLD_ENCRYPT_PASSWORDS = ('old', 'oldold')

# copied from heat/heat/openstack/common/crypto/utils.py

MAX_CB_SIZE = 256


class CryptoutilsException(Exception):
    """Generic Exception for Crypto utilities."""

    message = "An unknown error occurred in crypto utils."


class CipherBlockLengthTooBig(CryptoutilsException):
    """The block size is too big."""

    def __init__(self, requested, permitted):
        msg = "Block size of %(given)d is too big, max = %(maximum)d"
        message = msg % {'given': requested, 'maximum': permitted}
        super(CryptoutilsException, self).__init__(message)


def bchr(s):
    return bytes([s])


class SymmetricCrypto(object):
    """Symmetric Key Crypto object.
    This class creates a Symmetric Key Crypto object that can be used
    to encrypt, decrypt, or sign arbitrary data.
    :param enctype: Encryption Cipher name (default: AES)
    :param hashtype: Hash/HMAC type name (default: SHA256)
    """

    def __init__(self, enctype='AES', hashtype='SHA256'):
        self.cipher = importutils.import_module('Crypto.Cipher.' + enctype)
        self.hashfn = importutils.import_module('Crypto.Hash.' + hashtype)

    def new_key(self, size):
        return Random.new().read(size)

    def encrypt(self, key, msg, b64encode=True):
        """Encrypt the provided msg and returns the cyphertext optionally
        base64 encoded.
        Uses AES-128-CBC with a Random IV by default.
        The plaintext is padded to reach blocksize length.
        The last byte of the block is the length of the padding.
        The length of the padding does not include the length byte itself.
        :param key: The Encryption key.
        :param msg: the plain text.
        :returns enc: a block of encrypted data.
        """
        iv = Random.new().read(self.cipher.block_size)
        cipher = self.cipher.new(key, self.cipher.MODE_CBC, iv)

        # CBC mode requires a fixed block size. Append padding and length of
        # padding.
        if self.cipher.block_size > MAX_CB_SIZE:
            raise CipherBlockLengthTooBig(self.cipher.block_size, MAX_CB_SIZE)
        r = len(msg) % self.cipher.block_size
        padlen = self.cipher.block_size - r - 1
        msg += b'\x00' * padlen
        msg += bchr(padlen)

        enc = iv + cipher.encrypt(msg)
        if b64encode:
            enc = base64.b64encode(enc)
        return enc

    def decrypt(self, key, msg, b64decode=True):
        """Decrypts the provided ciphertext, optionally base64 encoded, and
        returns the plaintext message, after padding is removed.
        Uses AES-128-CBC with an IV by default.
        :param key: The Encryption key.
        :param msg: the ciphetext, the first block is the IV
        :returns plain: the plaintext message.
        """
        if b64decode:
            msg = base64.b64decode(msg)
        iv = msg[:self.cipher.block_size]
        cipher = self.cipher.new(key, self.cipher.MODE_CBC, iv)

        padded = cipher.decrypt(msg[self.cipher.block_size:])
        length = ord(padded[-1:]) + 1
        plain = padded[:-length]
        return plain

    def sign(self, key, msg, b64encode=True):
        """Signs a message string and returns a base64 encoded signature.
        Uses HMAC-SHA-256 by default.
        :param key: The Signing key.
        :param msg: the message to sign.
        :returns out: a base64 encoded signature.
        """
        h = HMAC.new(key, msg, self.hashfn)
        out = h.digest()
        if b64encode:
            out = base64.b64encode(out)
        return out

# copied from heat/heat/common/crypt.py


def encrypt(value, encryption_key=None):
    if value is None:
        return None, None
    encryption_key = get_valid_encryption_key(encryption_key, fix_length=True)
    encoded_key = base64.b64encode(encryption_key.encode('utf-8'))
    sym = fernet.Fernet(encoded_key)
    res = sym.encrypt(encodeutils.safe_encode(value))
    return 'cryptography_decrypt_v1', encodeutils.safe_decode(res)


def decrypt(method, data, encryption_key=None):
    if method is None or data is None:
        return None
    decryptor = getattr(sys.modules[__name__], method)
    value = decryptor(data, encryption_key)
    if value is not None:
        return encodeutils.safe_decode(value, 'utf-8')


def oslo_decrypt_v1(value, encryption_key=None):
    encryption_key = get_valid_encryption_key(encryption_key)
    sym = SymmetricCrypto()
    return sym.decrypt(encryption_key,
                       value, b64decode=True)


def cryptography_decrypt_v1(value, encryption_key=None):
    encryption_key = get_valid_encryption_key(encryption_key, fix_length=True)
    encoded_key = base64.b64encode(encryption_key.encode('utf-8'))
    sym = fernet.Fernet(encoded_key)
    return sym.decrypt(encodeutils.safe_encode(value))


def get_valid_encryption_key(encryption_key, fix_length=False):
    if encryption_key is None:
        encryption_key = CURRENT_ENCRYPT_PASSWORD
    if fix_length and len(encryption_key) < 32:
        # Backward compatible size
        encryption_key = encryption_key * 2
    return encryption_key[:32]

# fix script code


def mydecrypt(method, data, encryption_key=None):
    try:
        decrypted = decrypt(method, data, encryption_key)
        if decrypted is None or decrypted == '':
            return None
        return decrypted
    except Exception:
        return None


def is_ascii(s):
    return all(ord(c) < 128 for c in s)


update_user_cred_query = """UPDATE user_creds SET trust_id = %s, decrypt_method = %s WHERE id = %s"""

select_user_creds_query = """SELECT id, tenant, tenant_id, trustor_user_id, trust_id, decrypt_method FROM user_creds"""

select_resource_data_query = """SELECT id, decrypt_method, value, resource_id FROM resource_data"""

update_resource_data_query = """UPDATE resource_data SET value = %s, decrypt_method = %s, redact = %s WHERE id = %s"""

select_resource_from_id_query = """SELECT name, stack_id FROM resource WHERE id = %s"""

select_stack_from_user_cred_id_query = """SELECT id, name FROM stack WHERE user_creds_id = %s"""

select_stack_from_id_query = """SELECT id, name, owner_id, tenant, action, status FROM stack WHERE id = %s"""


def get_top_level_stack_id_from_user_cred_id(db, user_cred_id):
    select_cursor = db.cursor()
    select_cursor.execute(select_stack_from_user_cred_id_query, (user_cred_id, ))
    stack = select_cursor.fetchone()
    select_cursor.close()

    return get_top_level_stack_id_from_stack_id(db, stack[0])


def get_top_level_stack_id_from_resource_id(db, resource_id):
    select_cursor = db.cursor()
    select_cursor.execute(select_resource_from_id_query, (resource_id, ))
    resource = select_cursor.fetchone()
    select_cursor.close()
    return get_top_level_stack_id_from_stack_id(db, resource[1])


def get_top_level_stack_id_from_stack_id(db, stack_id):
    while stack_id is not None:
        select_cursor = db.cursor()
        select_cursor.execute(select_stack_from_id_query, (stack_id, ))
        stack = select_cursor.fetchone()
        select_cursor.close()
        if stack is None or stack[2] is None:
            return stack_id
        else:
            stack_id = stack[2]
    return stack_id


def print_stack_details(db, stack_id):
    select_cursor = db.cursor()
    select_cursor.execute(select_stack_from_id_query, (stack_id, ))
    stack = select_cursor.fetchone()
    select_cursor.close()

    if stack is not None:
        print('|' + stack[0] + '|' + stack[1] + '|' + stack[3] + '|' + stack[4] + '|' + stack[5] + '|')
    else:
        print(stack_id + ' ?????')


def main():
    keystone = client.Client(auth_url='https://keystone.cern.ch/main/v3', username='svcheat', password=SVCHEAT_PASSWORD, project_name='services')
    trusts = keystone.trusts.list(trustee_user='svcheat')

    db = MySQLdb.connect("dbod-heat.cern.ch", "heat", HEAT_DB_PASSWORD, "heat", port=5502)
    db2 = MySQLdb.connect("dbod-heat.cern.ch", "heat", HEAT_DB_PASSWORD, "heat", port=5502)

    user_creds_cursor = db.cursor()
    user_creds_cursor.execute(select_user_creds_query)

    all_impacted_stacks = Set()
    non_fixable_stacks = Set()

    while True:
        user_cred = user_creds_cursor.fetchone()
        if user_cred is None:
            break

        decrypted = mydecrypt(user_cred[5], user_cred[4])

        if decrypted is None:
            print
            print('handling non decryptable user_cred ' + str(user_cred[0]))

            top_level_stack_id = get_top_level_stack_id_from_user_cred_id(db2, user_cred[0])
            all_impacted_stacks.add(top_level_stack_id)

            # let's try older known encrypt pwds
            for old_encrypt_pwd in OLD_ENCRYPT_PASSWORDS:
                decrypted = mydecrypt(user_cred[5], user_cred[4], old_encrypt_pwd)
                if decrypted is not None:
                    print('trust id decrypted with an old pwd : ' + decrypted)
                    break

            if decrypted is None or len(decrypted) != 32:
                matching_trusts = filter(lambda t: t.trustor_user_id == user_cred[3] and t.project_id == user_cred[2]
                                         and t.impersonation and t.remaining_uses is None and t.expires_at is None, trusts)
                if len(matching_trusts) == 0:
                    print('trust id not decryptable and no matching trust, sorry')
                    non_fixable_stacks.add(top_level_stack_id)
                else:
                    decrypted = matching_trusts[0].id
                    print('matching trust ' + decrypted)

            if decrypted is not None and not LOG_ONLY:
                print('update user_cred with new encrypted trust id')
                encrypted_trust_id = encrypt(decrypted)
                data = (encrypted_trust_id[1], encrypted_trust_id[0], str(user_cred[0]))
                update_cursor = db2.cursor()
                update_cursor.execute(update_user_cred_query, data)
                db2.commit()
                update_cursor.close()
                update_cursor = None

    user_creds_cursor.close()

    resource_data_cursor = db.cursor()
    resource_data_cursor.execute(select_resource_data_query)

    while True:
        resource_data = resource_data_cursor.fetchone()
        if resource_data is None:
            break

        if resource_data[1] is not None and resource_data[1] != '':
            decrypted = mydecrypt(resource_data[1], resource_data[2])

            if decrypted is None:
                print
                print('handling non decryptable resource_data ' + str(resource_data[0]))

                top_level_stack_id = get_top_level_stack_id_from_resource_id(db2, resource_data[3])
                all_impacted_stacks.add(top_level_stack_id)

                # let's try older known encrypt pwds
                for old_encrypt_pwd in OLD_ENCRYPT_PASSWORDS:
                    decrypted = mydecrypt(resource_data[1], resource_data[2], old_encrypt_pwd)
                    if decrypted is not None:
                        break

                if decrypted is None:
                    print('resource_data value couldnt be decrypted with any old pwd, sorry')
                    non_fixable_stacks.add(top_level_stack_id)

                    if not LOG_ONLY:
                        print('the value will be erased and set to unencrypted')
                        data = ('', '', 0, str(resource_data[0]))
                        update_cursor = db2.cursor()
                        update_cursor.execute(update_resource_data_query, data)
                        db2.commit()
                        update_cursor.close()
                        update_cursor = None
                else:
                    print('resource_data value decrypted with an old pwd')

                    if not LOG_ONLY:
                        print('update resource_data with new encrypted value')
                        encrypted_value = encrypt(decrypted)
                        data = (encrypted_value[1], encrypted_value[0], 1, str(resource_data[0]))
                        update_cursor = db2.cursor()
                        update_cursor.execute(update_resource_data_query, data)
                        db2.commit()
                        update_cursor.close()
                        update_cursor = None

    print
    print('non fixable stacks:')
    print

    for stack_id in non_fixable_stacks:
        print_stack_details(db2, stack_id)

    print
    print('fixable stacks:')
    print

    for stack_id in all_impacted_stacks.difference(non_fixable_stacks):
        print_stack_details(db2, stack_id)

    db.close()
    db2.close()


if __name__ == '__main__':
    main()
