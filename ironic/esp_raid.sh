#!/bin/env sh

# Identify and RAID ESPs
#
# The script will find all ESP partitions, check if they
# are in a RAID-1 and create such a mirror if not.
#
# Assumptions:
#   - there is eaxctly only one vfat entry in the fstab
#   - the EFI mount point is /boot/efi

# check if the node is in UEFI mode
if [ ! -d "/sys/firmware/efi" ]; then
  echo "INFO: The node is not booted in UEFI mode, exiting"
  exit 0
fi

# get the ESPs and check if we need to RAID them
ESP_PARTS=`lsblk -o NAME,FSTYPE --list|grep vfat|grep -v md|cut -d ' ' -f1|xargs -I{} echo /dev/{}|xargs`
case ${ESP_PARTS} in
  (*[![:blank:]]*)
        echo "INFO: Found non-md ESPs: ${ESP_PARTS}"
        ;;
  (*)
        echo 'INFO: ESP seems already RAIDed, exiting'
        exit 0
        ;;
esac

# check if we have one vfat entry in fstab
VFAT_NUM=`grep -c vfat /etc/fstab`
if [ ${VFAT_NUM} -ne '1' ]; then
      echo 'ERROR: No or too many vfat entries found in fstab, exiting'
      exit 1
fi

# copy the original ESP contents
mkdir -p /tmp/efi.orig
cp -r /boot/efi /tmp/efi.orig/

# generate a config which has all kernels
grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
if [ $? -ne 0 ]; then
      echo "ERROR: Could not generate grub.cfg, exiting"
      exit 1
fi

# copy the current ESP contents
mkdir -p /tmp/efi.bak
cp -r /boot/efi /tmp/efi.bak/

# wipe the fs on all ESPs
umount /boot/efi
for i in ${ESP_PARTS} ; do wipefs -a $i ; done
RAID_DEVICES=`echo ${ESP_PARTS}|wc -w`

# create the mirrored ESP
MD_DEVICE=/dev/md/esp
mdadm --create ${MD_DEVICE} --level=1 --raid-devices=${RAID_DEVICES} --metadata=1.0 ${ESP_PARTS}
mkdosfs -i 1234ABCD ${MD_DEVICE}

# replace the UUID
ESP_UUID=`grep vfat /etc/fstab| cut -d ' ' -f 1`
sed -i.bak "s/$ESP_UUID/UUID=1234-ABCD/" /etc/fstab
mount -a

# restore the boot loader and rebuild grub.cfg
cp -r /tmp/efi.bak/efi/* /boot/efi/

# set the default kernel to sth sensible (the latest installed kernel, not the latest kernel!)
DEFAULT_KERNEL=`grubby --info=ALL|grep -E '^kernel='|grep -v rescue|cut -d '=' -f2|sed 's/\"//g'|head -n1`
grubby --set-default=${DEFAULT_KERNEL}
