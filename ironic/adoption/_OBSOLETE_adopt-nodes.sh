# Remaining nodes for teststack
# p06036580g17167
# p06036580g33235

echo "OBSOLETE"
exit 1

wait_for_file(){
while [ ! -f /tmp/$1 ]
do
    echo waiting for file /tmp/$1
    sleep 5
done
rm /tmp/$1
}

NODES='p06036580f43823 p06036580f84937 p06036580g17167 p06036580g33235'

RESOURCE_CLASS='BAREMETAL_P1_ADOPTME_1707'
FLAVOR='p1.adoptme.1707'  # Very important to create a new flavor for each delivery/cell etc. in order to avoid any user to instantiate an adopted node.
IMAGE='"CC7 - x86_64 [2018-12-03]"'

AGGREGATE_ID='a17abc2e-665c-4e06-8ebb-0f283561bc28'  # teststack
CELL_UUID='f3f0f26f-c30e-4244-bac5-04293dbdb881'  # teststack

IRONIC_DEPLOY_RAMDISK_KERNEL='6b9cd55d-1f1b-4d2e-912e-3f3cd8551a2a'  # teststack

echo about to enroll nodes $NODES
wait_for_file "adoption_begin"

for HOST in $NODES
do
    echo 'adding host to ironic: $HOST'
    NAME=$(python get_serial_number.py $HOST);
    MAC_ADDRESS=$(python get_mac_address.py $HOST);
    BMC_IP_ADDRESS=$(getent hosts $HOST-ipmi | awk '{ print $1 }');
    IPMI_USERNAME=$(ai-ipmi get-creds $HOST | grep Username | awk '{print $2}');
    IPMI_PASSWORD=$(ai-ipmi get-creds $HOST | grep Password | awk '{print $2}');
    openstack baremetal node create --driver ipmi --management-interface cern-ipmi --console-interface cern-webconsole --driver-info ipmi_port=623 --driver-info ipmi_username=$IPMI_USERNAME --driver-info ipmi_password=$IPMI_PASSWORD --driver-info ipmi_address=$BMC_IP_ADDRESS --driver-info deploy_kernel=$IRONIC_DEPLOY_RAMDISK_KERNEL --driver-info deploy_ramdisk=$IRONIC_DEPLOY_RAMDISK_KERNEL --name $NAME
    BM_NODE_UUID=$(openstack baremetal node show --fit $NAME -c uuid | grep uuid | awk '{print $4}');
    openstack baremetal port create --node $BM_NODE_UUID $MAC_ADDRESS
    openstack baremetal node manage $NAME
    openstack baremetal node set --driver fake-hardware $NAME  # ipmi
    openstack baremetal node set --management-interface fake $NAME  #cern-ipmi
    openstack baremetal node set --deploy-interface fake $NAME  # iscsi
    openstack baremetal node set --boot-interface fake $NAME  #pxe
    openstack baremetal node set --resource-class $RESOURCE_CLASS $NAME
    openstack baremetal node provide $NAME
    echo node $NAME is available
    BAREMETAL_NODES+=($BM_NODE_UUID)
done


echo 'nova discover all new nodes'
wait_for_file "adoption_nova_manage_discover_hosts"
ssh root@cci-teststack-nova-frontend nova-manage cell_v2 discover_hosts --cell_uuid $CELL_UUID  --cern-ironic --verbose
echo 'adding new nodes to the aggregate'
for BM_NODE_UUID in "${BAREMETAL_NODES[@]}"
do
    export OS_PLACEMENT_API_VERSION=1.10
    openstack resource provider aggregate set --aggregate $AGGREGATE_ID $BM_NODE_UUID
done
sleep 60
echo 'We want to ensure the resource_tracker has run in all nodes (if sharded)'

# Let the resource tracker run, update all resources
echo 'Mock nova to consider all recently added nodes/resource providers are in use'
wait_for_file "adoption_reserve_all_rp_inventories"
for BM_NODE_UUID in "${BAREMETAL_NODES[@]}"
do
    export OS_PLACEMENT_API_VERSION=1.26
    openstack resource provider inventory set --resource CUSTOM_$RESOURCE_CLASS:reserved=1 --resource CUSTOM_$RESOURCE_CLASS:total=1 $BM_NODE_UUID
done

echo 'Creating the instances'
wait_for_file "adoption_create_instances"
for BM_NODE_UUID in "${BAREMETAL_NODES[@]}"
do
    openstack resource provider inventory set --resource CUSTOM_$RESOURCE_CLASS:reserved=0 --resource CUSTOM_$RESOURCE_CLASS:total=1 $BM_NODE_UUID
    openstack allocation candidate list --resource CUSTOM_$RESOURCE_CLASS=1
    ALLOCATION_CANDIDATES=$(openstack allocation candidate list --resource CUSTOM_BAREMETAL_P1_ADOPTME_1707=1 | grep CUSTOM_BAREMETAL_P1_ADOPTME_1707=1);
    CANDIDATES_COUNT=$(openstack allocation candidate list --resource CUSTOM_BAREMETAL_P1_ADOPTME_1707=1 | grep -c CUSTOM_BAREMETAL_P1_ADOPTME_1707=1);
    if [[ $ALLOCATION_CANDIDATES == *"$BM_NODE_UUID"* ]] && [[ $CANDIDATES_COUNT -eq "1" ]]
    then
        echo "The only candidate is the desired baremetal node"
    else
        wait_for_file "adoption_only_the_desired_candidate_is_available"
    fi

    ALIAS=$(openstack baremetal node show --fit $BM_NODE_UUID -c name -f value)
    NAME=$(nslookup $ALIAS | grep 'Name:' | awk '{print $2}' | sed 's/.cern.ch//')

    cat << EOF >  /tmp/adoption_create_instance_$NAME.sh
OS_PROJECT_NAME="Cloud Test - teststack_cell03"
OS_AUTH_URL=https://keystonecmp.cern.ch/v3
openstack server create --image $IMAGE --flavor $FLAVOR --property cern-waitdns=false adopt-$NAME  #TODO remove the adopt prefix, we need to keep the same name
EOF
    cat /tmp/adoption_create_instance_$NAME.sh | ssh dabadaba@aiadm

    openstack baremetal node show --fit $BM_NODE_UUID | grep provision_state
    STATUS=$(openstack baremetal node show --fit $BM_NODE_UUID -c provision_state -f value)
    if [ $STATUS == "active" ]
    then
        openstack baremetal node maintenance set $BM_NODE_UUID
        openstack baremetal node set --management-interface cern-ipmi $BM_NODE_UUID  #cern-ipmi
        openstack baremetal node set --deploy-interface iscsi $BM_NODE_UUID  # iscsi
        openstack baremetal node set --boot-interface pxe $BM_NODE_UUID  #pxe
        openstack baremetal node set --driver ipmi $BM_NODE_UUID  # ipmi
        openstack baremetal node maintenance unset $BM_NODE_UUID
    else
        echo "Node didn't make it to ACTIVE, so not removing the fake drivers"
    fi
done
