import sys
import os
import urllib3
import socket
from landbclient.client import LanDB

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

landb_username = "vmmaster"
landb_password = os.environ["LANDB_PASSWORD"]
landb_hostname = "network.cern.ch"
landb_port = "443"
landb_protocol = "https"

landb = LanDB(landb_username, landb_password,
              landb_hostname, landb_port, version=6)

nodes = sys.argv[1:]

for node in nodes:
    node_ip = socket.gethostbyname(node)
    device = landb.device_info(landb.device_search(ip_addr=node_ip)[0])
    for interface in device.Interfaces:
        if node.lower() + '.cern.ch' == interface.Name.lower():
            print(interface.BoundInterfaceCard.HardwareAddress.lower().replace('-', ':'))
