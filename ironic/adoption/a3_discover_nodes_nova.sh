#!/bin/bash

# Step 3/5: discover the new nodes

USAGE="Usage: $0 -p|-t"
OPTP=false
OPTT=false
while getopts "pt" arg; do
    case $arg in
    p)
        OPTP=true
        CELL_UUID='715795fc-f18e-4630-bdbf-e8824b0a14e2'
        NOVA_DISCOVER_HOST='cci-nova-099ddv.cern.ch'
        ;;
    t)
        OPTT=true
        CELL_UUID='f3f0f26f-c30e-4244-bac5-04293dbdb881' 
        NOVA_DISCOVER_HOST='cci-teststack-nova-frontend.cern.ch'
        ;;
    esac
done
if ! $OPTP && ! $OPTT; then
    echo "One of -p and -t is needed"
    echo $USAGE
    exit 1
fi
if $OPTP && $OPTT; then
    echo "Options -p and -t are mutually exclusive"
    echo $USAGE
    exit 1
fi

ssh root@${NOVA_DISCOVER_HOST} nova-manage cell_v2 discover_hosts --cell_uuid $CELL_UUID --cern-ironic --verbose
