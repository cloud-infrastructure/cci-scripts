#!/bin/bash

# Step 2/5: enroll nodes into Ironic

USAGE="Usage: $0 -p|-t -r <resource_class> -g <conductor_group> -m <raid_level> HOST"
while getopts "ptr:g:m:" arg; do
    case $arg in
    p)
        OPTP=true
        IRONIC_DEPLOY_RAMDISK_KERNEL='d50eeaee-b64d-4f63-abe2-40e15e80791a'
        ;;
    t)
        OPTT=true
        IRONIC_DEPLOY_RAMDISK_KERNEL='6b9cd55d-1f1b-4d2e-912e-3f3cd8551a2a'
        ;;
    r)
        RESOURCE_CLASS=$OPTARG
        ;;
    g)
        CONDUCTOR_GROUP=$OPTARG
        ;;
    m)
        RAID_LEVEL=$OPTARG
        ;;
    esac
done
if [ -z "${RESOURCE_CLASS}" ]
then
    echo "Resource class is needed"
    echo ${USAGE}
    exit 1
fi
if [ -z "${CONDUCTOR_GROUP}" ]
then
    echo "Conductor group is needed"
    echo ${USAGE}
    exit 1
fi
if [ -z "${RAID_LEVEL}" ]
then
    echo "RAID level is needed"
    echo ${USAGE}
    exit 1
fi

shift $(expr $OPTIND - 1 )
HOST=("$@")
echo "$@"

echo "Enrolling: $HOST"
NAME=$(python get_serial_number.py $HOST)
MAC_ADDRESS=$(python get_mac_address.py $HOST)
BMC_IP_ADDRESS=$(getent ahosts $HOST-ipmi | head -n1 | awk '{ print $1 }')
IPMI_USERNAME=$(ai-ipmi get-creds $HOST 2>/dev/null | grep Username | awk '{print $2}')
IPMI_PASSWORD=$(ai-ipmi get-creds $HOST 2>/dev/null | grep Password | awk '{print $2}')

# we need to create the node with a real driver as the RP's resource class
# is marked reserved otherwise
openstack baremetal node create --conductor-group $CONDUCTOR_GROUP --resource-class $RESOURCE_CLASS --driver ipmi --management-interface cern-ipmi --console-interface cern-webconsole --driver-info ipmi_port=623 --driver-info ipmi_username=$IPMI_USERNAME --driver-info ipmi_password=$IPMI_PASSWORD --driver-info ipmi_address=$BMC_IP_ADDRESS --driver-info deploy_kernel=$IRONIC_DEPLOY_RAMDISK_KERNEL --driver-info deploy_ramdisk=$IRONIC_DEPLOY_RAMDISK_KERNEL --name $NAME
BM_NODE_UUID=$(openstack baremetal node show --fit $NAME -c uuid | grep uuid | awk '{print $4}')
openstack baremetal port create --node $BM_NODE_UUID $MAC_ADDRESS
openstack baremetal node manage $NAME
openstack baremetal node set --driver fake-hardware $NAME
openstack baremetal node set --management-interface fake $NAME
openstack baremetal node set --deploy-interface fake $NAME
openstack baremetal node set --boot-interface fake $NAME
openstack baremetal node set --property capabilities=boot_mode:bios $NAME

# configure the RAID
if ! [ "${RAID_LEVEL}" = "none" ]; then
    openstack baremetal node set --raid-interface agent $NAME
    openstack baremetal node set --target-raid-config '{"logical_disks": [{"size_gb": 100, "controller": "software", "raid_level": "1"}, {"size_gb": "MAX", "controller":"software", "raid_level": "'${RAID_LEVEL}'"}]}' $NAME
fi

# make sure the node uses the fake-hardware driver before providing it
DRIVER=$(openstack baremetal node show --fit $NAME -c driver -f value)
if [ $DRIVER == "fake-hardware" ]; then
    openstack baremetal node provide $NAME
else
    echo "Node $NAME not enrolled with 'fake-driver'!"
    exit 1
fi
