#!/bin/bash

USAGE="Usage: $0 HOST"
HOST=("$@")

IPMI_USERNAME=$(ai-ipmi get-creds $HOST 2>/dev/null | grep Username | awk '{print $2}')
IPMI_PASSWORD=$(ai-ipmi get-creds $HOST 2>/dev/null | grep Password | awk '{print $2}')

echo openstack baremetal node set --driver-info ipmi_username=${IPMI_USERNAME} --driver-info ipmi_password=${IPMI_PASSWORD} $HOST
