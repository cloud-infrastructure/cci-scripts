#!/bin/bash

# Step 5/5: arm the node in Ironic

USAGE="Usage: $0 HOST"
HOST=$1

if [ -z $HOST ]; then
    echo "HOST is required"
    echo $USAGE
    exit 1
fi

echo $HOST

BM_NODE_UUID=$(python get_serial_number.py $HOST)

STATUS=$(openstack baremetal node show --fit $BM_NODE_UUID -c provision_state -f value)
if [ $STATUS == "active" ]; then
    openstack baremetal node maintenance set $BM_NODE_UUID
    openstack baremetal node set --management-interface cern-ipmi $BM_NODE_UUID  #cern-ipmi
    openstack baremetal node set --deploy-interface direct $BM_NODE_UUID  # direct
    openstack baremetal node set --boot-interface pxe $BM_NODE_UUID  #pxe
    openstack baremetal node set --driver ipmi $BM_NODE_UUID  # ipmi
    openstack baremetal node maintenance unset $BM_NODE_UUID
    openstack baremetal node console enable $BM_NODE_UUID
else
    echo "Node didn't make it to ACTIVE, so not removing the fake drivers"
    exit 1
fi