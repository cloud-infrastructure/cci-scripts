#!/bin/bash

# Step 1/5: create flavor

USAGE="Usage: $0 -d <delivery ID> -i <IP service> -r <RAM MB> -s <disk GB> -c <cores>"
while getopts "d:i:r:s:c:" arg; do
    case $arg in
    d)
        DELIVERY_ID=$OPTARG
        ;;
    i)
        IP_SERVICE=$OPTARG
        ;;
    r)
        RAM=$OPTARG
        ;;
    s)
        DISK=$OPTARG
        ;;
    c)
        CORES=$OPTARG
        ;;
    esac
done
if [ -z "$DELIVERY_ID" ]
then
    echo ${USAGE}
    exit 1
fi
if [ -z "$IP_SERVICE" ]
then
    echo ${USAGE}
    exit 1
fi
if [ -z "$RAM" ]
then
    echo ${USAGE}
    exit 1
fi
if [ -z "$DISK" ]
then
    echo ${USAGE}
    exit 1
fi
if [ -z "$CORES" ]
then
    echo ${USAGE}
    exit 1
fi

FLAVOR_NAME="a1.`echo ${DELIVERY_ID} | sed 's/[A-Z]/\l&/g'`.${IP_SERVICE}" # Change the IP service at the end of the name
RESOURCE_NAME="CUSTOM_BAREMETAL_`echo $FLAVOR_NAME | sed 's/[\.\-]/_/g ; s/[a-z]/\U&/g'`"

echo "#FLAVOR: ${FLAVOR_NAME}"
echo "#RESOURCE: ${RESOURCE_NAME}"

echo OS_PROJECT_NAME=admin openstack flavor create --ram ${RAM} --disk ${DISK} --vcpus ${CORES} --private $FLAVOR_NAME
echo OS_PROJECT_NAME=admin openstack flavor set --property capabilities:infiniband_adapters=0 \
                     --property capabilities:disk_enclosures=0 \
                     --property capabilities:disk_label="gpt" \
                     --property cern:physical=true \
                     --property resources:MEMORY_MB=0 \
                     --property resources:VCPU=0 \
                     --property resources:DISK_GB=0 \
                     --property resources:$RESOURCE_NAME=1 $FLAVOR_NAME