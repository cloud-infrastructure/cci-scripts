#!/bin/bash

# Step 4/5: create the instance in nova

USAGE="Usage: $0 -p|-t -r <resource_class> -f <flavor> -i <image> -x <project_name> HOST"
OPTP=false
OPTT=false
while getopts "ptr:f:i:x:" arg; do
    case $arg in
    p)
        OPTP=true
        AGGREGATE_ID='cd0fa194-4bbd-42bc-bbf2-df2fadf2319d'
        NOVA_DISCOVER_HOST='cci-nova-099ddv.cern.ch'
        ;;
    t)
        OPTT=true
        AGGREGATE_ID='a17abc2e-665c-4e06-8ebb-0f283561bc28'
        NOVA_DISCOVER_HOST='cci-teststack-nova-frontend.cern.ch'
        OS_AUTH_URL='https://keystonecmp.cern.ch/v3'
        ;;
    r)
        RESOURCE_CLASS=$OPTARG
        ;;
    f)
        FLAVOR=$OPTARG
        ;;
    i)
        IMAGE=$OPTARG
        ;;
    x)
        PROJECT_NAME=$OPTARG
        ;;
    esac
done
if ! $OPTP && ! $OPTT; then
    echo "One of -p and -t is needed"
    echo $USAGE
    exit 1
fi
if $OPTP && $OPTT; then
    echo "Options -p and -t are mutually exclusive"
    echo $USAGE
    exit 1
fi
if [ -z $RESOURCE_CLASS ]; then
    echo "Resource class is required"
    echo $USAGE
    exit 1
fi
if [ -z $FLAVOR ]; then
    echo "Flavor is required"
    echo $USAGE
    exit 1
fi
if [ -z "$IMAGE" ]; then
    echo "Image is required"
    echo $USAGE
    exit 1
fi
if [ -z "$PROJECT_NAME" ]; then
    echo "Project name is required"
    echo $USAGE
    exit 1
fi

shift $(expr $OPTIND - 1 )
HOST=("$@")
echo "Creating instance: $@"

echo "Getting the baremetal node UUID ..."
NAME=$(python get_serial_number.py $HOST);
BM_NODE_UUID=$(openstack baremetal node show --fit $NAME -c uuid | grep uuid | awk '{print $4}')

# map the node to the cell aggregate
export OS_PLACEMENT_API_VERSION=1.22
GENERATION=$(openstack resource provider show -c generation -f value $BM_NODE_UUID)
openstack resource provider aggregate set --generation ${GENERATION} --aggregate $AGGREGATE_ID $BM_NODE_UUID

ALLOCATION_CANDIDATES=$(openstack allocation candidate list --resource CUSTOM_${RESOURCE_CLASS}=1 --aggregate-uuid $AGGREGATE_ID | grep CUSTOM_${RESOURCE_CLASS}=1)
CANDIDATES_COUNT=$(openstack allocation candidate list --resource CUSTOM_${RESOURCE_CLASS}=1  --aggregate-uuid $AGGREGATE_ID| grep -c CUSTOM_${RESOURCE_CLASS}=1)
if [[ $CANDIDATES_COUNT -eq "0" ]]; then
    echo "There is no allocation candidate ... exiting!"
    exit 1
fi
if [[ $CANDIDATES_COUNT -ne "1" ]]; then
    echo "There is more than 1 allocation candidate ... exiting!"
    exit 1
fi
if [[ $ALLOCATION_CANDIDATES == *"$BM_NODE_UUID"* ]];then
    echo "The only candidate is the desired baremetal node"
else
    echo "The candidate for ${BM_NODE_UUID} is not present ... exiting!"
    exit 1
fi

ALIAS=$(openstack baremetal node show --fit $BM_NODE_UUID -c name -f value)

echo $ALIAS
echo $HOST

PROJECT_ID=$(OS_PROJECT_NAME="admin" openstack project show "${PROJECT_NAME}" -c id -f value)
OS_PROJECT_NAME="admin" openstack role add --user svckey --project ${PROJECT_ID} Member

cat << EOF >  /tmp/adoption_create_instance_$HOST.sh
OS_PROJECT_NAME="${PROJECT_NAME}" OS_AUTH_URL=${OS_AUTH_URL} openstack server create --image $IMAGE --flavor $FLAVOR --property landb-ironicskip=true --property cern-checklandbdevice=false --property cern-checkdns=false $HOST  
EOF

cat /tmp/adoption_create_instance_$HOST.sh | sh
