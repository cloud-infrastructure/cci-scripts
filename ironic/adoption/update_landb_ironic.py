import sys
import os
import urllib3
import socket
from landbclient.client import LanDB

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

landb_username = "vmmaster"
landb_password = os.environ["LANDB_PASSWORD"]
landb_hostname = "network.cern.ch"
landb_port = "443"
landb_protocol = "https"

landb = LanDB(landb_username, landb_password,
              landb_hostname, landb_port, version=6)

nodes = sys.argv[1:]

for node in nodes:
    node_ip = socket.gethostbyname(node)
    device_name = landb.device_search(ip_addr=node_ip)[0]
    print("Updating {0} ...".format(device_name))

    metadata = landb.client.service.getDeviceBasicInfo(device_name)
    print(metadata)
    metadata.Tag = "OPENSTACK IRONIC NODE"
    metadata.Description = ''
    landb.client.service.deviceUpdate(device_name, metadata)
