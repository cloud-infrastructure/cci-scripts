#!/usr/bin/env python

""" Script to look for inconsistencies between Ironic DB and LanDB

"""
import argparse
import configparser
import json
import logging
import os
import socket
import sys

# TODO hack to use development version of landbclient
sys.path.insert(
    0, '/afs/cern.ch/user/z/zmatonis/public/'
       '_cloud-infrastructure/python-landbclient'
)
from landbclient.client import LanDB  # noqa: E402
# Logic to take care of imports depending on the python version
if sys.version_info[0] == 2:
    from commands import getstatusoutput as run_cmd  # noqa: E402
else:
    from subprocess import getstatusoutput as run_cmd  # noqa: E402

# ----- config -----
# logging
DEBUG_FILE = '/tmp/ironic_port_logging.txt'

logger = logging.getLogger(__file__)
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(DEBUG_FILE, mode='w')
fh.setLevel(logging.NOTSET)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter(
    '%(asctime)s - %(levelname)s -%(lineno)d: :: %(message)s'
)
ch.setFormatter(formatter)
fh.setFormatter(formatter)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

if 'OS_PROJECT_ID' in os.environ:
    # if OS_PROJECT_ID is set, then it is not possible to point to different
    # openstack environments
    del os.environ['OS_PROJECT_ID']
conf_f = '/etc/landb.conf'
config = configparser.ConfigParser()
config.read(conf_f)
if 'default' not in config:
    logger.error("error parsing config file in " + conf_f)
    sys.exit(1)

landb_c = config['default']
landb_client = LanDB(host=landb_c['landb_host'],
                     port=landb_c['landb_port'],
                     username=landb_c['landb_user'],
                     password=landb_c['landb_password'],
                     version=int(landb_c['landb_version']),
                     ca_file=landb_c['ca_file'])
landb_tag = 'OPENSTACK IRONIC NODE'
ironic_enviroments = [
    ('https://keystone.cern.ch/v3', 'Cloud Test - gva_baremetal_qa'),
    ('https://keystone.cern.ch/v3', 'Cloud Test - gva_baremetal_001')
]
# https://gist.github.com/nhoffman/3006600#file-pyscript-py-L18


def get_json_from_cmd(cmd):
    e, o = run_cmd(cmd)
    if e:
        logger.error("Failure executing command '{cmd}'. Error code '{code}'"
                     ", output: '{output}'".format(cmd=cmd, code=e, output=o))
        sys.exit(1)
    return json.loads(o)


def get_ip_from_hostname(hostname):
    return socket.gethostbyname(hostname)


def check_if_data_is_consistent(node, landb_client, ironic_name_set):
    """
    Check if node's data is consistent with data in LanDB
    """
    def check_search_results(res_list, property):
        if res_list is None or len(res_list) == 0:
            logger.warning(
                "Device search by {0} returned 0 devices".format(property)
            )
            return None
        if len(res_list) > 1:
            logger.warning(
                "Device search by {0} returned {1} device: {2}. "
                "Checking 1st one"
                .format(property, len(res_list), res_list)
            )
        return res_list[0]

    logger.debug(
        "Checking node {0} with UUID {1}".format(node['Name'], node['UUID'])
    )
    if not node['Name']:
        logger.debug(
            "Node {0} with UUID {1} has no name"
            .format(node['Name'], node['UUID'])
        )
        return
    res_list = landb_client.device_search(serialnumber=node['Name'])
    real_node_name = check_search_results(
        res_list, "serialnumber - " + node['Name']
    )
    if not real_node_name:
        return

    ironic_name_set.add(real_node_name)
    node_in_ldb = landb_client.device_info(real_node_name)
    node_mac_list_ldb = [
        x['HardwareAddress'] for x in node_in_ldb['NetworkInterfaceCards']
    ]
    node_mac_list_ldb = [
        x.lower().replace('-', ':') for x in node_mac_list_ldb
    ]
    match = 0
    for port in node['port_list']:
        if port['Address'] not in node_mac_list_ldb:
            logger.warning(
                "Port with UUID {uuid} and MAC {mac} does not belong to node"
                "{node}".format(uuid=port["UUID"],
                                mac=port["Address"],
                                node=real_node_name)
            )
        else:
            match += 1

    mac_ironic_list = [p["Address"] for p in node['port_list']]

    if match == 0:
        logger.warning(
            "NO MAC addresses match. For node {node} macs registered in Ironic"
            "DB: '{mac_ironic}', in LANDB: '{mac_landb}'."
            .format(node=real_node_name,
                    mac_ironic=mac_ironic_list,
                    mac_landb=node_mac_list_ldb)
        )

    if match != len(node_in_ldb) and match > 1:
        logger.debug(
            "MAC addresses mismatch. For node {node} macs registered in Ironic"
            "DB: '{mac_ironic}', in LANDB: '{mac_landb}'."
            .format(node=real_node_name,
                    mac_ironic=mac_ironic_list,
                    mac_landb=node_mac_list_ldb)
        )


def main(arguments):
    node_list = []
    port_list = []
    node_dict = {}
    node_serial_set = set()
    node_name_set = set()

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        '-l', '--logging', help="What level messages to log. Default INFO",
        type=str, default=logging.INFO)

    args = parser.parse_args(arguments)
    ch.setLevel(args.logging)

    logger.info("Starting script. Complete log is in {0}".format(DEBUG_FILE))
    logger.info("Queering Openstack API")
    for t in ironic_enviroments:
        logger.debug("Setting env var OS_AUTH_URL to '{0}' "
                     "and OS_PROJECT_NAME '{1}'".format(*t))
        # get testack, qa , production data
        os.environ['OS_AUTH_URL'] = t[0]
        os.environ['OS_PROJECT_NAME'] = t[1]
        node_list += get_json_from_cmd(
            "openstack baremetal node list -f json --limit 0"
        )
        port_list += get_json_from_cmd(
            "openstack baremetal port list --long -f json --limit 0"
        )

    # Build dict of nodes that uses node UUID as a key
    for node in node_list:
        node['port_list'] = []
        node_dict[node['UUID']] = node
        node_serial_set.add(node['Name'])
        if node['Name'] is None or node['Name'] == 'None':
            logger.warning("Node with UUID {uuid} has empty 'Name'"
                           .format(uuid=node['UUID']))

    # Matching ports and nodes
    # Checking if there are ports with no nodes
    for port in port_list:
        node_uuid = port['Node UUID']
        if node_uuid not in node_dict:
            logger.warning(
                "Port with UUID {port_uuid} exists, but node with "
                "UUID {node_uuid} does not".format(node_uuid=node_uuid,
                                                   port_uuid=port['UUID'])
            )
            continue
        node_dict[node_uuid]['port_list'].append(port)

    # Counting the number of ports on each node and checking if:
    # - Zero ports
    # - More than 1
    logger.info("Checking if number of ports for each node.")
    for node_k, node_v in node_dict.items():
        if len(node_v['port_list']) == 0:
            logger.warning(
                "Node with name/serial {serial} and "
                "UUID {uuid} has no ports"
                .format(serial=node_v['Name'], uuid=node_v['UUID'])
            )
        if len(node_v['port_list']) > 1:
            logger.warning(
                "Info - node with name/serial {serial} and "
                "UUID {uuid} has {p_len} ports"
                .format(serial=node_v['Name'],
                        uuid=node_v['UUID'],
                        p_len=len(node_v['port_list']))
            )

    logger.info("Comparing ironic data with LanDB")
    for node_k, node_v in node_dict.items():
        check_if_data_is_consistent(node_v, landb_client, node_name_set)

    logger.info("Checking if nodes with 'OPENSTACK IRONIC NODE' in "
                "LanDb is consistent with ironic")
    nodes_with_tag_set = set(landb_client.device_search(
        tag=landb_tag)
    )

    # check if all nodes with `landb_tag` are in ironic DB and vice versa
    nodes_only_in_landb = nodes_with_tag_set - node_name_set
    nodes_only_in_ironic = node_name_set - nodes_with_tag_set
    logger.warning(
        "Nodes' names only in LanDB: {0}".format(nodes_only_in_landb)
    )
    logger.warning(
        "Nodes' names only in Ironic: {0}".format(nodes_only_in_ironic)
    )
    logger.info("finish !")


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
