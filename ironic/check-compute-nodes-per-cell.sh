#! /bin/bash

# This script automates the addition of new compute nodes to an existing Nova cell, as documented in
# https://cci-internal-docs.web.cern.ch/compute/procedures/cell-add-compute-nodes/ . In particular,
# the script discovers all Nova compute nodes for a given Nova cell(s), and checks the list against
# Foreman hosts and Nova cell aggregates.

NOVA_HOSTS_FILE=$(mktemp --tmpdir nova-hosts-XXXXXX.txt)
FM_HOSTS_FILE=$(mktemp --tmpdir fm-hosts-XXXXXX.txt)
AGG_HOSTS_FILE=$(mktemp --tmpdir agg-hosts-XXXXXX.txt)
trap "/bin/rm ${NOVA_HOSTS_FILE} ${FM_HOSTS_FILE} ${AGG_HOSTS_FILE} 2>/dev/null" EXIT

unset $(env|grep -E "^OS_" |cut -f1 -d=)

#
# Print help text
#
function help()
{
    cat <<EOF
Usage: $0 [-r <region name>] [-u] [-h] <cell name> [<cell name> ...]

This script verifies the number of servers in a given cell, checking consistency between 
Nova-compute, Foreman, Nova-aggregates and project metadata.

Parameters:

       <cell name> : name of the Nova cell to be verified. Can be repeated.

Optional arguments:

       -r : cloud region to be used. Value should be one of "cern|next|poc". If not set, the
            value will be determined for each of the specified cells.
       -u : perform updates: try to discover new nova-compute nodes, and update Nova hostaggregates
       -h : display this help

EOF

}

#
# Get the name of the API server for the region
#
function get_api_server()
{
    local fm_region=$1
    api_server=$(ai-foreman --hg cloud_compute/level0/frontend/${fm_region} showhost | grep level0 | cut -d\  -f2 | head -1)
    if [ -z "${api_server}" ]
    then
        echo "[E] Could not determine which Nova API server to use, exiting"
        exit 1
    fi
    echo ${api_server}
}

#
# Parse the options
#
PARAMS=""
apply=0
while (( "$#" ))
do
    case "$1" in
        -r)
            region=$2
            if [[ ! $region =~ cern|next|poc ]]
            then
                echo "Incorrect region name provided, should be one of \"cern|next|poc\""
                exit 1
            fi
            shift
            shift
            ;;
        -h)
            help
            exit 0
            ;;
        -*) # unsupported flags
            echo "Error: Unsupported flag $1" >&2
            echo
            help
            exit 1
            ;;
        *) # preserve positional arguments
            PARAMS="$PARAMS $1"
            shift
            ;;
    esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

if [ $# -eq 0 ]
then
    help
    exit 1
fi

CELL_NAMES=$@

#echo "apply=$apply OS_CLOUD=$OS_CLOUD CELL_NAMES=\"${CELL_NAMES}\""
#exit

if [ -n "${region}" ]
then
    export OS_CLOUD=${region}
    
    FM_REGION=$([ "$OS_CLOUD" == 'cern' ] && echo "main" || echo "$OS_CLOUD") # define "Foreman region" to be used in ai-foreman calls

    # Get the name of the API server for the region
    NOVA_API_SERVER=$(get_api_server $FM_REGION)
    echo [V] NOVA_API_SERVER=$NOVA_API_SERVER
fi
 
#
# Loop over the cells
#
for CELL in ${CELL_NAMES}
do
    echo [I] Processing Nova cell ${CELL}

    #
    # Get the region name from the Foreman hostgroup of the cell controller
    #
    if [ -z "${region}" ]
    then
        FM_REGION=$(ai-foreman --filter "cloud_compute/level1 && ${CELL}" showhost 2>/dev/null | grep ${CELL} | cut -f3 -d\| | cut -f3 -d/)
        export OS_CLOUD=$([ "$FM_REGION" == 'main' ] && echo "cern" || echo "$FM_REGION")
    
        # Get the name of the API server for the region
        NOVA_API_SERVER=$(get_api_server $FM_REGION)
        echo [V] NOVA_API_SERVER=$NOVA_API_SERVER
    fi

    # Get the aggregates that match the cell_name
    declare -A AGG_HOSTS
    for aggregate in $(openstack aggregate list --long | grep "cell_name='${CELL}'" | cut -d'|' -f3 | xargs -n1)
    do
        AGG_HOSTS+=$(openstack aggregate show ${aggregate} -c hosts -f yaml | grep -v hosts: | cut -d\  -f2 | sort)
        AGG_HOSTS+=" "
    done

    #
    # Get list of compute nodes on the API server
    #
    CELL_UUID=$(ssh -q root@${NOVA_API_SERVER} nova-manage cell_v2 list_cells | grep -w ${CELL} | tr -d '[:blank:]' | cut -d\| -f3)
    if [ -z "${CELL_UUID}" ]
    then
        echo "[E] Could not find cell uuid for cell \"${CELL}\" on controller node \"${NOVA_API_SERVER}\", exiting"
        exit 1
    fi
    echo [V] CELL_UUID=$CELL_UUID

    #
    # Discover new compute nodes
    #
    if [ ${apply} -eq 1 ]
    then
        echo "[I] Discovering new compute nodes..."
        ssh -q root@${NOVA_API_SERVER} nova-manage cell_v2 discover_hosts --cell_uuid ${CELL_UUID}
    fi
    #
    # List nova-compute nodes
    #
    NOVA_HOSTS=$(ssh -q root@${NOVA_API_SERVER} nova-manage cell_v2 list_hosts --cell_uuid ${CELL_UUID} | grep -w ${CELL} | tr -d '[:blank:]' | cut -d\| -f4 | sort)
    
    #
    # Get list of compute nodes from Foreman
    #
    HOST_GROUP="cloud_compute/level2/${FM_REGION}/${CELL}"
    FM_HOSTS=$(ai-foreman --hl $HOST_GROUP showhost 2>/dev/null | grep $HOST_GROUP | cut -d\  -f2 | sort)
    
    #
    # Get list of compute nodes from "Cloud Compute Nodes" OS project
    #
    MAX_INSTANCES=$(openstack project show "Cloud Compute Nodes - ${CELL}" -f yaml 2>/dev/null | grep -E "^max-instances" | cut -d:  -f2 | xargs -r | sed 's/ / + /g' | xargs -r expr)
    [ -z "${MAX_INSTANCES}" ] && echo "[W] Could not find number of instances from metadata of project \"Cloud Compute Nodes - ${CELL}\""

    #
    # Perform consistency checks
    #
    echo $NOVA_HOSTS     | xargs -n1 > ${NOVA_HOSTS_FILE}
    echo $FM_HOSTS       | xargs -n1 > ${FM_HOSTS_FILE}
    echo $AGG_HOSTS      | xargs -n1 | sort > ${AGG_HOSTS_FILE}
    
    # In Foreman, but not in Nova
    
    HOSTS=$(/usr/bin/join --ignore-case -a 1 -v 1 ${FM_HOSTS_FILE} ${NOVA_HOSTS_FILE})
    
    if [ -n "${HOSTS}" ]
    then
        echo ${HOSTS} | xargs -n1 | xargs -n1 -i{} echo "[W][${OS_CLOUD}][${CELL}] Host \"{}\" found in Foreman, but not in Nova-compute."
    else
        echo "[I] All compute nodes listed in Foreman are also known to Nova."
    fi
    
    # In Nova, but not in Foreman

    HOSTS=$(/usr/bin/join --ignore-case -a 2 -v 2 ${FM_HOSTS_FILE} ${NOVA_HOSTS_FILE})
    
    if [ -n "${HOSTS}" ]
    then
        echo ${HOSTS} | xargs -n1 | xargs -n1 -i{} echo "[W][${OS_CLOUD}][${CELL}] Host \"{}\" found in Nova-compute, but not in Foreman."
    else
        echo "[I] All compute nodes listed in Nova are also known to Foreman."
    fi

    # In Nova, but not in cell aggregate
    
    HOSTS=$(/usr/bin/join --ignore-case -a 1 -v 1 ${NOVA_HOSTS_FILE} ${AGG_HOSTS_FILE})
    
    if [ -n "${HOSTS}" ]
    then
        for HOST in ${HOSTS}
        do
            echo "[W][${OS_CLOUD}][${CELL}] Host \"${HOST}\" found in Nova-compute, but not in the cell aggregate."
        done

        # Update the list of compute nodes from the Nova cell aggregate
        declare -A AGG_HOSTS
        for aggregate in $(openstack aggregate list --long | grep "cell_name='${CELL}'" | cut -d'|' -f3 | xargs -n1)
        do
            AGG_HOSTS+=$(openstack aggregate show ${aggregate} -c hosts -f yaml | grep -v hosts: | cut -d\  -f2 | sort)
            AGG_HOSTS+=" "
        done

    else
	echo "[I] All compute nodes listed in Nova are also listed in the corresponding cell aggregate."
    fi

    # Compare to the max. number of hosts listed for this project

    if [ -n "${MAX_INSTANCES}" ]
    then
	cnt=$(echo $FM_HOSTS | wc -w)
	if [ "${MAX_INSTANCES}" -ne $cnt ]
	then
	    echo "[W] Found $cnt compute nodes listed in Foreman, expecting ${MAX_INSTANCES} according to project metadata."
	else
	    echo "[I] Found $cnt compute nodes listed in Foreman, matching ${MAX_INSTANCES} according to project metadata."
	fi
	cnt=$(echo $NOVA_HOSTS | wc -w)
	if [ "${MAX_INSTANCES}" -ne $cnt ]
	then
	    echo "[W] Found $cnt Nova-compute nodes, expecting ${MAX_INSTANCES} according to project metadata."
	else
	    echo "[I] Found $cnt Nova-compute nodes, matching ${MAX_INSTANCES} according to project metadata."
	fi
	cnt=$(echo $AGG_HOSTS | wc -w)
	if [ "${MAX_INSTANCES}" -ne $cnt ]
	then
	    echo "[W] Found $cnt compute nodes listed in Nova aggregate, expecting ${MAX_INSTANCES} according to project metadata."
	else
	    echo "[I] Found $cnt compute nodes listed in Nova aggregate, matching ${MAX_INSTANCES} according to project metadata."
	fi
    fi

done

# Done

exit 0

