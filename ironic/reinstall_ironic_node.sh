#!/usr/bin/env bash

#set -x

HOSTNAME=$1

echo 
echo "Reinstalling $HOSTNAME ... - `date`"
echo "This will take up to several hours, so please be patient :)"

# get the UUID of the server
LIST=$(openstack server list -n --all --name $HOSTNAME -f csv --quote minimal|grep $HOSTNAME)
ID=$(echo ${LIST}|cut -d, -f1)

# get the flavor ID and name
FLAVOR=$(echo ${LIST}|cut -d, -f6)
FLAVOR_NAME=$(openstack flavor show ${FLAVOR} -c name -f value)

# get and log all server details from nova
echo 
echo "Getting server details ..."
SHOW=$(openstack server show --format yaml ${ID})
echo "${SHOW}"

# get the tenant name and id from the properties
PROJECT_NAME=$(echo ${SHOW}|xargs -n1|grep tenant-name|cut -d'=' -f2)
PROJECT_ID=$(echo ${SHOW}|xargs -n1|grep tenant-id|cut -d'=' -f2)
PROJECT_ID="${PROJECT_ID//,}"

# get the RESOURCE_CLASS from the FLAVOR
X=$(openstack flavor show ${FLAVOR}|xargs -n1|grep CUSTOM)
RE=(CUSTOM_BAREMETAL.*)=
[[ $X =~ $RE ]] && RESOURCE_CLASS=${BASH_REMATCH[1]}

# get the underlying physical node from ironic
echo
echo "Getting the physical node ..."
eval `ai-rc "Cloud Test - gva_baremetal_001"`
LIST=$(openstack baremetal node list -f csv --quote minimal|grep ${ID})
NODE_UUID=$(echo ${LIST}|cut -d, -f1)
NODE=$(echo ${LIST}|cut -d, -f2)

# get the host group and the environment from ai-dump
echo
echo "Getting the host group and the environment ..."
X=$(ai-dump $HOSTNAME)
HOSTGROUP=$(echo "$X"|grep Hostgroup|cut -d'|' -f3|xargs)
ENVIRONMENT=$(echo "$X"|grep Environment|cut -d'|' -f3|xargs)

# ID=71ccd4c5-534c-43ba-aa3c-1d07172e0afa
# FLAVOR=8cc6b979-764f-4201-97f0-4fb53af8dadb
# FLAVOR_NAME=p1.cd5795986.S6045-C6-IP105
# PROJECT_NAME="Cloud Compute Nodes - pt8_project_005"
# PROJECT_ID=5dbc4e90-39f1-494e-b500-9164da72c4a8
# RESOURCE_CLASS=CUSTOM_BAREMETAL_P1_CD5795986_S6045_C6_IP105
# NODE=CD5795986-S178292X4C03185-C
# NODE_UUID=c4f979e3-5f10-4f91-978e-181cb5ff9cc4
# HOSTGROUP=cloud_compute/level2/point8/pt8_project_005
# ENVIRONMENT=nova_master_2

# print all vars for potential reuse
echo
echo "ID=$ID"
echo "FLAVOR=$FLAVOR"
echo "FLAVOR_NAME=${FLAVOR_NAME}"
echo "PROJECT_NAME=\"$PROJECT_NAME\""
echo "PROJECT_ID=$PROJECT_ID"
echo "RESOURCE_CLASS=$RESOURCE_CLASS"
echo "NODE=$NODE"
echo "NODE_UUID=$NODE_UUID"
echo "HOSTGROUP=$HOSTGROUP"
echo "ENVIRONMENT=$ENVIRONMENT"
echo ""

# some simple sanity checking
if [ -z "$ID" ] || \
   [ -z "$FLAVOR" ] || \
   [ -z "$FLAVOR_NAME" ] || \
   [ -z "$PROJECT_NAME" ] || \
   [ -z "$PROJECT_ID" ] || \
   [ -z "$RESOURCE_CLASS" ] || \
   [ -z "$NODE" ] || \
   [ -z "$NODE_UUID" ] || \
   [ -z "$HOSTGROUP" ] || [ "$HOSTGROUP" == "-" ] || \
   [ -z "$ENVIRONMENT" ]; then
  echo "One or more variables are undefined or have invalid values"      
  exit 1
fi

# delete the instance from openstack
echo
echo -n "Deleting instance from OpenStack ..."
openstack baremetal node maintenance unset $NODE
openstack server delete ${ID}

# check if the node has a RAID config, exit if there is none
# TODO: add a config if it is missing 
echo
eval `ai-rc "Cloud Test - gva_baremetal_001"`
echo -n "Checking for RAID config ... "
RAID_CONFIG=$(openstack baremetal node show $NODE --fields target_raid_config --format value)
echo $RAID_CONFIG
if [ "$RAID_CONFIG" == '{}' ]; then
    echo "not ok: missing a RAID onfiguration!"
    exit 1
fi
echo "ok"

# wait for the node to have its RAID configured
echo
echo "Wait for $NODE to become available after initial cleaning ..."
WAIT=0
provision_state="$(openstack baremetal node show $NODE --fields provision_state --format value)"
while [ "${provision_state}" != "available" ]; do
    echo "Waiting for $NODE since $WAIT seconds to become available after initial cleaning" 
    echo "Current state is ${provision_state}"
    echo "Checking again in 30 seconds ..."
    sleep 30
    ((WAIT=WAIT+30))
    provision_state="$(openstack baremetal node show $NODE --fields provision_state --format value)"
done

echo
echo "The node is ready for the RAID setup ..."

# configure the RAID via cleaning
echo
echo "Send node into cleaning to trigger the RAID setup ..."
openstack baremetal node maintenance unset $NODE
openstack baremetal node manage $NODE
openstack baremetal node provide $NODE
echo "Wait for $NODE to become available ..."
WAIT=0
provision_state="$(openstack baremetal node show $NODE --fields provision_state --format value)"
while [ "${provision_state}" != "available" ]; do
    echo "Waiting for node $NODE since $WAIT seconds to come back from the RAID setup"
    echo "Current state is ${provision_state}, checking again in 30 seconds ..."
    sleep 30
    ((WAIT=WAIT+30))
    provision_state="$(openstack baremetal node show $NODE --fields provision_state --format value)"
done

# wait for the allocation candidate to appear
echo
echo -n "Waiting for the allocation candidate to appear ..."
WAIT=0
export OS_PLACEMENT_API_VERSION=1.10
ALLOCATION_CANDIDATES="$(openstack allocation candidate list --resource ${RESOURCE_CLASS}=1)"
echo $ALLOCATION_CANDIDATES
while [[ ! "${ALLOCATION_CANDIDATES}" =~ "${NODE_UUID}" ]]; do
    echo "Waiting allocation candidate ${NODE_UUID}/${RESOURCE_CLASS} since $WAIT seconds"
    echo "Checking again in 30 seconds ..."
    sleep 30
    ((WAIT=WAIT+30))
    ALLOCATION_CANDIDATES="$(openstack allocation candidate list --resource ${RESOURCE_CLASS}=1)"
done

echo
echo "$HOSTNAME from $HOSTGROUP is now ready for recreation!"

# add svckey to project and change to the project
openstack role add --user svckey --project $PROJECT_ID Member
eval `ai-rc "$PROJECT_NAME"`

# remove from foreman
ai-kill --nova-disable $HOSTNAME

# prepare the user data
tmpdir=$(mktemp -d -t ironic_raid.XXXXXX)
cat > $tmpdir/x-shellscript <<EOF
#!/bin/bash
# create /var/lib/nova
#
mount_point=/var/lib/nova
mkdir -p $mount_point
# find the unmounted md device
md_devices=`grep md /proc/mdstat|cut -d " " -f 1|xargs`
for d in $md_devices; do
        if ! grep -q $d /proc/mounts; then
                break
        fi
done
echo "found unmounted device $d"
# create an fs and mount the device
mkfs.xfs -L var_lib_nova /dev/$d
echo "LABEL=var_lib_nova $mount_point xfs defaults 0 0" >> /etc/fstab
mount -a
# create a swap space
#
dd if=/dev/zero of=/swapfile bs=1M count=20480
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
echo "/swapfile swap swap defaults 0 0" >> /etc/fstab
EOF

# re-create the server
echo
export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_ID=$PROJECT_ID
ai-bs --nova-flavor $FLAVOR_NAME \
      --landb-mainuser ai-openstack-admin \
      --landb-responsible ai-openstack-admin \
      --foreman-environment "$ENVIRONMENT" \
      --foreman-hostgroup "$HOSTGROUP" \
      --userdata-dir "$tmpdir" \
      --cc7 \
      $HOSTNAME

# rm -f $tmpfile

# wait for the server to come up
while [[ $(host $HOSTNAME) =~ "not found"  ]]; do
    echo "Waiting for instance to appear in DNS - `date`"
    sleep 30
done

UNREACHEABLE=1
while [ $UNREACHEABLE -ne "0" ]; do
   ping -q -c 1 -W 1 $HOSTNAME &> /dev/null
   UNREACHEABLE=$?
   echo "Waiting for $HOSTNAME to become up - `date`"
   sleep 30
done

# TODO: enable the server

# remove svckey from the project
eval `ai-rc admin`
openstack role remove --user svckey --project $PROJECT_ID Member


