#!/bin/sh

# example:
# ./get-ipmi-creds.sh < FILE_WITH_HOSTNAMES

while read h
    do
       echo -n "$h "
       ai-ipmi get-creds $h 2>&1 | grep -E '(Username|Password)'| awk '{printf "%s ", $2}'
       echo $h-ipmi
done
