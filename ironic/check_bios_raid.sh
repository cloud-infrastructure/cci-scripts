#!/usr/bin/env bash

# check if this is a BIOS boot mode node
if [ -d "/sys/firmware/efi" ]
then
  echo "INFO: The node is not booted in BIOS mode, exiting"
  exit 0
fi

# get all bootable disks: disks with GRUB in the MBR
BOOTDISKS=()
ALLDISKS=(`lshw -class disk|grep 'logical name'|cut -d':' -f2`)
for i in "${ALLDISKS[@]}"
do
    echo "Checking ${i}"
    if dd if=${i} bs=512 count=1 2>/dev/null|strings|grep -q GRUB;
    then
        echo "Found GRUB on ${i}"
        BOOTDISKS+=(${i})
    fi
done

# check initial jump
# first 3 bytes (usually "eb 63 90", "jump 0x63 noop")
# the bytes between 0x02 and (0x01 + 0x63) contain the 'BIOS parameter block',
# information about the specific disk layout as used by some file systems
# http://www.sharetechnote.com/html/Linux_MBR.html
JUMP=`dd if=${BOOTDISKS[0]} bs=1 count=3 2>/dev/null|hexdump -C|head -n1|cut -d ' ' -f2-5`
echo "Initial jump on ${BOOTDISKS[0]}:${JUMP}"
for i in "${BOOTDISKS[@]:1}"
do
    JUMP_TMP=`dd if=${i} bs=1 count=3 2>/dev/null|hexdump -C|head -n1|cut -d ' ' -f2-5`
    echo "Initial jump on ${i}:${JUMP_TMP}"
    if [ ! "${JUMP}" = "${JUMP_TMP}" ]
    then
        echo "ERROR: initial jump not the same on all disks"
        exit 1
    fi
done
JUMP_DISTANCE_HEX=`echo ${JUMP}|cut -d ' ' -f2`
JUMP_DISTANCE_DEC=`echo "ibase=16; ${JUMP_DISTANCE_HEX}" | bc`

# check boot.img
((BOOT_IMG_SIZE = 440 - ${JUMP_DISTANCE_DEC}))
MD5_BOOTIMG=`dd if=${BOOTDISKS[0]} skip=${JUMP_DISTANCE_DEC} bs=1 count=${BOOT_IMG_SIZE} 2>/dev/null|md5sum`
echo "BOOTIMG MD5UM on ${BOOTDISKS[0]}: ${MD5_BOOTIMG}"
for i in "${BOOTDISKS[@]:1}"
do
    MD5=`dd if=${i} skip=${JUMP_DISTANCE_DEC} bs=1 count=${BOOT_IMG_SIZE} 2>/dev/null|md5sum`
    echo "BOOTIMG MD5UM on ${i}: ${MD5}"
    if [ ! "${MD5}" = "${MD5_BOOTIMG}" ]
    then
        echo "ERROR: boot.img not the same on all disks"
        exit 1
    fi
done

# check if we have an MBR/dos or GPT partition table format
FDISK=$(fdisk -l ${BOOTDISKS[0]} 2>&1)
if grep -q "Disk label type: dos" <<< "${FDISK}"
then
    echo "This node has an MBR/dos partition table"
    PT_FORMAT="dos"
elif grep -q "Disk label type: gpt" <<< "${FDISK}"
then
    echo "This node has a GPT partition table"
    PT_FORMAT="gpt"
else
    echo "Error: could not determine partition table format"
    exit 1
fi

# check core.img
if [ "dos" = "${PT_FORMAT}" ]
then
    # with an MBR/dos partition table, core.img is in the MBR gap
    MD5_COREIMG=`dd if=${BOOTDISKS[0]} bs=512 skip=1 count=63 2>/dev/null|md5sum`
    echo "COREIMG MD5UM on ${BOOTDISKS[0]}: ${MD5_COREIMG}"
    for i in "${BOOTDISKS[@]:1}"
    do
        MD5=`dd if=${i} bs=512 skip=1 count=63 2>/dev/null|md5sum`
        echo "COREIMG MD5UM on ${i}: ${MD5}"
        if [ ! "${MD5}" = "${MD5_COREIMG}" ]
        then
            echo "ERROR: core.img not the same on all disks"
            exit 1
        fi
    done
elif [ "gpt" = "${PT_FORMAT}" ]
then
    # with a GPT partition table, core.img is in the boot partition
    BOOT_PART_NUMBER=$(fdisk -l ${BOOTDISKS[0]} 2>&1|grep -i "BIOS boot"|cut -d " " -f2)
    if [ -z "${BOOT_PART_NUMBER##*[!0-9]*}" ]
    then
        echo "ERROR: could not find grub partition"
        exit 1
    fi
    echo "Found boot partition ${BOOT_PART_NUMBER}"

    CORE_IMG_SIZE=$(stat -c%s "/boot/grub2/i386-pc/core.img")
    echo $CORE_IMG_SIZE
    if [ -z "${CORE_IMG_SIZE##*[!0-9]*}" ]
    then
        echo "ERROR: could not determine core.img size"
        exit 1
    fi

    MD5_COREIMG=`dd if=${BOOTDISKS[0]}${BOOT_PART_NUMBER} bs=${CORE_IMG_SIZE} count=1 2>/dev/null|md5sum`
    echo "COREIMG MD5UM on ${BOOTDISKS[0]}${BOOT_PART_NUMBER}: ${MD5_COREIMG}"
    for i in "${BOOTDISKS[@]:1}"
    do
        MD5=`dd if=${i}${BOOT_PART_NUMBER} bs=${CORE_IMG_SIZE} count=1 2>/dev/null|md5sum`
        echo "COREIMG MD5UM on ${i}${BOOT_PART_NUMBER}: ${MD5}"
        if [ ! "${MD5}" = "${MD5_COREIMG}" ]
        then
            echo "ERROR: core.img not the same on all disks"
            exit 1
        fi
    done
fi

echo "All good!"
exit 0


# Run like this:
#
# $ wassh -l root -q -c cloud_compute/level2/main/crit_project_003 'curl https://gitlab.cern.ch/cloud-infrastructure/cci-scripts/-/raw/grub_checker/ironic/check_bios_raid.sh|sh|grep -i error'
# i73368607372500.cern.ch: ERROR: core.img not the same on all disks
