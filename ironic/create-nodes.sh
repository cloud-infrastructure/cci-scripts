#! /bin/bash

USAGE="Usage: $(basename $0) --cell <cell name> --number <instance count> [--flavor <flavor name>] [--key-pair <key name>] [--apply]"

#
# Print help text
#
function help()
{
    local name=$(basename $0)
    cat <<EOF
Usage: $name --cell <cell name> --number <instance count> [--region REGION] [--flavor <flavor name>] [--key-pair <key name>] [--hostgroup <hostgroup name>] [--environment <env name>] [--apply]
       $name --project <project name --number <instance count> [--region REGION] [--flavor <flavor name>] [--key-pair <key name>] [--hostgroup <hostgroup name>] [--environment <env name>] [--apply]
       $name --help

This script creates new servers in the specified Nova cell or Cloud project. Its main use is to instantiate new
hypervisors in our Nova cells, but it can be used for other bare metal services as well

The recommended way to use this script is to invoke it with two options:

      $name --cell <cell name> --number <instance count>

and rely on auto-discovery of the other values. This should work just
fine for most of our cells, where a single flavor should be used for
the uniform hardware.

For more tricky cases, other arguments are avaialable. See also the examples below.

Arguments:

       -c|--cell        : Nova cell to which the servers will be added
       -p|--project     | OpenStack project to which the servers will be added

       Note the arguments "--cell" and "--project" are mutully exclusive

       -r|--region      : region to create the resources
       -n|--number      : number of servers to be created
       -f|--flavor      : flavor to be used in server creation
       -k|--key-pair    : name of key pair to be used in server creation
       -h|--hostgroup   : name of the Foreman hostgroup where the server should be added
       -e|--environment : name of the Puppet environment to be used



       -a|--apply    : actually create the servers. Otherwise, just print the generated ai-bs command
       -h|--help     : display this help

Examples:

       $ create-nodes.sh --cell pt8_project_001 --number 13

       $ create-nodes.sh --project "Cloud Networking - baremetal sdn2" --hostgroup "cloud_compute/level2/sdn2/gva_shared_001" --environment sdn2level2 --number 2

       $ create-nodes.sh --region pdc --project "Cloud Compute Nodes" --hostgroup "cloud_compute/level2/pdc/shared_001/dl1092789"  --number 2
EOF

}

#
# Define OS environment
#
unset `env|grep -E "^OS_" |cut -f1 -d=`
export OS_PROJECT_DOMAIN_ID=default
export OS_PROTOCOL=kerberos
export OS_IDENTITY_API_VERSION=3
export OS_IDENTITY_PROVIDER=sssd
export OS_AUTH_TYPE=v3fedkerb
export OS_AUTH_URL=https://keystone.cern.ch/v3
export OS_MUTUAL_AUTH=disabled

apply=0
#
# Parse the options
#
PARAMS=""
apply=0
while (( "$#" )); do
    case "$1" in
        -f|--region)
            REGION=$2
            shift
            shift
            ;;
        -f|--flavor)
            FLAVOR=$2
            shift
            shift
            ;;
        -c|--cell)
            CELL=$2
            shift
            shift
            ;;
        -p|--project)
            PROJECT=$2
            shift
            shift
            ;;
        -h|--hostgroup)
            HOSTGROUP=$2
            shift
            shift
            ;;
        -e|--environment)
            ENVIRONMENT=$2
            shift
            shift
            ;;
        -a|--apply)
            apply=1
            shift
            ;;
        -n|--number)
            NUM=$2
            shift
            shift
            ;;
        -k|--key-pair)
            USERKEY=$2
            shift
            shift
            ;;
        -h|--help)
            help
            exit 0
            ;;
        -*|--*=) # unsupported flags
            echo "Error: Unsupported flag $1" >&2
            echo
            help
            exit 1
            ;;
        *) # preserve positional arguments
            PARAMS="$PARAMS $1"
            shift
            ;;
    esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

if [ $# -ne 0 -o -z "${NUM}" ]; then
    help
    exit 1
fi

if [ "${CELL}" -a "${PROJECT}" ]; then
    echo 'Arguments "--cell" and "--project" are mutully exclusive'
    exit 1
fi

if [ -z "${CELL}" -a -z "${PROJECT}" ]; then
    help
    exit 1
fi

if [ "${REGION}" ]; then
    export OS_REGION_NAME="${REGION}"
else
    export OS_REGION_NAME=cern
fi

if [ "${CELL}" ]; then
    export OS_PROJECT_NAME="Cloud Compute Nodes - ${CELL}"
else
    export OS_PROJECT_NAME="${PROJECT}"
fi
echo "[I] Will create instances in project \"$OS_PROJECT_NAME\""

#
# Get the flavor name and the maximum number of instances from the project metadata
#
METADATA=$(openstack project show "${OS_PROJECT_NAME}" -f yaml 2>/dev/null | grep -E "^max-instances")

if [ -z "${METADATA}" ]; then
    echo "[E] Could not find max-instances-\* metadata for project \"$OS_PROJECT_NAME\", exiting."
    exit 1
fi

if [ -z "${FLAVOR}" -a $(echo "${METADATA}" | wc -l) -gt 1 ]; then
    echo "[E] Please specify one of the flavors available to this project:"
    echo "${METADATA}" | sed 's/max-instances_//g; s/\:.*//g' | xargs -n1 -i{} echo "[E]      {}"
    exit 1
fi

[ -z "${FLAVOR}" ] && FLAVOR=$(echo $METADATA | grep -E "^max-instances_${FLAVOR}" | cut -f2- -d_ | cut -f1 -d:)

MAXINST=$(echo "${METADATA}" | grep -E "^max-instances_${FLAVOR}" | cut -f2 -d\')
echo "[I] Will use flavor \"${FLAVOR}\" to create ${NUM} instances"

#
# Check current resource usage
#
CNT=$(openstack server list --flavor ${FLAVOR} -f value -c Status | grep -vw DELETED | wc -w)

if (( ${CNT} + ${NUM} > ${MAXINST} )); then
    echo "[E] Cannot create ${NUM} instances, there are already ${CNT} servers of a maximum ${MAXINST} servers in this project."
    exit 1
fi

#
# Get the keypair to be used
#
[ -z "${USERKEY}" ] && USERKEY=$(openstack keypair list -f value -c Name | head -1)
if [ -z "${USERKEY}" ]; then
    echo "[E] Could not determine key-pair to be used"
    echo "[E] ${USAGE}"
    exit 1
else
     echo "[I] Will use key-pair \"${USERKEY}\" for instance creation"
fi

#
# Get Foreman hostgroup and environment
#
if [ -z "${HOSTGROUP}" -o -z "${ENVIRONMENT}" ]; then
    FM_DATA=$(ai-foreman --filter "cloud_compute/level1/*/${CELL}" showhost 2>/dev/null | grep ${CELL})
fi
FM_HOSTGROUP=${HOSTGROUP-$(echo $FM_DATA | cut -f3 -d\| | sed 's/level1/level2/')}
FM_ENVIRONMENT=${ENVIRONMENT-$(echo $FM_DATA | cut -d\| -f4)}
# Get the delivery id from the flavor name
DELIVERY=$(echo ${FLAVOR} | cut -f2 -d. | cut -c3-)

#
# Launch instance creation
#
for i in $(seq 1 $NUM); do
    host=$(shuf --input-range=0-9 --zero-terminated --head-count=7 --repeat | tr -d "\000")
    host="i${DELIVERY}${host}"
    cmd="ai-bs -f $FLAVOR                                         \
           --landb-mainuser cloud-infrastructure-compute-admin    \
           --landb-responsible cloud-infrastructure-compute-admin \
           --foreman-environment $FM_ENVIRONMENT                  \
           --foreman-hostgroup $FM_HOSTGROUP                      \
           --userdata-dir $(dirname $0)/userdata                  \
           --nova-sshkey $USERKEY                                 \
           --rhel9                                                \
           --nova-property landb-ipv6ready=false                  \
           $host"
    [ $apply -eq 0 ] && cmd="echo [DRYRUN] $cmd"
    $cmd
done

#
# Done
#
exit 
