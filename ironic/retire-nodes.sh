#! /bin/bash

#
# https://its.cern.ch/jira/browse/OS-13335
#
#When the decision is taken to retire hosts, how about having a "Prepare/announce the retirement" Rundeck job which
#
#sets the retirement flag on the corresponding Ironic node (this will prevent re-use of a cleaned node)
#openstack baremetal node set --retired --retired-reason <retired_reason>
#sets the retirement reason to specific information, such as the target retirement date or a link to a retirement JIRA ticket
#sends retirement notifications to the message bus (as we do for the intervention jobs), these can be consumed by automated tooling (as batch does today)
#send a mail to the user (just as we do for other interventions for OpenStack instances)
#
#Then for hypervisors, there should be a "Hypervisor retirement" Rundeck job which
#
#disables the compute node
#removes the node from nova
#removes the node from neutron
#ai-kills the instance
#updates the max_instances metadata
#hands them over to the procurement team

unset `env | grep -E "^OS_" | cut -f1 -d=`

TMPDIR=$(mktemp --directory --tmpdir retire-compute-nodes.XXXX)
trap "/bin/rm -rf ${TMPDIR} 2>/dev/null" EXIT


USAGE="Usage: $(basename $0) [--apply] <compute node> [<compute node>]"

#
# Print help text
#
function help()
{
    local name=$(basename $0)
    cat <<EOF
Usage: $name --reason <retirement reason> <compute node> [<compute node>]
       $name --help

This script retires Nov compute nodes from the Cloud, and from Ironic.

Arguments:

       -r|--reason   : Retirement reason
       -h|--help     : display this help

Example:

       $ $name --reason 'Servers being retired [OS-13744]' i62539460818680.cern.ch i63339713642681.cern.ch

Note: to be executed on a ciadm8 node, with the latest openstack cli :)

EOF

}

#
# Cache the Foreman, Nova, Ironic info
#
function cache_info(){

    #
    # Foreman
    #
    ai-foreman showhost $HOSTS 2>/dev/null | grep cern.ch | sed 's|/main/|/cern/|' > ${TMPDIR}/foreman_hosts.txt
    if [ ! -s ${TMPDIR}/foreman_hosts.txt ]; then
        echo '# [E] Failed to get Foreman info'
        exit 1
    fi
    
    #
    # Ironic
    #
    OS_CLOUD=cern OS_PROJECT_NAME="Cloud Test - gva_baremetal_001" openstack baremetal node list --limit 0 > ${TMPDIR}/ironic_nodes.txt
    if [ ! -s ${TMPDIR}/ironic_nodes.txt ]; then
        echo '# [E] Failed to get Ironic info'
        exit 1
    fi

    #
    # Nova compute nodes
    #
    for host in $HOSTS; do
        name=$( echo $host | cut -d. -f1 )
        OS_CLOUD=cern openstack server list --all --no-name --name $name >> ${TMPDIR}/compute_nodes.txt
    done
    if [ ! -s ${TMPDIR}/compute_nodes.txt ]; then
        echo '# [E] Failed to get Nova compute info'
        exit 1
    fi

    #
    # Nova compute service
    #
    local regions=$(cut -d\| -f3 ${TMPDIR}/foreman_hosts.txt | cut -d/ -f3 | sort | uniq)
    for region in ${regions}; do
        OS_CLOUD=${region} openstack --os-compute-api-version=2.53 compute service  list --service nova-compute >> ${TMPDIR}/compute_service.txt
    done
    if [ ! -s ${TMPDIR}/compute_service.txt ]; then
        echo '# [E] Failed to get Nova compute service'
        exit 1
    fi

    #
    # Neutron agents
    #
    for region in ${regions}; do
        OS_CLOUD=${region} openstack network agent list >> ${TMPDIR}/network_agent.txt
    done
    if [ ! -s ${TMPDIR}/network_agent.txt ]; then
        echo '# [E] Failed to get Neutron network agent info'
        exit 1
    fi
    
    return
}

function get_nova_uuid(){
    local host=$1
    grep -w $(echo $host | cut -d. -f1) ${TMPDIR}/compute_nodes.txt | cut -f2 -d" "
}

function get_ironic_uuid(){
    local host=$1
    local uuid=$( get_nova_uuid $host )
    grep -w ${uuid} $TMPDIR/ironic_nodes.txt | cut -f2 -d" "
}

function get_region(){
    local host=$1
    grep -w $host ${TMPDIR}/foreman_hosts.txt | cut -f3 -d\| | cut -d/ -f3
}

function get_compute_service(){
    local host=$1
    grep -w ${host} ${TMPDIR}/compute_service.txt | cut -f2 -d" "
}

function get_network_agent(){
    local host=$1
    grep -w ${host} $TMPDIR/network_agent.txt | cut -f2 -d" "
}

#
# Parse the options
#
PARAMS=""
while (( "$#" )); do
    case "$1" in
        -r|--reason)
            REASON=$2
            shift
            shift
            ;;
        -h|--help)
            help
            exit 0
            ;;
        -*|--*=) # unsupported flags
            echo "Error: Unsupported flag $1" >&2
            echo
            help
            exit 1
            ;;
        *) # preserve positional arguments
            PARAMS="$PARAMS $1"
            shift
            ;;
    esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

if [ $# -eq 0 -o -z "${REASON}" ]; then
    help
    exit 1
fi

HOSTS=$PARAMS
#echo HOSTS $HOSTS

echo "# [I] Caching all necessary information from Foreman, Ironic, Nova and Neutron in directory ${TMPDIR}. This will take one minute..."
cache_info
ls -lt ${TMPDIR} | sed 's/^/# /'

#
# Unset Ironic maintenance state
#
echo "# [I] Unset Ironic maintenance state"
for host in $HOSTS; do
    uuid=$( get_ironic_uuid $host )
    if [ -z $uuid ]; then
        echo "# [W] No Ironic node found for host $host, skipping"
        continue
    fi
    echo OS_CLOUD=cern OS_PROJECT_NAME=\"Cloud Test - gva_baremetal_001\" openstack baremetal node maintenance unset ${uuid}
done

#
# Set Ironic retirement state
#
echo "# [I] Set Ironic retirement state"
for host in $HOSTS; do
    uuid=$( get_ironic_uuid $host )
    if [ -z $uuid ]; then
        echo "# [W] No Ironic node found for host $host, skipping"
        continue
    fi
    echo OS_CLOUD=cern OS_PROJECT_NAME=\"Cloud Test - gva_baremetal_001\" openstack baremetal node set --retired --retired-reason \"${REASON}\" ${uuid}
done

#
# Delete RAID to avoid cleaning errors
#
echo "# [I] Remove the RAID setup"
for host in $HOSTS; do
    uuid=$( get_ironic_uuid $host )
    if [ -z $uuid ]; then
        echo "# [W] No Ironic node found for host $host, skipping"
        continue
    fi
    echo OS_CLOUD=cern OS_PROJECT_NAME=\"Cloud Test - gva_baremetal_001\" openstack baremetal node set --target-raid-config '{}' $uuid
done

#
# Disable the Nova compute service
#
echo "# [I] Disable the Nova compute service"
for host in $HOSTS; do
    region=$( get_region $host ) 
    uuid=$( get_compute_service $host )
    if [ -z $uuid ]; then
        echo "# [W] No compute service found for host $host, skipping"
        continue
    fi
    echo OS_CLOUD=$region openstack compute service set --disable --disable-reason \"${REASON}\" $host nova-compute
done

#
# Delete the Nova compute service
#
echo "# [I] Delete the Nova compute service"
for host in $HOSTS; do
    region=$( get_region $host )
    uuid=$( get_compute_service $host )
    if [ -z $uuid ]; then
        echo "# [W] No compute service found for host $host, skipping"
        continue
    fi
    echo OS_CLOUD=$region openstack --os-compute-api-version=2.53 compute service delete $uuid
done

#
# Delete the Neutron network agent
#
echo "# [I] Delete the Neutron network agent"
for host in $HOSTS; do
    region=$( get_region $host )
    uuid=$( get_network_agent $host )
    if [ -z $uuid ]; then
        echo "# [W] No network agent found for host $host, skipping"
        continue
    fi
    echo OS_CLOUD=$region openstack network agent delete $uuid
done

#
# ai-kill the instance
#
echo "# [I] Delete the instances by running \"ai-kill\""

# ai-kill (ai-tools-18.2.0-1.config8.noarch) expects OS_* env vars to be set. Jose is following up w/ Config team
vars="OS_PROJECT_DOMAIN_ID=default OS_REGION_NAME=cern OS_PROTOCOL=kerberos OS_IDENTITY_API_VERSION=3 OS_IDENTITY_PROVIDER=sssd OS_AUTH_TYPE=v3fedkerb OS_AUTH_URL=https://keystone.cern.ch/v3 OS_MUTUAL_AUTH=disabled"

for host in $HOSTS; do
    uuid=$( get_nova_uuid $host )
    project_id=$( OS_CLOUD=cern openstack server show $uuid -f value -c project_id )
    project_name=$( OS_CLOUD=cern openstack project show $project_id -f value -c name )

    if [ -z "$project_name" ]; then
        echo "# [W] No project found for host $host, skipping"
        continue
    fi
    echo ${vars} OS_CLOUD=krb OS_PROJECT_NAME=\"$(echo $project_name | xargs)\" ai-kill ${host}
done

# (TODO) updates the max_instances metadata


#
# Done
#
exit

